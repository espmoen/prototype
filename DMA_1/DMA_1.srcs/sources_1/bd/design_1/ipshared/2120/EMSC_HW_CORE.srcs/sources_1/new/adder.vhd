library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity adder is
    Generic(
        bit_width : integer := 63
    );
    
    Port ( 
        inA : in signed(63 downto 0);
        inB : in signed(63 downto 0);
        Res : out signed(63 downto 0)
    );
end adder;

architecture Behavioral of adder is

begin

Res <= inA + inB;

end Behavioral;
