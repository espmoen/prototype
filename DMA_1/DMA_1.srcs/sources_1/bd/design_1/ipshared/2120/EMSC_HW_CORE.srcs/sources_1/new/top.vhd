library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity adder_top is
  generic (
    C_S_AXI_DATA_WIDTH : integer := 32;
    C_S_AXI_ADDR_WIDTH : integer := 6
    );
  port (
    clk     : in std_logic;
    aresetn : in std_logic;

    debug_leds : out std_logic_vector(7 downto 0);

    -- Stream in
    s_axis_tdata  : in  std_logic_vector(31 downto 0);
    s_axis_tvalid : in  std_logic;
    s_axis_tready : out std_logic;
    s_axis_tlast  : in  std_logic;

    -- Stream out
    m_axis_tdata  : out std_logic_vector(63 downto 0);
    m_axis_tvalid : out std_logic;
    m_axis_tready : in  std_logic;
    m_axis_tlast  : out std_logic;

    -- Control interface
    s_axi_awaddr  : in  std_logic_vector(c_s_axi_addr_width-1 downto 0);
    s_axi_awprot  : in  std_logic_vector(2 downto 0);
    s_axi_awvalid : in  std_logic;
    s_axi_awready : out std_logic;
    s_axi_wdata   : in  std_logic_vector(c_s_axi_data_width-1 downto 0);
    s_axi_wstrb   : in  std_logic_vector((c_s_axi_data_width/8)-1 downto 0);
    s_axi_wvalid  : in  std_logic;
    s_axi_wready  : out std_logic;
    s_axi_bresp   : out std_logic_vector(1 downto 0);
    s_axi_bvalid  : out std_logic;
    s_axi_bready  : in  std_logic;
    s_axi_araddr  : in  std_logic_vector(c_s_axi_addr_width-1 downto 0);
    s_axi_arprot  : in  std_logic_vector(2 downto 0);
    s_axi_arvalid : in  std_logic;
    s_axi_arready : out std_logic;
    s_axi_rdata   : out std_logic_vector(c_s_axi_data_width-1 downto 0);
    s_axi_rresp   : out std_logic_vector(1 downto 0);
    s_axi_rvalid  : out std_logic;
    s_axi_rready  : in  std_logic
    );
end adder_top;

architecture rtl of adder_top is
  signal n     : std_logic_vector(15 downto 0);
  signal count : std_logic_vector(31 downto 0);
  
  signal data_in_DP : signed(31 downto 0);
  signal data_out_DP: signed(63 downto 0);
  signal enable_DP: std_logic;
  signal res_rdy: std_logic;

  signal in_data  : signed(31 downto 0);
  signal in_valid : std_logic;
  signal in_ready : std_logic;
  signal in_last  : std_logic;

  signal out_data  : signed(63 downto 0);
  signal out_valid : std_logic;
  signal out_ready : std_logic;
  signal out_last  : std_logic;

  -- Helper signals
  signal in_handshake  : std_logic;
  signal out_handshake : std_logic;
begin

  in_data       <= signed(s_axis_tdata);
  in_valid      <= s_axis_tvalid;
  in_last       <= s_axis_tlast;
  s_axis_tready <= in_ready;

  m_axis_tdata  <= std_logic_vector(data_out_DP);
  m_axis_tvalid <= out_valid;
  m_axis_tlast  <= out_last;
  out_ready     <= m_axis_tready;

  i_register_interface : entity work.register_interface
    generic map (
      C_S_AXI_DATA_WIDTH => C_S_AXI_DATA_WIDTH,
      C_S_AXI_ADDR_WIDTH => C_S_AXI_ADDR_WIDTH)
    port map (
      n             => n,
      count         => count,
      S_AXI_ACLK    => CLK,
      S_AXI_ARESETN => ARESETN,
      S_AXI_AWADDR  => S_AXI_AWADDR,
      S_AXI_AWPROT  => S_AXI_AWPROT,
      S_AXI_AWVALID => S_AXI_AWVALID,
      S_AXI_AWREADY => S_AXI_AWREADY,
      S_AXI_WDATA   => S_AXI_WDATA,
      S_AXI_WSTRB   => S_AXI_WSTRB,
      S_AXI_WVALID  => S_AXI_WVALID,
      S_AXI_WREADY  => S_AXI_WREADY,
      S_AXI_BRESP   => S_AXI_BRESP,
      S_AXI_BVALID  => S_AXI_BVALID,
      S_AXI_BREADY  => S_AXI_BREADY,
      S_AXI_ARADDR  => S_AXI_ARADDR,
      S_AXI_ARPROT  => S_AXI_ARPROT,
      S_AXI_ARVALID => S_AXI_ARVALID,
      S_AXI_ARREADY => S_AXI_ARREADY,
      S_AXI_RDATA   => S_AXI_RDATA,
      S_AXI_RRESP   => S_AXI_RRESP,
      S_AXI_RVALID  => S_AXI_RVALID,
      S_AXI_RREADY  => S_AXI_RREADY);

  
  
  i_dotproduct : entity work.dot_product
    generic map(
        REGSIZE         =>  100,
        bit_width       =>  32, 
        num_elements    => 100)
    port map(
        clk         =>  clk,
        reset_n     =>  aresetn,
        enable      =>  enable_DP,
        data_in     =>  data_in_DP,
        data_out    =>  data_out_DP,
        res_rdy     =>  res_rdy);
    
  
  process (clk)
    variable count_i : integer range 0 to 2**16-1;
  begin
    if (rising_edge(clk)) then
      if (aresetn = '0') then
        out_data  <= (others => '0');
        out_valid <= '0';
        out_last  <= '0';
        count_i   := 0;
      else
        if (in_handshake = '1') then
          enable_DP <= '1';
          data_in_DP  <= in_data;
          out_last  <= in_last;
          count_i   := count_i + 1;
        elsif (out_handshake = '1') then
          out_valid <= '0';
          out_last  <= '0';
        elsif(res_rdy = '1') then
            out_valid <= '1';
        end if;
        count <= std_logic_vector(to_unsigned(count_i, 32));
      end if;
    end if;
  end process;

  in_handshake  <= in_valid and in_ready; -- should include reset
  out_handshake <= out_valid and out_ready; -- should include reset
  in_ready      <= '1' when out_valid = '0' or out_handshake = '1' else '0';

  debug_leds <= count(7 downto 0);

end rtl;
