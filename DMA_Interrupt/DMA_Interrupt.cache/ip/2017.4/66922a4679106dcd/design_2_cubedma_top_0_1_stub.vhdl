-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
-- Date        : Wed Feb 28 16:25:11 2018
-- Host        : DESKTOP-0KA73P0 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_2_cubedma_top_0_1_stub.vhdl
-- Design      : design_2_cubedma_top_0_1
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    clk : in STD_LOGIC;
    aresetn : in STD_LOGIC;
    m_axi_mem_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_mem_arlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_mem_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_mem_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_mem_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_mem_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_mem_arvalid : out STD_LOGIC;
    m_axi_mem_arready : in STD_LOGIC;
    m_axi_mem_rdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    m_axi_mem_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_mem_rlast : in STD_LOGIC;
    m_axi_mem_rvalid : in STD_LOGIC;
    m_axi_mem_rready : out STD_LOGIC;
    m_axi_mem_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_mem_awlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_mem_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_mem_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_mem_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_mem_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_mem_awvalid : out STD_LOGIC;
    m_axi_mem_awready : in STD_LOGIC;
    m_axi_mem_wdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    m_axi_mem_wstrb : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axi_mem_wlast : out STD_LOGIC;
    m_axi_mem_wvalid : out STD_LOGIC;
    m_axi_mem_wready : in STD_LOGIC;
    m_axi_mem_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_mem_bvalid : in STD_LOGIC;
    m_axi_mem_bready : out STD_LOGIC;
    m_axis_mm2s_tdata : out STD_LOGIC_VECTOR ( 15 downto 0 );
    m_axis_mm2s_tlast : out STD_LOGIC;
    m_axis_mm2s_tvalid : out STD_LOGIC;
    m_axis_mm2s_tready : in STD_LOGIC;
    s_axis_s2mm_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_s2mm_tlast : in STD_LOGIC;
    s_axis_s2mm_tvalid : in STD_LOGIC;
    s_axis_s2mm_tready : out STD_LOGIC;
    s_axi_ctrl_status_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_ctrl_status_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_ctrl_status_awvalid : in STD_LOGIC;
    s_axi_ctrl_status_awready : out STD_LOGIC;
    s_axi_ctrl_status_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_ctrl_status_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_ctrl_status_wvalid : in STD_LOGIC;
    s_axi_ctrl_status_wready : out STD_LOGIC;
    s_axi_ctrl_status_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_ctrl_status_bvalid : out STD_LOGIC;
    s_axi_ctrl_status_bready : in STD_LOGIC;
    s_axi_ctrl_status_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_ctrl_status_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_ctrl_status_arvalid : in STD_LOGIC;
    s_axi_ctrl_status_arready : out STD_LOGIC;
    s_axi_ctrl_status_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_ctrl_status_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_ctrl_status_rvalid : out STD_LOGIC;
    s_axi_ctrl_status_rready : in STD_LOGIC;
    mm2s_irq_out : out STD_LOGIC;
    s2mm_irq_out : out STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,aresetn,m_axi_mem_araddr[31:0],m_axi_mem_arlen[3:0],m_axi_mem_arsize[2:0],m_axi_mem_arburst[1:0],m_axi_mem_arprot[2:0],m_axi_mem_arcache[3:0],m_axi_mem_arvalid,m_axi_mem_arready,m_axi_mem_rdata[63:0],m_axi_mem_rresp[1:0],m_axi_mem_rlast,m_axi_mem_rvalid,m_axi_mem_rready,m_axi_mem_awaddr[31:0],m_axi_mem_awlen[3:0],m_axi_mem_awsize[2:0],m_axi_mem_awburst[1:0],m_axi_mem_awprot[2:0],m_axi_mem_awcache[3:0],m_axi_mem_awvalid,m_axi_mem_awready,m_axi_mem_wdata[63:0],m_axi_mem_wstrb[7:0],m_axi_mem_wlast,m_axi_mem_wvalid,m_axi_mem_wready,m_axi_mem_bresp[1:0],m_axi_mem_bvalid,m_axi_mem_bready,m_axis_mm2s_tdata[15:0],m_axis_mm2s_tlast,m_axis_mm2s_tvalid,m_axis_mm2s_tready,s_axis_s2mm_tdata[31:0],s_axis_s2mm_tlast,s_axis_s2mm_tvalid,s_axis_s2mm_tready,s_axi_ctrl_status_awaddr[5:0],s_axi_ctrl_status_awprot[2:0],s_axi_ctrl_status_awvalid,s_axi_ctrl_status_awready,s_axi_ctrl_status_wdata[31:0],s_axi_ctrl_status_wstrb[3:0],s_axi_ctrl_status_wvalid,s_axi_ctrl_status_wready,s_axi_ctrl_status_bresp[1:0],s_axi_ctrl_status_bvalid,s_axi_ctrl_status_bready,s_axi_ctrl_status_araddr[5:0],s_axi_ctrl_status_arprot[2:0],s_axi_ctrl_status_arvalid,s_axi_ctrl_status_arready,s_axi_ctrl_status_rdata[31:0],s_axi_ctrl_status_rresp[1:0],s_axi_ctrl_status_rvalid,s_axi_ctrl_status_rready,mm2s_irq_out,s2mm_irq_out";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "cubedma_top,Vivado 2017.4";
begin
end;
