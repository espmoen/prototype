/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */



//SOME BIT OPERATORS

	//Clearing bit x
	//dma_regs[0] &= ~(1UL << x);

	//Setting a bit,  bit x
	//dma_regs[0] |= 1UL << x;

	//Toggling bit x
	//dma_regs[0] ^= 1UL << x;

	//Checking bit x
	//int bit = (dma_regs[0] >> x) & 1U;
	//xil_printf("%08x",bit);

#include <stdio.h>
#include "platform.h"
#include "xparameters.h"
#include "xil_printf.h"
#include "xil_cache.h"
#include "xscugic.h"
#include "xil_types.h"
#include "xil_exception.h"
#include "xil_io.h"
#include <stdio.h>
#include <stdlib.h>

#include "interrupt.h"

#define NUM_VALUES 10



#define INTC_DEVICE_ID		XPAR_SCUGIC_0_DEVICE_ID





/************************** Variable Definitions *****************************/

XScuGic InterruptController; 	     /* Instance of the Interrupt Controller */
static XScuGic_Config *GicConfig;    /* The configuration parameters of the
                                       controller */



int Gic_Init(u16 DeviceId);
int Gic_Cleanup();
int SetUpInterruptSystem(XScuGic *XScuGicInstancePtr);
void DeviceDriverHandler(void *CallbackRef);



int mm2s_complete;
int s2mm_complete;
u32* dma_regs = (u32*)XPAR_CUBEDMA_1_BASEADDR;

static void dma_irq_handler(void* ref) {
    int instance = (int)ref;
    int status_reg;
    u32 mask = 0;

    if (instance == 0) {
    	print("mm2s_interrupt\n");
        status_reg = 1;
        mm2s_complete = 1;
    }
    else {
    	print("s2mm_interrupt\n");
        status_reg = 9;
        s2mm_complete = 1;
    }

    mask = dma_regs[status_reg];
    dma_regs[status_reg] = (mask & 0x300);
}


void simple_transfer_dma(){
	  uint16_t values[NUM_VALUES]__attribute__((aligned(32)));
	  uint32_t result[NUM_VALUES]__attribute__((aligned(32))) = {};

	   print("Before processing:\n");
	   for (int i = 0; i < NUM_VALUES; i++) {
	   	values[i] = i + 1;
	   	xil_printf("%d ", values[i]);
	   }
	   print("\n");

	   // Important!
	   Xil_DCacheInvalidateRange((UINTPTR)values, 2*NUM_VALUES);
	   Xil_DCacheInvalidateRange((UINTPTR)result, 4*NUM_VALUES);

	   // Set up our adder core
	   uint32_t *adder_regs = (uint32_t*)XPAR_ADDERCORE_0_BASEADDR;

	   // We want to add 5
	   adder_regs[0] = 5;

	   uint32_t *dma_regs = (uint32_t*)XPAR_CUBEDMA_1_BASEADDR;

	   dma_regs[0] = 0;
	   dma_regs[2] = (uint32_t)values;

	   //MYIP_WITH_INTERRUPT_EnableInterrupt(MYIP_WITH_INTERRUPT);

	   // When this is written, DMA MM2S transfer will start
	   dma_regs[0] = (NUM_VALUES << 12) | 1 << 5 |(1 << 4) | 1;

	   dma_regs[8] = 0;
	   dma_regs[10] = (uint32_t)result;

	   // This will start S2MM DMA
	   dma_regs[8] = (NUM_VALUES << 12) | (1 << 4) | 1 << 5 | 1;

	   // Wait for DMA to complete
	   while (s2mm_complete != 1);
	   //while (InterruptProcessed == FALSE);

	   // Now we can look at the results:
	   print("Result after processing:\n");
	   for (int i = 0; i < NUM_VALUES; i++) {
	   	xil_printf("%d ", result[i]);
	   }
	    xil_printf("\nCount: %d\n", adder_regs[1]);
}


int main()
{
	 //pre_init();

	 init_platform();

	 Gic_Init(XPAR_PS7_SCUGIC_0_DEVICE_ID);

	 simple_transfer_dma();

	 Gic_Cleanup();

}



int Gic_Init(u16 DeviceId){
	int Status;
	GicConfig = XScuGic_LookupConfig(DeviceId);
	Status = XScuGic_CfgInitialize(&InterruptController, GicConfig, GicConfig->CpuBaseAddress);
	if (Status != XST_SUCCESS) {
	  	 print("Failed to initialize GIC\n");
	   	 return Status;
	}


    u32 id_full = XScuGic_CPUReadReg(&InterruptController, XSCUGIC_INT_ACK_OFFSET);
    XScuGic_CPUWriteReg(&InterruptController, XSCUGIC_EOI_OFFSET, id_full);

    Status = XScuGic_Connect(&InterruptController, MM2S_INT, (Xil_InterruptHandler)dma_irq_handler, (void*)0);
	if (Status != XST_SUCCESS)
	  	 return Status;
	Status = XScuGic_Connect(&InterruptController, S2MM_INT, (Xil_InterruptHandler)dma_irq_handler, (void*)1);
     if (Status != XST_SUCCESS)
    	 return Status;

     XScuGic_SetPriorityTriggerType(&InterruptController, MM2S_INT, 0xA0, 0x3);
     XScuGic_SetPriorityTriggerType(&InterruptController, S2MM_INT, 0xA0, 0x3);

     XScuGic_Enable(&InterruptController, MM2S_INT);
     XScuGic_Enable(&InterruptController, S2MM_INT);

     Xil_ExceptionInit();
     Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT, (Xil_ExceptionHandler)XScuGic_InterruptHandler, &InterruptController);
     Xil_ExceptionEnable();

}


int Gic_Cleanup(){
	XScuGic_Disable(&InterruptController, MM2S_INT);
	XScuGic_Disable(&InterruptController, S2MM_INT);

	XScuGic_Disconnect(&InterruptController, MM2S_INT);
	XScuGic_Disconnect(&InterruptController, S2MM_INT);


	Xil_ExceptionDisable();
	cleanup_platform();

	return XST_SUCCESS;
}

