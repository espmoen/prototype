/*
 * accelerator.c
 *
 *  Created on: 28. feb. 2018
 *      Author: espen
 */

#include "accelerator.h"




void simple_transfer_dma(){
	  uint16_t values[NUM_VALUES]__attribute__((aligned(32)));
	  uint32_t result[NUM_VALUES]__attribute__((aligned(32))) = {};

	   print("Before processing:\n");
	   for (int i = 0; i < NUM_VALUES; i++) {
	   	values[i] = i + 1;
	   	xil_printf("%d ", values[i]);
	   }
	   print("\n");

	   // Important!
	   Xil_DCacheInvalidateRange((UINTPTR)values, 2*NUM_VALUES);
	   Xil_DCacheInvalidateRange((UINTPTR)result, 4*NUM_VALUES);

	   // Set up our adder core
	   uint32_t *adder_regs = (uint32_t*)XPAR_ADDERCORE_0_BASEADDR;

	   // We want to add 5
	   adder_regs[0] = 5;



	   dma_regs[0] = 0;
	   dma_regs[2] = (uint32_t)values;

	   //MYIP_WITH_INTERRUPT_EnableInterrupt(MYIP_WITH_INTERRUPT);

	   // When this is written, DMA MM2S transfer will start
	   dma_regs[0] = (NUM_VALUES << 12) | 1 << 5 |(1 << 4) | 1;

	   dma_regs[8] = 0;
	   dma_regs[10] = (uint32_t)result;

	   // This will start S2MM DMA
	   dma_regs[8] = (NUM_VALUES << 12) | (1 << 4) | 1 << 5 | 1;

	   // Wait for DMA to complete
	   while (((dma_regs[1] >> 9) & 1U) != 1);
	   //while (InterruptProcessed == FALSE);

	   // Now we can look at the results:
	   print("Result after processing:\n");
	   for (int i = 0; i < NUM_VALUES; i++) {
	   	xil_printf("%d ", result[i]);
	   }
	    xil_printf("\nCount: %d\n", adder_regs[1]);
}
