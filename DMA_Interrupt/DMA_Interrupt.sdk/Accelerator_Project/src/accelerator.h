/*
 * accelerator.h
 *
 *  Created on: 28. feb. 2018
 *      Author: espen
 */

#ifndef SRC_ACCELERATOR_H_
#define SRC_ACCELERATOR_H_

#include "xil_cache.h"
#include "xparameters.h"

#define NUM_VALUES 10

uint32_t *dma_regs = (uint32_t*)XPAR_CUBEDMA_1_BASEADDR;


void simple_transfer_dma();

#endif /* SRC_ACCELERATOR_H_ */
