/*
 * interrupt.c
 *
 *  Created on: 28. feb. 2018
 *      Author: espen
 */
#include "interrupt.h"


static void dma_irq_handler(void* ref) {
    int instance = (int)ref;
    int status_reg;
    u32 mask = 0;

    if (instance == 0) {
    	print("mm2s_interrupt\n");
        status_reg = 1;
        mm2s_complete = 1;
    }
    else {
    	print("s2mm_interrupt\n");
        status_reg = 9;
        s2mm_complete = 1;
    }

    mask = dma_regs[status_reg];
    dma_regs[status_reg] = (mask & 0x300);
}

int Gic_Init(u16 DeviceId){
	int Status;
	GicConfig = XScuGic_LookupConfig(DeviceId);
	Status = XScuGic_CfgInitialize(&InterruptController, GicConfig, GicConfig->CpuBaseAddress);
	if (Status != XST_SUCCESS) {
	  	 print("Failed to initialize GIC\n");
	   	 return Status;
	}


    u32 id_full = XScuGic_CPUReadReg(&InterruptController, XSCUGIC_INT_ACK_OFFSET);
    XScuGic_CPUWriteReg(&InterruptController, XSCUGIC_EOI_OFFSET, id_full);

    Status = XScuGic_Connect(&InterruptController, MM2S_INT, (Xil_InterruptHandler)dma_irq_handler, (void*)0);
	if (Status != XST_SUCCESS)
	  	 return Status;
	Status = XScuGic_Connect(&InterruptController, S2MM_INT, (Xil_InterruptHandler)dma_irq_handler, (void*)1);
     if (Status != XST_SUCCESS)
    	 return Status;

     XScuGic_SetPriorityTriggerType(&InterruptController, MM2S_INT, 0xA0, 0x3);
     XScuGic_SetPriorityTriggerType(&InterruptController, S2MM_INT, 0xA0, 0x3);

     XScuGic_Enable(&InterruptController, MM2S_INT);
     XScuGic_Enable(&InterruptController, S2MM_INT);

     Xil_ExceptionInit();
     Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT, (Xil_ExceptionHandler)XScuGic_InterruptHandler, &InterruptController);
     Xil_ExceptionEnable();

}


int Gic_Cleanup(){
	XScuGic_Disable(&InterruptController, MM2S_INT);
	XScuGic_Disable(&InterruptController, S2MM_INT);

	XScuGic_Disconnect(&InterruptController, MM2S_INT);
	XScuGic_Disconnect(&InterruptController, S2MM_INT);


	Xil_ExceptionDisable();
	cleanup_platform();

	return XST_SUCCESS;
}
