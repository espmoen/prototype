/*
 * interrupt.h
 *
 *  Created on: 28. feb. 2018
 *      Author: espen
 */
#include "xscugic.h"
#include "accelerator.h"


#ifndef SRC_INTERRUPT_H_
#define SRC_INTERRUPT_H_

static void dma_irq_handler(void* ref);
int Gic_Init(u16 DeviceId);
int Gic_Cleanup();


XScuGic InterruptController; 	     /* Instance of the Interrupt Controller */
static XScuGic_Config *GicConfig;    /* The configuration parameters of the
                                       controller */

#define MM2S_INT			61
#define S2MM_INT			62

int mm2s_complete;
int s2mm_complete;


#endif /* SRC_INTERRUPT_H_ */
