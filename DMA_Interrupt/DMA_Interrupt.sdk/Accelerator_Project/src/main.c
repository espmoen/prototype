/*
 * main.c
 *
 *  Created on: 28. feb. 2018
 *      Author: espen
 */

#include "interrupt.h"
#include "accelerator.h"


int main(void){

	 Gic_Init(XPAR_PS7_SCUGIC_0_DEVICE_ID);

	 simple_transfer_dma();


	 Gic_Cleanup();
}
