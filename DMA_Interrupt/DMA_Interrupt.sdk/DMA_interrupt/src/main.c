#include "xparameters.h"
#include "xuartps.h"
#include "xil_printf.h"
#include "xil_io.h"

#define UART_DEVICE_ID              XPAR_XUARTPS_0_DEVICE_ID


XUartPs Uart_Ps;



int init_uart(u16 DeviceId);
void start_mm2s_simple_transfer();
void start_s2mm_simple_transfer();

int main(void){
	//int status;
	//status = init_uart(UART_DEVICE_ID);
	//if (status != XST_SUCCESS) {
	//	xil_printf("UART Selftest Example Failed\r\n");
	//	return XST_FAILURE;
	//}
	//xil_printf("UART Successfully Initialized!\r\n");



	//u32 past_reg[10] = {0};

	//xil_printf("%p", &pre_reg);


	//u16 * p = (u16*)0x43C10000;
	//p[0] = 0xA;

	fill_memory(10);

	start_s2mm_simple_transfer();

	start_mm2s_simple_transfer();


}

void fill_memory(int num){
	u16 * p = (u16*)0x00100000;
	for(int i = 0; i < num*2; i++ ){
	p[i] = i;
	}
}



void start_transfer(u32 chn, u32 base, u32 length, int offset, int intr){
	u32 * p = (u32*)chn;
	p[0] = 0x0;
	p[2] = base;
	p[3] = 0x0;
	p[4] = 0x0;
	p[5] = 0x0;
	p[6] = 0x0;
	p[7] = (offset & 0xFFF);
	p[0] = ((length & 0xFFFFF) << 12) | (intr << 4) | 1;
}

void start_mm2s_simple_transfer(){
	start_transfer(0x43C00000, 0x100000, 0xA, 0, 0);
}

void start_s2mm_simple_transfer(){
	start_transfer(0x43C00020, 0x110000, 0xA, 0, 0);
}


int init_uart(u16 DeviceId){

	int Status;
	XUartPs_Config *Config;
	/*
	 * Initialize the UART driver so that it's ready to use
	 * Look up the configuration in the config table,
	 * then initialize it.
	 */
	Config = XUartPs_LookupConfig(DeviceId);
	if (NULL == Config) {
		return XST_FAILURE;
	}
	Status = XUartPs_CfgInitialize(&Uart_Ps, Config, Config->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	return XST_SUCCESS;
}


