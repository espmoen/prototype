// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Wed Feb 21 16:27:44 2018
// Host        : DESKTOP-0KA73P0 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_adderCore_0_0_sim_netlist.v
// Design      : design_1_adderCore_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adder_top
   (s_axi_awready,
    s_axi_wready,
    s_axi_arready,
    s_axi_rvalid,
    s_axis_tready,
    m_axis_tvalid,
    debug_leds,
    s_axi_rdata,
    m_axis_tdata,
    s_axi_bvalid,
    m_axis_tlast,
    s_axi_awvalid,
    s_axi_wvalid,
    s_axi_arvalid,
    m_axis_tready,
    clk,
    aresetn,
    s_axi_awaddr,
    s_axi_wdata,
    s_axi_araddr,
    s_axi_wstrb,
    s_axis_tvalid,
    s_axis_tdata,
    s_axi_bready,
    s_axi_rready,
    s_axis_tlast);
  output s_axi_awready;
  output s_axi_wready;
  output s_axi_arready;
  output s_axi_rvalid;
  output s_axis_tready;
  output m_axis_tvalid;
  output [7:0]debug_leds;
  output [31:0]s_axi_rdata;
  output [16:0]m_axis_tdata;
  output s_axi_bvalid;
  output m_axis_tlast;
  input s_axi_awvalid;
  input s_axi_wvalid;
  input s_axi_arvalid;
  input m_axis_tready;
  input clk;
  input aresetn;
  input [3:0]s_axi_awaddr;
  input [31:0]s_axi_wdata;
  input [3:0]s_axi_araddr;
  input [3:0]s_axi_wstrb;
  input s_axis_tvalid;
  input [15:0]s_axis_tdata;
  input s_axi_bready;
  input s_axi_rready;
  input s_axis_tlast;

  wire aresetn;
  wire clear;
  wire clk;
  wire [15:8]count;
  wire \count[3]_i_2_n_0 ;
  wire [15:0]count_i;
  wire \count_i[0]_i_2_n_0 ;
  wire [15:0]count_i_reg;
  wire \count_i_reg[0]_i_1_n_0 ;
  wire \count_i_reg[0]_i_1_n_1 ;
  wire \count_i_reg[0]_i_1_n_2 ;
  wire \count_i_reg[0]_i_1_n_3 ;
  wire \count_i_reg[0]_i_1_n_4 ;
  wire \count_i_reg[0]_i_1_n_5 ;
  wire \count_i_reg[0]_i_1_n_6 ;
  wire \count_i_reg[0]_i_1_n_7 ;
  wire \count_i_reg[12]_i_1_n_1 ;
  wire \count_i_reg[12]_i_1_n_2 ;
  wire \count_i_reg[12]_i_1_n_3 ;
  wire \count_i_reg[12]_i_1_n_4 ;
  wire \count_i_reg[12]_i_1_n_5 ;
  wire \count_i_reg[12]_i_1_n_6 ;
  wire \count_i_reg[12]_i_1_n_7 ;
  wire \count_i_reg[4]_i_1_n_0 ;
  wire \count_i_reg[4]_i_1_n_1 ;
  wire \count_i_reg[4]_i_1_n_2 ;
  wire \count_i_reg[4]_i_1_n_3 ;
  wire \count_i_reg[4]_i_1_n_4 ;
  wire \count_i_reg[4]_i_1_n_5 ;
  wire \count_i_reg[4]_i_1_n_6 ;
  wire \count_i_reg[4]_i_1_n_7 ;
  wire \count_i_reg[8]_i_1_n_0 ;
  wire \count_i_reg[8]_i_1_n_1 ;
  wire \count_i_reg[8]_i_1_n_2 ;
  wire \count_i_reg[8]_i_1_n_3 ;
  wire \count_i_reg[8]_i_1_n_4 ;
  wire \count_i_reg[8]_i_1_n_5 ;
  wire \count_i_reg[8]_i_1_n_6 ;
  wire \count_i_reg[8]_i_1_n_7 ;
  wire \count_reg[11]_i_1_n_0 ;
  wire \count_reg[11]_i_1_n_1 ;
  wire \count_reg[11]_i_1_n_2 ;
  wire \count_reg[11]_i_1_n_3 ;
  wire \count_reg[15]_i_1_n_1 ;
  wire \count_reg[15]_i_1_n_2 ;
  wire \count_reg[15]_i_1_n_3 ;
  wire \count_reg[3]_i_1_n_0 ;
  wire \count_reg[3]_i_1_n_1 ;
  wire \count_reg[3]_i_1_n_2 ;
  wire \count_reg[3]_i_1_n_3 ;
  wire \count_reg[7]_i_1_n_0 ;
  wire \count_reg[7]_i_1_n_1 ;
  wire \count_reg[7]_i_1_n_2 ;
  wire \count_reg[7]_i_1_n_3 ;
  wire [7:0]debug_leds;
  wire i_register_interface_n_10;
  wire i_register_interface_n_11;
  wire i_register_interface_n_12;
  wire i_register_interface_n_13;
  wire i_register_interface_n_14;
  wire i_register_interface_n_15;
  wire i_register_interface_n_16;
  wire i_register_interface_n_17;
  wire i_register_interface_n_18;
  wire i_register_interface_n_19;
  wire i_register_interface_n_20;
  wire i_register_interface_n_21;
  wire i_register_interface_n_22;
  wire i_register_interface_n_23;
  wire i_register_interface_n_24;
  wire i_register_interface_n_25;
  wire i_register_interface_n_26;
  wire i_register_interface_n_27;
  wire i_register_interface_n_28;
  wire i_register_interface_n_29;
  wire i_register_interface_n_30;
  wire i_register_interface_n_31;
  wire i_register_interface_n_32;
  wire i_register_interface_n_33;
  wire i_register_interface_n_34;
  wire i_register_interface_n_35;
  wire i_register_interface_n_36;
  wire i_register_interface_n_37;
  wire i_register_interface_n_38;
  wire i_register_interface_n_6;
  wire i_register_interface_n_7;
  wire i_register_interface_n_8;
  wire i_register_interface_n_9;
  wire in_handshake;
  wire [16:0]m_axis_tdata;
  wire m_axis_tlast;
  wire m_axis_tready;
  wire m_axis_tvalid;
  wire \out_data[11]_i_2_n_0 ;
  wire \out_data[11]_i_3_n_0 ;
  wire \out_data[11]_i_4_n_0 ;
  wire \out_data[11]_i_5_n_0 ;
  wire \out_data[15]_i_2_n_0 ;
  wire \out_data[15]_i_3_n_0 ;
  wire \out_data[15]_i_4_n_0 ;
  wire \out_data[15]_i_5_n_0 ;
  wire \out_data[3]_i_2_n_0 ;
  wire \out_data[3]_i_3_n_0 ;
  wire \out_data[3]_i_4_n_0 ;
  wire \out_data[3]_i_5_n_0 ;
  wire \out_data[7]_i_2_n_0 ;
  wire \out_data[7]_i_3_n_0 ;
  wire \out_data[7]_i_4_n_0 ;
  wire \out_data[7]_i_5_n_0 ;
  wire out_last_i_1_n_0;
  wire out_valid_i_1_n_0;
  wire [3:0]s_axi_araddr;
  wire s_axi_arready;
  wire s_axi_arvalid;
  wire [3:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire s_axi_bvalid;
  wire [31:0]s_axi_rdata;
  wire s_axi_rready;
  wire s_axi_rvalid;
  wire [31:0]s_axi_wdata;
  wire s_axi_wready;
  wire [3:0]s_axi_wstrb;
  wire s_axi_wvalid;
  wire [15:0]s_axis_tdata;
  wire s_axis_tlast;
  wire s_axis_tready;
  wire s_axis_tvalid;
  wire [3:3]\NLW_count_i_reg[12]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_count_reg[15]_i_1_CO_UNCONNECTED ;

  LUT4 #(
    .INIT(16'h65AA)) 
    \count[3]_i_2 
       (.I0(count_i_reg[0]),
        .I1(m_axis_tready),
        .I2(m_axis_tvalid),
        .I3(s_axis_tvalid),
        .O(\count[3]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h4FB0)) 
    \count_i[0]_i_2 
       (.I0(m_axis_tready),
        .I1(m_axis_tvalid),
        .I2(s_axis_tvalid),
        .I3(count_i_reg[0]),
        .O(\count_i[0]_i_2_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\count_i_reg[0]_i_1_n_7 ),
        .Q(count_i_reg[0]),
        .R(clear));
  CARRY4 \count_i_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\count_i_reg[0]_i_1_n_0 ,\count_i_reg[0]_i_1_n_1 ,\count_i_reg[0]_i_1_n_2 ,\count_i_reg[0]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,count_i_reg[0]}),
        .O({\count_i_reg[0]_i_1_n_4 ,\count_i_reg[0]_i_1_n_5 ,\count_i_reg[0]_i_1_n_6 ,\count_i_reg[0]_i_1_n_7 }),
        .S({count_i_reg[3:1],\count_i[0]_i_2_n_0 }));
  FDRE \count_i_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(\count_i_reg[8]_i_1_n_5 ),
        .Q(count_i_reg[10]),
        .R(clear));
  FDRE \count_i_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(\count_i_reg[8]_i_1_n_4 ),
        .Q(count_i_reg[11]),
        .R(clear));
  FDRE \count_i_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(\count_i_reg[12]_i_1_n_7 ),
        .Q(count_i_reg[12]),
        .R(clear));
  CARRY4 \count_i_reg[12]_i_1 
       (.CI(\count_i_reg[8]_i_1_n_0 ),
        .CO({\NLW_count_i_reg[12]_i_1_CO_UNCONNECTED [3],\count_i_reg[12]_i_1_n_1 ,\count_i_reg[12]_i_1_n_2 ,\count_i_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_i_reg[12]_i_1_n_4 ,\count_i_reg[12]_i_1_n_5 ,\count_i_reg[12]_i_1_n_6 ,\count_i_reg[12]_i_1_n_7 }),
        .S(count_i_reg[15:12]));
  FDRE \count_i_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(\count_i_reg[12]_i_1_n_6 ),
        .Q(count_i_reg[13]),
        .R(clear));
  FDRE \count_i_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(\count_i_reg[12]_i_1_n_5 ),
        .Q(count_i_reg[14]),
        .R(clear));
  FDRE \count_i_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(\count_i_reg[12]_i_1_n_4 ),
        .Q(count_i_reg[15]),
        .R(clear));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\count_i_reg[0]_i_1_n_6 ),
        .Q(count_i_reg[1]),
        .R(clear));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\count_i_reg[0]_i_1_n_5 ),
        .Q(count_i_reg[2]),
        .R(clear));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\count_i_reg[0]_i_1_n_4 ),
        .Q(count_i_reg[3]),
        .R(clear));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\count_i_reg[4]_i_1_n_7 ),
        .Q(count_i_reg[4]),
        .R(clear));
  CARRY4 \count_i_reg[4]_i_1 
       (.CI(\count_i_reg[0]_i_1_n_0 ),
        .CO({\count_i_reg[4]_i_1_n_0 ,\count_i_reg[4]_i_1_n_1 ,\count_i_reg[4]_i_1_n_2 ,\count_i_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_i_reg[4]_i_1_n_4 ,\count_i_reg[4]_i_1_n_5 ,\count_i_reg[4]_i_1_n_6 ,\count_i_reg[4]_i_1_n_7 }),
        .S(count_i_reg[7:4]));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(\count_i_reg[4]_i_1_n_6 ),
        .Q(count_i_reg[5]),
        .R(clear));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(\count_i_reg[4]_i_1_n_5 ),
        .Q(count_i_reg[6]),
        .R(clear));
  FDRE \count_i_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(\count_i_reg[4]_i_1_n_4 ),
        .Q(count_i_reg[7]),
        .R(clear));
  FDRE \count_i_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(\count_i_reg[8]_i_1_n_7 ),
        .Q(count_i_reg[8]),
        .R(clear));
  CARRY4 \count_i_reg[8]_i_1 
       (.CI(\count_i_reg[4]_i_1_n_0 ),
        .CO({\count_i_reg[8]_i_1_n_0 ,\count_i_reg[8]_i_1_n_1 ,\count_i_reg[8]_i_1_n_2 ,\count_i_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_i_reg[8]_i_1_n_4 ,\count_i_reg[8]_i_1_n_5 ,\count_i_reg[8]_i_1_n_6 ,\count_i_reg[8]_i_1_n_7 }),
        .S(count_i_reg[11:8]));
  FDRE \count_i_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(\count_i_reg[8]_i_1_n_6 ),
        .Q(count_i_reg[9]),
        .R(clear));
  FDRE \count_reg[0] 
       (.C(clk),
        .CE(aresetn),
        .D(count_i[0]),
        .Q(debug_leds[0]),
        .R(1'b0));
  FDRE \count_reg[10] 
       (.C(clk),
        .CE(aresetn),
        .D(count_i[10]),
        .Q(count[10]),
        .R(1'b0));
  FDRE \count_reg[11] 
       (.C(clk),
        .CE(aresetn),
        .D(count_i[11]),
        .Q(count[11]),
        .R(1'b0));
  CARRY4 \count_reg[11]_i_1 
       (.CI(\count_reg[7]_i_1_n_0 ),
        .CO({\count_reg[11]_i_1_n_0 ,\count_reg[11]_i_1_n_1 ,\count_reg[11]_i_1_n_2 ,\count_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count_i[11:8]),
        .S(count_i_reg[11:8]));
  FDRE \count_reg[12] 
       (.C(clk),
        .CE(aresetn),
        .D(count_i[12]),
        .Q(count[12]),
        .R(1'b0));
  FDRE \count_reg[13] 
       (.C(clk),
        .CE(aresetn),
        .D(count_i[13]),
        .Q(count[13]),
        .R(1'b0));
  FDRE \count_reg[14] 
       (.C(clk),
        .CE(aresetn),
        .D(count_i[14]),
        .Q(count[14]),
        .R(1'b0));
  FDRE \count_reg[15] 
       (.C(clk),
        .CE(aresetn),
        .D(count_i[15]),
        .Q(count[15]),
        .R(1'b0));
  CARRY4 \count_reg[15]_i_1 
       (.CI(\count_reg[11]_i_1_n_0 ),
        .CO({\NLW_count_reg[15]_i_1_CO_UNCONNECTED [3],\count_reg[15]_i_1_n_1 ,\count_reg[15]_i_1_n_2 ,\count_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count_i[15:12]),
        .S(count_i_reg[15:12]));
  FDRE \count_reg[1] 
       (.C(clk),
        .CE(aresetn),
        .D(count_i[1]),
        .Q(debug_leds[1]),
        .R(1'b0));
  FDRE \count_reg[2] 
       (.C(clk),
        .CE(aresetn),
        .D(count_i[2]),
        .Q(debug_leds[2]),
        .R(1'b0));
  FDRE \count_reg[3] 
       (.C(clk),
        .CE(aresetn),
        .D(count_i[3]),
        .Q(debug_leds[3]),
        .R(1'b0));
  CARRY4 \count_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\count_reg[3]_i_1_n_0 ,\count_reg[3]_i_1_n_1 ,\count_reg[3]_i_1_n_2 ,\count_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,count_i_reg[0]}),
        .O(count_i[3:0]),
        .S({count_i_reg[3:1],\count[3]_i_2_n_0 }));
  FDRE \count_reg[4] 
       (.C(clk),
        .CE(aresetn),
        .D(count_i[4]),
        .Q(debug_leds[4]),
        .R(1'b0));
  FDRE \count_reg[5] 
       (.C(clk),
        .CE(aresetn),
        .D(count_i[5]),
        .Q(debug_leds[5]),
        .R(1'b0));
  FDRE \count_reg[6] 
       (.C(clk),
        .CE(aresetn),
        .D(count_i[6]),
        .Q(debug_leds[6]),
        .R(1'b0));
  FDRE \count_reg[7] 
       (.C(clk),
        .CE(aresetn),
        .D(count_i[7]),
        .Q(debug_leds[7]),
        .R(1'b0));
  CARRY4 \count_reg[7]_i_1 
       (.CI(\count_reg[3]_i_1_n_0 ),
        .CO({\count_reg[7]_i_1_n_0 ,\count_reg[7]_i_1_n_1 ,\count_reg[7]_i_1_n_2 ,\count_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(count_i[7:4]),
        .S(count_i_reg[7:4]));
  FDRE \count_reg[8] 
       (.C(clk),
        .CE(aresetn),
        .D(count_i[8]),
        .Q(count[8]),
        .R(1'b0));
  FDRE \count_reg[9] 
       (.C(clk),
        .CE(aresetn),
        .D(count_i[9]),
        .Q(count[9]),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_register_interface i_register_interface
       (.D({i_register_interface_n_6,i_register_interface_n_7,i_register_interface_n_8,i_register_interface_n_9,i_register_interface_n_10,i_register_interface_n_11,i_register_interface_n_12,i_register_interface_n_13,i_register_interface_n_14,i_register_interface_n_15,i_register_interface_n_16,i_register_interface_n_17,i_register_interface_n_18,i_register_interface_n_19,i_register_interface_n_20,i_register_interface_n_21,i_register_interface_n_22}),
        .Q({i_register_interface_n_23,i_register_interface_n_24,i_register_interface_n_25,i_register_interface_n_26,i_register_interface_n_27,i_register_interface_n_28,i_register_interface_n_29,i_register_interface_n_30,i_register_interface_n_31,i_register_interface_n_32,i_register_interface_n_33,i_register_interface_n_34,i_register_interface_n_35,i_register_interface_n_36,i_register_interface_n_37,i_register_interface_n_38}),
        .S({\out_data[3]_i_2_n_0 ,\out_data[3]_i_3_n_0 ,\out_data[3]_i_4_n_0 ,\out_data[3]_i_5_n_0 }),
        .aresetn(aresetn),
        .clear(clear),
        .clk(clk),
        .\count_reg[15] ({count,debug_leds}),
        .s_axi_araddr(s_axi_araddr),
        .s_axi_arready(s_axi_arready),
        .s_axi_arvalid(s_axi_arvalid),
        .s_axi_awaddr(s_axi_awaddr),
        .s_axi_awready(s_axi_awready),
        .s_axi_awvalid(s_axi_awvalid),
        .s_axi_bready(s_axi_bready),
        .s_axi_bvalid(s_axi_bvalid),
        .s_axi_rdata(s_axi_rdata),
        .s_axi_rready(s_axi_rready),
        .s_axi_rvalid(s_axi_rvalid),
        .s_axi_wdata(s_axi_wdata),
        .s_axi_wready(s_axi_wready),
        .s_axi_wstrb(s_axi_wstrb),
        .s_axi_wvalid(s_axi_wvalid),
        .s_axis_tdata(s_axis_tdata),
        .\slv_regs_reg[0][11]_0 ({\out_data[11]_i_2_n_0 ,\out_data[11]_i_3_n_0 ,\out_data[11]_i_4_n_0 ,\out_data[11]_i_5_n_0 }),
        .\slv_regs_reg[0][15]_0 ({\out_data[15]_i_2_n_0 ,\out_data[15]_i_3_n_0 ,\out_data[15]_i_4_n_0 ,\out_data[15]_i_5_n_0 }),
        .\slv_regs_reg[0][7]_0 ({\out_data[7]_i_2_n_0 ,\out_data[7]_i_3_n_0 ,\out_data[7]_i_4_n_0 ,\out_data[7]_i_5_n_0 }));
  LUT2 #(
    .INIT(4'h6)) 
    \out_data[11]_i_2 
       (.I0(s_axis_tdata[11]),
        .I1(i_register_interface_n_27),
        .O(\out_data[11]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out_data[11]_i_3 
       (.I0(s_axis_tdata[10]),
        .I1(i_register_interface_n_28),
        .O(\out_data[11]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out_data[11]_i_4 
       (.I0(s_axis_tdata[9]),
        .I1(i_register_interface_n_29),
        .O(\out_data[11]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out_data[11]_i_5 
       (.I0(s_axis_tdata[8]),
        .I1(i_register_interface_n_30),
        .O(\out_data[11]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out_data[15]_i_2 
       (.I0(s_axis_tdata[15]),
        .I1(i_register_interface_n_23),
        .O(\out_data[15]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out_data[15]_i_3 
       (.I0(s_axis_tdata[14]),
        .I1(i_register_interface_n_24),
        .O(\out_data[15]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out_data[15]_i_4 
       (.I0(s_axis_tdata[13]),
        .I1(i_register_interface_n_25),
        .O(\out_data[15]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out_data[15]_i_5 
       (.I0(s_axis_tdata[12]),
        .I1(i_register_interface_n_26),
        .O(\out_data[15]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hA2)) 
    \out_data[16]_i_2 
       (.I0(s_axis_tvalid),
        .I1(m_axis_tvalid),
        .I2(m_axis_tready),
        .O(in_handshake));
  LUT2 #(
    .INIT(4'h6)) 
    \out_data[3]_i_2 
       (.I0(s_axis_tdata[3]),
        .I1(i_register_interface_n_35),
        .O(\out_data[3]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out_data[3]_i_3 
       (.I0(s_axis_tdata[2]),
        .I1(i_register_interface_n_36),
        .O(\out_data[3]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out_data[3]_i_4 
       (.I0(s_axis_tdata[1]),
        .I1(i_register_interface_n_37),
        .O(\out_data[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out_data[3]_i_5 
       (.I0(s_axis_tdata[0]),
        .I1(i_register_interface_n_38),
        .O(\out_data[3]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out_data[7]_i_2 
       (.I0(s_axis_tdata[7]),
        .I1(i_register_interface_n_31),
        .O(\out_data[7]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out_data[7]_i_3 
       (.I0(s_axis_tdata[6]),
        .I1(i_register_interface_n_32),
        .O(\out_data[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out_data[7]_i_4 
       (.I0(s_axis_tdata[5]),
        .I1(i_register_interface_n_33),
        .O(\out_data[7]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out_data[7]_i_5 
       (.I0(s_axis_tdata[4]),
        .I1(i_register_interface_n_34),
        .O(\out_data[7]_i_5_n_0 ));
  FDRE \out_data_reg[0] 
       (.C(clk),
        .CE(in_handshake),
        .D(i_register_interface_n_22),
        .Q(m_axis_tdata[0]),
        .R(clear));
  FDRE \out_data_reg[10] 
       (.C(clk),
        .CE(in_handshake),
        .D(i_register_interface_n_12),
        .Q(m_axis_tdata[10]),
        .R(clear));
  FDRE \out_data_reg[11] 
       (.C(clk),
        .CE(in_handshake),
        .D(i_register_interface_n_11),
        .Q(m_axis_tdata[11]),
        .R(clear));
  FDRE \out_data_reg[12] 
       (.C(clk),
        .CE(in_handshake),
        .D(i_register_interface_n_10),
        .Q(m_axis_tdata[12]),
        .R(clear));
  FDRE \out_data_reg[13] 
       (.C(clk),
        .CE(in_handshake),
        .D(i_register_interface_n_9),
        .Q(m_axis_tdata[13]),
        .R(clear));
  FDRE \out_data_reg[14] 
       (.C(clk),
        .CE(in_handshake),
        .D(i_register_interface_n_8),
        .Q(m_axis_tdata[14]),
        .R(clear));
  FDRE \out_data_reg[15] 
       (.C(clk),
        .CE(in_handshake),
        .D(i_register_interface_n_7),
        .Q(m_axis_tdata[15]),
        .R(clear));
  FDRE \out_data_reg[16] 
       (.C(clk),
        .CE(in_handshake),
        .D(i_register_interface_n_6),
        .Q(m_axis_tdata[16]),
        .R(clear));
  FDRE \out_data_reg[1] 
       (.C(clk),
        .CE(in_handshake),
        .D(i_register_interface_n_21),
        .Q(m_axis_tdata[1]),
        .R(clear));
  FDRE \out_data_reg[2] 
       (.C(clk),
        .CE(in_handshake),
        .D(i_register_interface_n_20),
        .Q(m_axis_tdata[2]),
        .R(clear));
  FDRE \out_data_reg[3] 
       (.C(clk),
        .CE(in_handshake),
        .D(i_register_interface_n_19),
        .Q(m_axis_tdata[3]),
        .R(clear));
  FDRE \out_data_reg[4] 
       (.C(clk),
        .CE(in_handshake),
        .D(i_register_interface_n_18),
        .Q(m_axis_tdata[4]),
        .R(clear));
  FDRE \out_data_reg[5] 
       (.C(clk),
        .CE(in_handshake),
        .D(i_register_interface_n_17),
        .Q(m_axis_tdata[5]),
        .R(clear));
  FDRE \out_data_reg[6] 
       (.C(clk),
        .CE(in_handshake),
        .D(i_register_interface_n_16),
        .Q(m_axis_tdata[6]),
        .R(clear));
  FDRE \out_data_reg[7] 
       (.C(clk),
        .CE(in_handshake),
        .D(i_register_interface_n_15),
        .Q(m_axis_tdata[7]),
        .R(clear));
  FDRE \out_data_reg[8] 
       (.C(clk),
        .CE(in_handshake),
        .D(i_register_interface_n_14),
        .Q(m_axis_tdata[8]),
        .R(clear));
  FDRE \out_data_reg[9] 
       (.C(clk),
        .CE(in_handshake),
        .D(i_register_interface_n_13),
        .Q(m_axis_tdata[9]),
        .R(clear));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h8FBB8088)) 
    out_last_i_1
       (.I0(s_axis_tlast),
        .I1(s_axis_tvalid),
        .I2(m_axis_tready),
        .I3(m_axis_tvalid),
        .I4(m_axis_tlast),
        .O(out_last_i_1_n_0));
  FDRE out_last_reg
       (.C(clk),
        .CE(1'b1),
        .D(out_last_i_1_n_0),
        .Q(m_axis_tlast),
        .R(clear));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    out_valid_i_1
       (.I0(s_axis_tvalid),
        .I1(m_axis_tready),
        .I2(m_axis_tvalid),
        .O(out_valid_i_1_n_0));
  FDRE out_valid_reg
       (.C(clk),
        .CE(1'b1),
        .D(out_valid_i_1_n_0),
        .Q(m_axis_tvalid),
        .R(clear));
  LUT2 #(
    .INIT(4'hB)) 
    s_axis_tready_INST_0
       (.I0(m_axis_tready),
        .I1(m_axis_tvalid),
        .O(s_axis_tready));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_adderCore_0_0,adder_top,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "adder_top,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk,
    aresetn,
    debug_leds,
    s_axis_tdata,
    s_axis_tvalid,
    s_axis_tready,
    s_axis_tlast,
    m_axis_tdata,
    m_axis_tvalid,
    m_axis_tready,
    m_axis_tlast,
    s_axi_awaddr,
    s_axi_awprot,
    s_axi_awvalid,
    s_axi_awready,
    s_axi_wdata,
    s_axi_wstrb,
    s_axi_wvalid,
    s_axi_wready,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_bready,
    s_axi_araddr,
    s_axi_arprot,
    s_axi_arvalid,
    s_axi_arready,
    s_axi_rdata,
    s_axi_rresp,
    s_axi_rvalid,
    s_axi_rready);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk, ASSOCIATED_BUSIF m_axis:s_axis:s_axi, ASSOCIATED_RESET aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0" *) input clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 aresetn RST" *) (* x_interface_parameter = "XIL_INTERFACENAME aresetn, POLARITY ACTIVE_LOW" *) input aresetn;
  output [7:0]debug_leds;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 s_axis TDATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_axis, TDATA_NUM_BYTES 2, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef" *) input [15:0]s_axis_tdata;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 s_axis TVALID" *) input s_axis_tvalid;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 s_axis TREADY" *) output s_axis_tready;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 s_axis TLAST" *) input s_axis_tlast;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 m_axis TDATA" *) (* x_interface_parameter = "XIL_INTERFACENAME m_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef" *) output [31:0]m_axis_tdata;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 m_axis TVALID" *) output m_axis_tvalid;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 m_axis TREADY" *) input m_axis_tready;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 m_axis TLAST" *) output m_axis_tlast;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME s_axi, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [5:0]s_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi AWPROT" *) input [2:0]s_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi AWVALID" *) input s_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi AWREADY" *) output s_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi WDATA" *) input [31:0]s_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi WSTRB" *) input [3:0]s_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi WVALID" *) input s_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi WREADY" *) output s_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi BRESP" *) output [1:0]s_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi BVALID" *) output s_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi BREADY" *) input s_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi ARADDR" *) input [5:0]s_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi ARPROT" *) input [2:0]s_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi ARVALID" *) input s_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi ARREADY" *) output s_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi RDATA" *) output [31:0]s_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi RRESP" *) output [1:0]s_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi RVALID" *) output s_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi RREADY" *) input s_axi_rready;

  wire \<const0> ;
  wire aresetn;
  wire clk;
  wire [7:0]debug_leds;
  wire [16:0]\^m_axis_tdata ;
  wire m_axis_tlast;
  wire m_axis_tready;
  wire m_axis_tvalid;
  wire [5:0]s_axi_araddr;
  wire s_axi_arready;
  wire s_axi_arvalid;
  wire [5:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire s_axi_bvalid;
  wire [31:0]s_axi_rdata;
  wire s_axi_rready;
  wire s_axi_rvalid;
  wire [31:0]s_axi_wdata;
  wire s_axi_wready;
  wire [3:0]s_axi_wstrb;
  wire s_axi_wvalid;
  wire [15:0]s_axis_tdata;
  wire s_axis_tlast;
  wire s_axis_tready;
  wire s_axis_tvalid;

  assign m_axis_tdata[31] = \<const0> ;
  assign m_axis_tdata[30] = \<const0> ;
  assign m_axis_tdata[29] = \<const0> ;
  assign m_axis_tdata[28] = \<const0> ;
  assign m_axis_tdata[27] = \<const0> ;
  assign m_axis_tdata[26] = \<const0> ;
  assign m_axis_tdata[25] = \<const0> ;
  assign m_axis_tdata[24] = \<const0> ;
  assign m_axis_tdata[23] = \<const0> ;
  assign m_axis_tdata[22] = \<const0> ;
  assign m_axis_tdata[21] = \<const0> ;
  assign m_axis_tdata[20] = \<const0> ;
  assign m_axis_tdata[19] = \<const0> ;
  assign m_axis_tdata[18] = \<const0> ;
  assign m_axis_tdata[17] = \<const0> ;
  assign m_axis_tdata[16:0] = \^m_axis_tdata [16:0];
  assign s_axi_bresp[1] = \<const0> ;
  assign s_axi_bresp[0] = \<const0> ;
  assign s_axi_rresp[1] = \<const0> ;
  assign s_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adder_top U0
       (.aresetn(aresetn),
        .clk(clk),
        .debug_leds(debug_leds),
        .m_axis_tdata(\^m_axis_tdata ),
        .m_axis_tlast(m_axis_tlast),
        .m_axis_tready(m_axis_tready),
        .m_axis_tvalid(m_axis_tvalid),
        .s_axi_araddr(s_axi_araddr[5:2]),
        .s_axi_arready(s_axi_arready),
        .s_axi_arvalid(s_axi_arvalid),
        .s_axi_awaddr(s_axi_awaddr[5:2]),
        .s_axi_awready(s_axi_awready),
        .s_axi_awvalid(s_axi_awvalid),
        .s_axi_bready(s_axi_bready),
        .s_axi_bvalid(s_axi_bvalid),
        .s_axi_rdata(s_axi_rdata),
        .s_axi_rready(s_axi_rready),
        .s_axi_rvalid(s_axi_rvalid),
        .s_axi_wdata(s_axi_wdata),
        .s_axi_wready(s_axi_wready),
        .s_axi_wstrb(s_axi_wstrb),
        .s_axi_wvalid(s_axi_wvalid),
        .s_axis_tdata(s_axis_tdata),
        .s_axis_tlast(s_axis_tlast),
        .s_axis_tready(s_axis_tready),
        .s_axis_tvalid(s_axis_tvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_register_interface
   (s_axi_awready,
    clear,
    s_axi_wready,
    s_axi_arready,
    s_axi_bvalid,
    s_axi_rvalid,
    D,
    Q,
    s_axi_rdata,
    clk,
    s_axi_awvalid,
    s_axi_wvalid,
    s_axi_arvalid,
    aresetn,
    s_axis_tdata,
    S,
    \slv_regs_reg[0][7]_0 ,
    \slv_regs_reg[0][11]_0 ,
    \slv_regs_reg[0][15]_0 ,
    s_axi_bready,
    s_axi_rready,
    s_axi_awaddr,
    s_axi_wdata,
    s_axi_araddr,
    s_axi_wstrb,
    \count_reg[15] );
  output s_axi_awready;
  output clear;
  output s_axi_wready;
  output s_axi_arready;
  output s_axi_bvalid;
  output s_axi_rvalid;
  output [16:0]D;
  output [15:0]Q;
  output [31:0]s_axi_rdata;
  input clk;
  input s_axi_awvalid;
  input s_axi_wvalid;
  input s_axi_arvalid;
  input aresetn;
  input [15:0]s_axis_tdata;
  input [3:0]S;
  input [3:0]\slv_regs_reg[0][7]_0 ;
  input [3:0]\slv_regs_reg[0][11]_0 ;
  input [3:0]\slv_regs_reg[0][15]_0 ;
  input s_axi_bready;
  input s_axi_rready;
  input [3:0]s_axi_awaddr;
  input [31:0]s_axi_wdata;
  input [3:0]s_axi_araddr;
  input [3:0]s_axi_wstrb;
  input [15:0]\count_reg[15] ;

  wire [16:0]D;
  wire [15:0]Q;
  wire [3:0]S;
  wire aresetn;
  wire axi_arready_i_1_n_0;
  wire axi_awready_i_1_n_0;
  wire axi_bvalid_i_1_n_0;
  wire \axi_rdata[0]_i_4_n_0 ;
  wire \axi_rdata[0]_i_5_n_0 ;
  wire \axi_rdata[0]_i_6_n_0 ;
  wire \axi_rdata[0]_i_7_n_0 ;
  wire \axi_rdata[10]_i_4_n_0 ;
  wire \axi_rdata[10]_i_5_n_0 ;
  wire \axi_rdata[10]_i_6_n_0 ;
  wire \axi_rdata[10]_i_7_n_0 ;
  wire \axi_rdata[11]_i_4_n_0 ;
  wire \axi_rdata[11]_i_5_n_0 ;
  wire \axi_rdata[11]_i_6_n_0 ;
  wire \axi_rdata[11]_i_7_n_0 ;
  wire \axi_rdata[12]_i_4_n_0 ;
  wire \axi_rdata[12]_i_5_n_0 ;
  wire \axi_rdata[12]_i_6_n_0 ;
  wire \axi_rdata[12]_i_7_n_0 ;
  wire \axi_rdata[13]_i_4_n_0 ;
  wire \axi_rdata[13]_i_5_n_0 ;
  wire \axi_rdata[13]_i_6_n_0 ;
  wire \axi_rdata[13]_i_7_n_0 ;
  wire \axi_rdata[14]_i_4_n_0 ;
  wire \axi_rdata[14]_i_5_n_0 ;
  wire \axi_rdata[14]_i_6_n_0 ;
  wire \axi_rdata[14]_i_7_n_0 ;
  wire \axi_rdata[15]_i_4_n_0 ;
  wire \axi_rdata[15]_i_5_n_0 ;
  wire \axi_rdata[15]_i_6_n_0 ;
  wire \axi_rdata[15]_i_7_n_0 ;
  wire \axi_rdata[16]_i_4_n_0 ;
  wire \axi_rdata[16]_i_5_n_0 ;
  wire \axi_rdata[16]_i_6_n_0 ;
  wire \axi_rdata[16]_i_7_n_0 ;
  wire \axi_rdata[17]_i_4_n_0 ;
  wire \axi_rdata[17]_i_5_n_0 ;
  wire \axi_rdata[17]_i_6_n_0 ;
  wire \axi_rdata[17]_i_7_n_0 ;
  wire \axi_rdata[18]_i_4_n_0 ;
  wire \axi_rdata[18]_i_5_n_0 ;
  wire \axi_rdata[18]_i_6_n_0 ;
  wire \axi_rdata[18]_i_7_n_0 ;
  wire \axi_rdata[19]_i_4_n_0 ;
  wire \axi_rdata[19]_i_5_n_0 ;
  wire \axi_rdata[19]_i_6_n_0 ;
  wire \axi_rdata[19]_i_7_n_0 ;
  wire \axi_rdata[1]_i_4_n_0 ;
  wire \axi_rdata[1]_i_5_n_0 ;
  wire \axi_rdata[1]_i_6_n_0 ;
  wire \axi_rdata[1]_i_7_n_0 ;
  wire \axi_rdata[20]_i_4_n_0 ;
  wire \axi_rdata[20]_i_5_n_0 ;
  wire \axi_rdata[20]_i_6_n_0 ;
  wire \axi_rdata[20]_i_7_n_0 ;
  wire \axi_rdata[21]_i_4_n_0 ;
  wire \axi_rdata[21]_i_5_n_0 ;
  wire \axi_rdata[21]_i_6_n_0 ;
  wire \axi_rdata[21]_i_7_n_0 ;
  wire \axi_rdata[22]_i_4_n_0 ;
  wire \axi_rdata[22]_i_5_n_0 ;
  wire \axi_rdata[22]_i_6_n_0 ;
  wire \axi_rdata[22]_i_7_n_0 ;
  wire \axi_rdata[23]_i_4_n_0 ;
  wire \axi_rdata[23]_i_5_n_0 ;
  wire \axi_rdata[23]_i_6_n_0 ;
  wire \axi_rdata[23]_i_7_n_0 ;
  wire \axi_rdata[24]_i_4_n_0 ;
  wire \axi_rdata[24]_i_5_n_0 ;
  wire \axi_rdata[24]_i_6_n_0 ;
  wire \axi_rdata[24]_i_7_n_0 ;
  wire \axi_rdata[25]_i_4_n_0 ;
  wire \axi_rdata[25]_i_5_n_0 ;
  wire \axi_rdata[25]_i_6_n_0 ;
  wire \axi_rdata[25]_i_7_n_0 ;
  wire \axi_rdata[26]_i_4_n_0 ;
  wire \axi_rdata[26]_i_5_n_0 ;
  wire \axi_rdata[26]_i_6_n_0 ;
  wire \axi_rdata[26]_i_7_n_0 ;
  wire \axi_rdata[27]_i_4_n_0 ;
  wire \axi_rdata[27]_i_5_n_0 ;
  wire \axi_rdata[27]_i_6_n_0 ;
  wire \axi_rdata[27]_i_7_n_0 ;
  wire \axi_rdata[28]_i_4_n_0 ;
  wire \axi_rdata[28]_i_5_n_0 ;
  wire \axi_rdata[28]_i_6_n_0 ;
  wire \axi_rdata[28]_i_7_n_0 ;
  wire \axi_rdata[29]_i_4_n_0 ;
  wire \axi_rdata[29]_i_5_n_0 ;
  wire \axi_rdata[29]_i_6_n_0 ;
  wire \axi_rdata[29]_i_7_n_0 ;
  wire \axi_rdata[2]_i_4_n_0 ;
  wire \axi_rdata[2]_i_5_n_0 ;
  wire \axi_rdata[2]_i_6_n_0 ;
  wire \axi_rdata[2]_i_7_n_0 ;
  wire \axi_rdata[30]_i_4_n_0 ;
  wire \axi_rdata[30]_i_5_n_0 ;
  wire \axi_rdata[30]_i_6_n_0 ;
  wire \axi_rdata[30]_i_7_n_0 ;
  wire \axi_rdata[31]_i_1_n_0 ;
  wire \axi_rdata[31]_i_5_n_0 ;
  wire \axi_rdata[31]_i_6_n_0 ;
  wire \axi_rdata[31]_i_7_n_0 ;
  wire \axi_rdata[31]_i_8_n_0 ;
  wire \axi_rdata[3]_i_4_n_0 ;
  wire \axi_rdata[3]_i_5_n_0 ;
  wire \axi_rdata[3]_i_6_n_0 ;
  wire \axi_rdata[3]_i_7_n_0 ;
  wire \axi_rdata[4]_i_4_n_0 ;
  wire \axi_rdata[4]_i_5_n_0 ;
  wire \axi_rdata[4]_i_6_n_0 ;
  wire \axi_rdata[4]_i_7_n_0 ;
  wire \axi_rdata[5]_i_4_n_0 ;
  wire \axi_rdata[5]_i_5_n_0 ;
  wire \axi_rdata[5]_i_6_n_0 ;
  wire \axi_rdata[5]_i_7_n_0 ;
  wire \axi_rdata[6]_i_4_n_0 ;
  wire \axi_rdata[6]_i_5_n_0 ;
  wire \axi_rdata[6]_i_6_n_0 ;
  wire \axi_rdata[6]_i_7_n_0 ;
  wire \axi_rdata[7]_i_4_n_0 ;
  wire \axi_rdata[7]_i_5_n_0 ;
  wire \axi_rdata[7]_i_6_n_0 ;
  wire \axi_rdata[7]_i_7_n_0 ;
  wire \axi_rdata[8]_i_4_n_0 ;
  wire \axi_rdata[8]_i_5_n_0 ;
  wire \axi_rdata[8]_i_6_n_0 ;
  wire \axi_rdata[8]_i_7_n_0 ;
  wire \axi_rdata[9]_i_4_n_0 ;
  wire \axi_rdata[9]_i_5_n_0 ;
  wire \axi_rdata[9]_i_6_n_0 ;
  wire \axi_rdata[9]_i_7_n_0 ;
  wire \axi_rdata_reg[0]_i_2_n_0 ;
  wire \axi_rdata_reg[0]_i_3_n_0 ;
  wire \axi_rdata_reg[10]_i_2_n_0 ;
  wire \axi_rdata_reg[10]_i_3_n_0 ;
  wire \axi_rdata_reg[11]_i_2_n_0 ;
  wire \axi_rdata_reg[11]_i_3_n_0 ;
  wire \axi_rdata_reg[12]_i_2_n_0 ;
  wire \axi_rdata_reg[12]_i_3_n_0 ;
  wire \axi_rdata_reg[13]_i_2_n_0 ;
  wire \axi_rdata_reg[13]_i_3_n_0 ;
  wire \axi_rdata_reg[14]_i_2_n_0 ;
  wire \axi_rdata_reg[14]_i_3_n_0 ;
  wire \axi_rdata_reg[15]_i_2_n_0 ;
  wire \axi_rdata_reg[15]_i_3_n_0 ;
  wire \axi_rdata_reg[16]_i_2_n_0 ;
  wire \axi_rdata_reg[16]_i_3_n_0 ;
  wire \axi_rdata_reg[17]_i_2_n_0 ;
  wire \axi_rdata_reg[17]_i_3_n_0 ;
  wire \axi_rdata_reg[18]_i_2_n_0 ;
  wire \axi_rdata_reg[18]_i_3_n_0 ;
  wire \axi_rdata_reg[19]_i_2_n_0 ;
  wire \axi_rdata_reg[19]_i_3_n_0 ;
  wire \axi_rdata_reg[1]_i_2_n_0 ;
  wire \axi_rdata_reg[1]_i_3_n_0 ;
  wire \axi_rdata_reg[20]_i_2_n_0 ;
  wire \axi_rdata_reg[20]_i_3_n_0 ;
  wire \axi_rdata_reg[21]_i_2_n_0 ;
  wire \axi_rdata_reg[21]_i_3_n_0 ;
  wire \axi_rdata_reg[22]_i_2_n_0 ;
  wire \axi_rdata_reg[22]_i_3_n_0 ;
  wire \axi_rdata_reg[23]_i_2_n_0 ;
  wire \axi_rdata_reg[23]_i_3_n_0 ;
  wire \axi_rdata_reg[24]_i_2_n_0 ;
  wire \axi_rdata_reg[24]_i_3_n_0 ;
  wire \axi_rdata_reg[25]_i_2_n_0 ;
  wire \axi_rdata_reg[25]_i_3_n_0 ;
  wire \axi_rdata_reg[26]_i_2_n_0 ;
  wire \axi_rdata_reg[26]_i_3_n_0 ;
  wire \axi_rdata_reg[27]_i_2_n_0 ;
  wire \axi_rdata_reg[27]_i_3_n_0 ;
  wire \axi_rdata_reg[28]_i_2_n_0 ;
  wire \axi_rdata_reg[28]_i_3_n_0 ;
  wire \axi_rdata_reg[29]_i_2_n_0 ;
  wire \axi_rdata_reg[29]_i_3_n_0 ;
  wire \axi_rdata_reg[2]_i_2_n_0 ;
  wire \axi_rdata_reg[2]_i_3_n_0 ;
  wire \axi_rdata_reg[30]_i_2_n_0 ;
  wire \axi_rdata_reg[30]_i_3_n_0 ;
  wire \axi_rdata_reg[31]_i_3_n_0 ;
  wire \axi_rdata_reg[31]_i_4_n_0 ;
  wire \axi_rdata_reg[3]_i_2_n_0 ;
  wire \axi_rdata_reg[3]_i_3_n_0 ;
  wire \axi_rdata_reg[4]_i_2_n_0 ;
  wire \axi_rdata_reg[4]_i_3_n_0 ;
  wire \axi_rdata_reg[5]_i_2_n_0 ;
  wire \axi_rdata_reg[5]_i_3_n_0 ;
  wire \axi_rdata_reg[6]_i_2_n_0 ;
  wire \axi_rdata_reg[6]_i_3_n_0 ;
  wire \axi_rdata_reg[7]_i_2_n_0 ;
  wire \axi_rdata_reg[7]_i_3_n_0 ;
  wire \axi_rdata_reg[8]_i_2_n_0 ;
  wire \axi_rdata_reg[8]_i_3_n_0 ;
  wire \axi_rdata_reg[9]_i_2_n_0 ;
  wire \axi_rdata_reg[9]_i_3_n_0 ;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready_i_1_n_0;
  wire clear;
  wire clk;
  wire [15:0]\count_reg[15] ;
  wire \out_data_reg[11]_i_1_n_0 ;
  wire \out_data_reg[11]_i_1_n_1 ;
  wire \out_data_reg[11]_i_1_n_2 ;
  wire \out_data_reg[11]_i_1_n_3 ;
  wire \out_data_reg[15]_i_1_n_0 ;
  wire \out_data_reg[15]_i_1_n_1 ;
  wire \out_data_reg[15]_i_1_n_2 ;
  wire \out_data_reg[15]_i_1_n_3 ;
  wire \out_data_reg[3]_i_1_n_0 ;
  wire \out_data_reg[3]_i_1_n_1 ;
  wire \out_data_reg[3]_i_1_n_2 ;
  wire \out_data_reg[3]_i_1_n_3 ;
  wire \out_data_reg[7]_i_1_n_0 ;
  wire \out_data_reg[7]_i_1_n_1 ;
  wire \out_data_reg[7]_i_1_n_2 ;
  wire \out_data_reg[7]_i_1_n_3 ;
  wire [3:0]p_0_in;
  wire [31:7]p_1_in;
  wire [31:0]\read_data[0]_0 ;
  wire [3:0]s_axi_araddr;
  wire s_axi_arready;
  wire s_axi_arvalid;
  wire [3:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire s_axi_bvalid;
  wire [31:0]s_axi_rdata;
  wire s_axi_rready;
  wire s_axi_rvalid;
  wire [31:0]s_axi_wdata;
  wire s_axi_wready;
  wire [3:0]s_axi_wstrb;
  wire s_axi_wvalid;
  wire [15:0]s_axis_tdata;
  wire [3:0]sel0;
  wire slv_reg_wren__2;
  wire \slv_regs[10][15]_i_1_n_0 ;
  wire \slv_regs[10][23]_i_1_n_0 ;
  wire \slv_regs[10][31]_i_1_n_0 ;
  wire \slv_regs[10][7]_i_1_n_0 ;
  wire \slv_regs[11][15]_i_1_n_0 ;
  wire \slv_regs[11][23]_i_1_n_0 ;
  wire \slv_regs[11][31]_i_1_n_0 ;
  wire \slv_regs[11][7]_i_1_n_0 ;
  wire \slv_regs[12][15]_i_1_n_0 ;
  wire \slv_regs[12][23]_i_1_n_0 ;
  wire \slv_regs[12][31]_i_1_n_0 ;
  wire \slv_regs[12][7]_i_1_n_0 ;
  wire \slv_regs[13][15]_i_1_n_0 ;
  wire \slv_regs[13][23]_i_1_n_0 ;
  wire \slv_regs[13][31]_i_1_n_0 ;
  wire \slv_regs[13][7]_i_1_n_0 ;
  wire \slv_regs[14][15]_i_1_n_0 ;
  wire \slv_regs[14][23]_i_1_n_0 ;
  wire \slv_regs[14][31]_i_1_n_0 ;
  wire \slv_regs[14][7]_i_1_n_0 ;
  wire \slv_regs[15][15]_i_1_n_0 ;
  wire \slv_regs[15][23]_i_1_n_0 ;
  wire \slv_regs[15][31]_i_1_n_0 ;
  wire \slv_regs[15][7]_i_1_n_0 ;
  wire \slv_regs[2][15]_i_1_n_0 ;
  wire \slv_regs[2][23]_i_1_n_0 ;
  wire \slv_regs[2][31]_i_1_n_0 ;
  wire \slv_regs[2][7]_i_1_n_0 ;
  wire \slv_regs[3][15]_i_1_n_0 ;
  wire \slv_regs[3][23]_i_1_n_0 ;
  wire \slv_regs[3][31]_i_1_n_0 ;
  wire \slv_regs[3][7]_i_1_n_0 ;
  wire \slv_regs[4][15]_i_1_n_0 ;
  wire \slv_regs[4][23]_i_1_n_0 ;
  wire \slv_regs[4][31]_i_1_n_0 ;
  wire \slv_regs[4][7]_i_1_n_0 ;
  wire \slv_regs[5][15]_i_1_n_0 ;
  wire \slv_regs[5][23]_i_1_n_0 ;
  wire \slv_regs[5][31]_i_1_n_0 ;
  wire \slv_regs[5][7]_i_1_n_0 ;
  wire \slv_regs[6][15]_i_1_n_0 ;
  wire \slv_regs[6][23]_i_1_n_0 ;
  wire \slv_regs[6][31]_i_1_n_0 ;
  wire \slv_regs[6][7]_i_1_n_0 ;
  wire \slv_regs[7][15]_i_1_n_0 ;
  wire \slv_regs[7][23]_i_1_n_0 ;
  wire \slv_regs[7][31]_i_1_n_0 ;
  wire \slv_regs[7][7]_i_1_n_0 ;
  wire \slv_regs[8][15]_i_1_n_0 ;
  wire \slv_regs[8][23]_i_1_n_0 ;
  wire \slv_regs[8][31]_i_1_n_0 ;
  wire \slv_regs[8][7]_i_1_n_0 ;
  wire \slv_regs[9][15]_i_1_n_0 ;
  wire \slv_regs[9][23]_i_1_n_0 ;
  wire \slv_regs[9][31]_i_1_n_0 ;
  wire \slv_regs[9][7]_i_1_n_0 ;
  wire [3:0]\slv_regs_reg[0][11]_0 ;
  wire [3:0]\slv_regs_reg[0][15]_0 ;
  wire [3:0]\slv_regs_reg[0][7]_0 ;
  wire [31:0]\slv_regs_reg[10] ;
  wire [31:0]\slv_regs_reg[11] ;
  wire [31:0]\slv_regs_reg[12] ;
  wire [31:0]\slv_regs_reg[13] ;
  wire [31:0]\slv_regs_reg[14] ;
  wire [31:0]\slv_regs_reg[15] ;
  wire [31:0]\slv_regs_reg[2] ;
  wire [31:0]\slv_regs_reg[3] ;
  wire [31:0]\slv_regs_reg[4] ;
  wire [31:0]\slv_regs_reg[5] ;
  wire [31:0]\slv_regs_reg[6] ;
  wire [31:0]\slv_regs_reg[7] ;
  wire [31:0]\slv_regs_reg[8] ;
  wire [31:0]\slv_regs_reg[9] ;
  wire \slv_regs_reg_n_0_[0][16] ;
  wire \slv_regs_reg_n_0_[0][17] ;
  wire \slv_regs_reg_n_0_[0][18] ;
  wire \slv_regs_reg_n_0_[0][19] ;
  wire \slv_regs_reg_n_0_[0][20] ;
  wire \slv_regs_reg_n_0_[0][21] ;
  wire \slv_regs_reg_n_0_[0][22] ;
  wire \slv_regs_reg_n_0_[0][23] ;
  wire \slv_regs_reg_n_0_[0][24] ;
  wire \slv_regs_reg_n_0_[0][25] ;
  wire \slv_regs_reg_n_0_[0][26] ;
  wire \slv_regs_reg_n_0_[0][27] ;
  wire \slv_regs_reg_n_0_[0][28] ;
  wire \slv_regs_reg_n_0_[0][29] ;
  wire \slv_regs_reg_n_0_[0][30] ;
  wire \slv_regs_reg_n_0_[0][31] ;
  wire [3:1]\NLW_out_data_reg[16]_i_3_CO_UNCONNECTED ;
  wire [3:0]\NLW_out_data_reg[16]_i_3_O_UNCONNECTED ;

  FDSE \axi_araddr_reg[2] 
       (.C(clk),
        .CE(axi_arready_i_1_n_0),
        .D(s_axi_araddr[0]),
        .Q(sel0[0]),
        .S(clear));
  FDSE \axi_araddr_reg[3] 
       (.C(clk),
        .CE(axi_arready_i_1_n_0),
        .D(s_axi_araddr[1]),
        .Q(sel0[1]),
        .S(clear));
  FDSE \axi_araddr_reg[4] 
       (.C(clk),
        .CE(axi_arready_i_1_n_0),
        .D(s_axi_araddr[2]),
        .Q(sel0[2]),
        .S(clear));
  FDSE \axi_araddr_reg[5] 
       (.C(clk),
        .CE(axi_arready_i_1_n_0),
        .D(s_axi_araddr[3]),
        .Q(sel0[3]),
        .S(clear));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s_axi_arvalid),
        .I1(s_axi_arready),
        .O(axi_arready_i_1_n_0));
  FDRE axi_arready_reg
       (.C(clk),
        .CE(1'b1),
        .D(axi_arready_i_1_n_0),
        .Q(s_axi_arready),
        .R(clear));
  FDRE \axi_awaddr_reg[2] 
       (.C(clk),
        .CE(axi_awready_i_1_n_0),
        .D(s_axi_awaddr[0]),
        .Q(p_0_in[0]),
        .R(clear));
  FDRE \axi_awaddr_reg[3] 
       (.C(clk),
        .CE(axi_awready_i_1_n_0),
        .D(s_axi_awaddr[1]),
        .Q(p_0_in[1]),
        .R(clear));
  FDRE \axi_awaddr_reg[4] 
       (.C(clk),
        .CE(axi_awready_i_1_n_0),
        .D(s_axi_awaddr[2]),
        .Q(p_0_in[2]),
        .R(clear));
  FDRE \axi_awaddr_reg[5] 
       (.C(clk),
        .CE(axi_awready_i_1_n_0),
        .D(s_axi_awaddr[3]),
        .Q(p_0_in[3]),
        .R(clear));
  LUT3 #(
    .INIT(8'h08)) 
    axi_awready_i_1
       (.I0(s_axi_awvalid),
        .I1(s_axi_wvalid),
        .I2(s_axi_awready),
        .O(axi_awready_i_1_n_0));
  FDRE axi_awready_reg
       (.C(clk),
        .CE(1'b1),
        .D(axi_awready_i_1_n_0),
        .Q(s_axi_awready),
        .R(clear));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s_axi_wready),
        .I1(s_axi_wvalid),
        .I2(s_axi_awready),
        .I3(s_axi_awvalid),
        .I4(s_axi_bready),
        .I5(s_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(clk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s_axi_bvalid),
        .R(clear));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_4 
       (.I0(\slv_regs_reg[3] [0]),
        .I1(\slv_regs_reg[2] [0]),
        .I2(sel0[1]),
        .I3(\count_reg[15] [0]),
        .I4(sel0[0]),
        .I5(Q[0]),
        .O(\axi_rdata[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_5 
       (.I0(\slv_regs_reg[7] [0]),
        .I1(\slv_regs_reg[6] [0]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [0]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [0]),
        .O(\axi_rdata[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_6 
       (.I0(\slv_regs_reg[11] [0]),
        .I1(\slv_regs_reg[10] [0]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [0]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [0]),
        .O(\axi_rdata[0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_7 
       (.I0(\slv_regs_reg[15] [0]),
        .I1(\slv_regs_reg[14] [0]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [0]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [0]),
        .O(\axi_rdata[0]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_4 
       (.I0(\slv_regs_reg[3] [10]),
        .I1(\slv_regs_reg[2] [10]),
        .I2(sel0[1]),
        .I3(\count_reg[15] [10]),
        .I4(sel0[0]),
        .I5(Q[10]),
        .O(\axi_rdata[10]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_5 
       (.I0(\slv_regs_reg[7] [10]),
        .I1(\slv_regs_reg[6] [10]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [10]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [10]),
        .O(\axi_rdata[10]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_6 
       (.I0(\slv_regs_reg[11] [10]),
        .I1(\slv_regs_reg[10] [10]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [10]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [10]),
        .O(\axi_rdata[10]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_7 
       (.I0(\slv_regs_reg[15] [10]),
        .I1(\slv_regs_reg[14] [10]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [10]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [10]),
        .O(\axi_rdata[10]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_4 
       (.I0(\slv_regs_reg[3] [11]),
        .I1(\slv_regs_reg[2] [11]),
        .I2(sel0[1]),
        .I3(\count_reg[15] [11]),
        .I4(sel0[0]),
        .I5(Q[11]),
        .O(\axi_rdata[11]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_5 
       (.I0(\slv_regs_reg[7] [11]),
        .I1(\slv_regs_reg[6] [11]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [11]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [11]),
        .O(\axi_rdata[11]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_6 
       (.I0(\slv_regs_reg[11] [11]),
        .I1(\slv_regs_reg[10] [11]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [11]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [11]),
        .O(\axi_rdata[11]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_7 
       (.I0(\slv_regs_reg[15] [11]),
        .I1(\slv_regs_reg[14] [11]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [11]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [11]),
        .O(\axi_rdata[11]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_4 
       (.I0(\slv_regs_reg[3] [12]),
        .I1(\slv_regs_reg[2] [12]),
        .I2(sel0[1]),
        .I3(\count_reg[15] [12]),
        .I4(sel0[0]),
        .I5(Q[12]),
        .O(\axi_rdata[12]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_5 
       (.I0(\slv_regs_reg[7] [12]),
        .I1(\slv_regs_reg[6] [12]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [12]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [12]),
        .O(\axi_rdata[12]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_6 
       (.I0(\slv_regs_reg[11] [12]),
        .I1(\slv_regs_reg[10] [12]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [12]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [12]),
        .O(\axi_rdata[12]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_7 
       (.I0(\slv_regs_reg[15] [12]),
        .I1(\slv_regs_reg[14] [12]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [12]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [12]),
        .O(\axi_rdata[12]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_4 
       (.I0(\slv_regs_reg[3] [13]),
        .I1(\slv_regs_reg[2] [13]),
        .I2(sel0[1]),
        .I3(\count_reg[15] [13]),
        .I4(sel0[0]),
        .I5(Q[13]),
        .O(\axi_rdata[13]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_5 
       (.I0(\slv_regs_reg[7] [13]),
        .I1(\slv_regs_reg[6] [13]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [13]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [13]),
        .O(\axi_rdata[13]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_6 
       (.I0(\slv_regs_reg[11] [13]),
        .I1(\slv_regs_reg[10] [13]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [13]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [13]),
        .O(\axi_rdata[13]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_7 
       (.I0(\slv_regs_reg[15] [13]),
        .I1(\slv_regs_reg[14] [13]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [13]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [13]),
        .O(\axi_rdata[13]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_4 
       (.I0(\slv_regs_reg[3] [14]),
        .I1(\slv_regs_reg[2] [14]),
        .I2(sel0[1]),
        .I3(\count_reg[15] [14]),
        .I4(sel0[0]),
        .I5(Q[14]),
        .O(\axi_rdata[14]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_5 
       (.I0(\slv_regs_reg[7] [14]),
        .I1(\slv_regs_reg[6] [14]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [14]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [14]),
        .O(\axi_rdata[14]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_6 
       (.I0(\slv_regs_reg[11] [14]),
        .I1(\slv_regs_reg[10] [14]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [14]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [14]),
        .O(\axi_rdata[14]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_7 
       (.I0(\slv_regs_reg[15] [14]),
        .I1(\slv_regs_reg[14] [14]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [14]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [14]),
        .O(\axi_rdata[14]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_4 
       (.I0(\slv_regs_reg[3] [15]),
        .I1(\slv_regs_reg[2] [15]),
        .I2(sel0[1]),
        .I3(\count_reg[15] [15]),
        .I4(sel0[0]),
        .I5(Q[15]),
        .O(\axi_rdata[15]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_5 
       (.I0(\slv_regs_reg[7] [15]),
        .I1(\slv_regs_reg[6] [15]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [15]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [15]),
        .O(\axi_rdata[15]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_6 
       (.I0(\slv_regs_reg[11] [15]),
        .I1(\slv_regs_reg[10] [15]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [15]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [15]),
        .O(\axi_rdata[15]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_7 
       (.I0(\slv_regs_reg[15] [15]),
        .I1(\slv_regs_reg[14] [15]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [15]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [15]),
        .O(\axi_rdata[15]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[16]_i_4 
       (.I0(\slv_regs_reg[3] [16]),
        .I1(\slv_regs_reg[2] [16]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][16] ),
        .I4(sel0[0]),
        .O(\axi_rdata[16]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_5 
       (.I0(\slv_regs_reg[7] [16]),
        .I1(\slv_regs_reg[6] [16]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [16]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [16]),
        .O(\axi_rdata[16]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_6 
       (.I0(\slv_regs_reg[11] [16]),
        .I1(\slv_regs_reg[10] [16]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [16]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [16]),
        .O(\axi_rdata[16]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_7 
       (.I0(\slv_regs_reg[15] [16]),
        .I1(\slv_regs_reg[14] [16]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [16]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [16]),
        .O(\axi_rdata[16]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[17]_i_4 
       (.I0(\slv_regs_reg[3] [17]),
        .I1(\slv_regs_reg[2] [17]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][17] ),
        .I4(sel0[0]),
        .O(\axi_rdata[17]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_5 
       (.I0(\slv_regs_reg[7] [17]),
        .I1(\slv_regs_reg[6] [17]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [17]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [17]),
        .O(\axi_rdata[17]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_6 
       (.I0(\slv_regs_reg[11] [17]),
        .I1(\slv_regs_reg[10] [17]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [17]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [17]),
        .O(\axi_rdata[17]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_7 
       (.I0(\slv_regs_reg[15] [17]),
        .I1(\slv_regs_reg[14] [17]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [17]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [17]),
        .O(\axi_rdata[17]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[18]_i_4 
       (.I0(\slv_regs_reg[3] [18]),
        .I1(\slv_regs_reg[2] [18]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][18] ),
        .I4(sel0[0]),
        .O(\axi_rdata[18]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_5 
       (.I0(\slv_regs_reg[7] [18]),
        .I1(\slv_regs_reg[6] [18]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [18]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [18]),
        .O(\axi_rdata[18]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_6 
       (.I0(\slv_regs_reg[11] [18]),
        .I1(\slv_regs_reg[10] [18]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [18]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [18]),
        .O(\axi_rdata[18]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_7 
       (.I0(\slv_regs_reg[15] [18]),
        .I1(\slv_regs_reg[14] [18]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [18]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [18]),
        .O(\axi_rdata[18]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[19]_i_4 
       (.I0(\slv_regs_reg[3] [19]),
        .I1(\slv_regs_reg[2] [19]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][19] ),
        .I4(sel0[0]),
        .O(\axi_rdata[19]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_5 
       (.I0(\slv_regs_reg[7] [19]),
        .I1(\slv_regs_reg[6] [19]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [19]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [19]),
        .O(\axi_rdata[19]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_6 
       (.I0(\slv_regs_reg[11] [19]),
        .I1(\slv_regs_reg[10] [19]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [19]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [19]),
        .O(\axi_rdata[19]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_7 
       (.I0(\slv_regs_reg[15] [19]),
        .I1(\slv_regs_reg[14] [19]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [19]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [19]),
        .O(\axi_rdata[19]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_4 
       (.I0(\slv_regs_reg[3] [1]),
        .I1(\slv_regs_reg[2] [1]),
        .I2(sel0[1]),
        .I3(\count_reg[15] [1]),
        .I4(sel0[0]),
        .I5(Q[1]),
        .O(\axi_rdata[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_5 
       (.I0(\slv_regs_reg[7] [1]),
        .I1(\slv_regs_reg[6] [1]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [1]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [1]),
        .O(\axi_rdata[1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_6 
       (.I0(\slv_regs_reg[11] [1]),
        .I1(\slv_regs_reg[10] [1]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [1]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [1]),
        .O(\axi_rdata[1]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_7 
       (.I0(\slv_regs_reg[15] [1]),
        .I1(\slv_regs_reg[14] [1]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [1]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [1]),
        .O(\axi_rdata[1]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[20]_i_4 
       (.I0(\slv_regs_reg[3] [20]),
        .I1(\slv_regs_reg[2] [20]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][20] ),
        .I4(sel0[0]),
        .O(\axi_rdata[20]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_5 
       (.I0(\slv_regs_reg[7] [20]),
        .I1(\slv_regs_reg[6] [20]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [20]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [20]),
        .O(\axi_rdata[20]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_6 
       (.I0(\slv_regs_reg[11] [20]),
        .I1(\slv_regs_reg[10] [20]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [20]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [20]),
        .O(\axi_rdata[20]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_7 
       (.I0(\slv_regs_reg[15] [20]),
        .I1(\slv_regs_reg[14] [20]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [20]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [20]),
        .O(\axi_rdata[20]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[21]_i_4 
       (.I0(\slv_regs_reg[3] [21]),
        .I1(\slv_regs_reg[2] [21]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][21] ),
        .I4(sel0[0]),
        .O(\axi_rdata[21]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_5 
       (.I0(\slv_regs_reg[7] [21]),
        .I1(\slv_regs_reg[6] [21]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [21]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [21]),
        .O(\axi_rdata[21]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_6 
       (.I0(\slv_regs_reg[11] [21]),
        .I1(\slv_regs_reg[10] [21]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [21]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [21]),
        .O(\axi_rdata[21]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_7 
       (.I0(\slv_regs_reg[15] [21]),
        .I1(\slv_regs_reg[14] [21]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [21]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [21]),
        .O(\axi_rdata[21]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[22]_i_4 
       (.I0(\slv_regs_reg[3] [22]),
        .I1(\slv_regs_reg[2] [22]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][22] ),
        .I4(sel0[0]),
        .O(\axi_rdata[22]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_5 
       (.I0(\slv_regs_reg[7] [22]),
        .I1(\slv_regs_reg[6] [22]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [22]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [22]),
        .O(\axi_rdata[22]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_6 
       (.I0(\slv_regs_reg[11] [22]),
        .I1(\slv_regs_reg[10] [22]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [22]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [22]),
        .O(\axi_rdata[22]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_7 
       (.I0(\slv_regs_reg[15] [22]),
        .I1(\slv_regs_reg[14] [22]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [22]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [22]),
        .O(\axi_rdata[22]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[23]_i_4 
       (.I0(\slv_regs_reg[3] [23]),
        .I1(\slv_regs_reg[2] [23]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][23] ),
        .I4(sel0[0]),
        .O(\axi_rdata[23]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_5 
       (.I0(\slv_regs_reg[7] [23]),
        .I1(\slv_regs_reg[6] [23]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [23]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [23]),
        .O(\axi_rdata[23]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_6 
       (.I0(\slv_regs_reg[11] [23]),
        .I1(\slv_regs_reg[10] [23]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [23]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [23]),
        .O(\axi_rdata[23]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_7 
       (.I0(\slv_regs_reg[15] [23]),
        .I1(\slv_regs_reg[14] [23]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [23]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [23]),
        .O(\axi_rdata[23]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[24]_i_4 
       (.I0(\slv_regs_reg[3] [24]),
        .I1(\slv_regs_reg[2] [24]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][24] ),
        .I4(sel0[0]),
        .O(\axi_rdata[24]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_5 
       (.I0(\slv_regs_reg[7] [24]),
        .I1(\slv_regs_reg[6] [24]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [24]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [24]),
        .O(\axi_rdata[24]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_6 
       (.I0(\slv_regs_reg[11] [24]),
        .I1(\slv_regs_reg[10] [24]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [24]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [24]),
        .O(\axi_rdata[24]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_7 
       (.I0(\slv_regs_reg[15] [24]),
        .I1(\slv_regs_reg[14] [24]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [24]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [24]),
        .O(\axi_rdata[24]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[25]_i_4 
       (.I0(\slv_regs_reg[3] [25]),
        .I1(\slv_regs_reg[2] [25]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][25] ),
        .I4(sel0[0]),
        .O(\axi_rdata[25]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_5 
       (.I0(\slv_regs_reg[7] [25]),
        .I1(\slv_regs_reg[6] [25]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [25]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [25]),
        .O(\axi_rdata[25]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_6 
       (.I0(\slv_regs_reg[11] [25]),
        .I1(\slv_regs_reg[10] [25]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [25]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [25]),
        .O(\axi_rdata[25]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_7 
       (.I0(\slv_regs_reg[15] [25]),
        .I1(\slv_regs_reg[14] [25]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [25]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [25]),
        .O(\axi_rdata[25]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[26]_i_4 
       (.I0(\slv_regs_reg[3] [26]),
        .I1(\slv_regs_reg[2] [26]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][26] ),
        .I4(sel0[0]),
        .O(\axi_rdata[26]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_5 
       (.I0(\slv_regs_reg[7] [26]),
        .I1(\slv_regs_reg[6] [26]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [26]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [26]),
        .O(\axi_rdata[26]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_6 
       (.I0(\slv_regs_reg[11] [26]),
        .I1(\slv_regs_reg[10] [26]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [26]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [26]),
        .O(\axi_rdata[26]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_7 
       (.I0(\slv_regs_reg[15] [26]),
        .I1(\slv_regs_reg[14] [26]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [26]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [26]),
        .O(\axi_rdata[26]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[27]_i_4 
       (.I0(\slv_regs_reg[3] [27]),
        .I1(\slv_regs_reg[2] [27]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][27] ),
        .I4(sel0[0]),
        .O(\axi_rdata[27]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_5 
       (.I0(\slv_regs_reg[7] [27]),
        .I1(\slv_regs_reg[6] [27]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [27]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [27]),
        .O(\axi_rdata[27]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_6 
       (.I0(\slv_regs_reg[11] [27]),
        .I1(\slv_regs_reg[10] [27]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [27]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [27]),
        .O(\axi_rdata[27]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_7 
       (.I0(\slv_regs_reg[15] [27]),
        .I1(\slv_regs_reg[14] [27]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [27]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [27]),
        .O(\axi_rdata[27]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[28]_i_4 
       (.I0(\slv_regs_reg[3] [28]),
        .I1(\slv_regs_reg[2] [28]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][28] ),
        .I4(sel0[0]),
        .O(\axi_rdata[28]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_5 
       (.I0(\slv_regs_reg[7] [28]),
        .I1(\slv_regs_reg[6] [28]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [28]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [28]),
        .O(\axi_rdata[28]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_6 
       (.I0(\slv_regs_reg[11] [28]),
        .I1(\slv_regs_reg[10] [28]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [28]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [28]),
        .O(\axi_rdata[28]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_7 
       (.I0(\slv_regs_reg[15] [28]),
        .I1(\slv_regs_reg[14] [28]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [28]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [28]),
        .O(\axi_rdata[28]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[29]_i_4 
       (.I0(\slv_regs_reg[3] [29]),
        .I1(\slv_regs_reg[2] [29]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][29] ),
        .I4(sel0[0]),
        .O(\axi_rdata[29]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_5 
       (.I0(\slv_regs_reg[7] [29]),
        .I1(\slv_regs_reg[6] [29]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [29]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [29]),
        .O(\axi_rdata[29]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_6 
       (.I0(\slv_regs_reg[11] [29]),
        .I1(\slv_regs_reg[10] [29]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [29]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [29]),
        .O(\axi_rdata[29]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_7 
       (.I0(\slv_regs_reg[15] [29]),
        .I1(\slv_regs_reg[14] [29]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [29]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [29]),
        .O(\axi_rdata[29]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_4 
       (.I0(\slv_regs_reg[3] [2]),
        .I1(\slv_regs_reg[2] [2]),
        .I2(sel0[1]),
        .I3(\count_reg[15] [2]),
        .I4(sel0[0]),
        .I5(Q[2]),
        .O(\axi_rdata[2]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_5 
       (.I0(\slv_regs_reg[7] [2]),
        .I1(\slv_regs_reg[6] [2]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [2]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [2]),
        .O(\axi_rdata[2]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_6 
       (.I0(\slv_regs_reg[11] [2]),
        .I1(\slv_regs_reg[10] [2]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [2]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [2]),
        .O(\axi_rdata[2]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_7 
       (.I0(\slv_regs_reg[15] [2]),
        .I1(\slv_regs_reg[14] [2]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [2]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [2]),
        .O(\axi_rdata[2]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[30]_i_4 
       (.I0(\slv_regs_reg[3] [30]),
        .I1(\slv_regs_reg[2] [30]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][30] ),
        .I4(sel0[0]),
        .O(\axi_rdata[30]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_5 
       (.I0(\slv_regs_reg[7] [30]),
        .I1(\slv_regs_reg[6] [30]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [30]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [30]),
        .O(\axi_rdata[30]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_6 
       (.I0(\slv_regs_reg[11] [30]),
        .I1(\slv_regs_reg[10] [30]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [30]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [30]),
        .O(\axi_rdata[30]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_7 
       (.I0(\slv_regs_reg[15] [30]),
        .I1(\slv_regs_reg[14] [30]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [30]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [30]),
        .O(\axi_rdata[30]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(s_axi_arready),
        .I1(s_axi_arvalid),
        .I2(s_axi_rvalid),
        .O(\axi_rdata[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[31]_i_5 
       (.I0(\slv_regs_reg[3] [31]),
        .I1(\slv_regs_reg[2] [31]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][31] ),
        .I4(sel0[0]),
        .O(\axi_rdata[31]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_6 
       (.I0(\slv_regs_reg[7] [31]),
        .I1(\slv_regs_reg[6] [31]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [31]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [31]),
        .O(\axi_rdata[31]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_7 
       (.I0(\slv_regs_reg[11] [31]),
        .I1(\slv_regs_reg[10] [31]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [31]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [31]),
        .O(\axi_rdata[31]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_8 
       (.I0(\slv_regs_reg[15] [31]),
        .I1(\slv_regs_reg[14] [31]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [31]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [31]),
        .O(\axi_rdata[31]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_4 
       (.I0(\slv_regs_reg[3] [3]),
        .I1(\slv_regs_reg[2] [3]),
        .I2(sel0[1]),
        .I3(\count_reg[15] [3]),
        .I4(sel0[0]),
        .I5(Q[3]),
        .O(\axi_rdata[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_5 
       (.I0(\slv_regs_reg[7] [3]),
        .I1(\slv_regs_reg[6] [3]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [3]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [3]),
        .O(\axi_rdata[3]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_6 
       (.I0(\slv_regs_reg[11] [3]),
        .I1(\slv_regs_reg[10] [3]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [3]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [3]),
        .O(\axi_rdata[3]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_7 
       (.I0(\slv_regs_reg[15] [3]),
        .I1(\slv_regs_reg[14] [3]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [3]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [3]),
        .O(\axi_rdata[3]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_4 
       (.I0(\slv_regs_reg[3] [4]),
        .I1(\slv_regs_reg[2] [4]),
        .I2(sel0[1]),
        .I3(\count_reg[15] [4]),
        .I4(sel0[0]),
        .I5(Q[4]),
        .O(\axi_rdata[4]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_5 
       (.I0(\slv_regs_reg[7] [4]),
        .I1(\slv_regs_reg[6] [4]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [4]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [4]),
        .O(\axi_rdata[4]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_6 
       (.I0(\slv_regs_reg[11] [4]),
        .I1(\slv_regs_reg[10] [4]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [4]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [4]),
        .O(\axi_rdata[4]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_7 
       (.I0(\slv_regs_reg[15] [4]),
        .I1(\slv_regs_reg[14] [4]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [4]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [4]),
        .O(\axi_rdata[4]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_4 
       (.I0(\slv_regs_reg[3] [5]),
        .I1(\slv_regs_reg[2] [5]),
        .I2(sel0[1]),
        .I3(\count_reg[15] [5]),
        .I4(sel0[0]),
        .I5(Q[5]),
        .O(\axi_rdata[5]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_5 
       (.I0(\slv_regs_reg[7] [5]),
        .I1(\slv_regs_reg[6] [5]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [5]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [5]),
        .O(\axi_rdata[5]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_6 
       (.I0(\slv_regs_reg[11] [5]),
        .I1(\slv_regs_reg[10] [5]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [5]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [5]),
        .O(\axi_rdata[5]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_7 
       (.I0(\slv_regs_reg[15] [5]),
        .I1(\slv_regs_reg[14] [5]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [5]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [5]),
        .O(\axi_rdata[5]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_4 
       (.I0(\slv_regs_reg[3] [6]),
        .I1(\slv_regs_reg[2] [6]),
        .I2(sel0[1]),
        .I3(\count_reg[15] [6]),
        .I4(sel0[0]),
        .I5(Q[6]),
        .O(\axi_rdata[6]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_5 
       (.I0(\slv_regs_reg[7] [6]),
        .I1(\slv_regs_reg[6] [6]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [6]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [6]),
        .O(\axi_rdata[6]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_6 
       (.I0(\slv_regs_reg[11] [6]),
        .I1(\slv_regs_reg[10] [6]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [6]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [6]),
        .O(\axi_rdata[6]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_7 
       (.I0(\slv_regs_reg[15] [6]),
        .I1(\slv_regs_reg[14] [6]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [6]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [6]),
        .O(\axi_rdata[6]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_4 
       (.I0(\slv_regs_reg[3] [7]),
        .I1(\slv_regs_reg[2] [7]),
        .I2(sel0[1]),
        .I3(\count_reg[15] [7]),
        .I4(sel0[0]),
        .I5(Q[7]),
        .O(\axi_rdata[7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_5 
       (.I0(\slv_regs_reg[7] [7]),
        .I1(\slv_regs_reg[6] [7]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [7]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [7]),
        .O(\axi_rdata[7]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_6 
       (.I0(\slv_regs_reg[11] [7]),
        .I1(\slv_regs_reg[10] [7]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [7]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [7]),
        .O(\axi_rdata[7]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_7 
       (.I0(\slv_regs_reg[15] [7]),
        .I1(\slv_regs_reg[14] [7]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [7]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [7]),
        .O(\axi_rdata[7]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_4 
       (.I0(\slv_regs_reg[3] [8]),
        .I1(\slv_regs_reg[2] [8]),
        .I2(sel0[1]),
        .I3(\count_reg[15] [8]),
        .I4(sel0[0]),
        .I5(Q[8]),
        .O(\axi_rdata[8]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_5 
       (.I0(\slv_regs_reg[7] [8]),
        .I1(\slv_regs_reg[6] [8]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [8]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [8]),
        .O(\axi_rdata[8]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_6 
       (.I0(\slv_regs_reg[11] [8]),
        .I1(\slv_regs_reg[10] [8]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [8]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [8]),
        .O(\axi_rdata[8]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_7 
       (.I0(\slv_regs_reg[15] [8]),
        .I1(\slv_regs_reg[14] [8]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [8]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [8]),
        .O(\axi_rdata[8]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_4 
       (.I0(\slv_regs_reg[3] [9]),
        .I1(\slv_regs_reg[2] [9]),
        .I2(sel0[1]),
        .I3(\count_reg[15] [9]),
        .I4(sel0[0]),
        .I5(Q[9]),
        .O(\axi_rdata[9]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_5 
       (.I0(\slv_regs_reg[7] [9]),
        .I1(\slv_regs_reg[6] [9]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5] [9]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4] [9]),
        .O(\axi_rdata[9]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_6 
       (.I0(\slv_regs_reg[11] [9]),
        .I1(\slv_regs_reg[10] [9]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9] [9]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8] [9]),
        .O(\axi_rdata[9]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_7 
       (.I0(\slv_regs_reg[15] [9]),
        .I1(\slv_regs_reg[14] [9]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13] [9]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12] [9]),
        .O(\axi_rdata[9]_i_7_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [0]),
        .Q(s_axi_rdata[0]),
        .R(clear));
  MUXF8 \axi_rdata_reg[0]_i_1 
       (.I0(\axi_rdata_reg[0]_i_2_n_0 ),
        .I1(\axi_rdata_reg[0]_i_3_n_0 ),
        .O(\read_data[0]_0 [0]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[0]_i_2 
       (.I0(\axi_rdata[0]_i_4_n_0 ),
        .I1(\axi_rdata[0]_i_5_n_0 ),
        .O(\axi_rdata_reg[0]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[0]_i_3 
       (.I0(\axi_rdata[0]_i_6_n_0 ),
        .I1(\axi_rdata[0]_i_7_n_0 ),
        .O(\axi_rdata_reg[0]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[10] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [10]),
        .Q(s_axi_rdata[10]),
        .R(clear));
  MUXF8 \axi_rdata_reg[10]_i_1 
       (.I0(\axi_rdata_reg[10]_i_2_n_0 ),
        .I1(\axi_rdata_reg[10]_i_3_n_0 ),
        .O(\read_data[0]_0 [10]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[10]_i_2 
       (.I0(\axi_rdata[10]_i_4_n_0 ),
        .I1(\axi_rdata[10]_i_5_n_0 ),
        .O(\axi_rdata_reg[10]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[10]_i_3 
       (.I0(\axi_rdata[10]_i_6_n_0 ),
        .I1(\axi_rdata[10]_i_7_n_0 ),
        .O(\axi_rdata_reg[10]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[11] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [11]),
        .Q(s_axi_rdata[11]),
        .R(clear));
  MUXF8 \axi_rdata_reg[11]_i_1 
       (.I0(\axi_rdata_reg[11]_i_2_n_0 ),
        .I1(\axi_rdata_reg[11]_i_3_n_0 ),
        .O(\read_data[0]_0 [11]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[11]_i_2 
       (.I0(\axi_rdata[11]_i_4_n_0 ),
        .I1(\axi_rdata[11]_i_5_n_0 ),
        .O(\axi_rdata_reg[11]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[11]_i_3 
       (.I0(\axi_rdata[11]_i_6_n_0 ),
        .I1(\axi_rdata[11]_i_7_n_0 ),
        .O(\axi_rdata_reg[11]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[12] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [12]),
        .Q(s_axi_rdata[12]),
        .R(clear));
  MUXF8 \axi_rdata_reg[12]_i_1 
       (.I0(\axi_rdata_reg[12]_i_2_n_0 ),
        .I1(\axi_rdata_reg[12]_i_3_n_0 ),
        .O(\read_data[0]_0 [12]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[12]_i_2 
       (.I0(\axi_rdata[12]_i_4_n_0 ),
        .I1(\axi_rdata[12]_i_5_n_0 ),
        .O(\axi_rdata_reg[12]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[12]_i_3 
       (.I0(\axi_rdata[12]_i_6_n_0 ),
        .I1(\axi_rdata[12]_i_7_n_0 ),
        .O(\axi_rdata_reg[12]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[13] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [13]),
        .Q(s_axi_rdata[13]),
        .R(clear));
  MUXF8 \axi_rdata_reg[13]_i_1 
       (.I0(\axi_rdata_reg[13]_i_2_n_0 ),
        .I1(\axi_rdata_reg[13]_i_3_n_0 ),
        .O(\read_data[0]_0 [13]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[13]_i_2 
       (.I0(\axi_rdata[13]_i_4_n_0 ),
        .I1(\axi_rdata[13]_i_5_n_0 ),
        .O(\axi_rdata_reg[13]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[13]_i_3 
       (.I0(\axi_rdata[13]_i_6_n_0 ),
        .I1(\axi_rdata[13]_i_7_n_0 ),
        .O(\axi_rdata_reg[13]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[14] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [14]),
        .Q(s_axi_rdata[14]),
        .R(clear));
  MUXF8 \axi_rdata_reg[14]_i_1 
       (.I0(\axi_rdata_reg[14]_i_2_n_0 ),
        .I1(\axi_rdata_reg[14]_i_3_n_0 ),
        .O(\read_data[0]_0 [14]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[14]_i_2 
       (.I0(\axi_rdata[14]_i_4_n_0 ),
        .I1(\axi_rdata[14]_i_5_n_0 ),
        .O(\axi_rdata_reg[14]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[14]_i_3 
       (.I0(\axi_rdata[14]_i_6_n_0 ),
        .I1(\axi_rdata[14]_i_7_n_0 ),
        .O(\axi_rdata_reg[14]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[15] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [15]),
        .Q(s_axi_rdata[15]),
        .R(clear));
  MUXF8 \axi_rdata_reg[15]_i_1 
       (.I0(\axi_rdata_reg[15]_i_2_n_0 ),
        .I1(\axi_rdata_reg[15]_i_3_n_0 ),
        .O(\read_data[0]_0 [15]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[15]_i_2 
       (.I0(\axi_rdata[15]_i_4_n_0 ),
        .I1(\axi_rdata[15]_i_5_n_0 ),
        .O(\axi_rdata_reg[15]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[15]_i_3 
       (.I0(\axi_rdata[15]_i_6_n_0 ),
        .I1(\axi_rdata[15]_i_7_n_0 ),
        .O(\axi_rdata_reg[15]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[16] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [16]),
        .Q(s_axi_rdata[16]),
        .R(clear));
  MUXF8 \axi_rdata_reg[16]_i_1 
       (.I0(\axi_rdata_reg[16]_i_2_n_0 ),
        .I1(\axi_rdata_reg[16]_i_3_n_0 ),
        .O(\read_data[0]_0 [16]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[16]_i_2 
       (.I0(\axi_rdata[16]_i_4_n_0 ),
        .I1(\axi_rdata[16]_i_5_n_0 ),
        .O(\axi_rdata_reg[16]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[16]_i_3 
       (.I0(\axi_rdata[16]_i_6_n_0 ),
        .I1(\axi_rdata[16]_i_7_n_0 ),
        .O(\axi_rdata_reg[16]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[17] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [17]),
        .Q(s_axi_rdata[17]),
        .R(clear));
  MUXF8 \axi_rdata_reg[17]_i_1 
       (.I0(\axi_rdata_reg[17]_i_2_n_0 ),
        .I1(\axi_rdata_reg[17]_i_3_n_0 ),
        .O(\read_data[0]_0 [17]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[17]_i_2 
       (.I0(\axi_rdata[17]_i_4_n_0 ),
        .I1(\axi_rdata[17]_i_5_n_0 ),
        .O(\axi_rdata_reg[17]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[17]_i_3 
       (.I0(\axi_rdata[17]_i_6_n_0 ),
        .I1(\axi_rdata[17]_i_7_n_0 ),
        .O(\axi_rdata_reg[17]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[18] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [18]),
        .Q(s_axi_rdata[18]),
        .R(clear));
  MUXF8 \axi_rdata_reg[18]_i_1 
       (.I0(\axi_rdata_reg[18]_i_2_n_0 ),
        .I1(\axi_rdata_reg[18]_i_3_n_0 ),
        .O(\read_data[0]_0 [18]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[18]_i_2 
       (.I0(\axi_rdata[18]_i_4_n_0 ),
        .I1(\axi_rdata[18]_i_5_n_0 ),
        .O(\axi_rdata_reg[18]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[18]_i_3 
       (.I0(\axi_rdata[18]_i_6_n_0 ),
        .I1(\axi_rdata[18]_i_7_n_0 ),
        .O(\axi_rdata_reg[18]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[19] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [19]),
        .Q(s_axi_rdata[19]),
        .R(clear));
  MUXF8 \axi_rdata_reg[19]_i_1 
       (.I0(\axi_rdata_reg[19]_i_2_n_0 ),
        .I1(\axi_rdata_reg[19]_i_3_n_0 ),
        .O(\read_data[0]_0 [19]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[19]_i_2 
       (.I0(\axi_rdata[19]_i_4_n_0 ),
        .I1(\axi_rdata[19]_i_5_n_0 ),
        .O(\axi_rdata_reg[19]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[19]_i_3 
       (.I0(\axi_rdata[19]_i_6_n_0 ),
        .I1(\axi_rdata[19]_i_7_n_0 ),
        .O(\axi_rdata_reg[19]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[1] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [1]),
        .Q(s_axi_rdata[1]),
        .R(clear));
  MUXF8 \axi_rdata_reg[1]_i_1 
       (.I0(\axi_rdata_reg[1]_i_2_n_0 ),
        .I1(\axi_rdata_reg[1]_i_3_n_0 ),
        .O(\read_data[0]_0 [1]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[1]_i_2 
       (.I0(\axi_rdata[1]_i_4_n_0 ),
        .I1(\axi_rdata[1]_i_5_n_0 ),
        .O(\axi_rdata_reg[1]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[1]_i_3 
       (.I0(\axi_rdata[1]_i_6_n_0 ),
        .I1(\axi_rdata[1]_i_7_n_0 ),
        .O(\axi_rdata_reg[1]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[20] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [20]),
        .Q(s_axi_rdata[20]),
        .R(clear));
  MUXF8 \axi_rdata_reg[20]_i_1 
       (.I0(\axi_rdata_reg[20]_i_2_n_0 ),
        .I1(\axi_rdata_reg[20]_i_3_n_0 ),
        .O(\read_data[0]_0 [20]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[20]_i_2 
       (.I0(\axi_rdata[20]_i_4_n_0 ),
        .I1(\axi_rdata[20]_i_5_n_0 ),
        .O(\axi_rdata_reg[20]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[20]_i_3 
       (.I0(\axi_rdata[20]_i_6_n_0 ),
        .I1(\axi_rdata[20]_i_7_n_0 ),
        .O(\axi_rdata_reg[20]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[21] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [21]),
        .Q(s_axi_rdata[21]),
        .R(clear));
  MUXF8 \axi_rdata_reg[21]_i_1 
       (.I0(\axi_rdata_reg[21]_i_2_n_0 ),
        .I1(\axi_rdata_reg[21]_i_3_n_0 ),
        .O(\read_data[0]_0 [21]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[21]_i_2 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(\axi_rdata[21]_i_5_n_0 ),
        .O(\axi_rdata_reg[21]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[21]_i_3 
       (.I0(\axi_rdata[21]_i_6_n_0 ),
        .I1(\axi_rdata[21]_i_7_n_0 ),
        .O(\axi_rdata_reg[21]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[22] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [22]),
        .Q(s_axi_rdata[22]),
        .R(clear));
  MUXF8 \axi_rdata_reg[22]_i_1 
       (.I0(\axi_rdata_reg[22]_i_2_n_0 ),
        .I1(\axi_rdata_reg[22]_i_3_n_0 ),
        .O(\read_data[0]_0 [22]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[22]_i_2 
       (.I0(\axi_rdata[22]_i_4_n_0 ),
        .I1(\axi_rdata[22]_i_5_n_0 ),
        .O(\axi_rdata_reg[22]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[22]_i_3 
       (.I0(\axi_rdata[22]_i_6_n_0 ),
        .I1(\axi_rdata[22]_i_7_n_0 ),
        .O(\axi_rdata_reg[22]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[23] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [23]),
        .Q(s_axi_rdata[23]),
        .R(clear));
  MUXF8 \axi_rdata_reg[23]_i_1 
       (.I0(\axi_rdata_reg[23]_i_2_n_0 ),
        .I1(\axi_rdata_reg[23]_i_3_n_0 ),
        .O(\read_data[0]_0 [23]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[23]_i_2 
       (.I0(\axi_rdata[23]_i_4_n_0 ),
        .I1(\axi_rdata[23]_i_5_n_0 ),
        .O(\axi_rdata_reg[23]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[23]_i_3 
       (.I0(\axi_rdata[23]_i_6_n_0 ),
        .I1(\axi_rdata[23]_i_7_n_0 ),
        .O(\axi_rdata_reg[23]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[24] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [24]),
        .Q(s_axi_rdata[24]),
        .R(clear));
  MUXF8 \axi_rdata_reg[24]_i_1 
       (.I0(\axi_rdata_reg[24]_i_2_n_0 ),
        .I1(\axi_rdata_reg[24]_i_3_n_0 ),
        .O(\read_data[0]_0 [24]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[24]_i_2 
       (.I0(\axi_rdata[24]_i_4_n_0 ),
        .I1(\axi_rdata[24]_i_5_n_0 ),
        .O(\axi_rdata_reg[24]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[24]_i_3 
       (.I0(\axi_rdata[24]_i_6_n_0 ),
        .I1(\axi_rdata[24]_i_7_n_0 ),
        .O(\axi_rdata_reg[24]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[25] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [25]),
        .Q(s_axi_rdata[25]),
        .R(clear));
  MUXF8 \axi_rdata_reg[25]_i_1 
       (.I0(\axi_rdata_reg[25]_i_2_n_0 ),
        .I1(\axi_rdata_reg[25]_i_3_n_0 ),
        .O(\read_data[0]_0 [25]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[25]_i_2 
       (.I0(\axi_rdata[25]_i_4_n_0 ),
        .I1(\axi_rdata[25]_i_5_n_0 ),
        .O(\axi_rdata_reg[25]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[25]_i_3 
       (.I0(\axi_rdata[25]_i_6_n_0 ),
        .I1(\axi_rdata[25]_i_7_n_0 ),
        .O(\axi_rdata_reg[25]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[26] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [26]),
        .Q(s_axi_rdata[26]),
        .R(clear));
  MUXF8 \axi_rdata_reg[26]_i_1 
       (.I0(\axi_rdata_reg[26]_i_2_n_0 ),
        .I1(\axi_rdata_reg[26]_i_3_n_0 ),
        .O(\read_data[0]_0 [26]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[26]_i_2 
       (.I0(\axi_rdata[26]_i_4_n_0 ),
        .I1(\axi_rdata[26]_i_5_n_0 ),
        .O(\axi_rdata_reg[26]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[26]_i_3 
       (.I0(\axi_rdata[26]_i_6_n_0 ),
        .I1(\axi_rdata[26]_i_7_n_0 ),
        .O(\axi_rdata_reg[26]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[27] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [27]),
        .Q(s_axi_rdata[27]),
        .R(clear));
  MUXF8 \axi_rdata_reg[27]_i_1 
       (.I0(\axi_rdata_reg[27]_i_2_n_0 ),
        .I1(\axi_rdata_reg[27]_i_3_n_0 ),
        .O(\read_data[0]_0 [27]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[27]_i_2 
       (.I0(\axi_rdata[27]_i_4_n_0 ),
        .I1(\axi_rdata[27]_i_5_n_0 ),
        .O(\axi_rdata_reg[27]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[27]_i_3 
       (.I0(\axi_rdata[27]_i_6_n_0 ),
        .I1(\axi_rdata[27]_i_7_n_0 ),
        .O(\axi_rdata_reg[27]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[28] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [28]),
        .Q(s_axi_rdata[28]),
        .R(clear));
  MUXF8 \axi_rdata_reg[28]_i_1 
       (.I0(\axi_rdata_reg[28]_i_2_n_0 ),
        .I1(\axi_rdata_reg[28]_i_3_n_0 ),
        .O(\read_data[0]_0 [28]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[28]_i_2 
       (.I0(\axi_rdata[28]_i_4_n_0 ),
        .I1(\axi_rdata[28]_i_5_n_0 ),
        .O(\axi_rdata_reg[28]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[28]_i_3 
       (.I0(\axi_rdata[28]_i_6_n_0 ),
        .I1(\axi_rdata[28]_i_7_n_0 ),
        .O(\axi_rdata_reg[28]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[29] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [29]),
        .Q(s_axi_rdata[29]),
        .R(clear));
  MUXF8 \axi_rdata_reg[29]_i_1 
       (.I0(\axi_rdata_reg[29]_i_2_n_0 ),
        .I1(\axi_rdata_reg[29]_i_3_n_0 ),
        .O(\read_data[0]_0 [29]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[29]_i_2 
       (.I0(\axi_rdata[29]_i_4_n_0 ),
        .I1(\axi_rdata[29]_i_5_n_0 ),
        .O(\axi_rdata_reg[29]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[29]_i_3 
       (.I0(\axi_rdata[29]_i_6_n_0 ),
        .I1(\axi_rdata[29]_i_7_n_0 ),
        .O(\axi_rdata_reg[29]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[2] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [2]),
        .Q(s_axi_rdata[2]),
        .R(clear));
  MUXF8 \axi_rdata_reg[2]_i_1 
       (.I0(\axi_rdata_reg[2]_i_2_n_0 ),
        .I1(\axi_rdata_reg[2]_i_3_n_0 ),
        .O(\read_data[0]_0 [2]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[2]_i_2 
       (.I0(\axi_rdata[2]_i_4_n_0 ),
        .I1(\axi_rdata[2]_i_5_n_0 ),
        .O(\axi_rdata_reg[2]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[2]_i_3 
       (.I0(\axi_rdata[2]_i_6_n_0 ),
        .I1(\axi_rdata[2]_i_7_n_0 ),
        .O(\axi_rdata_reg[2]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[30] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [30]),
        .Q(s_axi_rdata[30]),
        .R(clear));
  MUXF8 \axi_rdata_reg[30]_i_1 
       (.I0(\axi_rdata_reg[30]_i_2_n_0 ),
        .I1(\axi_rdata_reg[30]_i_3_n_0 ),
        .O(\read_data[0]_0 [30]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[30]_i_2 
       (.I0(\axi_rdata[30]_i_4_n_0 ),
        .I1(\axi_rdata[30]_i_5_n_0 ),
        .O(\axi_rdata_reg[30]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[30]_i_3 
       (.I0(\axi_rdata[30]_i_6_n_0 ),
        .I1(\axi_rdata[30]_i_7_n_0 ),
        .O(\axi_rdata_reg[30]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[31] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [31]),
        .Q(s_axi_rdata[31]),
        .R(clear));
  MUXF8 \axi_rdata_reg[31]_i_2 
       (.I0(\axi_rdata_reg[31]_i_3_n_0 ),
        .I1(\axi_rdata_reg[31]_i_4_n_0 ),
        .O(\read_data[0]_0 [31]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[31]_i_3 
       (.I0(\axi_rdata[31]_i_5_n_0 ),
        .I1(\axi_rdata[31]_i_6_n_0 ),
        .O(\axi_rdata_reg[31]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[31]_i_4 
       (.I0(\axi_rdata[31]_i_7_n_0 ),
        .I1(\axi_rdata[31]_i_8_n_0 ),
        .O(\axi_rdata_reg[31]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[3] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [3]),
        .Q(s_axi_rdata[3]),
        .R(clear));
  MUXF8 \axi_rdata_reg[3]_i_1 
       (.I0(\axi_rdata_reg[3]_i_2_n_0 ),
        .I1(\axi_rdata_reg[3]_i_3_n_0 ),
        .O(\read_data[0]_0 [3]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[3]_i_2 
       (.I0(\axi_rdata[3]_i_4_n_0 ),
        .I1(\axi_rdata[3]_i_5_n_0 ),
        .O(\axi_rdata_reg[3]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[3]_i_3 
       (.I0(\axi_rdata[3]_i_6_n_0 ),
        .I1(\axi_rdata[3]_i_7_n_0 ),
        .O(\axi_rdata_reg[3]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[4] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [4]),
        .Q(s_axi_rdata[4]),
        .R(clear));
  MUXF8 \axi_rdata_reg[4]_i_1 
       (.I0(\axi_rdata_reg[4]_i_2_n_0 ),
        .I1(\axi_rdata_reg[4]_i_3_n_0 ),
        .O(\read_data[0]_0 [4]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[4]_i_2 
       (.I0(\axi_rdata[4]_i_4_n_0 ),
        .I1(\axi_rdata[4]_i_5_n_0 ),
        .O(\axi_rdata_reg[4]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[4]_i_3 
       (.I0(\axi_rdata[4]_i_6_n_0 ),
        .I1(\axi_rdata[4]_i_7_n_0 ),
        .O(\axi_rdata_reg[4]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[5] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [5]),
        .Q(s_axi_rdata[5]),
        .R(clear));
  MUXF8 \axi_rdata_reg[5]_i_1 
       (.I0(\axi_rdata_reg[5]_i_2_n_0 ),
        .I1(\axi_rdata_reg[5]_i_3_n_0 ),
        .O(\read_data[0]_0 [5]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[5]_i_2 
       (.I0(\axi_rdata[5]_i_4_n_0 ),
        .I1(\axi_rdata[5]_i_5_n_0 ),
        .O(\axi_rdata_reg[5]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[5]_i_3 
       (.I0(\axi_rdata[5]_i_6_n_0 ),
        .I1(\axi_rdata[5]_i_7_n_0 ),
        .O(\axi_rdata_reg[5]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[6] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [6]),
        .Q(s_axi_rdata[6]),
        .R(clear));
  MUXF8 \axi_rdata_reg[6]_i_1 
       (.I0(\axi_rdata_reg[6]_i_2_n_0 ),
        .I1(\axi_rdata_reg[6]_i_3_n_0 ),
        .O(\read_data[0]_0 [6]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[6]_i_2 
       (.I0(\axi_rdata[6]_i_4_n_0 ),
        .I1(\axi_rdata[6]_i_5_n_0 ),
        .O(\axi_rdata_reg[6]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[6]_i_3 
       (.I0(\axi_rdata[6]_i_6_n_0 ),
        .I1(\axi_rdata[6]_i_7_n_0 ),
        .O(\axi_rdata_reg[6]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[7] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [7]),
        .Q(s_axi_rdata[7]),
        .R(clear));
  MUXF8 \axi_rdata_reg[7]_i_1 
       (.I0(\axi_rdata_reg[7]_i_2_n_0 ),
        .I1(\axi_rdata_reg[7]_i_3_n_0 ),
        .O(\read_data[0]_0 [7]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[7]_i_2 
       (.I0(\axi_rdata[7]_i_4_n_0 ),
        .I1(\axi_rdata[7]_i_5_n_0 ),
        .O(\axi_rdata_reg[7]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[7]_i_3 
       (.I0(\axi_rdata[7]_i_6_n_0 ),
        .I1(\axi_rdata[7]_i_7_n_0 ),
        .O(\axi_rdata_reg[7]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[8] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [8]),
        .Q(s_axi_rdata[8]),
        .R(clear));
  MUXF8 \axi_rdata_reg[8]_i_1 
       (.I0(\axi_rdata_reg[8]_i_2_n_0 ),
        .I1(\axi_rdata_reg[8]_i_3_n_0 ),
        .O(\read_data[0]_0 [8]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[8]_i_2 
       (.I0(\axi_rdata[8]_i_4_n_0 ),
        .I1(\axi_rdata[8]_i_5_n_0 ),
        .O(\axi_rdata_reg[8]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[8]_i_3 
       (.I0(\axi_rdata[8]_i_6_n_0 ),
        .I1(\axi_rdata[8]_i_7_n_0 ),
        .O(\axi_rdata_reg[8]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[9] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_0 [9]),
        .Q(s_axi_rdata[9]),
        .R(clear));
  MUXF8 \axi_rdata_reg[9]_i_1 
       (.I0(\axi_rdata_reg[9]_i_2_n_0 ),
        .I1(\axi_rdata_reg[9]_i_3_n_0 ),
        .O(\read_data[0]_0 [9]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[9]_i_2 
       (.I0(\axi_rdata[9]_i_4_n_0 ),
        .I1(\axi_rdata[9]_i_5_n_0 ),
        .O(\axi_rdata_reg[9]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[9]_i_3 
       (.I0(\axi_rdata[9]_i_6_n_0 ),
        .I1(\axi_rdata[9]_i_7_n_0 ),
        .O(\axi_rdata_reg[9]_i_3_n_0 ),
        .S(sel0[2]));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s_axi_arvalid),
        .I1(s_axi_arready),
        .I2(s_axi_rvalid),
        .I3(s_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(clk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s_axi_rvalid),
        .R(clear));
  LUT3 #(
    .INIT(8'h08)) 
    axi_wready_i_1
       (.I0(s_axi_awvalid),
        .I1(s_axi_wvalid),
        .I2(s_axi_wready),
        .O(axi_wready_i_1_n_0));
  FDRE axi_wready_reg
       (.C(clk),
        .CE(1'b1),
        .D(axi_wready_i_1_n_0),
        .Q(s_axi_wready),
        .R(clear));
  LUT1 #(
    .INIT(2'h1)) 
    \out_data[16]_i_1 
       (.I0(aresetn),
        .O(clear));
  CARRY4 \out_data_reg[11]_i_1 
       (.CI(\out_data_reg[7]_i_1_n_0 ),
        .CO({\out_data_reg[11]_i_1_n_0 ,\out_data_reg[11]_i_1_n_1 ,\out_data_reg[11]_i_1_n_2 ,\out_data_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(s_axis_tdata[11:8]),
        .O(D[11:8]),
        .S(\slv_regs_reg[0][11]_0 ));
  CARRY4 \out_data_reg[15]_i_1 
       (.CI(\out_data_reg[11]_i_1_n_0 ),
        .CO({\out_data_reg[15]_i_1_n_0 ,\out_data_reg[15]_i_1_n_1 ,\out_data_reg[15]_i_1_n_2 ,\out_data_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(s_axis_tdata[15:12]),
        .O(D[15:12]),
        .S(\slv_regs_reg[0][15]_0 ));
  CARRY4 \out_data_reg[16]_i_3 
       (.CI(\out_data_reg[15]_i_1_n_0 ),
        .CO({\NLW_out_data_reg[16]_i_3_CO_UNCONNECTED [3:1],D[16]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_out_data_reg[16]_i_3_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  CARRY4 \out_data_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\out_data_reg[3]_i_1_n_0 ,\out_data_reg[3]_i_1_n_1 ,\out_data_reg[3]_i_1_n_2 ,\out_data_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(s_axis_tdata[3:0]),
        .O(D[3:0]),
        .S(S));
  CARRY4 \out_data_reg[7]_i_1 
       (.CI(\out_data_reg[3]_i_1_n_0 ),
        .CO({\out_data_reg[7]_i_1_n_0 ,\out_data_reg[7]_i_1_n_1 ,\out_data_reg[7]_i_1_n_2 ,\out_data_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(s_axis_tdata[7:4]),
        .O(D[7:4]),
        .S(\slv_regs_reg[0][7]_0 ));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \slv_regs[0][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[1]),
        .O(p_1_in[15]));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \slv_regs[0][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[2]),
        .O(p_1_in[23]));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \slv_regs[0][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[3]),
        .O(p_1_in[31]));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_regs[0][31]_i_2 
       (.I0(s_axi_wready),
        .I1(s_axi_wvalid),
        .I2(s_axi_awready),
        .I3(s_axi_awvalid),
        .O(slv_reg_wren__2));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \slv_regs[0][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[0]),
        .O(p_1_in[7]));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[10][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[10][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[10][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[10][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[10][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[10][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[10][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[10][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[11][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[11][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[11][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[11][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[11][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[11][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[11][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[11][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[12][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[12][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[12][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[12][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[12][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[12][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[12][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[12][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[13][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[13][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[13][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[13][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[13][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[13][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[13][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[13][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[14][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[14][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[14][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[14][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[14][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[14][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[14][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[14][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_regs[15][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[15][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_regs[15][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[15][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_regs[15][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[15][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_regs[15][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[15][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[2][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[2][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[2][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[2][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[2][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[2][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[2][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[2][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[3][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(p_0_in[3]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[3][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[3][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(p_0_in[3]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[3][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[3][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(p_0_in[3]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[3][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[3][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(p_0_in[3]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[3][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[4][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[4][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[4][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[4][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[4][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[4][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[4][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[4][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[5][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[5][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[5][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[5][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[5][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[5][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[5][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[5][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[6][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[6][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[6][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[6][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[6][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[6][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[6][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[6][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[7][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[7][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[7][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[7][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[7][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[7][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[7][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[7][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[8][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[8][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[8][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[8][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[8][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[8][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[8][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[8][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[9][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[9][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[9][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[9][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[9][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[9][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[9][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[9][7]_i_1_n_0 ));
  FDRE \slv_regs_reg[0][0] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[0]),
        .Q(Q[0]),
        .R(clear));
  FDRE \slv_regs_reg[0][10] 
       (.C(clk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[10]),
        .Q(Q[10]),
        .R(clear));
  FDRE \slv_regs_reg[0][11] 
       (.C(clk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[11]),
        .Q(Q[11]),
        .R(clear));
  FDRE \slv_regs_reg[0][12] 
       (.C(clk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[12]),
        .Q(Q[12]),
        .R(clear));
  FDRE \slv_regs_reg[0][13] 
       (.C(clk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[13]),
        .Q(Q[13]),
        .R(clear));
  FDRE \slv_regs_reg[0][14] 
       (.C(clk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[14]),
        .Q(Q[14]),
        .R(clear));
  FDRE \slv_regs_reg[0][15] 
       (.C(clk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[15]),
        .Q(Q[15]),
        .R(clear));
  FDRE \slv_regs_reg[0][16] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg_n_0_[0][16] ),
        .R(clear));
  FDRE \slv_regs_reg[0][17] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg_n_0_[0][17] ),
        .R(clear));
  FDRE \slv_regs_reg[0][18] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg_n_0_[0][18] ),
        .R(clear));
  FDRE \slv_regs_reg[0][19] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg_n_0_[0][19] ),
        .R(clear));
  FDRE \slv_regs_reg[0][1] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[1]),
        .Q(Q[1]),
        .R(clear));
  FDRE \slv_regs_reg[0][20] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg_n_0_[0][20] ),
        .R(clear));
  FDRE \slv_regs_reg[0][21] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg_n_0_[0][21] ),
        .R(clear));
  FDRE \slv_regs_reg[0][22] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg_n_0_[0][22] ),
        .R(clear));
  FDRE \slv_regs_reg[0][23] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg_n_0_[0][23] ),
        .R(clear));
  FDRE \slv_regs_reg[0][24] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg_n_0_[0][24] ),
        .R(clear));
  FDRE \slv_regs_reg[0][25] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg_n_0_[0][25] ),
        .R(clear));
  FDRE \slv_regs_reg[0][26] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg_n_0_[0][26] ),
        .R(clear));
  FDRE \slv_regs_reg[0][27] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg_n_0_[0][27] ),
        .R(clear));
  FDRE \slv_regs_reg[0][28] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg_n_0_[0][28] ),
        .R(clear));
  FDRE \slv_regs_reg[0][29] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg_n_0_[0][29] ),
        .R(clear));
  FDRE \slv_regs_reg[0][2] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[2]),
        .Q(Q[2]),
        .R(clear));
  FDRE \slv_regs_reg[0][30] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg_n_0_[0][30] ),
        .R(clear));
  FDRE \slv_regs_reg[0][31] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg_n_0_[0][31] ),
        .R(clear));
  FDRE \slv_regs_reg[0][3] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[3]),
        .Q(Q[3]),
        .R(clear));
  FDRE \slv_regs_reg[0][4] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[4]),
        .Q(Q[4]),
        .R(clear));
  FDRE \slv_regs_reg[0][5] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[5]),
        .Q(Q[5]),
        .R(clear));
  FDRE \slv_regs_reg[0][6] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[6]),
        .Q(Q[6]),
        .R(clear));
  FDRE \slv_regs_reg[0][7] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[7]),
        .Q(Q[7]),
        .R(clear));
  FDRE \slv_regs_reg[0][8] 
       (.C(clk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[8]),
        .Q(Q[8]),
        .R(clear));
  FDRE \slv_regs_reg[0][9] 
       (.C(clk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[9]),
        .Q(Q[9]),
        .R(clear));
  FDRE \slv_regs_reg[10][0] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[10] [0]),
        .R(clear));
  FDRE \slv_regs_reg[10][10] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[10] [10]),
        .R(clear));
  FDRE \slv_regs_reg[10][11] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[10] [11]),
        .R(clear));
  FDRE \slv_regs_reg[10][12] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[10] [12]),
        .R(clear));
  FDRE \slv_regs_reg[10][13] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[10] [13]),
        .R(clear));
  FDRE \slv_regs_reg[10][14] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[10] [14]),
        .R(clear));
  FDRE \slv_regs_reg[10][15] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[10] [15]),
        .R(clear));
  FDRE \slv_regs_reg[10][16] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[10] [16]),
        .R(clear));
  FDRE \slv_regs_reg[10][17] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[10] [17]),
        .R(clear));
  FDRE \slv_regs_reg[10][18] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[10] [18]),
        .R(clear));
  FDRE \slv_regs_reg[10][19] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[10] [19]),
        .R(clear));
  FDRE \slv_regs_reg[10][1] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[10] [1]),
        .R(clear));
  FDRE \slv_regs_reg[10][20] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[10] [20]),
        .R(clear));
  FDRE \slv_regs_reg[10][21] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[10] [21]),
        .R(clear));
  FDRE \slv_regs_reg[10][22] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[10] [22]),
        .R(clear));
  FDRE \slv_regs_reg[10][23] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[10] [23]),
        .R(clear));
  FDRE \slv_regs_reg[10][24] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[10] [24]),
        .R(clear));
  FDRE \slv_regs_reg[10][25] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[10] [25]),
        .R(clear));
  FDRE \slv_regs_reg[10][26] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[10] [26]),
        .R(clear));
  FDRE \slv_regs_reg[10][27] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[10] [27]),
        .R(clear));
  FDRE \slv_regs_reg[10][28] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[10] [28]),
        .R(clear));
  FDRE \slv_regs_reg[10][29] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[10] [29]),
        .R(clear));
  FDRE \slv_regs_reg[10][2] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[10] [2]),
        .R(clear));
  FDRE \slv_regs_reg[10][30] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[10] [30]),
        .R(clear));
  FDRE \slv_regs_reg[10][31] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[10] [31]),
        .R(clear));
  FDRE \slv_regs_reg[10][3] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[10] [3]),
        .R(clear));
  FDRE \slv_regs_reg[10][4] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[10] [4]),
        .R(clear));
  FDRE \slv_regs_reg[10][5] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[10] [5]),
        .R(clear));
  FDRE \slv_regs_reg[10][6] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[10] [6]),
        .R(clear));
  FDRE \slv_regs_reg[10][7] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[10] [7]),
        .R(clear));
  FDRE \slv_regs_reg[10][8] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[10] [8]),
        .R(clear));
  FDRE \slv_regs_reg[10][9] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[10] [9]),
        .R(clear));
  FDRE \slv_regs_reg[11][0] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[11] [0]),
        .R(clear));
  FDRE \slv_regs_reg[11][10] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[11] [10]),
        .R(clear));
  FDRE \slv_regs_reg[11][11] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[11] [11]),
        .R(clear));
  FDRE \slv_regs_reg[11][12] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[11] [12]),
        .R(clear));
  FDRE \slv_regs_reg[11][13] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[11] [13]),
        .R(clear));
  FDRE \slv_regs_reg[11][14] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[11] [14]),
        .R(clear));
  FDRE \slv_regs_reg[11][15] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[11] [15]),
        .R(clear));
  FDRE \slv_regs_reg[11][16] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[11] [16]),
        .R(clear));
  FDRE \slv_regs_reg[11][17] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[11] [17]),
        .R(clear));
  FDRE \slv_regs_reg[11][18] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[11] [18]),
        .R(clear));
  FDRE \slv_regs_reg[11][19] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[11] [19]),
        .R(clear));
  FDRE \slv_regs_reg[11][1] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[11] [1]),
        .R(clear));
  FDRE \slv_regs_reg[11][20] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[11] [20]),
        .R(clear));
  FDRE \slv_regs_reg[11][21] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[11] [21]),
        .R(clear));
  FDRE \slv_regs_reg[11][22] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[11] [22]),
        .R(clear));
  FDRE \slv_regs_reg[11][23] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[11] [23]),
        .R(clear));
  FDRE \slv_regs_reg[11][24] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[11] [24]),
        .R(clear));
  FDRE \slv_regs_reg[11][25] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[11] [25]),
        .R(clear));
  FDRE \slv_regs_reg[11][26] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[11] [26]),
        .R(clear));
  FDRE \slv_regs_reg[11][27] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[11] [27]),
        .R(clear));
  FDRE \slv_regs_reg[11][28] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[11] [28]),
        .R(clear));
  FDRE \slv_regs_reg[11][29] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[11] [29]),
        .R(clear));
  FDRE \slv_regs_reg[11][2] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[11] [2]),
        .R(clear));
  FDRE \slv_regs_reg[11][30] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[11] [30]),
        .R(clear));
  FDRE \slv_regs_reg[11][31] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[11] [31]),
        .R(clear));
  FDRE \slv_regs_reg[11][3] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[11] [3]),
        .R(clear));
  FDRE \slv_regs_reg[11][4] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[11] [4]),
        .R(clear));
  FDRE \slv_regs_reg[11][5] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[11] [5]),
        .R(clear));
  FDRE \slv_regs_reg[11][6] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[11] [6]),
        .R(clear));
  FDRE \slv_regs_reg[11][7] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[11] [7]),
        .R(clear));
  FDRE \slv_regs_reg[11][8] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[11] [8]),
        .R(clear));
  FDRE \slv_regs_reg[11][9] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[11] [9]),
        .R(clear));
  FDRE \slv_regs_reg[12][0] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[12] [0]),
        .R(clear));
  FDRE \slv_regs_reg[12][10] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[12] [10]),
        .R(clear));
  FDRE \slv_regs_reg[12][11] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[12] [11]),
        .R(clear));
  FDRE \slv_regs_reg[12][12] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[12] [12]),
        .R(clear));
  FDRE \slv_regs_reg[12][13] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[12] [13]),
        .R(clear));
  FDRE \slv_regs_reg[12][14] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[12] [14]),
        .R(clear));
  FDRE \slv_regs_reg[12][15] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[12] [15]),
        .R(clear));
  FDRE \slv_regs_reg[12][16] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[12] [16]),
        .R(clear));
  FDRE \slv_regs_reg[12][17] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[12] [17]),
        .R(clear));
  FDRE \slv_regs_reg[12][18] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[12] [18]),
        .R(clear));
  FDRE \slv_regs_reg[12][19] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[12] [19]),
        .R(clear));
  FDRE \slv_regs_reg[12][1] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[12] [1]),
        .R(clear));
  FDRE \slv_regs_reg[12][20] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[12] [20]),
        .R(clear));
  FDRE \slv_regs_reg[12][21] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[12] [21]),
        .R(clear));
  FDRE \slv_regs_reg[12][22] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[12] [22]),
        .R(clear));
  FDRE \slv_regs_reg[12][23] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[12] [23]),
        .R(clear));
  FDRE \slv_regs_reg[12][24] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[12] [24]),
        .R(clear));
  FDRE \slv_regs_reg[12][25] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[12] [25]),
        .R(clear));
  FDRE \slv_regs_reg[12][26] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[12] [26]),
        .R(clear));
  FDRE \slv_regs_reg[12][27] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[12] [27]),
        .R(clear));
  FDRE \slv_regs_reg[12][28] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[12] [28]),
        .R(clear));
  FDRE \slv_regs_reg[12][29] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[12] [29]),
        .R(clear));
  FDRE \slv_regs_reg[12][2] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[12] [2]),
        .R(clear));
  FDRE \slv_regs_reg[12][30] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[12] [30]),
        .R(clear));
  FDRE \slv_regs_reg[12][31] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[12] [31]),
        .R(clear));
  FDRE \slv_regs_reg[12][3] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[12] [3]),
        .R(clear));
  FDRE \slv_regs_reg[12][4] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[12] [4]),
        .R(clear));
  FDRE \slv_regs_reg[12][5] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[12] [5]),
        .R(clear));
  FDRE \slv_regs_reg[12][6] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[12] [6]),
        .R(clear));
  FDRE \slv_regs_reg[12][7] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[12] [7]),
        .R(clear));
  FDRE \slv_regs_reg[12][8] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[12] [8]),
        .R(clear));
  FDRE \slv_regs_reg[12][9] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[12] [9]),
        .R(clear));
  FDRE \slv_regs_reg[13][0] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[13] [0]),
        .R(clear));
  FDRE \slv_regs_reg[13][10] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[13] [10]),
        .R(clear));
  FDRE \slv_regs_reg[13][11] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[13] [11]),
        .R(clear));
  FDRE \slv_regs_reg[13][12] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[13] [12]),
        .R(clear));
  FDRE \slv_regs_reg[13][13] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[13] [13]),
        .R(clear));
  FDRE \slv_regs_reg[13][14] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[13] [14]),
        .R(clear));
  FDRE \slv_regs_reg[13][15] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[13] [15]),
        .R(clear));
  FDRE \slv_regs_reg[13][16] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[13] [16]),
        .R(clear));
  FDRE \slv_regs_reg[13][17] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[13] [17]),
        .R(clear));
  FDRE \slv_regs_reg[13][18] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[13] [18]),
        .R(clear));
  FDRE \slv_regs_reg[13][19] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[13] [19]),
        .R(clear));
  FDRE \slv_regs_reg[13][1] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[13] [1]),
        .R(clear));
  FDRE \slv_regs_reg[13][20] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[13] [20]),
        .R(clear));
  FDRE \slv_regs_reg[13][21] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[13] [21]),
        .R(clear));
  FDRE \slv_regs_reg[13][22] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[13] [22]),
        .R(clear));
  FDRE \slv_regs_reg[13][23] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[13] [23]),
        .R(clear));
  FDRE \slv_regs_reg[13][24] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[13] [24]),
        .R(clear));
  FDRE \slv_regs_reg[13][25] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[13] [25]),
        .R(clear));
  FDRE \slv_regs_reg[13][26] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[13] [26]),
        .R(clear));
  FDRE \slv_regs_reg[13][27] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[13] [27]),
        .R(clear));
  FDRE \slv_regs_reg[13][28] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[13] [28]),
        .R(clear));
  FDRE \slv_regs_reg[13][29] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[13] [29]),
        .R(clear));
  FDRE \slv_regs_reg[13][2] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[13] [2]),
        .R(clear));
  FDRE \slv_regs_reg[13][30] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[13] [30]),
        .R(clear));
  FDRE \slv_regs_reg[13][31] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[13] [31]),
        .R(clear));
  FDRE \slv_regs_reg[13][3] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[13] [3]),
        .R(clear));
  FDRE \slv_regs_reg[13][4] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[13] [4]),
        .R(clear));
  FDRE \slv_regs_reg[13][5] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[13] [5]),
        .R(clear));
  FDRE \slv_regs_reg[13][6] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[13] [6]),
        .R(clear));
  FDRE \slv_regs_reg[13][7] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[13] [7]),
        .R(clear));
  FDRE \slv_regs_reg[13][8] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[13] [8]),
        .R(clear));
  FDRE \slv_regs_reg[13][9] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[13] [9]),
        .R(clear));
  FDRE \slv_regs_reg[14][0] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[14] [0]),
        .R(clear));
  FDRE \slv_regs_reg[14][10] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[14] [10]),
        .R(clear));
  FDRE \slv_regs_reg[14][11] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[14] [11]),
        .R(clear));
  FDRE \slv_regs_reg[14][12] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[14] [12]),
        .R(clear));
  FDRE \slv_regs_reg[14][13] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[14] [13]),
        .R(clear));
  FDRE \slv_regs_reg[14][14] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[14] [14]),
        .R(clear));
  FDRE \slv_regs_reg[14][15] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[14] [15]),
        .R(clear));
  FDRE \slv_regs_reg[14][16] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[14] [16]),
        .R(clear));
  FDRE \slv_regs_reg[14][17] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[14] [17]),
        .R(clear));
  FDRE \slv_regs_reg[14][18] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[14] [18]),
        .R(clear));
  FDRE \slv_regs_reg[14][19] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[14] [19]),
        .R(clear));
  FDRE \slv_regs_reg[14][1] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[14] [1]),
        .R(clear));
  FDRE \slv_regs_reg[14][20] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[14] [20]),
        .R(clear));
  FDRE \slv_regs_reg[14][21] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[14] [21]),
        .R(clear));
  FDRE \slv_regs_reg[14][22] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[14] [22]),
        .R(clear));
  FDRE \slv_regs_reg[14][23] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[14] [23]),
        .R(clear));
  FDRE \slv_regs_reg[14][24] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[14] [24]),
        .R(clear));
  FDRE \slv_regs_reg[14][25] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[14] [25]),
        .R(clear));
  FDRE \slv_regs_reg[14][26] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[14] [26]),
        .R(clear));
  FDRE \slv_regs_reg[14][27] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[14] [27]),
        .R(clear));
  FDRE \slv_regs_reg[14][28] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[14] [28]),
        .R(clear));
  FDRE \slv_regs_reg[14][29] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[14] [29]),
        .R(clear));
  FDRE \slv_regs_reg[14][2] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[14] [2]),
        .R(clear));
  FDRE \slv_regs_reg[14][30] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[14] [30]),
        .R(clear));
  FDRE \slv_regs_reg[14][31] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[14] [31]),
        .R(clear));
  FDRE \slv_regs_reg[14][3] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[14] [3]),
        .R(clear));
  FDRE \slv_regs_reg[14][4] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[14] [4]),
        .R(clear));
  FDRE \slv_regs_reg[14][5] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[14] [5]),
        .R(clear));
  FDRE \slv_regs_reg[14][6] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[14] [6]),
        .R(clear));
  FDRE \slv_regs_reg[14][7] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[14] [7]),
        .R(clear));
  FDRE \slv_regs_reg[14][8] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[14] [8]),
        .R(clear));
  FDRE \slv_regs_reg[14][9] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[14] [9]),
        .R(clear));
  FDRE \slv_regs_reg[15][0] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[15] [0]),
        .R(clear));
  FDRE \slv_regs_reg[15][10] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[15] [10]),
        .R(clear));
  FDRE \slv_regs_reg[15][11] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[15] [11]),
        .R(clear));
  FDRE \slv_regs_reg[15][12] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[15] [12]),
        .R(clear));
  FDRE \slv_regs_reg[15][13] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[15] [13]),
        .R(clear));
  FDRE \slv_regs_reg[15][14] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[15] [14]),
        .R(clear));
  FDRE \slv_regs_reg[15][15] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[15] [15]),
        .R(clear));
  FDRE \slv_regs_reg[15][16] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[15] [16]),
        .R(clear));
  FDRE \slv_regs_reg[15][17] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[15] [17]),
        .R(clear));
  FDRE \slv_regs_reg[15][18] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[15] [18]),
        .R(clear));
  FDRE \slv_regs_reg[15][19] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[15] [19]),
        .R(clear));
  FDRE \slv_regs_reg[15][1] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[15] [1]),
        .R(clear));
  FDRE \slv_regs_reg[15][20] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[15] [20]),
        .R(clear));
  FDRE \slv_regs_reg[15][21] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[15] [21]),
        .R(clear));
  FDRE \slv_regs_reg[15][22] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[15] [22]),
        .R(clear));
  FDRE \slv_regs_reg[15][23] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[15] [23]),
        .R(clear));
  FDRE \slv_regs_reg[15][24] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[15] [24]),
        .R(clear));
  FDRE \slv_regs_reg[15][25] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[15] [25]),
        .R(clear));
  FDRE \slv_regs_reg[15][26] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[15] [26]),
        .R(clear));
  FDRE \slv_regs_reg[15][27] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[15] [27]),
        .R(clear));
  FDRE \slv_regs_reg[15][28] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[15] [28]),
        .R(clear));
  FDRE \slv_regs_reg[15][29] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[15] [29]),
        .R(clear));
  FDRE \slv_regs_reg[15][2] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[15] [2]),
        .R(clear));
  FDRE \slv_regs_reg[15][30] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[15] [30]),
        .R(clear));
  FDRE \slv_regs_reg[15][31] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[15] [31]),
        .R(clear));
  FDRE \slv_regs_reg[15][3] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[15] [3]),
        .R(clear));
  FDRE \slv_regs_reg[15][4] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[15] [4]),
        .R(clear));
  FDRE \slv_regs_reg[15][5] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[15] [5]),
        .R(clear));
  FDRE \slv_regs_reg[15][6] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[15] [6]),
        .R(clear));
  FDRE \slv_regs_reg[15][7] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[15] [7]),
        .R(clear));
  FDRE \slv_regs_reg[15][8] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[15] [8]),
        .R(clear));
  FDRE \slv_regs_reg[15][9] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[15] [9]),
        .R(clear));
  FDRE \slv_regs_reg[2][0] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[2] [0]),
        .R(clear));
  FDRE \slv_regs_reg[2][10] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[2] [10]),
        .R(clear));
  FDRE \slv_regs_reg[2][11] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[2] [11]),
        .R(clear));
  FDRE \slv_regs_reg[2][12] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[2] [12]),
        .R(clear));
  FDRE \slv_regs_reg[2][13] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[2] [13]),
        .R(clear));
  FDRE \slv_regs_reg[2][14] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[2] [14]),
        .R(clear));
  FDRE \slv_regs_reg[2][15] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[2] [15]),
        .R(clear));
  FDRE \slv_regs_reg[2][16] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[2] [16]),
        .R(clear));
  FDRE \slv_regs_reg[2][17] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[2] [17]),
        .R(clear));
  FDRE \slv_regs_reg[2][18] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[2] [18]),
        .R(clear));
  FDRE \slv_regs_reg[2][19] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[2] [19]),
        .R(clear));
  FDRE \slv_regs_reg[2][1] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[2] [1]),
        .R(clear));
  FDRE \slv_regs_reg[2][20] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[2] [20]),
        .R(clear));
  FDRE \slv_regs_reg[2][21] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[2] [21]),
        .R(clear));
  FDRE \slv_regs_reg[2][22] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[2] [22]),
        .R(clear));
  FDRE \slv_regs_reg[2][23] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[2] [23]),
        .R(clear));
  FDRE \slv_regs_reg[2][24] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[2] [24]),
        .R(clear));
  FDRE \slv_regs_reg[2][25] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[2] [25]),
        .R(clear));
  FDRE \slv_regs_reg[2][26] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[2] [26]),
        .R(clear));
  FDRE \slv_regs_reg[2][27] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[2] [27]),
        .R(clear));
  FDRE \slv_regs_reg[2][28] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[2] [28]),
        .R(clear));
  FDRE \slv_regs_reg[2][29] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[2] [29]),
        .R(clear));
  FDRE \slv_regs_reg[2][2] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[2] [2]),
        .R(clear));
  FDRE \slv_regs_reg[2][30] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[2] [30]),
        .R(clear));
  FDRE \slv_regs_reg[2][31] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[2] [31]),
        .R(clear));
  FDRE \slv_regs_reg[2][3] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[2] [3]),
        .R(clear));
  FDRE \slv_regs_reg[2][4] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[2] [4]),
        .R(clear));
  FDRE \slv_regs_reg[2][5] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[2] [5]),
        .R(clear));
  FDRE \slv_regs_reg[2][6] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[2] [6]),
        .R(clear));
  FDRE \slv_regs_reg[2][7] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[2] [7]),
        .R(clear));
  FDRE \slv_regs_reg[2][8] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[2] [8]),
        .R(clear));
  FDRE \slv_regs_reg[2][9] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[2] [9]),
        .R(clear));
  FDRE \slv_regs_reg[3][0] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[3] [0]),
        .R(clear));
  FDRE \slv_regs_reg[3][10] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[3] [10]),
        .R(clear));
  FDRE \slv_regs_reg[3][11] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[3] [11]),
        .R(clear));
  FDRE \slv_regs_reg[3][12] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[3] [12]),
        .R(clear));
  FDRE \slv_regs_reg[3][13] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[3] [13]),
        .R(clear));
  FDRE \slv_regs_reg[3][14] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[3] [14]),
        .R(clear));
  FDRE \slv_regs_reg[3][15] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[3] [15]),
        .R(clear));
  FDRE \slv_regs_reg[3][16] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[3] [16]),
        .R(clear));
  FDRE \slv_regs_reg[3][17] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[3] [17]),
        .R(clear));
  FDRE \slv_regs_reg[3][18] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[3] [18]),
        .R(clear));
  FDRE \slv_regs_reg[3][19] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[3] [19]),
        .R(clear));
  FDRE \slv_regs_reg[3][1] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[3] [1]),
        .R(clear));
  FDRE \slv_regs_reg[3][20] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[3] [20]),
        .R(clear));
  FDRE \slv_regs_reg[3][21] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[3] [21]),
        .R(clear));
  FDRE \slv_regs_reg[3][22] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[3] [22]),
        .R(clear));
  FDRE \slv_regs_reg[3][23] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[3] [23]),
        .R(clear));
  FDRE \slv_regs_reg[3][24] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[3] [24]),
        .R(clear));
  FDRE \slv_regs_reg[3][25] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[3] [25]),
        .R(clear));
  FDRE \slv_regs_reg[3][26] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[3] [26]),
        .R(clear));
  FDRE \slv_regs_reg[3][27] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[3] [27]),
        .R(clear));
  FDRE \slv_regs_reg[3][28] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[3] [28]),
        .R(clear));
  FDRE \slv_regs_reg[3][29] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[3] [29]),
        .R(clear));
  FDRE \slv_regs_reg[3][2] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[3] [2]),
        .R(clear));
  FDRE \slv_regs_reg[3][30] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[3] [30]),
        .R(clear));
  FDRE \slv_regs_reg[3][31] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[3] [31]),
        .R(clear));
  FDRE \slv_regs_reg[3][3] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[3] [3]),
        .R(clear));
  FDRE \slv_regs_reg[3][4] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[3] [4]),
        .R(clear));
  FDRE \slv_regs_reg[3][5] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[3] [5]),
        .R(clear));
  FDRE \slv_regs_reg[3][6] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[3] [6]),
        .R(clear));
  FDRE \slv_regs_reg[3][7] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[3] [7]),
        .R(clear));
  FDRE \slv_regs_reg[3][8] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[3] [8]),
        .R(clear));
  FDRE \slv_regs_reg[3][9] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[3] [9]),
        .R(clear));
  FDRE \slv_regs_reg[4][0] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[4] [0]),
        .R(clear));
  FDRE \slv_regs_reg[4][10] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[4] [10]),
        .R(clear));
  FDRE \slv_regs_reg[4][11] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[4] [11]),
        .R(clear));
  FDRE \slv_regs_reg[4][12] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[4] [12]),
        .R(clear));
  FDRE \slv_regs_reg[4][13] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[4] [13]),
        .R(clear));
  FDRE \slv_regs_reg[4][14] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[4] [14]),
        .R(clear));
  FDRE \slv_regs_reg[4][15] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[4] [15]),
        .R(clear));
  FDRE \slv_regs_reg[4][16] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[4] [16]),
        .R(clear));
  FDRE \slv_regs_reg[4][17] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[4] [17]),
        .R(clear));
  FDRE \slv_regs_reg[4][18] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[4] [18]),
        .R(clear));
  FDRE \slv_regs_reg[4][19] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[4] [19]),
        .R(clear));
  FDRE \slv_regs_reg[4][1] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[4] [1]),
        .R(clear));
  FDRE \slv_regs_reg[4][20] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[4] [20]),
        .R(clear));
  FDRE \slv_regs_reg[4][21] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[4] [21]),
        .R(clear));
  FDRE \slv_regs_reg[4][22] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[4] [22]),
        .R(clear));
  FDRE \slv_regs_reg[4][23] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[4] [23]),
        .R(clear));
  FDRE \slv_regs_reg[4][24] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[4] [24]),
        .R(clear));
  FDRE \slv_regs_reg[4][25] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[4] [25]),
        .R(clear));
  FDRE \slv_regs_reg[4][26] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[4] [26]),
        .R(clear));
  FDRE \slv_regs_reg[4][27] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[4] [27]),
        .R(clear));
  FDRE \slv_regs_reg[4][28] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[4] [28]),
        .R(clear));
  FDRE \slv_regs_reg[4][29] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[4] [29]),
        .R(clear));
  FDRE \slv_regs_reg[4][2] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[4] [2]),
        .R(clear));
  FDRE \slv_regs_reg[4][30] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[4] [30]),
        .R(clear));
  FDRE \slv_regs_reg[4][31] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[4] [31]),
        .R(clear));
  FDRE \slv_regs_reg[4][3] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[4] [3]),
        .R(clear));
  FDRE \slv_regs_reg[4][4] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[4] [4]),
        .R(clear));
  FDRE \slv_regs_reg[4][5] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[4] [5]),
        .R(clear));
  FDRE \slv_regs_reg[4][6] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[4] [6]),
        .R(clear));
  FDRE \slv_regs_reg[4][7] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[4] [7]),
        .R(clear));
  FDRE \slv_regs_reg[4][8] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[4] [8]),
        .R(clear));
  FDRE \slv_regs_reg[4][9] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[4] [9]),
        .R(clear));
  FDRE \slv_regs_reg[5][0] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[5] [0]),
        .R(clear));
  FDRE \slv_regs_reg[5][10] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[5] [10]),
        .R(clear));
  FDRE \slv_regs_reg[5][11] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[5] [11]),
        .R(clear));
  FDRE \slv_regs_reg[5][12] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[5] [12]),
        .R(clear));
  FDRE \slv_regs_reg[5][13] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[5] [13]),
        .R(clear));
  FDRE \slv_regs_reg[5][14] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[5] [14]),
        .R(clear));
  FDRE \slv_regs_reg[5][15] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[5] [15]),
        .R(clear));
  FDRE \slv_regs_reg[5][16] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[5] [16]),
        .R(clear));
  FDRE \slv_regs_reg[5][17] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[5] [17]),
        .R(clear));
  FDRE \slv_regs_reg[5][18] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[5] [18]),
        .R(clear));
  FDRE \slv_regs_reg[5][19] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[5] [19]),
        .R(clear));
  FDRE \slv_regs_reg[5][1] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[5] [1]),
        .R(clear));
  FDRE \slv_regs_reg[5][20] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[5] [20]),
        .R(clear));
  FDRE \slv_regs_reg[5][21] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[5] [21]),
        .R(clear));
  FDRE \slv_regs_reg[5][22] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[5] [22]),
        .R(clear));
  FDRE \slv_regs_reg[5][23] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[5] [23]),
        .R(clear));
  FDRE \slv_regs_reg[5][24] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[5] [24]),
        .R(clear));
  FDRE \slv_regs_reg[5][25] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[5] [25]),
        .R(clear));
  FDRE \slv_regs_reg[5][26] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[5] [26]),
        .R(clear));
  FDRE \slv_regs_reg[5][27] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[5] [27]),
        .R(clear));
  FDRE \slv_regs_reg[5][28] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[5] [28]),
        .R(clear));
  FDRE \slv_regs_reg[5][29] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[5] [29]),
        .R(clear));
  FDRE \slv_regs_reg[5][2] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[5] [2]),
        .R(clear));
  FDRE \slv_regs_reg[5][30] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[5] [30]),
        .R(clear));
  FDRE \slv_regs_reg[5][31] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[5] [31]),
        .R(clear));
  FDRE \slv_regs_reg[5][3] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[5] [3]),
        .R(clear));
  FDRE \slv_regs_reg[5][4] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[5] [4]),
        .R(clear));
  FDRE \slv_regs_reg[5][5] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[5] [5]),
        .R(clear));
  FDRE \slv_regs_reg[5][6] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[5] [6]),
        .R(clear));
  FDRE \slv_regs_reg[5][7] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[5] [7]),
        .R(clear));
  FDRE \slv_regs_reg[5][8] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[5] [8]),
        .R(clear));
  FDRE \slv_regs_reg[5][9] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[5] [9]),
        .R(clear));
  FDRE \slv_regs_reg[6][0] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[6] [0]),
        .R(clear));
  FDRE \slv_regs_reg[6][10] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[6] [10]),
        .R(clear));
  FDRE \slv_regs_reg[6][11] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[6] [11]),
        .R(clear));
  FDRE \slv_regs_reg[6][12] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[6] [12]),
        .R(clear));
  FDRE \slv_regs_reg[6][13] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[6] [13]),
        .R(clear));
  FDRE \slv_regs_reg[6][14] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[6] [14]),
        .R(clear));
  FDRE \slv_regs_reg[6][15] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[6] [15]),
        .R(clear));
  FDRE \slv_regs_reg[6][16] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[6] [16]),
        .R(clear));
  FDRE \slv_regs_reg[6][17] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[6] [17]),
        .R(clear));
  FDRE \slv_regs_reg[6][18] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[6] [18]),
        .R(clear));
  FDRE \slv_regs_reg[6][19] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[6] [19]),
        .R(clear));
  FDRE \slv_regs_reg[6][1] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[6] [1]),
        .R(clear));
  FDRE \slv_regs_reg[6][20] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[6] [20]),
        .R(clear));
  FDRE \slv_regs_reg[6][21] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[6] [21]),
        .R(clear));
  FDRE \slv_regs_reg[6][22] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[6] [22]),
        .R(clear));
  FDRE \slv_regs_reg[6][23] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[6] [23]),
        .R(clear));
  FDRE \slv_regs_reg[6][24] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[6] [24]),
        .R(clear));
  FDRE \slv_regs_reg[6][25] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[6] [25]),
        .R(clear));
  FDRE \slv_regs_reg[6][26] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[6] [26]),
        .R(clear));
  FDRE \slv_regs_reg[6][27] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[6] [27]),
        .R(clear));
  FDRE \slv_regs_reg[6][28] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[6] [28]),
        .R(clear));
  FDRE \slv_regs_reg[6][29] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[6] [29]),
        .R(clear));
  FDRE \slv_regs_reg[6][2] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[6] [2]),
        .R(clear));
  FDRE \slv_regs_reg[6][30] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[6] [30]),
        .R(clear));
  FDRE \slv_regs_reg[6][31] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[6] [31]),
        .R(clear));
  FDRE \slv_regs_reg[6][3] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[6] [3]),
        .R(clear));
  FDRE \slv_regs_reg[6][4] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[6] [4]),
        .R(clear));
  FDRE \slv_regs_reg[6][5] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[6] [5]),
        .R(clear));
  FDRE \slv_regs_reg[6][6] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[6] [6]),
        .R(clear));
  FDRE \slv_regs_reg[6][7] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[6] [7]),
        .R(clear));
  FDRE \slv_regs_reg[6][8] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[6] [8]),
        .R(clear));
  FDRE \slv_regs_reg[6][9] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[6] [9]),
        .R(clear));
  FDRE \slv_regs_reg[7][0] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[7] [0]),
        .R(clear));
  FDRE \slv_regs_reg[7][10] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[7] [10]),
        .R(clear));
  FDRE \slv_regs_reg[7][11] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[7] [11]),
        .R(clear));
  FDRE \slv_regs_reg[7][12] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[7] [12]),
        .R(clear));
  FDRE \slv_regs_reg[7][13] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[7] [13]),
        .R(clear));
  FDRE \slv_regs_reg[7][14] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[7] [14]),
        .R(clear));
  FDRE \slv_regs_reg[7][15] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[7] [15]),
        .R(clear));
  FDRE \slv_regs_reg[7][16] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[7] [16]),
        .R(clear));
  FDRE \slv_regs_reg[7][17] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[7] [17]),
        .R(clear));
  FDRE \slv_regs_reg[7][18] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[7] [18]),
        .R(clear));
  FDRE \slv_regs_reg[7][19] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[7] [19]),
        .R(clear));
  FDRE \slv_regs_reg[7][1] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[7] [1]),
        .R(clear));
  FDRE \slv_regs_reg[7][20] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[7] [20]),
        .R(clear));
  FDRE \slv_regs_reg[7][21] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[7] [21]),
        .R(clear));
  FDRE \slv_regs_reg[7][22] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[7] [22]),
        .R(clear));
  FDRE \slv_regs_reg[7][23] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[7] [23]),
        .R(clear));
  FDRE \slv_regs_reg[7][24] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[7] [24]),
        .R(clear));
  FDRE \slv_regs_reg[7][25] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[7] [25]),
        .R(clear));
  FDRE \slv_regs_reg[7][26] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[7] [26]),
        .R(clear));
  FDRE \slv_regs_reg[7][27] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[7] [27]),
        .R(clear));
  FDRE \slv_regs_reg[7][28] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[7] [28]),
        .R(clear));
  FDRE \slv_regs_reg[7][29] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[7] [29]),
        .R(clear));
  FDRE \slv_regs_reg[7][2] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[7] [2]),
        .R(clear));
  FDRE \slv_regs_reg[7][30] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[7] [30]),
        .R(clear));
  FDRE \slv_regs_reg[7][31] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[7] [31]),
        .R(clear));
  FDRE \slv_regs_reg[7][3] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[7] [3]),
        .R(clear));
  FDRE \slv_regs_reg[7][4] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[7] [4]),
        .R(clear));
  FDRE \slv_regs_reg[7][5] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[7] [5]),
        .R(clear));
  FDRE \slv_regs_reg[7][6] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[7] [6]),
        .R(clear));
  FDRE \slv_regs_reg[7][7] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[7] [7]),
        .R(clear));
  FDRE \slv_regs_reg[7][8] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[7] [8]),
        .R(clear));
  FDRE \slv_regs_reg[7][9] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[7] [9]),
        .R(clear));
  FDRE \slv_regs_reg[8][0] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[8] [0]),
        .R(clear));
  FDRE \slv_regs_reg[8][10] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[8] [10]),
        .R(clear));
  FDRE \slv_regs_reg[8][11] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[8] [11]),
        .R(clear));
  FDRE \slv_regs_reg[8][12] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[8] [12]),
        .R(clear));
  FDRE \slv_regs_reg[8][13] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[8] [13]),
        .R(clear));
  FDRE \slv_regs_reg[8][14] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[8] [14]),
        .R(clear));
  FDRE \slv_regs_reg[8][15] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[8] [15]),
        .R(clear));
  FDRE \slv_regs_reg[8][16] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[8] [16]),
        .R(clear));
  FDRE \slv_regs_reg[8][17] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[8] [17]),
        .R(clear));
  FDRE \slv_regs_reg[8][18] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[8] [18]),
        .R(clear));
  FDRE \slv_regs_reg[8][19] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[8] [19]),
        .R(clear));
  FDRE \slv_regs_reg[8][1] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[8] [1]),
        .R(clear));
  FDRE \slv_regs_reg[8][20] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[8] [20]),
        .R(clear));
  FDRE \slv_regs_reg[8][21] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[8] [21]),
        .R(clear));
  FDRE \slv_regs_reg[8][22] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[8] [22]),
        .R(clear));
  FDRE \slv_regs_reg[8][23] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[8] [23]),
        .R(clear));
  FDRE \slv_regs_reg[8][24] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[8] [24]),
        .R(clear));
  FDRE \slv_regs_reg[8][25] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[8] [25]),
        .R(clear));
  FDRE \slv_regs_reg[8][26] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[8] [26]),
        .R(clear));
  FDRE \slv_regs_reg[8][27] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[8] [27]),
        .R(clear));
  FDRE \slv_regs_reg[8][28] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[8] [28]),
        .R(clear));
  FDRE \slv_regs_reg[8][29] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[8] [29]),
        .R(clear));
  FDRE \slv_regs_reg[8][2] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[8] [2]),
        .R(clear));
  FDRE \slv_regs_reg[8][30] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[8] [30]),
        .R(clear));
  FDRE \slv_regs_reg[8][31] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[8] [31]),
        .R(clear));
  FDRE \slv_regs_reg[8][3] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[8] [3]),
        .R(clear));
  FDRE \slv_regs_reg[8][4] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[8] [4]),
        .R(clear));
  FDRE \slv_regs_reg[8][5] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[8] [5]),
        .R(clear));
  FDRE \slv_regs_reg[8][6] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[8] [6]),
        .R(clear));
  FDRE \slv_regs_reg[8][7] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[8] [7]),
        .R(clear));
  FDRE \slv_regs_reg[8][8] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[8] [8]),
        .R(clear));
  FDRE \slv_regs_reg[8][9] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[8] [9]),
        .R(clear));
  FDRE \slv_regs_reg[9][0] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[9] [0]),
        .R(clear));
  FDRE \slv_regs_reg[9][10] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[9] [10]),
        .R(clear));
  FDRE \slv_regs_reg[9][11] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[9] [11]),
        .R(clear));
  FDRE \slv_regs_reg[9][12] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[9] [12]),
        .R(clear));
  FDRE \slv_regs_reg[9][13] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[9] [13]),
        .R(clear));
  FDRE \slv_regs_reg[9][14] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[9] [14]),
        .R(clear));
  FDRE \slv_regs_reg[9][15] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[9] [15]),
        .R(clear));
  FDRE \slv_regs_reg[9][16] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[9] [16]),
        .R(clear));
  FDRE \slv_regs_reg[9][17] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[9] [17]),
        .R(clear));
  FDRE \slv_regs_reg[9][18] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[9] [18]),
        .R(clear));
  FDRE \slv_regs_reg[9][19] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[9] [19]),
        .R(clear));
  FDRE \slv_regs_reg[9][1] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[9] [1]),
        .R(clear));
  FDRE \slv_regs_reg[9][20] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[9] [20]),
        .R(clear));
  FDRE \slv_regs_reg[9][21] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[9] [21]),
        .R(clear));
  FDRE \slv_regs_reg[9][22] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[9] [22]),
        .R(clear));
  FDRE \slv_regs_reg[9][23] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[9] [23]),
        .R(clear));
  FDRE \slv_regs_reg[9][24] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[9] [24]),
        .R(clear));
  FDRE \slv_regs_reg[9][25] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[9] [25]),
        .R(clear));
  FDRE \slv_regs_reg[9][26] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[9] [26]),
        .R(clear));
  FDRE \slv_regs_reg[9][27] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[9] [27]),
        .R(clear));
  FDRE \slv_regs_reg[9][28] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[9] [28]),
        .R(clear));
  FDRE \slv_regs_reg[9][29] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[9] [29]),
        .R(clear));
  FDRE \slv_regs_reg[9][2] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[9] [2]),
        .R(clear));
  FDRE \slv_regs_reg[9][30] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[9] [30]),
        .R(clear));
  FDRE \slv_regs_reg[9][31] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[9] [31]),
        .R(clear));
  FDRE \slv_regs_reg[9][3] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[9] [3]),
        .R(clear));
  FDRE \slv_regs_reg[9][4] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[9] [4]),
        .R(clear));
  FDRE \slv_regs_reg[9][5] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[9] [5]),
        .R(clear));
  FDRE \slv_regs_reg[9][6] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[9] [6]),
        .R(clear));
  FDRE \slv_regs_reg[9][7] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[9] [7]),
        .R(clear));
  FDRE \slv_regs_reg[9][8] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[9] [8]),
        .R(clear));
  FDRE \slv_regs_reg[9][9] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[9] [9]),
        .R(clear));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
