-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
-- Date        : Wed Feb 21 16:27:44 2018
-- Host        : DESKTOP-0KA73P0 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_adderCore_0_0_sim_netlist.vhdl
-- Design      : design_1_adderCore_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_register_interface is
  port (
    s_axi_awready : out STD_LOGIC;
    clear : out STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_bvalid : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 16 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clk : in STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_arvalid : in STD_LOGIC;
    aresetn : in STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 15 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_regs_reg[0][7]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_regs_reg[0][11]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_regs_reg[0][15]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_bready : in STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \count_reg[15]\ : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_register_interface;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_register_interface is
  signal \^q\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_arready_i_1_n_0 : STD_LOGIC;
  signal axi_awready_i_1_n_0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal \axi_rdata[0]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[10]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[11]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[12]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[13]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[14]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[15]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[16]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[16]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[17]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[18]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[19]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[19]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[1]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[20]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[21]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[22]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[22]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[23]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[23]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[24]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[25]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[26]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[26]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[27]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[27]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[28]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[28]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[29]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[29]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[2]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[2]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[30]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[30]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[31]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[31]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[3]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[4]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[5]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[6]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[7]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[8]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[9]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[9]_i_3_n_0\ : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal axi_wready_i_1_n_0 : STD_LOGIC;
  signal \^clear\ : STD_LOGIC;
  signal \out_data_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \out_data_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \out_data_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \out_data_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \out_data_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \out_data_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \out_data_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \out_data_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \out_data_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \out_data_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \out_data_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \out_data_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \out_data_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \out_data_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \out_data_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \out_data_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 7 );
  signal \read_data[0]_0\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^s_axi_arready\ : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal \^s_axi_bvalid\ : STD_LOGIC;
  signal \^s_axi_rvalid\ : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal sel0 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \slv_reg_wren__2\ : STD_LOGIC;
  signal \slv_regs[10][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[10][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[10][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[10][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[11][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[11][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[11][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[11][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[12][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[12][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[12][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[12][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[13][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[13][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[13][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[13][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[14][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[14][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[14][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[14][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[15][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[15][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[15][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[15][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[2][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[2][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[2][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[2][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[3][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[3][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[3][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[3][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[4][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[4][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[4][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[4][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[5][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[5][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[5][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[5][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[6][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[6][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[6][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[6][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[7][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[7][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[7][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[7][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[8][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[8][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[8][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[8][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[9][15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[9][23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[9][31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs[9][7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_regs_reg[10]\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_regs_reg[11]\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_regs_reg[12]\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_regs_reg[13]\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_regs_reg[14]\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_regs_reg[15]\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_regs_reg[2]\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_regs_reg[3]\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_regs_reg[4]\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_regs_reg[5]\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_regs_reg[6]\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_regs_reg[7]\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_regs_reg[8]\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_regs_reg[9]\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_regs_reg_n_0_[0][16]\ : STD_LOGIC;
  signal \slv_regs_reg_n_0_[0][17]\ : STD_LOGIC;
  signal \slv_regs_reg_n_0_[0][18]\ : STD_LOGIC;
  signal \slv_regs_reg_n_0_[0][19]\ : STD_LOGIC;
  signal \slv_regs_reg_n_0_[0][20]\ : STD_LOGIC;
  signal \slv_regs_reg_n_0_[0][21]\ : STD_LOGIC;
  signal \slv_regs_reg_n_0_[0][22]\ : STD_LOGIC;
  signal \slv_regs_reg_n_0_[0][23]\ : STD_LOGIC;
  signal \slv_regs_reg_n_0_[0][24]\ : STD_LOGIC;
  signal \slv_regs_reg_n_0_[0][25]\ : STD_LOGIC;
  signal \slv_regs_reg_n_0_[0][26]\ : STD_LOGIC;
  signal \slv_regs_reg_n_0_[0][27]\ : STD_LOGIC;
  signal \slv_regs_reg_n_0_[0][28]\ : STD_LOGIC;
  signal \slv_regs_reg_n_0_[0][29]\ : STD_LOGIC;
  signal \slv_regs_reg_n_0_[0][30]\ : STD_LOGIC;
  signal \slv_regs_reg_n_0_[0][31]\ : STD_LOGIC;
  signal \NLW_out_data_reg[16]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_out_data_reg[16]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
  Q(15 downto 0) <= \^q\(15 downto 0);
  clear <= \^clear\;
  s_axi_arready <= \^s_axi_arready\;
  s_axi_awready <= \^s_axi_awready\;
  s_axi_bvalid <= \^s_axi_bvalid\;
  s_axi_rvalid <= \^s_axi_rvalid\;
  s_axi_wready <= \^s_axi_wready\;
\axi_araddr_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => clk,
      CE => axi_arready_i_1_n_0,
      D => s_axi_araddr(0),
      Q => sel0(0),
      S => \^clear\
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => clk,
      CE => axi_arready_i_1_n_0,
      D => s_axi_araddr(1),
      Q => sel0(1),
      S => \^clear\
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDSE
     port map (
      C => clk,
      CE => axi_arready_i_1_n_0,
      D => s_axi_araddr(2),
      Q => sel0(2),
      S => \^clear\
    );
\axi_araddr_reg[5]\: unisim.vcomponents.FDSE
     port map (
      C => clk,
      CE => axi_arready_i_1_n_0,
      D => s_axi_araddr(3),
      Q => sel0(3),
      S => \^clear\
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s_axi_arvalid,
      I1 => \^s_axi_arready\,
      O => axi_arready_i_1_n_0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => axi_arready_i_1_n_0,
      Q => \^s_axi_arready\,
      R => \^clear\
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => axi_awready_i_1_n_0,
      D => s_axi_awaddr(0),
      Q => p_0_in(0),
      R => \^clear\
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => axi_awready_i_1_n_0,
      D => s_axi_awaddr(1),
      Q => p_0_in(1),
      R => \^clear\
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => axi_awready_i_1_n_0,
      D => s_axi_awaddr(2),
      Q => p_0_in(2),
      R => \^clear\
    );
\axi_awaddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => axi_awready_i_1_n_0,
      D => s_axi_awaddr(3),
      Q => p_0_in(3),
      R => \^clear\
    );
axi_awready_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => s_axi_awvalid,
      I1 => s_axi_wvalid,
      I2 => \^s_axi_awready\,
      O => axi_awready_i_1_n_0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => axi_awready_i_1_n_0,
      Q => \^s_axi_awready\,
      R => \^clear\
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF80008000"
    )
        port map (
      I0 => \^s_axi_wready\,
      I1 => s_axi_wvalid,
      I2 => \^s_axi_awready\,
      I3 => s_axi_awvalid,
      I4 => s_axi_bready,
      I5 => \^s_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => axi_bvalid_i_1_n_0,
      Q => \^s_axi_bvalid\,
      R => \^clear\
    );
\axi_rdata[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(0),
      I1 => \slv_regs_reg[2]\(0),
      I2 => sel0(1),
      I3 => \count_reg[15]\(0),
      I4 => sel0(0),
      I5 => \^q\(0),
      O => \axi_rdata[0]_i_4_n_0\
    );
\axi_rdata[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(0),
      I1 => \slv_regs_reg[6]\(0),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(0),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(0),
      O => \axi_rdata[0]_i_5_n_0\
    );
\axi_rdata[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(0),
      I1 => \slv_regs_reg[10]\(0),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(0),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(0),
      O => \axi_rdata[0]_i_6_n_0\
    );
\axi_rdata[0]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(0),
      I1 => \slv_regs_reg[14]\(0),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(0),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(0),
      O => \axi_rdata[0]_i_7_n_0\
    );
\axi_rdata[10]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(10),
      I1 => \slv_regs_reg[2]\(10),
      I2 => sel0(1),
      I3 => \count_reg[15]\(10),
      I4 => sel0(0),
      I5 => \^q\(10),
      O => \axi_rdata[10]_i_4_n_0\
    );
\axi_rdata[10]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(10),
      I1 => \slv_regs_reg[6]\(10),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(10),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(10),
      O => \axi_rdata[10]_i_5_n_0\
    );
\axi_rdata[10]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(10),
      I1 => \slv_regs_reg[10]\(10),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(10),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(10),
      O => \axi_rdata[10]_i_6_n_0\
    );
\axi_rdata[10]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(10),
      I1 => \slv_regs_reg[14]\(10),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(10),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(10),
      O => \axi_rdata[10]_i_7_n_0\
    );
\axi_rdata[11]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(11),
      I1 => \slv_regs_reg[2]\(11),
      I2 => sel0(1),
      I3 => \count_reg[15]\(11),
      I4 => sel0(0),
      I5 => \^q\(11),
      O => \axi_rdata[11]_i_4_n_0\
    );
\axi_rdata[11]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(11),
      I1 => \slv_regs_reg[6]\(11),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(11),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(11),
      O => \axi_rdata[11]_i_5_n_0\
    );
\axi_rdata[11]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(11),
      I1 => \slv_regs_reg[10]\(11),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(11),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(11),
      O => \axi_rdata[11]_i_6_n_0\
    );
\axi_rdata[11]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(11),
      I1 => \slv_regs_reg[14]\(11),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(11),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(11),
      O => \axi_rdata[11]_i_7_n_0\
    );
\axi_rdata[12]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(12),
      I1 => \slv_regs_reg[2]\(12),
      I2 => sel0(1),
      I3 => \count_reg[15]\(12),
      I4 => sel0(0),
      I5 => \^q\(12),
      O => \axi_rdata[12]_i_4_n_0\
    );
\axi_rdata[12]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(12),
      I1 => \slv_regs_reg[6]\(12),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(12),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(12),
      O => \axi_rdata[12]_i_5_n_0\
    );
\axi_rdata[12]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(12),
      I1 => \slv_regs_reg[10]\(12),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(12),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(12),
      O => \axi_rdata[12]_i_6_n_0\
    );
\axi_rdata[12]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(12),
      I1 => \slv_regs_reg[14]\(12),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(12),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(12),
      O => \axi_rdata[12]_i_7_n_0\
    );
\axi_rdata[13]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(13),
      I1 => \slv_regs_reg[2]\(13),
      I2 => sel0(1),
      I3 => \count_reg[15]\(13),
      I4 => sel0(0),
      I5 => \^q\(13),
      O => \axi_rdata[13]_i_4_n_0\
    );
\axi_rdata[13]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(13),
      I1 => \slv_regs_reg[6]\(13),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(13),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(13),
      O => \axi_rdata[13]_i_5_n_0\
    );
\axi_rdata[13]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(13),
      I1 => \slv_regs_reg[10]\(13),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(13),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(13),
      O => \axi_rdata[13]_i_6_n_0\
    );
\axi_rdata[13]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(13),
      I1 => \slv_regs_reg[14]\(13),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(13),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(13),
      O => \axi_rdata[13]_i_7_n_0\
    );
\axi_rdata[14]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(14),
      I1 => \slv_regs_reg[2]\(14),
      I2 => sel0(1),
      I3 => \count_reg[15]\(14),
      I4 => sel0(0),
      I5 => \^q\(14),
      O => \axi_rdata[14]_i_4_n_0\
    );
\axi_rdata[14]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(14),
      I1 => \slv_regs_reg[6]\(14),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(14),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(14),
      O => \axi_rdata[14]_i_5_n_0\
    );
\axi_rdata[14]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(14),
      I1 => \slv_regs_reg[10]\(14),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(14),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(14),
      O => \axi_rdata[14]_i_6_n_0\
    );
\axi_rdata[14]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(14),
      I1 => \slv_regs_reg[14]\(14),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(14),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(14),
      O => \axi_rdata[14]_i_7_n_0\
    );
\axi_rdata[15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(15),
      I1 => \slv_regs_reg[2]\(15),
      I2 => sel0(1),
      I3 => \count_reg[15]\(15),
      I4 => sel0(0),
      I5 => \^q\(15),
      O => \axi_rdata[15]_i_4_n_0\
    );
\axi_rdata[15]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(15),
      I1 => \slv_regs_reg[6]\(15),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(15),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(15),
      O => \axi_rdata[15]_i_5_n_0\
    );
\axi_rdata[15]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(15),
      I1 => \slv_regs_reg[10]\(15),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(15),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(15),
      O => \axi_rdata[15]_i_6_n_0\
    );
\axi_rdata[15]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(15),
      I1 => \slv_regs_reg[14]\(15),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(15),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(15),
      O => \axi_rdata[15]_i_7_n_0\
    );
\axi_rdata[16]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(16),
      I1 => \slv_regs_reg[2]\(16),
      I2 => sel0(1),
      I3 => \slv_regs_reg_n_0_[0][16]\,
      I4 => sel0(0),
      O => \axi_rdata[16]_i_4_n_0\
    );
\axi_rdata[16]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(16),
      I1 => \slv_regs_reg[6]\(16),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(16),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(16),
      O => \axi_rdata[16]_i_5_n_0\
    );
\axi_rdata[16]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(16),
      I1 => \slv_regs_reg[10]\(16),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(16),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(16),
      O => \axi_rdata[16]_i_6_n_0\
    );
\axi_rdata[16]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(16),
      I1 => \slv_regs_reg[14]\(16),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(16),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(16),
      O => \axi_rdata[16]_i_7_n_0\
    );
\axi_rdata[17]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(17),
      I1 => \slv_regs_reg[2]\(17),
      I2 => sel0(1),
      I3 => \slv_regs_reg_n_0_[0][17]\,
      I4 => sel0(0),
      O => \axi_rdata[17]_i_4_n_0\
    );
\axi_rdata[17]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(17),
      I1 => \slv_regs_reg[6]\(17),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(17),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(17),
      O => \axi_rdata[17]_i_5_n_0\
    );
\axi_rdata[17]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(17),
      I1 => \slv_regs_reg[10]\(17),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(17),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(17),
      O => \axi_rdata[17]_i_6_n_0\
    );
\axi_rdata[17]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(17),
      I1 => \slv_regs_reg[14]\(17),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(17),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(17),
      O => \axi_rdata[17]_i_7_n_0\
    );
\axi_rdata[18]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(18),
      I1 => \slv_regs_reg[2]\(18),
      I2 => sel0(1),
      I3 => \slv_regs_reg_n_0_[0][18]\,
      I4 => sel0(0),
      O => \axi_rdata[18]_i_4_n_0\
    );
\axi_rdata[18]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(18),
      I1 => \slv_regs_reg[6]\(18),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(18),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(18),
      O => \axi_rdata[18]_i_5_n_0\
    );
\axi_rdata[18]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(18),
      I1 => \slv_regs_reg[10]\(18),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(18),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(18),
      O => \axi_rdata[18]_i_6_n_0\
    );
\axi_rdata[18]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(18),
      I1 => \slv_regs_reg[14]\(18),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(18),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(18),
      O => \axi_rdata[18]_i_7_n_0\
    );
\axi_rdata[19]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(19),
      I1 => \slv_regs_reg[2]\(19),
      I2 => sel0(1),
      I3 => \slv_regs_reg_n_0_[0][19]\,
      I4 => sel0(0),
      O => \axi_rdata[19]_i_4_n_0\
    );
\axi_rdata[19]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(19),
      I1 => \slv_regs_reg[6]\(19),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(19),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(19),
      O => \axi_rdata[19]_i_5_n_0\
    );
\axi_rdata[19]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(19),
      I1 => \slv_regs_reg[10]\(19),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(19),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(19),
      O => \axi_rdata[19]_i_6_n_0\
    );
\axi_rdata[19]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(19),
      I1 => \slv_regs_reg[14]\(19),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(19),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(19),
      O => \axi_rdata[19]_i_7_n_0\
    );
\axi_rdata[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(1),
      I1 => \slv_regs_reg[2]\(1),
      I2 => sel0(1),
      I3 => \count_reg[15]\(1),
      I4 => sel0(0),
      I5 => \^q\(1),
      O => \axi_rdata[1]_i_4_n_0\
    );
\axi_rdata[1]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(1),
      I1 => \slv_regs_reg[6]\(1),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(1),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(1),
      O => \axi_rdata[1]_i_5_n_0\
    );
\axi_rdata[1]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(1),
      I1 => \slv_regs_reg[10]\(1),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(1),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(1),
      O => \axi_rdata[1]_i_6_n_0\
    );
\axi_rdata[1]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(1),
      I1 => \slv_regs_reg[14]\(1),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(1),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(1),
      O => \axi_rdata[1]_i_7_n_0\
    );
\axi_rdata[20]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(20),
      I1 => \slv_regs_reg[2]\(20),
      I2 => sel0(1),
      I3 => \slv_regs_reg_n_0_[0][20]\,
      I4 => sel0(0),
      O => \axi_rdata[20]_i_4_n_0\
    );
\axi_rdata[20]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(20),
      I1 => \slv_regs_reg[6]\(20),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(20),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(20),
      O => \axi_rdata[20]_i_5_n_0\
    );
\axi_rdata[20]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(20),
      I1 => \slv_regs_reg[10]\(20),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(20),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(20),
      O => \axi_rdata[20]_i_6_n_0\
    );
\axi_rdata[20]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(20),
      I1 => \slv_regs_reg[14]\(20),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(20),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(20),
      O => \axi_rdata[20]_i_7_n_0\
    );
\axi_rdata[21]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(21),
      I1 => \slv_regs_reg[2]\(21),
      I2 => sel0(1),
      I3 => \slv_regs_reg_n_0_[0][21]\,
      I4 => sel0(0),
      O => \axi_rdata[21]_i_4_n_0\
    );
\axi_rdata[21]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(21),
      I1 => \slv_regs_reg[6]\(21),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(21),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(21),
      O => \axi_rdata[21]_i_5_n_0\
    );
\axi_rdata[21]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(21),
      I1 => \slv_regs_reg[10]\(21),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(21),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(21),
      O => \axi_rdata[21]_i_6_n_0\
    );
\axi_rdata[21]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(21),
      I1 => \slv_regs_reg[14]\(21),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(21),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(21),
      O => \axi_rdata[21]_i_7_n_0\
    );
\axi_rdata[22]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(22),
      I1 => \slv_regs_reg[2]\(22),
      I2 => sel0(1),
      I3 => \slv_regs_reg_n_0_[0][22]\,
      I4 => sel0(0),
      O => \axi_rdata[22]_i_4_n_0\
    );
\axi_rdata[22]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(22),
      I1 => \slv_regs_reg[6]\(22),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(22),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(22),
      O => \axi_rdata[22]_i_5_n_0\
    );
\axi_rdata[22]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(22),
      I1 => \slv_regs_reg[10]\(22),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(22),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(22),
      O => \axi_rdata[22]_i_6_n_0\
    );
\axi_rdata[22]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(22),
      I1 => \slv_regs_reg[14]\(22),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(22),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(22),
      O => \axi_rdata[22]_i_7_n_0\
    );
\axi_rdata[23]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(23),
      I1 => \slv_regs_reg[2]\(23),
      I2 => sel0(1),
      I3 => \slv_regs_reg_n_0_[0][23]\,
      I4 => sel0(0),
      O => \axi_rdata[23]_i_4_n_0\
    );
\axi_rdata[23]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(23),
      I1 => \slv_regs_reg[6]\(23),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(23),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(23),
      O => \axi_rdata[23]_i_5_n_0\
    );
\axi_rdata[23]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(23),
      I1 => \slv_regs_reg[10]\(23),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(23),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(23),
      O => \axi_rdata[23]_i_6_n_0\
    );
\axi_rdata[23]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(23),
      I1 => \slv_regs_reg[14]\(23),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(23),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(23),
      O => \axi_rdata[23]_i_7_n_0\
    );
\axi_rdata[24]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(24),
      I1 => \slv_regs_reg[2]\(24),
      I2 => sel0(1),
      I3 => \slv_regs_reg_n_0_[0][24]\,
      I4 => sel0(0),
      O => \axi_rdata[24]_i_4_n_0\
    );
\axi_rdata[24]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(24),
      I1 => \slv_regs_reg[6]\(24),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(24),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(24),
      O => \axi_rdata[24]_i_5_n_0\
    );
\axi_rdata[24]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(24),
      I1 => \slv_regs_reg[10]\(24),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(24),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(24),
      O => \axi_rdata[24]_i_6_n_0\
    );
\axi_rdata[24]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(24),
      I1 => \slv_regs_reg[14]\(24),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(24),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(24),
      O => \axi_rdata[24]_i_7_n_0\
    );
\axi_rdata[25]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(25),
      I1 => \slv_regs_reg[2]\(25),
      I2 => sel0(1),
      I3 => \slv_regs_reg_n_0_[0][25]\,
      I4 => sel0(0),
      O => \axi_rdata[25]_i_4_n_0\
    );
\axi_rdata[25]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(25),
      I1 => \slv_regs_reg[6]\(25),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(25),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(25),
      O => \axi_rdata[25]_i_5_n_0\
    );
\axi_rdata[25]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(25),
      I1 => \slv_regs_reg[10]\(25),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(25),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(25),
      O => \axi_rdata[25]_i_6_n_0\
    );
\axi_rdata[25]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(25),
      I1 => \slv_regs_reg[14]\(25),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(25),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(25),
      O => \axi_rdata[25]_i_7_n_0\
    );
\axi_rdata[26]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(26),
      I1 => \slv_regs_reg[2]\(26),
      I2 => sel0(1),
      I3 => \slv_regs_reg_n_0_[0][26]\,
      I4 => sel0(0),
      O => \axi_rdata[26]_i_4_n_0\
    );
\axi_rdata[26]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(26),
      I1 => \slv_regs_reg[6]\(26),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(26),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(26),
      O => \axi_rdata[26]_i_5_n_0\
    );
\axi_rdata[26]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(26),
      I1 => \slv_regs_reg[10]\(26),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(26),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(26),
      O => \axi_rdata[26]_i_6_n_0\
    );
\axi_rdata[26]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(26),
      I1 => \slv_regs_reg[14]\(26),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(26),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(26),
      O => \axi_rdata[26]_i_7_n_0\
    );
\axi_rdata[27]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(27),
      I1 => \slv_regs_reg[2]\(27),
      I2 => sel0(1),
      I3 => \slv_regs_reg_n_0_[0][27]\,
      I4 => sel0(0),
      O => \axi_rdata[27]_i_4_n_0\
    );
\axi_rdata[27]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(27),
      I1 => \slv_regs_reg[6]\(27),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(27),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(27),
      O => \axi_rdata[27]_i_5_n_0\
    );
\axi_rdata[27]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(27),
      I1 => \slv_regs_reg[10]\(27),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(27),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(27),
      O => \axi_rdata[27]_i_6_n_0\
    );
\axi_rdata[27]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(27),
      I1 => \slv_regs_reg[14]\(27),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(27),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(27),
      O => \axi_rdata[27]_i_7_n_0\
    );
\axi_rdata[28]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(28),
      I1 => \slv_regs_reg[2]\(28),
      I2 => sel0(1),
      I3 => \slv_regs_reg_n_0_[0][28]\,
      I4 => sel0(0),
      O => \axi_rdata[28]_i_4_n_0\
    );
\axi_rdata[28]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(28),
      I1 => \slv_regs_reg[6]\(28),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(28),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(28),
      O => \axi_rdata[28]_i_5_n_0\
    );
\axi_rdata[28]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(28),
      I1 => \slv_regs_reg[10]\(28),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(28),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(28),
      O => \axi_rdata[28]_i_6_n_0\
    );
\axi_rdata[28]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(28),
      I1 => \slv_regs_reg[14]\(28),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(28),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(28),
      O => \axi_rdata[28]_i_7_n_0\
    );
\axi_rdata[29]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(29),
      I1 => \slv_regs_reg[2]\(29),
      I2 => sel0(1),
      I3 => \slv_regs_reg_n_0_[0][29]\,
      I4 => sel0(0),
      O => \axi_rdata[29]_i_4_n_0\
    );
\axi_rdata[29]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(29),
      I1 => \slv_regs_reg[6]\(29),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(29),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(29),
      O => \axi_rdata[29]_i_5_n_0\
    );
\axi_rdata[29]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(29),
      I1 => \slv_regs_reg[10]\(29),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(29),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(29),
      O => \axi_rdata[29]_i_6_n_0\
    );
\axi_rdata[29]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(29),
      I1 => \slv_regs_reg[14]\(29),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(29),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(29),
      O => \axi_rdata[29]_i_7_n_0\
    );
\axi_rdata[2]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(2),
      I1 => \slv_regs_reg[2]\(2),
      I2 => sel0(1),
      I3 => \count_reg[15]\(2),
      I4 => sel0(0),
      I5 => \^q\(2),
      O => \axi_rdata[2]_i_4_n_0\
    );
\axi_rdata[2]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(2),
      I1 => \slv_regs_reg[6]\(2),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(2),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(2),
      O => \axi_rdata[2]_i_5_n_0\
    );
\axi_rdata[2]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(2),
      I1 => \slv_regs_reg[10]\(2),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(2),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(2),
      O => \axi_rdata[2]_i_6_n_0\
    );
\axi_rdata[2]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(2),
      I1 => \slv_regs_reg[14]\(2),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(2),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(2),
      O => \axi_rdata[2]_i_7_n_0\
    );
\axi_rdata[30]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(30),
      I1 => \slv_regs_reg[2]\(30),
      I2 => sel0(1),
      I3 => \slv_regs_reg_n_0_[0][30]\,
      I4 => sel0(0),
      O => \axi_rdata[30]_i_4_n_0\
    );
\axi_rdata[30]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(30),
      I1 => \slv_regs_reg[6]\(30),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(30),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(30),
      O => \axi_rdata[30]_i_5_n_0\
    );
\axi_rdata[30]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(30),
      I1 => \slv_regs_reg[10]\(30),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(30),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(30),
      O => \axi_rdata[30]_i_6_n_0\
    );
\axi_rdata[30]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(30),
      I1 => \slv_regs_reg[14]\(30),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(30),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(30),
      O => \axi_rdata[30]_i_7_n_0\
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^s_axi_arready\,
      I1 => s_axi_arvalid,
      I2 => \^s_axi_rvalid\,
      O => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata[31]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(31),
      I1 => \slv_regs_reg[2]\(31),
      I2 => sel0(1),
      I3 => \slv_regs_reg_n_0_[0][31]\,
      I4 => sel0(0),
      O => \axi_rdata[31]_i_5_n_0\
    );
\axi_rdata[31]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(31),
      I1 => \slv_regs_reg[6]\(31),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(31),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(31),
      O => \axi_rdata[31]_i_6_n_0\
    );
\axi_rdata[31]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(31),
      I1 => \slv_regs_reg[10]\(31),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(31),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(31),
      O => \axi_rdata[31]_i_7_n_0\
    );
\axi_rdata[31]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(31),
      I1 => \slv_regs_reg[14]\(31),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(31),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(31),
      O => \axi_rdata[31]_i_8_n_0\
    );
\axi_rdata[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(3),
      I1 => \slv_regs_reg[2]\(3),
      I2 => sel0(1),
      I3 => \count_reg[15]\(3),
      I4 => sel0(0),
      I5 => \^q\(3),
      O => \axi_rdata[3]_i_4_n_0\
    );
\axi_rdata[3]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(3),
      I1 => \slv_regs_reg[6]\(3),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(3),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(3),
      O => \axi_rdata[3]_i_5_n_0\
    );
\axi_rdata[3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(3),
      I1 => \slv_regs_reg[10]\(3),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(3),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(3),
      O => \axi_rdata[3]_i_6_n_0\
    );
\axi_rdata[3]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(3),
      I1 => \slv_regs_reg[14]\(3),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(3),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(3),
      O => \axi_rdata[3]_i_7_n_0\
    );
\axi_rdata[4]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(4),
      I1 => \slv_regs_reg[2]\(4),
      I2 => sel0(1),
      I3 => \count_reg[15]\(4),
      I4 => sel0(0),
      I5 => \^q\(4),
      O => \axi_rdata[4]_i_4_n_0\
    );
\axi_rdata[4]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(4),
      I1 => \slv_regs_reg[6]\(4),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(4),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(4),
      O => \axi_rdata[4]_i_5_n_0\
    );
\axi_rdata[4]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(4),
      I1 => \slv_regs_reg[10]\(4),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(4),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(4),
      O => \axi_rdata[4]_i_6_n_0\
    );
\axi_rdata[4]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(4),
      I1 => \slv_regs_reg[14]\(4),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(4),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(4),
      O => \axi_rdata[4]_i_7_n_0\
    );
\axi_rdata[5]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(5),
      I1 => \slv_regs_reg[2]\(5),
      I2 => sel0(1),
      I3 => \count_reg[15]\(5),
      I4 => sel0(0),
      I5 => \^q\(5),
      O => \axi_rdata[5]_i_4_n_0\
    );
\axi_rdata[5]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(5),
      I1 => \slv_regs_reg[6]\(5),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(5),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(5),
      O => \axi_rdata[5]_i_5_n_0\
    );
\axi_rdata[5]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(5),
      I1 => \slv_regs_reg[10]\(5),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(5),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(5),
      O => \axi_rdata[5]_i_6_n_0\
    );
\axi_rdata[5]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(5),
      I1 => \slv_regs_reg[14]\(5),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(5),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(5),
      O => \axi_rdata[5]_i_7_n_0\
    );
\axi_rdata[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(6),
      I1 => \slv_regs_reg[2]\(6),
      I2 => sel0(1),
      I3 => \count_reg[15]\(6),
      I4 => sel0(0),
      I5 => \^q\(6),
      O => \axi_rdata[6]_i_4_n_0\
    );
\axi_rdata[6]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(6),
      I1 => \slv_regs_reg[6]\(6),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(6),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(6),
      O => \axi_rdata[6]_i_5_n_0\
    );
\axi_rdata[6]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(6),
      I1 => \slv_regs_reg[10]\(6),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(6),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(6),
      O => \axi_rdata[6]_i_6_n_0\
    );
\axi_rdata[6]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(6),
      I1 => \slv_regs_reg[14]\(6),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(6),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(6),
      O => \axi_rdata[6]_i_7_n_0\
    );
\axi_rdata[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(7),
      I1 => \slv_regs_reg[2]\(7),
      I2 => sel0(1),
      I3 => \count_reg[15]\(7),
      I4 => sel0(0),
      I5 => \^q\(7),
      O => \axi_rdata[7]_i_4_n_0\
    );
\axi_rdata[7]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(7),
      I1 => \slv_regs_reg[6]\(7),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(7),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(7),
      O => \axi_rdata[7]_i_5_n_0\
    );
\axi_rdata[7]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(7),
      I1 => \slv_regs_reg[10]\(7),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(7),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(7),
      O => \axi_rdata[7]_i_6_n_0\
    );
\axi_rdata[7]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(7),
      I1 => \slv_regs_reg[14]\(7),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(7),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(7),
      O => \axi_rdata[7]_i_7_n_0\
    );
\axi_rdata[8]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(8),
      I1 => \slv_regs_reg[2]\(8),
      I2 => sel0(1),
      I3 => \count_reg[15]\(8),
      I4 => sel0(0),
      I5 => \^q\(8),
      O => \axi_rdata[8]_i_4_n_0\
    );
\axi_rdata[8]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(8),
      I1 => \slv_regs_reg[6]\(8),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(8),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(8),
      O => \axi_rdata[8]_i_5_n_0\
    );
\axi_rdata[8]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(8),
      I1 => \slv_regs_reg[10]\(8),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(8),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(8),
      O => \axi_rdata[8]_i_6_n_0\
    );
\axi_rdata[8]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(8),
      I1 => \slv_regs_reg[14]\(8),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(8),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(8),
      O => \axi_rdata[8]_i_7_n_0\
    );
\axi_rdata[9]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[3]\(9),
      I1 => \slv_regs_reg[2]\(9),
      I2 => sel0(1),
      I3 => \count_reg[15]\(9),
      I4 => sel0(0),
      I5 => \^q\(9),
      O => \axi_rdata[9]_i_4_n_0\
    );
\axi_rdata[9]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[7]\(9),
      I1 => \slv_regs_reg[6]\(9),
      I2 => sel0(1),
      I3 => \slv_regs_reg[5]\(9),
      I4 => sel0(0),
      I5 => \slv_regs_reg[4]\(9),
      O => \axi_rdata[9]_i_5_n_0\
    );
\axi_rdata[9]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[11]\(9),
      I1 => \slv_regs_reg[10]\(9),
      I2 => sel0(1),
      I3 => \slv_regs_reg[9]\(9),
      I4 => sel0(0),
      I5 => \slv_regs_reg[8]\(9),
      O => \axi_rdata[9]_i_6_n_0\
    );
\axi_rdata[9]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \slv_regs_reg[15]\(9),
      I1 => \slv_regs_reg[14]\(9),
      I2 => sel0(1),
      I3 => \slv_regs_reg[13]\(9),
      I4 => sel0(0),
      I5 => \slv_regs_reg[12]\(9),
      O => \axi_rdata[9]_i_7_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(0),
      Q => s_axi_rdata(0),
      R => \^clear\
    );
\axi_rdata_reg[0]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[0]_i_2_n_0\,
      I1 => \axi_rdata_reg[0]_i_3_n_0\,
      O => \read_data[0]_0\(0),
      S => sel0(3)
    );
\axi_rdata_reg[0]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[0]_i_4_n_0\,
      I1 => \axi_rdata[0]_i_5_n_0\,
      O => \axi_rdata_reg[0]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[0]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[0]_i_6_n_0\,
      I1 => \axi_rdata[0]_i_7_n_0\,
      O => \axi_rdata_reg[0]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(10),
      Q => s_axi_rdata(10),
      R => \^clear\
    );
\axi_rdata_reg[10]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[10]_i_2_n_0\,
      I1 => \axi_rdata_reg[10]_i_3_n_0\,
      O => \read_data[0]_0\(10),
      S => sel0(3)
    );
\axi_rdata_reg[10]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[10]_i_4_n_0\,
      I1 => \axi_rdata[10]_i_5_n_0\,
      O => \axi_rdata_reg[10]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[10]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[10]_i_6_n_0\,
      I1 => \axi_rdata[10]_i_7_n_0\,
      O => \axi_rdata_reg[10]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(11),
      Q => s_axi_rdata(11),
      R => \^clear\
    );
\axi_rdata_reg[11]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[11]_i_2_n_0\,
      I1 => \axi_rdata_reg[11]_i_3_n_0\,
      O => \read_data[0]_0\(11),
      S => sel0(3)
    );
\axi_rdata_reg[11]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[11]_i_4_n_0\,
      I1 => \axi_rdata[11]_i_5_n_0\,
      O => \axi_rdata_reg[11]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[11]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[11]_i_6_n_0\,
      I1 => \axi_rdata[11]_i_7_n_0\,
      O => \axi_rdata_reg[11]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(12),
      Q => s_axi_rdata(12),
      R => \^clear\
    );
\axi_rdata_reg[12]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[12]_i_2_n_0\,
      I1 => \axi_rdata_reg[12]_i_3_n_0\,
      O => \read_data[0]_0\(12),
      S => sel0(3)
    );
\axi_rdata_reg[12]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[12]_i_4_n_0\,
      I1 => \axi_rdata[12]_i_5_n_0\,
      O => \axi_rdata_reg[12]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[12]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[12]_i_6_n_0\,
      I1 => \axi_rdata[12]_i_7_n_0\,
      O => \axi_rdata_reg[12]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(13),
      Q => s_axi_rdata(13),
      R => \^clear\
    );
\axi_rdata_reg[13]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[13]_i_2_n_0\,
      I1 => \axi_rdata_reg[13]_i_3_n_0\,
      O => \read_data[0]_0\(13),
      S => sel0(3)
    );
\axi_rdata_reg[13]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[13]_i_4_n_0\,
      I1 => \axi_rdata[13]_i_5_n_0\,
      O => \axi_rdata_reg[13]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[13]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[13]_i_6_n_0\,
      I1 => \axi_rdata[13]_i_7_n_0\,
      O => \axi_rdata_reg[13]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(14),
      Q => s_axi_rdata(14),
      R => \^clear\
    );
\axi_rdata_reg[14]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[14]_i_2_n_0\,
      I1 => \axi_rdata_reg[14]_i_3_n_0\,
      O => \read_data[0]_0\(14),
      S => sel0(3)
    );
\axi_rdata_reg[14]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[14]_i_4_n_0\,
      I1 => \axi_rdata[14]_i_5_n_0\,
      O => \axi_rdata_reg[14]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[14]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[14]_i_6_n_0\,
      I1 => \axi_rdata[14]_i_7_n_0\,
      O => \axi_rdata_reg[14]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(15),
      Q => s_axi_rdata(15),
      R => \^clear\
    );
\axi_rdata_reg[15]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[15]_i_2_n_0\,
      I1 => \axi_rdata_reg[15]_i_3_n_0\,
      O => \read_data[0]_0\(15),
      S => sel0(3)
    );
\axi_rdata_reg[15]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[15]_i_4_n_0\,
      I1 => \axi_rdata[15]_i_5_n_0\,
      O => \axi_rdata_reg[15]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[15]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[15]_i_6_n_0\,
      I1 => \axi_rdata[15]_i_7_n_0\,
      O => \axi_rdata_reg[15]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(16),
      Q => s_axi_rdata(16),
      R => \^clear\
    );
\axi_rdata_reg[16]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[16]_i_2_n_0\,
      I1 => \axi_rdata_reg[16]_i_3_n_0\,
      O => \read_data[0]_0\(16),
      S => sel0(3)
    );
\axi_rdata_reg[16]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[16]_i_4_n_0\,
      I1 => \axi_rdata[16]_i_5_n_0\,
      O => \axi_rdata_reg[16]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[16]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[16]_i_6_n_0\,
      I1 => \axi_rdata[16]_i_7_n_0\,
      O => \axi_rdata_reg[16]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(17),
      Q => s_axi_rdata(17),
      R => \^clear\
    );
\axi_rdata_reg[17]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[17]_i_2_n_0\,
      I1 => \axi_rdata_reg[17]_i_3_n_0\,
      O => \read_data[0]_0\(17),
      S => sel0(3)
    );
\axi_rdata_reg[17]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[17]_i_4_n_0\,
      I1 => \axi_rdata[17]_i_5_n_0\,
      O => \axi_rdata_reg[17]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[17]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[17]_i_6_n_0\,
      I1 => \axi_rdata[17]_i_7_n_0\,
      O => \axi_rdata_reg[17]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(18),
      Q => s_axi_rdata(18),
      R => \^clear\
    );
\axi_rdata_reg[18]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[18]_i_2_n_0\,
      I1 => \axi_rdata_reg[18]_i_3_n_0\,
      O => \read_data[0]_0\(18),
      S => sel0(3)
    );
\axi_rdata_reg[18]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[18]_i_4_n_0\,
      I1 => \axi_rdata[18]_i_5_n_0\,
      O => \axi_rdata_reg[18]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[18]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[18]_i_6_n_0\,
      I1 => \axi_rdata[18]_i_7_n_0\,
      O => \axi_rdata_reg[18]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(19),
      Q => s_axi_rdata(19),
      R => \^clear\
    );
\axi_rdata_reg[19]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[19]_i_2_n_0\,
      I1 => \axi_rdata_reg[19]_i_3_n_0\,
      O => \read_data[0]_0\(19),
      S => sel0(3)
    );
\axi_rdata_reg[19]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[19]_i_4_n_0\,
      I1 => \axi_rdata[19]_i_5_n_0\,
      O => \axi_rdata_reg[19]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[19]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[19]_i_6_n_0\,
      I1 => \axi_rdata[19]_i_7_n_0\,
      O => \axi_rdata_reg[19]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(1),
      Q => s_axi_rdata(1),
      R => \^clear\
    );
\axi_rdata_reg[1]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[1]_i_2_n_0\,
      I1 => \axi_rdata_reg[1]_i_3_n_0\,
      O => \read_data[0]_0\(1),
      S => sel0(3)
    );
\axi_rdata_reg[1]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[1]_i_4_n_0\,
      I1 => \axi_rdata[1]_i_5_n_0\,
      O => \axi_rdata_reg[1]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[1]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[1]_i_6_n_0\,
      I1 => \axi_rdata[1]_i_7_n_0\,
      O => \axi_rdata_reg[1]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(20),
      Q => s_axi_rdata(20),
      R => \^clear\
    );
\axi_rdata_reg[20]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[20]_i_2_n_0\,
      I1 => \axi_rdata_reg[20]_i_3_n_0\,
      O => \read_data[0]_0\(20),
      S => sel0(3)
    );
\axi_rdata_reg[20]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[20]_i_4_n_0\,
      I1 => \axi_rdata[20]_i_5_n_0\,
      O => \axi_rdata_reg[20]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[20]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[20]_i_6_n_0\,
      I1 => \axi_rdata[20]_i_7_n_0\,
      O => \axi_rdata_reg[20]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(21),
      Q => s_axi_rdata(21),
      R => \^clear\
    );
\axi_rdata_reg[21]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[21]_i_2_n_0\,
      I1 => \axi_rdata_reg[21]_i_3_n_0\,
      O => \read_data[0]_0\(21),
      S => sel0(3)
    );
\axi_rdata_reg[21]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[21]_i_4_n_0\,
      I1 => \axi_rdata[21]_i_5_n_0\,
      O => \axi_rdata_reg[21]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[21]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[21]_i_6_n_0\,
      I1 => \axi_rdata[21]_i_7_n_0\,
      O => \axi_rdata_reg[21]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(22),
      Q => s_axi_rdata(22),
      R => \^clear\
    );
\axi_rdata_reg[22]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[22]_i_2_n_0\,
      I1 => \axi_rdata_reg[22]_i_3_n_0\,
      O => \read_data[0]_0\(22),
      S => sel0(3)
    );
\axi_rdata_reg[22]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[22]_i_4_n_0\,
      I1 => \axi_rdata[22]_i_5_n_0\,
      O => \axi_rdata_reg[22]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[22]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[22]_i_6_n_0\,
      I1 => \axi_rdata[22]_i_7_n_0\,
      O => \axi_rdata_reg[22]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(23),
      Q => s_axi_rdata(23),
      R => \^clear\
    );
\axi_rdata_reg[23]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[23]_i_2_n_0\,
      I1 => \axi_rdata_reg[23]_i_3_n_0\,
      O => \read_data[0]_0\(23),
      S => sel0(3)
    );
\axi_rdata_reg[23]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[23]_i_4_n_0\,
      I1 => \axi_rdata[23]_i_5_n_0\,
      O => \axi_rdata_reg[23]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[23]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[23]_i_6_n_0\,
      I1 => \axi_rdata[23]_i_7_n_0\,
      O => \axi_rdata_reg[23]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(24),
      Q => s_axi_rdata(24),
      R => \^clear\
    );
\axi_rdata_reg[24]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[24]_i_2_n_0\,
      I1 => \axi_rdata_reg[24]_i_3_n_0\,
      O => \read_data[0]_0\(24),
      S => sel0(3)
    );
\axi_rdata_reg[24]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[24]_i_4_n_0\,
      I1 => \axi_rdata[24]_i_5_n_0\,
      O => \axi_rdata_reg[24]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[24]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[24]_i_6_n_0\,
      I1 => \axi_rdata[24]_i_7_n_0\,
      O => \axi_rdata_reg[24]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(25),
      Q => s_axi_rdata(25),
      R => \^clear\
    );
\axi_rdata_reg[25]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[25]_i_2_n_0\,
      I1 => \axi_rdata_reg[25]_i_3_n_0\,
      O => \read_data[0]_0\(25),
      S => sel0(3)
    );
\axi_rdata_reg[25]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[25]_i_4_n_0\,
      I1 => \axi_rdata[25]_i_5_n_0\,
      O => \axi_rdata_reg[25]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[25]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[25]_i_6_n_0\,
      I1 => \axi_rdata[25]_i_7_n_0\,
      O => \axi_rdata_reg[25]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(26),
      Q => s_axi_rdata(26),
      R => \^clear\
    );
\axi_rdata_reg[26]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[26]_i_2_n_0\,
      I1 => \axi_rdata_reg[26]_i_3_n_0\,
      O => \read_data[0]_0\(26),
      S => sel0(3)
    );
\axi_rdata_reg[26]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[26]_i_4_n_0\,
      I1 => \axi_rdata[26]_i_5_n_0\,
      O => \axi_rdata_reg[26]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[26]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[26]_i_6_n_0\,
      I1 => \axi_rdata[26]_i_7_n_0\,
      O => \axi_rdata_reg[26]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(27),
      Q => s_axi_rdata(27),
      R => \^clear\
    );
\axi_rdata_reg[27]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[27]_i_2_n_0\,
      I1 => \axi_rdata_reg[27]_i_3_n_0\,
      O => \read_data[0]_0\(27),
      S => sel0(3)
    );
\axi_rdata_reg[27]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[27]_i_4_n_0\,
      I1 => \axi_rdata[27]_i_5_n_0\,
      O => \axi_rdata_reg[27]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[27]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[27]_i_6_n_0\,
      I1 => \axi_rdata[27]_i_7_n_0\,
      O => \axi_rdata_reg[27]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(28),
      Q => s_axi_rdata(28),
      R => \^clear\
    );
\axi_rdata_reg[28]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[28]_i_2_n_0\,
      I1 => \axi_rdata_reg[28]_i_3_n_0\,
      O => \read_data[0]_0\(28),
      S => sel0(3)
    );
\axi_rdata_reg[28]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[28]_i_4_n_0\,
      I1 => \axi_rdata[28]_i_5_n_0\,
      O => \axi_rdata_reg[28]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[28]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[28]_i_6_n_0\,
      I1 => \axi_rdata[28]_i_7_n_0\,
      O => \axi_rdata_reg[28]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(29),
      Q => s_axi_rdata(29),
      R => \^clear\
    );
\axi_rdata_reg[29]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[29]_i_2_n_0\,
      I1 => \axi_rdata_reg[29]_i_3_n_0\,
      O => \read_data[0]_0\(29),
      S => sel0(3)
    );
\axi_rdata_reg[29]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[29]_i_4_n_0\,
      I1 => \axi_rdata[29]_i_5_n_0\,
      O => \axi_rdata_reg[29]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[29]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[29]_i_6_n_0\,
      I1 => \axi_rdata[29]_i_7_n_0\,
      O => \axi_rdata_reg[29]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(2),
      Q => s_axi_rdata(2),
      R => \^clear\
    );
\axi_rdata_reg[2]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[2]_i_2_n_0\,
      I1 => \axi_rdata_reg[2]_i_3_n_0\,
      O => \read_data[0]_0\(2),
      S => sel0(3)
    );
\axi_rdata_reg[2]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[2]_i_4_n_0\,
      I1 => \axi_rdata[2]_i_5_n_0\,
      O => \axi_rdata_reg[2]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[2]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[2]_i_6_n_0\,
      I1 => \axi_rdata[2]_i_7_n_0\,
      O => \axi_rdata_reg[2]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(30),
      Q => s_axi_rdata(30),
      R => \^clear\
    );
\axi_rdata_reg[30]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[30]_i_2_n_0\,
      I1 => \axi_rdata_reg[30]_i_3_n_0\,
      O => \read_data[0]_0\(30),
      S => sel0(3)
    );
\axi_rdata_reg[30]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[30]_i_4_n_0\,
      I1 => \axi_rdata[30]_i_5_n_0\,
      O => \axi_rdata_reg[30]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[30]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[30]_i_6_n_0\,
      I1 => \axi_rdata[30]_i_7_n_0\,
      O => \axi_rdata_reg[30]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(31),
      Q => s_axi_rdata(31),
      R => \^clear\
    );
\axi_rdata_reg[31]_i_2\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[31]_i_3_n_0\,
      I1 => \axi_rdata_reg[31]_i_4_n_0\,
      O => \read_data[0]_0\(31),
      S => sel0(3)
    );
\axi_rdata_reg[31]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[31]_i_5_n_0\,
      I1 => \axi_rdata[31]_i_6_n_0\,
      O => \axi_rdata_reg[31]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[31]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[31]_i_7_n_0\,
      I1 => \axi_rdata[31]_i_8_n_0\,
      O => \axi_rdata_reg[31]_i_4_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(3),
      Q => s_axi_rdata(3),
      R => \^clear\
    );
\axi_rdata_reg[3]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[3]_i_2_n_0\,
      I1 => \axi_rdata_reg[3]_i_3_n_0\,
      O => \read_data[0]_0\(3),
      S => sel0(3)
    );
\axi_rdata_reg[3]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[3]_i_4_n_0\,
      I1 => \axi_rdata[3]_i_5_n_0\,
      O => \axi_rdata_reg[3]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[3]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[3]_i_6_n_0\,
      I1 => \axi_rdata[3]_i_7_n_0\,
      O => \axi_rdata_reg[3]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(4),
      Q => s_axi_rdata(4),
      R => \^clear\
    );
\axi_rdata_reg[4]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[4]_i_2_n_0\,
      I1 => \axi_rdata_reg[4]_i_3_n_0\,
      O => \read_data[0]_0\(4),
      S => sel0(3)
    );
\axi_rdata_reg[4]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[4]_i_4_n_0\,
      I1 => \axi_rdata[4]_i_5_n_0\,
      O => \axi_rdata_reg[4]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[4]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[4]_i_6_n_0\,
      I1 => \axi_rdata[4]_i_7_n_0\,
      O => \axi_rdata_reg[4]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(5),
      Q => s_axi_rdata(5),
      R => \^clear\
    );
\axi_rdata_reg[5]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[5]_i_2_n_0\,
      I1 => \axi_rdata_reg[5]_i_3_n_0\,
      O => \read_data[0]_0\(5),
      S => sel0(3)
    );
\axi_rdata_reg[5]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[5]_i_4_n_0\,
      I1 => \axi_rdata[5]_i_5_n_0\,
      O => \axi_rdata_reg[5]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[5]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[5]_i_6_n_0\,
      I1 => \axi_rdata[5]_i_7_n_0\,
      O => \axi_rdata_reg[5]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(6),
      Q => s_axi_rdata(6),
      R => \^clear\
    );
\axi_rdata_reg[6]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[6]_i_2_n_0\,
      I1 => \axi_rdata_reg[6]_i_3_n_0\,
      O => \read_data[0]_0\(6),
      S => sel0(3)
    );
\axi_rdata_reg[6]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[6]_i_4_n_0\,
      I1 => \axi_rdata[6]_i_5_n_0\,
      O => \axi_rdata_reg[6]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[6]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[6]_i_6_n_0\,
      I1 => \axi_rdata[6]_i_7_n_0\,
      O => \axi_rdata_reg[6]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(7),
      Q => s_axi_rdata(7),
      R => \^clear\
    );
\axi_rdata_reg[7]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[7]_i_2_n_0\,
      I1 => \axi_rdata_reg[7]_i_3_n_0\,
      O => \read_data[0]_0\(7),
      S => sel0(3)
    );
\axi_rdata_reg[7]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[7]_i_4_n_0\,
      I1 => \axi_rdata[7]_i_5_n_0\,
      O => \axi_rdata_reg[7]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[7]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[7]_i_6_n_0\,
      I1 => \axi_rdata[7]_i_7_n_0\,
      O => \axi_rdata_reg[7]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(8),
      Q => s_axi_rdata(8),
      R => \^clear\
    );
\axi_rdata_reg[8]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[8]_i_2_n_0\,
      I1 => \axi_rdata_reg[8]_i_3_n_0\,
      O => \read_data[0]_0\(8),
      S => sel0(3)
    );
\axi_rdata_reg[8]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[8]_i_4_n_0\,
      I1 => \axi_rdata[8]_i_5_n_0\,
      O => \axi_rdata_reg[8]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[8]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[8]_i_6_n_0\,
      I1 => \axi_rdata[8]_i_7_n_0\,
      O => \axi_rdata_reg[8]_i_3_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \axi_rdata[31]_i_1_n_0\,
      D => \read_data[0]_0\(9),
      Q => s_axi_rdata(9),
      R => \^clear\
    );
\axi_rdata_reg[9]_i_1\: unisim.vcomponents.MUXF8
     port map (
      I0 => \axi_rdata_reg[9]_i_2_n_0\,
      I1 => \axi_rdata_reg[9]_i_3_n_0\,
      O => \read_data[0]_0\(9),
      S => sel0(3)
    );
\axi_rdata_reg[9]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[9]_i_4_n_0\,
      I1 => \axi_rdata[9]_i_5_n_0\,
      O => \axi_rdata_reg[9]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[9]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[9]_i_6_n_0\,
      I1 => \axi_rdata[9]_i_7_n_0\,
      O => \axi_rdata_reg[9]_i_3_n_0\,
      S => sel0(2)
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => s_axi_arvalid,
      I1 => \^s_axi_arready\,
      I2 => \^s_axi_rvalid\,
      I3 => s_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => axi_rvalid_i_1_n_0,
      Q => \^s_axi_rvalid\,
      R => \^clear\
    );
axi_wready_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => s_axi_awvalid,
      I1 => s_axi_wvalid,
      I2 => \^s_axi_wready\,
      O => axi_wready_i_1_n_0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => axi_wready_i_1_n_0,
      Q => \^s_axi_wready\,
      R => \^clear\
    );
\out_data[16]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => aresetn,
      O => \^clear\
    );
\out_data_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \out_data_reg[7]_i_1_n_0\,
      CO(3) => \out_data_reg[11]_i_1_n_0\,
      CO(2) => \out_data_reg[11]_i_1_n_1\,
      CO(1) => \out_data_reg[11]_i_1_n_2\,
      CO(0) => \out_data_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => s_axis_tdata(11 downto 8),
      O(3 downto 0) => D(11 downto 8),
      S(3 downto 0) => \slv_regs_reg[0][11]_0\(3 downto 0)
    );
\out_data_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \out_data_reg[11]_i_1_n_0\,
      CO(3) => \out_data_reg[15]_i_1_n_0\,
      CO(2) => \out_data_reg[15]_i_1_n_1\,
      CO(1) => \out_data_reg[15]_i_1_n_2\,
      CO(0) => \out_data_reg[15]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => s_axis_tdata(15 downto 12),
      O(3 downto 0) => D(15 downto 12),
      S(3 downto 0) => \slv_regs_reg[0][15]_0\(3 downto 0)
    );
\out_data_reg[16]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \out_data_reg[15]_i_1_n_0\,
      CO(3 downto 1) => \NLW_out_data_reg[16]_i_3_CO_UNCONNECTED\(3 downto 1),
      CO(0) => D(16),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_out_data_reg[16]_i_3_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\out_data_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \out_data_reg[3]_i_1_n_0\,
      CO(2) => \out_data_reg[3]_i_1_n_1\,
      CO(1) => \out_data_reg[3]_i_1_n_2\,
      CO(0) => \out_data_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => s_axis_tdata(3 downto 0),
      O(3 downto 0) => D(3 downto 0),
      S(3 downto 0) => S(3 downto 0)
    );
\out_data_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \out_data_reg[3]_i_1_n_0\,
      CO(3) => \out_data_reg[7]_i_1_n_0\,
      CO(2) => \out_data_reg[7]_i_1_n_1\,
      CO(1) => \out_data_reg[7]_i_1_n_2\,
      CO(0) => \out_data_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => s_axis_tdata(7 downto 4),
      O(3 downto 0) => D(7 downto 4),
      S(3 downto 0) => \slv_regs_reg[0][7]_0\(3 downto 0)
    );
\slv_regs[0][15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => p_0_in(2),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(1),
      O => p_1_in(15)
    );
\slv_regs[0][23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => p_0_in(2),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(2),
      O => p_1_in(23)
    );
\slv_regs[0][31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => p_0_in(2),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(3),
      O => p_1_in(31)
    );
\slv_regs[0][31]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \^s_axi_wready\,
      I1 => s_axi_wvalid,
      I2 => \^s_axi_awready\,
      I3 => s_axi_awvalid,
      O => \slv_reg_wren__2\
    );
\slv_regs[0][7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => p_0_in(2),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(0),
      O => p_1_in(7)
    );
\slv_regs[10][15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(3),
      I3 => p_0_in(0),
      I4 => p_0_in(2),
      I5 => s_axi_wstrb(1),
      O => \slv_regs[10][15]_i_1_n_0\
    );
\slv_regs[10][23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(3),
      I3 => p_0_in(0),
      I4 => p_0_in(2),
      I5 => s_axi_wstrb(2),
      O => \slv_regs[10][23]_i_1_n_0\
    );
\slv_regs[10][31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(3),
      I3 => p_0_in(0),
      I4 => p_0_in(2),
      I5 => s_axi_wstrb(3),
      O => \slv_regs[10][31]_i_1_n_0\
    );
\slv_regs[10][7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(3),
      I3 => p_0_in(0),
      I4 => p_0_in(2),
      I5 => s_axi_wstrb(0),
      O => \slv_regs[10][7]_i_1_n_0\
    );
\slv_regs[11][15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => s_axi_wstrb(1),
      O => \slv_regs[11][15]_i_1_n_0\
    );
\slv_regs[11][23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => s_axi_wstrb(2),
      O => \slv_regs[11][23]_i_1_n_0\
    );
\slv_regs[11][31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => s_axi_wstrb(3),
      O => \slv_regs[11][31]_i_1_n_0\
    );
\slv_regs[11][7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => s_axi_wstrb(0),
      O => \slv_regs[11][7]_i_1_n_0\
    );
\slv_regs[12][15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => p_0_in(2),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(1),
      O => \slv_regs[12][15]_i_1_n_0\
    );
\slv_regs[12][23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => p_0_in(2),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(2),
      O => \slv_regs[12][23]_i_1_n_0\
    );
\slv_regs[12][31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => p_0_in(2),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(3),
      O => \slv_regs[12][31]_i_1_n_0\
    );
\slv_regs[12][7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => p_0_in(2),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(0),
      O => \slv_regs[12][7]_i_1_n_0\
    );
\slv_regs[13][15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(0),
      I3 => p_0_in(3),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(1),
      O => \slv_regs[13][15]_i_1_n_0\
    );
\slv_regs[13][23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(0),
      I3 => p_0_in(3),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(2),
      O => \slv_regs[13][23]_i_1_n_0\
    );
\slv_regs[13][31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(0),
      I3 => p_0_in(3),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(3),
      O => \slv_regs[13][31]_i_1_n_0\
    );
\slv_regs[13][7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(0),
      I3 => p_0_in(3),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(0),
      O => \slv_regs[13][7]_i_1_n_0\
    );
\slv_regs[14][15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => s_axi_wstrb(1),
      O => \slv_regs[14][15]_i_1_n_0\
    );
\slv_regs[14][23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => s_axi_wstrb(2),
      O => \slv_regs[14][23]_i_1_n_0\
    );
\slv_regs[14][31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => s_axi_wstrb(3),
      O => \slv_regs[14][31]_i_1_n_0\
    );
\slv_regs[14][7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => s_axi_wstrb(0),
      O => \slv_regs[14][7]_i_1_n_0\
    );
\slv_regs[15][15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => p_0_in(2),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(1),
      O => \slv_regs[15][15]_i_1_n_0\
    );
\slv_regs[15][23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => p_0_in(2),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(2),
      O => \slv_regs[15][23]_i_1_n_0\
    );
\slv_regs[15][31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => p_0_in(2),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(3),
      O => \slv_regs[15][31]_i_1_n_0\
    );
\slv_regs[15][7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => p_0_in(2),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(0),
      O => \slv_regs[15][7]_i_1_n_0\
    );
\slv_regs[2][15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(2),
      I3 => p_0_in(3),
      I4 => p_0_in(0),
      I5 => s_axi_wstrb(1),
      O => \slv_regs[2][15]_i_1_n_0\
    );
\slv_regs[2][23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(2),
      I3 => p_0_in(3),
      I4 => p_0_in(0),
      I5 => s_axi_wstrb(2),
      O => \slv_regs[2][23]_i_1_n_0\
    );
\slv_regs[2][31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(2),
      I3 => p_0_in(3),
      I4 => p_0_in(0),
      I5 => s_axi_wstrb(3),
      O => \slv_regs[2][31]_i_1_n_0\
    );
\slv_regs[2][7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(2),
      I3 => p_0_in(3),
      I4 => p_0_in(0),
      I5 => s_axi_wstrb(0),
      O => \slv_regs[2][7]_i_1_n_0\
    );
\slv_regs[3][15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(0),
      I2 => p_0_in(1),
      I3 => p_0_in(3),
      I4 => p_0_in(2),
      I5 => s_axi_wstrb(1),
      O => \slv_regs[3][15]_i_1_n_0\
    );
\slv_regs[3][23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(0),
      I2 => p_0_in(1),
      I3 => p_0_in(3),
      I4 => p_0_in(2),
      I5 => s_axi_wstrb(2),
      O => \slv_regs[3][23]_i_1_n_0\
    );
\slv_regs[3][31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(0),
      I2 => p_0_in(1),
      I3 => p_0_in(3),
      I4 => p_0_in(2),
      I5 => s_axi_wstrb(3),
      O => \slv_regs[3][31]_i_1_n_0\
    );
\slv_regs[3][7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(0),
      I2 => p_0_in(1),
      I3 => p_0_in(3),
      I4 => p_0_in(2),
      I5 => s_axi_wstrb(0),
      O => \slv_regs[3][7]_i_1_n_0\
    );
\slv_regs[4][15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(0),
      I3 => p_0_in(3),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(1),
      O => \slv_regs[4][15]_i_1_n_0\
    );
\slv_regs[4][23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(0),
      I3 => p_0_in(3),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(2),
      O => \slv_regs[4][23]_i_1_n_0\
    );
\slv_regs[4][31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(0),
      I3 => p_0_in(3),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(3),
      O => \slv_regs[4][31]_i_1_n_0\
    );
\slv_regs[4][7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(0),
      I3 => p_0_in(3),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(0),
      O => \slv_regs[4][7]_i_1_n_0\
    );
\slv_regs[5][15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(0),
      I2 => p_0_in(2),
      I3 => p_0_in(3),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(1),
      O => \slv_regs[5][15]_i_1_n_0\
    );
\slv_regs[5][23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(0),
      I2 => p_0_in(2),
      I3 => p_0_in(3),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(2),
      O => \slv_regs[5][23]_i_1_n_0\
    );
\slv_regs[5][31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(0),
      I2 => p_0_in(2),
      I3 => p_0_in(3),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(3),
      O => \slv_regs[5][31]_i_1_n_0\
    );
\slv_regs[5][7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(0),
      I2 => p_0_in(2),
      I3 => p_0_in(3),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(0),
      O => \slv_regs[5][7]_i_1_n_0\
    );
\slv_regs[6][15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(2),
      I3 => p_0_in(3),
      I4 => p_0_in(0),
      I5 => s_axi_wstrb(1),
      O => \slv_regs[6][15]_i_1_n_0\
    );
\slv_regs[6][23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(2),
      I3 => p_0_in(3),
      I4 => p_0_in(0),
      I5 => s_axi_wstrb(2),
      O => \slv_regs[6][23]_i_1_n_0\
    );
\slv_regs[6][31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(2),
      I3 => p_0_in(3),
      I4 => p_0_in(0),
      I5 => s_axi_wstrb(3),
      O => \slv_regs[6][31]_i_1_n_0\
    );
\slv_regs[6][7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(2),
      I3 => p_0_in(3),
      I4 => p_0_in(0),
      I5 => s_axi_wstrb(0),
      O => \slv_regs[6][7]_i_1_n_0\
    );
\slv_regs[7][15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s_axi_wstrb(1),
      O => \slv_regs[7][15]_i_1_n_0\
    );
\slv_regs[7][23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s_axi_wstrb(2),
      O => \slv_regs[7][23]_i_1_n_0\
    );
\slv_regs[7][31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s_axi_wstrb(3),
      O => \slv_regs[7][31]_i_1_n_0\
    );
\slv_regs[7][7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => p_0_in(3),
      I5 => s_axi_wstrb(0),
      O => \slv_regs[7][7]_i_1_n_0\
    );
\slv_regs[8][15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => p_0_in(2),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(1),
      O => \slv_regs[8][15]_i_1_n_0\
    );
\slv_regs[8][23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => p_0_in(2),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(2),
      O => \slv_regs[8][23]_i_1_n_0\
    );
\slv_regs[8][31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => p_0_in(2),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(3),
      O => \slv_regs[8][31]_i_1_n_0\
    );
\slv_regs[8][7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => p_0_in(2),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => s_axi_wstrb(0),
      O => \slv_regs[8][7]_i_1_n_0\
    );
\slv_regs[9][15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(0),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => s_axi_wstrb(1),
      O => \slv_regs[9][15]_i_1_n_0\
    );
\slv_regs[9][23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(0),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => s_axi_wstrb(2),
      O => \slv_regs[9][23]_i_1_n_0\
    );
\slv_regs[9][31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(0),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => s_axi_wstrb(3),
      O => \slv_regs[9][31]_i_1_n_0\
    );
\slv_regs[9][7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(0),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => s_axi_wstrb(0),
      O => \slv_regs[9][7]_i_1_n_0\
    );
\slv_regs_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(7),
      D => s_axi_wdata(0),
      Q => \^q\(0),
      R => \^clear\
    );
\slv_regs_reg[0][10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(15),
      D => s_axi_wdata(10),
      Q => \^q\(10),
      R => \^clear\
    );
\slv_regs_reg[0][11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(15),
      D => s_axi_wdata(11),
      Q => \^q\(11),
      R => \^clear\
    );
\slv_regs_reg[0][12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(15),
      D => s_axi_wdata(12),
      Q => \^q\(12),
      R => \^clear\
    );
\slv_regs_reg[0][13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(15),
      D => s_axi_wdata(13),
      Q => \^q\(13),
      R => \^clear\
    );
\slv_regs_reg[0][14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(15),
      D => s_axi_wdata(14),
      Q => \^q\(14),
      R => \^clear\
    );
\slv_regs_reg[0][15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(15),
      D => s_axi_wdata(15),
      Q => \^q\(15),
      R => \^clear\
    );
\slv_regs_reg[0][16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(23),
      D => s_axi_wdata(16),
      Q => \slv_regs_reg_n_0_[0][16]\,
      R => \^clear\
    );
\slv_regs_reg[0][17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(23),
      D => s_axi_wdata(17),
      Q => \slv_regs_reg_n_0_[0][17]\,
      R => \^clear\
    );
\slv_regs_reg[0][18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(23),
      D => s_axi_wdata(18),
      Q => \slv_regs_reg_n_0_[0][18]\,
      R => \^clear\
    );
\slv_regs_reg[0][19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(23),
      D => s_axi_wdata(19),
      Q => \slv_regs_reg_n_0_[0][19]\,
      R => \^clear\
    );
\slv_regs_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(7),
      D => s_axi_wdata(1),
      Q => \^q\(1),
      R => \^clear\
    );
\slv_regs_reg[0][20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(23),
      D => s_axi_wdata(20),
      Q => \slv_regs_reg_n_0_[0][20]\,
      R => \^clear\
    );
\slv_regs_reg[0][21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(23),
      D => s_axi_wdata(21),
      Q => \slv_regs_reg_n_0_[0][21]\,
      R => \^clear\
    );
\slv_regs_reg[0][22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(23),
      D => s_axi_wdata(22),
      Q => \slv_regs_reg_n_0_[0][22]\,
      R => \^clear\
    );
\slv_regs_reg[0][23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(23),
      D => s_axi_wdata(23),
      Q => \slv_regs_reg_n_0_[0][23]\,
      R => \^clear\
    );
\slv_regs_reg[0][24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(31),
      D => s_axi_wdata(24),
      Q => \slv_regs_reg_n_0_[0][24]\,
      R => \^clear\
    );
\slv_regs_reg[0][25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(31),
      D => s_axi_wdata(25),
      Q => \slv_regs_reg_n_0_[0][25]\,
      R => \^clear\
    );
\slv_regs_reg[0][26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(31),
      D => s_axi_wdata(26),
      Q => \slv_regs_reg_n_0_[0][26]\,
      R => \^clear\
    );
\slv_regs_reg[0][27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(31),
      D => s_axi_wdata(27),
      Q => \slv_regs_reg_n_0_[0][27]\,
      R => \^clear\
    );
\slv_regs_reg[0][28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(31),
      D => s_axi_wdata(28),
      Q => \slv_regs_reg_n_0_[0][28]\,
      R => \^clear\
    );
\slv_regs_reg[0][29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(31),
      D => s_axi_wdata(29),
      Q => \slv_regs_reg_n_0_[0][29]\,
      R => \^clear\
    );
\slv_regs_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(7),
      D => s_axi_wdata(2),
      Q => \^q\(2),
      R => \^clear\
    );
\slv_regs_reg[0][30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(31),
      D => s_axi_wdata(30),
      Q => \slv_regs_reg_n_0_[0][30]\,
      R => \^clear\
    );
\slv_regs_reg[0][31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(31),
      D => s_axi_wdata(31),
      Q => \slv_regs_reg_n_0_[0][31]\,
      R => \^clear\
    );
\slv_regs_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(7),
      D => s_axi_wdata(3),
      Q => \^q\(3),
      R => \^clear\
    );
\slv_regs_reg[0][4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(7),
      D => s_axi_wdata(4),
      Q => \^q\(4),
      R => \^clear\
    );
\slv_regs_reg[0][5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(7),
      D => s_axi_wdata(5),
      Q => \^q\(5),
      R => \^clear\
    );
\slv_regs_reg[0][6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(7),
      D => s_axi_wdata(6),
      Q => \^q\(6),
      R => \^clear\
    );
\slv_regs_reg[0][7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(7),
      D => s_axi_wdata(7),
      Q => \^q\(7),
      R => \^clear\
    );
\slv_regs_reg[0][8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(15),
      D => s_axi_wdata(8),
      Q => \^q\(8),
      R => \^clear\
    );
\slv_regs_reg[0][9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_1_in(15),
      D => s_axi_wdata(9),
      Q => \^q\(9),
      R => \^clear\
    );
\slv_regs_reg[10][0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => \slv_regs_reg[10]\(0),
      R => \^clear\
    );
\slv_regs_reg[10][10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => \slv_regs_reg[10]\(10),
      R => \^clear\
    );
\slv_regs_reg[10][11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => \slv_regs_reg[10]\(11),
      R => \^clear\
    );
\slv_regs_reg[10][12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => \slv_regs_reg[10]\(12),
      R => \^clear\
    );
\slv_regs_reg[10][13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => \slv_regs_reg[10]\(13),
      R => \^clear\
    );
\slv_regs_reg[10][14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => \slv_regs_reg[10]\(14),
      R => \^clear\
    );
\slv_regs_reg[10][15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => \slv_regs_reg[10]\(15),
      R => \^clear\
    );
\slv_regs_reg[10][16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => \slv_regs_reg[10]\(16),
      R => \^clear\
    );
\slv_regs_reg[10][17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => \slv_regs_reg[10]\(17),
      R => \^clear\
    );
\slv_regs_reg[10][18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => \slv_regs_reg[10]\(18),
      R => \^clear\
    );
\slv_regs_reg[10][19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => \slv_regs_reg[10]\(19),
      R => \^clear\
    );
\slv_regs_reg[10][1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => \slv_regs_reg[10]\(1),
      R => \^clear\
    );
\slv_regs_reg[10][20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => \slv_regs_reg[10]\(20),
      R => \^clear\
    );
\slv_regs_reg[10][21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => \slv_regs_reg[10]\(21),
      R => \^clear\
    );
\slv_regs_reg[10][22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => \slv_regs_reg[10]\(22),
      R => \^clear\
    );
\slv_regs_reg[10][23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => \slv_regs_reg[10]\(23),
      R => \^clear\
    );
\slv_regs_reg[10][24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => \slv_regs_reg[10]\(24),
      R => \^clear\
    );
\slv_regs_reg[10][25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => \slv_regs_reg[10]\(25),
      R => \^clear\
    );
\slv_regs_reg[10][26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => \slv_regs_reg[10]\(26),
      R => \^clear\
    );
\slv_regs_reg[10][27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => \slv_regs_reg[10]\(27),
      R => \^clear\
    );
\slv_regs_reg[10][28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => \slv_regs_reg[10]\(28),
      R => \^clear\
    );
\slv_regs_reg[10][29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => \slv_regs_reg[10]\(29),
      R => \^clear\
    );
\slv_regs_reg[10][2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => \slv_regs_reg[10]\(2),
      R => \^clear\
    );
\slv_regs_reg[10][30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => \slv_regs_reg[10]\(30),
      R => \^clear\
    );
\slv_regs_reg[10][31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => \slv_regs_reg[10]\(31),
      R => \^clear\
    );
\slv_regs_reg[10][3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => \slv_regs_reg[10]\(3),
      R => \^clear\
    );
\slv_regs_reg[10][4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => \slv_regs_reg[10]\(4),
      R => \^clear\
    );
\slv_regs_reg[10][5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => \slv_regs_reg[10]\(5),
      R => \^clear\
    );
\slv_regs_reg[10][6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => \slv_regs_reg[10]\(6),
      R => \^clear\
    );
\slv_regs_reg[10][7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => \slv_regs_reg[10]\(7),
      R => \^clear\
    );
\slv_regs_reg[10][8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => \slv_regs_reg[10]\(8),
      R => \^clear\
    );
\slv_regs_reg[10][9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[10][15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => \slv_regs_reg[10]\(9),
      R => \^clear\
    );
\slv_regs_reg[11][0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => \slv_regs_reg[11]\(0),
      R => \^clear\
    );
\slv_regs_reg[11][10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => \slv_regs_reg[11]\(10),
      R => \^clear\
    );
\slv_regs_reg[11][11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => \slv_regs_reg[11]\(11),
      R => \^clear\
    );
\slv_regs_reg[11][12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => \slv_regs_reg[11]\(12),
      R => \^clear\
    );
\slv_regs_reg[11][13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => \slv_regs_reg[11]\(13),
      R => \^clear\
    );
\slv_regs_reg[11][14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => \slv_regs_reg[11]\(14),
      R => \^clear\
    );
\slv_regs_reg[11][15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => \slv_regs_reg[11]\(15),
      R => \^clear\
    );
\slv_regs_reg[11][16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => \slv_regs_reg[11]\(16),
      R => \^clear\
    );
\slv_regs_reg[11][17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => \slv_regs_reg[11]\(17),
      R => \^clear\
    );
\slv_regs_reg[11][18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => \slv_regs_reg[11]\(18),
      R => \^clear\
    );
\slv_regs_reg[11][19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => \slv_regs_reg[11]\(19),
      R => \^clear\
    );
\slv_regs_reg[11][1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => \slv_regs_reg[11]\(1),
      R => \^clear\
    );
\slv_regs_reg[11][20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => \slv_regs_reg[11]\(20),
      R => \^clear\
    );
\slv_regs_reg[11][21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => \slv_regs_reg[11]\(21),
      R => \^clear\
    );
\slv_regs_reg[11][22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => \slv_regs_reg[11]\(22),
      R => \^clear\
    );
\slv_regs_reg[11][23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => \slv_regs_reg[11]\(23),
      R => \^clear\
    );
\slv_regs_reg[11][24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => \slv_regs_reg[11]\(24),
      R => \^clear\
    );
\slv_regs_reg[11][25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => \slv_regs_reg[11]\(25),
      R => \^clear\
    );
\slv_regs_reg[11][26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => \slv_regs_reg[11]\(26),
      R => \^clear\
    );
\slv_regs_reg[11][27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => \slv_regs_reg[11]\(27),
      R => \^clear\
    );
\slv_regs_reg[11][28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => \slv_regs_reg[11]\(28),
      R => \^clear\
    );
\slv_regs_reg[11][29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => \slv_regs_reg[11]\(29),
      R => \^clear\
    );
\slv_regs_reg[11][2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => \slv_regs_reg[11]\(2),
      R => \^clear\
    );
\slv_regs_reg[11][30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => \slv_regs_reg[11]\(30),
      R => \^clear\
    );
\slv_regs_reg[11][31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => \slv_regs_reg[11]\(31),
      R => \^clear\
    );
\slv_regs_reg[11][3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => \slv_regs_reg[11]\(3),
      R => \^clear\
    );
\slv_regs_reg[11][4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => \slv_regs_reg[11]\(4),
      R => \^clear\
    );
\slv_regs_reg[11][5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => \slv_regs_reg[11]\(5),
      R => \^clear\
    );
\slv_regs_reg[11][6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => \slv_regs_reg[11]\(6),
      R => \^clear\
    );
\slv_regs_reg[11][7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => \slv_regs_reg[11]\(7),
      R => \^clear\
    );
\slv_regs_reg[11][8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => \slv_regs_reg[11]\(8),
      R => \^clear\
    );
\slv_regs_reg[11][9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[11][15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => \slv_regs_reg[11]\(9),
      R => \^clear\
    );
\slv_regs_reg[12][0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => \slv_regs_reg[12]\(0),
      R => \^clear\
    );
\slv_regs_reg[12][10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => \slv_regs_reg[12]\(10),
      R => \^clear\
    );
\slv_regs_reg[12][11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => \slv_regs_reg[12]\(11),
      R => \^clear\
    );
\slv_regs_reg[12][12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => \slv_regs_reg[12]\(12),
      R => \^clear\
    );
\slv_regs_reg[12][13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => \slv_regs_reg[12]\(13),
      R => \^clear\
    );
\slv_regs_reg[12][14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => \slv_regs_reg[12]\(14),
      R => \^clear\
    );
\slv_regs_reg[12][15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => \slv_regs_reg[12]\(15),
      R => \^clear\
    );
\slv_regs_reg[12][16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => \slv_regs_reg[12]\(16),
      R => \^clear\
    );
\slv_regs_reg[12][17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => \slv_regs_reg[12]\(17),
      R => \^clear\
    );
\slv_regs_reg[12][18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => \slv_regs_reg[12]\(18),
      R => \^clear\
    );
\slv_regs_reg[12][19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => \slv_regs_reg[12]\(19),
      R => \^clear\
    );
\slv_regs_reg[12][1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => \slv_regs_reg[12]\(1),
      R => \^clear\
    );
\slv_regs_reg[12][20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => \slv_regs_reg[12]\(20),
      R => \^clear\
    );
\slv_regs_reg[12][21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => \slv_regs_reg[12]\(21),
      R => \^clear\
    );
\slv_regs_reg[12][22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => \slv_regs_reg[12]\(22),
      R => \^clear\
    );
\slv_regs_reg[12][23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => \slv_regs_reg[12]\(23),
      R => \^clear\
    );
\slv_regs_reg[12][24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => \slv_regs_reg[12]\(24),
      R => \^clear\
    );
\slv_regs_reg[12][25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => \slv_regs_reg[12]\(25),
      R => \^clear\
    );
\slv_regs_reg[12][26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => \slv_regs_reg[12]\(26),
      R => \^clear\
    );
\slv_regs_reg[12][27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => \slv_regs_reg[12]\(27),
      R => \^clear\
    );
\slv_regs_reg[12][28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => \slv_regs_reg[12]\(28),
      R => \^clear\
    );
\slv_regs_reg[12][29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => \slv_regs_reg[12]\(29),
      R => \^clear\
    );
\slv_regs_reg[12][2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => \slv_regs_reg[12]\(2),
      R => \^clear\
    );
\slv_regs_reg[12][30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => \slv_regs_reg[12]\(30),
      R => \^clear\
    );
\slv_regs_reg[12][31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => \slv_regs_reg[12]\(31),
      R => \^clear\
    );
\slv_regs_reg[12][3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => \slv_regs_reg[12]\(3),
      R => \^clear\
    );
\slv_regs_reg[12][4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => \slv_regs_reg[12]\(4),
      R => \^clear\
    );
\slv_regs_reg[12][5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => \slv_regs_reg[12]\(5),
      R => \^clear\
    );
\slv_regs_reg[12][6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => \slv_regs_reg[12]\(6),
      R => \^clear\
    );
\slv_regs_reg[12][7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => \slv_regs_reg[12]\(7),
      R => \^clear\
    );
\slv_regs_reg[12][8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => \slv_regs_reg[12]\(8),
      R => \^clear\
    );
\slv_regs_reg[12][9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[12][15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => \slv_regs_reg[12]\(9),
      R => \^clear\
    );
\slv_regs_reg[13][0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => \slv_regs_reg[13]\(0),
      R => \^clear\
    );
\slv_regs_reg[13][10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => \slv_regs_reg[13]\(10),
      R => \^clear\
    );
\slv_regs_reg[13][11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => \slv_regs_reg[13]\(11),
      R => \^clear\
    );
\slv_regs_reg[13][12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => \slv_regs_reg[13]\(12),
      R => \^clear\
    );
\slv_regs_reg[13][13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => \slv_regs_reg[13]\(13),
      R => \^clear\
    );
\slv_regs_reg[13][14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => \slv_regs_reg[13]\(14),
      R => \^clear\
    );
\slv_regs_reg[13][15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => \slv_regs_reg[13]\(15),
      R => \^clear\
    );
\slv_regs_reg[13][16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => \slv_regs_reg[13]\(16),
      R => \^clear\
    );
\slv_regs_reg[13][17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => \slv_regs_reg[13]\(17),
      R => \^clear\
    );
\slv_regs_reg[13][18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => \slv_regs_reg[13]\(18),
      R => \^clear\
    );
\slv_regs_reg[13][19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => \slv_regs_reg[13]\(19),
      R => \^clear\
    );
\slv_regs_reg[13][1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => \slv_regs_reg[13]\(1),
      R => \^clear\
    );
\slv_regs_reg[13][20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => \slv_regs_reg[13]\(20),
      R => \^clear\
    );
\slv_regs_reg[13][21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => \slv_regs_reg[13]\(21),
      R => \^clear\
    );
\slv_regs_reg[13][22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => \slv_regs_reg[13]\(22),
      R => \^clear\
    );
\slv_regs_reg[13][23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => \slv_regs_reg[13]\(23),
      R => \^clear\
    );
\slv_regs_reg[13][24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => \slv_regs_reg[13]\(24),
      R => \^clear\
    );
\slv_regs_reg[13][25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => \slv_regs_reg[13]\(25),
      R => \^clear\
    );
\slv_regs_reg[13][26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => \slv_regs_reg[13]\(26),
      R => \^clear\
    );
\slv_regs_reg[13][27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => \slv_regs_reg[13]\(27),
      R => \^clear\
    );
\slv_regs_reg[13][28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => \slv_regs_reg[13]\(28),
      R => \^clear\
    );
\slv_regs_reg[13][29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => \slv_regs_reg[13]\(29),
      R => \^clear\
    );
\slv_regs_reg[13][2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => \slv_regs_reg[13]\(2),
      R => \^clear\
    );
\slv_regs_reg[13][30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => \slv_regs_reg[13]\(30),
      R => \^clear\
    );
\slv_regs_reg[13][31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => \slv_regs_reg[13]\(31),
      R => \^clear\
    );
\slv_regs_reg[13][3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => \slv_regs_reg[13]\(3),
      R => \^clear\
    );
\slv_regs_reg[13][4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => \slv_regs_reg[13]\(4),
      R => \^clear\
    );
\slv_regs_reg[13][5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => \slv_regs_reg[13]\(5),
      R => \^clear\
    );
\slv_regs_reg[13][6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => \slv_regs_reg[13]\(6),
      R => \^clear\
    );
\slv_regs_reg[13][7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => \slv_regs_reg[13]\(7),
      R => \^clear\
    );
\slv_regs_reg[13][8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => \slv_regs_reg[13]\(8),
      R => \^clear\
    );
\slv_regs_reg[13][9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[13][15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => \slv_regs_reg[13]\(9),
      R => \^clear\
    );
\slv_regs_reg[14][0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => \slv_regs_reg[14]\(0),
      R => \^clear\
    );
\slv_regs_reg[14][10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => \slv_regs_reg[14]\(10),
      R => \^clear\
    );
\slv_regs_reg[14][11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => \slv_regs_reg[14]\(11),
      R => \^clear\
    );
\slv_regs_reg[14][12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => \slv_regs_reg[14]\(12),
      R => \^clear\
    );
\slv_regs_reg[14][13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => \slv_regs_reg[14]\(13),
      R => \^clear\
    );
\slv_regs_reg[14][14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => \slv_regs_reg[14]\(14),
      R => \^clear\
    );
\slv_regs_reg[14][15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => \slv_regs_reg[14]\(15),
      R => \^clear\
    );
\slv_regs_reg[14][16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => \slv_regs_reg[14]\(16),
      R => \^clear\
    );
\slv_regs_reg[14][17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => \slv_regs_reg[14]\(17),
      R => \^clear\
    );
\slv_regs_reg[14][18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => \slv_regs_reg[14]\(18),
      R => \^clear\
    );
\slv_regs_reg[14][19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => \slv_regs_reg[14]\(19),
      R => \^clear\
    );
\slv_regs_reg[14][1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => \slv_regs_reg[14]\(1),
      R => \^clear\
    );
\slv_regs_reg[14][20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => \slv_regs_reg[14]\(20),
      R => \^clear\
    );
\slv_regs_reg[14][21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => \slv_regs_reg[14]\(21),
      R => \^clear\
    );
\slv_regs_reg[14][22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => \slv_regs_reg[14]\(22),
      R => \^clear\
    );
\slv_regs_reg[14][23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => \slv_regs_reg[14]\(23),
      R => \^clear\
    );
\slv_regs_reg[14][24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => \slv_regs_reg[14]\(24),
      R => \^clear\
    );
\slv_regs_reg[14][25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => \slv_regs_reg[14]\(25),
      R => \^clear\
    );
\slv_regs_reg[14][26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => \slv_regs_reg[14]\(26),
      R => \^clear\
    );
\slv_regs_reg[14][27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => \slv_regs_reg[14]\(27),
      R => \^clear\
    );
\slv_regs_reg[14][28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => \slv_regs_reg[14]\(28),
      R => \^clear\
    );
\slv_regs_reg[14][29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => \slv_regs_reg[14]\(29),
      R => \^clear\
    );
\slv_regs_reg[14][2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => \slv_regs_reg[14]\(2),
      R => \^clear\
    );
\slv_regs_reg[14][30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => \slv_regs_reg[14]\(30),
      R => \^clear\
    );
\slv_regs_reg[14][31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => \slv_regs_reg[14]\(31),
      R => \^clear\
    );
\slv_regs_reg[14][3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => \slv_regs_reg[14]\(3),
      R => \^clear\
    );
\slv_regs_reg[14][4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => \slv_regs_reg[14]\(4),
      R => \^clear\
    );
\slv_regs_reg[14][5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => \slv_regs_reg[14]\(5),
      R => \^clear\
    );
\slv_regs_reg[14][6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => \slv_regs_reg[14]\(6),
      R => \^clear\
    );
\slv_regs_reg[14][7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => \slv_regs_reg[14]\(7),
      R => \^clear\
    );
\slv_regs_reg[14][8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => \slv_regs_reg[14]\(8),
      R => \^clear\
    );
\slv_regs_reg[14][9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[14][15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => \slv_regs_reg[14]\(9),
      R => \^clear\
    );
\slv_regs_reg[15][0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => \slv_regs_reg[15]\(0),
      R => \^clear\
    );
\slv_regs_reg[15][10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => \slv_regs_reg[15]\(10),
      R => \^clear\
    );
\slv_regs_reg[15][11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => \slv_regs_reg[15]\(11),
      R => \^clear\
    );
\slv_regs_reg[15][12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => \slv_regs_reg[15]\(12),
      R => \^clear\
    );
\slv_regs_reg[15][13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => \slv_regs_reg[15]\(13),
      R => \^clear\
    );
\slv_regs_reg[15][14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => \slv_regs_reg[15]\(14),
      R => \^clear\
    );
\slv_regs_reg[15][15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => \slv_regs_reg[15]\(15),
      R => \^clear\
    );
\slv_regs_reg[15][16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => \slv_regs_reg[15]\(16),
      R => \^clear\
    );
\slv_regs_reg[15][17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => \slv_regs_reg[15]\(17),
      R => \^clear\
    );
\slv_regs_reg[15][18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => \slv_regs_reg[15]\(18),
      R => \^clear\
    );
\slv_regs_reg[15][19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => \slv_regs_reg[15]\(19),
      R => \^clear\
    );
\slv_regs_reg[15][1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => \slv_regs_reg[15]\(1),
      R => \^clear\
    );
\slv_regs_reg[15][20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => \slv_regs_reg[15]\(20),
      R => \^clear\
    );
\slv_regs_reg[15][21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => \slv_regs_reg[15]\(21),
      R => \^clear\
    );
\slv_regs_reg[15][22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => \slv_regs_reg[15]\(22),
      R => \^clear\
    );
\slv_regs_reg[15][23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => \slv_regs_reg[15]\(23),
      R => \^clear\
    );
\slv_regs_reg[15][24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => \slv_regs_reg[15]\(24),
      R => \^clear\
    );
\slv_regs_reg[15][25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => \slv_regs_reg[15]\(25),
      R => \^clear\
    );
\slv_regs_reg[15][26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => \slv_regs_reg[15]\(26),
      R => \^clear\
    );
\slv_regs_reg[15][27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => \slv_regs_reg[15]\(27),
      R => \^clear\
    );
\slv_regs_reg[15][28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => \slv_regs_reg[15]\(28),
      R => \^clear\
    );
\slv_regs_reg[15][29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => \slv_regs_reg[15]\(29),
      R => \^clear\
    );
\slv_regs_reg[15][2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => \slv_regs_reg[15]\(2),
      R => \^clear\
    );
\slv_regs_reg[15][30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => \slv_regs_reg[15]\(30),
      R => \^clear\
    );
\slv_regs_reg[15][31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => \slv_regs_reg[15]\(31),
      R => \^clear\
    );
\slv_regs_reg[15][3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => \slv_regs_reg[15]\(3),
      R => \^clear\
    );
\slv_regs_reg[15][4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => \slv_regs_reg[15]\(4),
      R => \^clear\
    );
\slv_regs_reg[15][5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => \slv_regs_reg[15]\(5),
      R => \^clear\
    );
\slv_regs_reg[15][6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => \slv_regs_reg[15]\(6),
      R => \^clear\
    );
\slv_regs_reg[15][7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => \slv_regs_reg[15]\(7),
      R => \^clear\
    );
\slv_regs_reg[15][8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => \slv_regs_reg[15]\(8),
      R => \^clear\
    );
\slv_regs_reg[15][9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[15][15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => \slv_regs_reg[15]\(9),
      R => \^clear\
    );
\slv_regs_reg[2][0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => \slv_regs_reg[2]\(0),
      R => \^clear\
    );
\slv_regs_reg[2][10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => \slv_regs_reg[2]\(10),
      R => \^clear\
    );
\slv_regs_reg[2][11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => \slv_regs_reg[2]\(11),
      R => \^clear\
    );
\slv_regs_reg[2][12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => \slv_regs_reg[2]\(12),
      R => \^clear\
    );
\slv_regs_reg[2][13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => \slv_regs_reg[2]\(13),
      R => \^clear\
    );
\slv_regs_reg[2][14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => \slv_regs_reg[2]\(14),
      R => \^clear\
    );
\slv_regs_reg[2][15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => \slv_regs_reg[2]\(15),
      R => \^clear\
    );
\slv_regs_reg[2][16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => \slv_regs_reg[2]\(16),
      R => \^clear\
    );
\slv_regs_reg[2][17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => \slv_regs_reg[2]\(17),
      R => \^clear\
    );
\slv_regs_reg[2][18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => \slv_regs_reg[2]\(18),
      R => \^clear\
    );
\slv_regs_reg[2][19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => \slv_regs_reg[2]\(19),
      R => \^clear\
    );
\slv_regs_reg[2][1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => \slv_regs_reg[2]\(1),
      R => \^clear\
    );
\slv_regs_reg[2][20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => \slv_regs_reg[2]\(20),
      R => \^clear\
    );
\slv_regs_reg[2][21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => \slv_regs_reg[2]\(21),
      R => \^clear\
    );
\slv_regs_reg[2][22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => \slv_regs_reg[2]\(22),
      R => \^clear\
    );
\slv_regs_reg[2][23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => \slv_regs_reg[2]\(23),
      R => \^clear\
    );
\slv_regs_reg[2][24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => \slv_regs_reg[2]\(24),
      R => \^clear\
    );
\slv_regs_reg[2][25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => \slv_regs_reg[2]\(25),
      R => \^clear\
    );
\slv_regs_reg[2][26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => \slv_regs_reg[2]\(26),
      R => \^clear\
    );
\slv_regs_reg[2][27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => \slv_regs_reg[2]\(27),
      R => \^clear\
    );
\slv_regs_reg[2][28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => \slv_regs_reg[2]\(28),
      R => \^clear\
    );
\slv_regs_reg[2][29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => \slv_regs_reg[2]\(29),
      R => \^clear\
    );
\slv_regs_reg[2][2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => \slv_regs_reg[2]\(2),
      R => \^clear\
    );
\slv_regs_reg[2][30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => \slv_regs_reg[2]\(30),
      R => \^clear\
    );
\slv_regs_reg[2][31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => \slv_regs_reg[2]\(31),
      R => \^clear\
    );
\slv_regs_reg[2][3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => \slv_regs_reg[2]\(3),
      R => \^clear\
    );
\slv_regs_reg[2][4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => \slv_regs_reg[2]\(4),
      R => \^clear\
    );
\slv_regs_reg[2][5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => \slv_regs_reg[2]\(5),
      R => \^clear\
    );
\slv_regs_reg[2][6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => \slv_regs_reg[2]\(6),
      R => \^clear\
    );
\slv_regs_reg[2][7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => \slv_regs_reg[2]\(7),
      R => \^clear\
    );
\slv_regs_reg[2][8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => \slv_regs_reg[2]\(8),
      R => \^clear\
    );
\slv_regs_reg[2][9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[2][15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => \slv_regs_reg[2]\(9),
      R => \^clear\
    );
\slv_regs_reg[3][0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => \slv_regs_reg[3]\(0),
      R => \^clear\
    );
\slv_regs_reg[3][10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => \slv_regs_reg[3]\(10),
      R => \^clear\
    );
\slv_regs_reg[3][11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => \slv_regs_reg[3]\(11),
      R => \^clear\
    );
\slv_regs_reg[3][12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => \slv_regs_reg[3]\(12),
      R => \^clear\
    );
\slv_regs_reg[3][13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => \slv_regs_reg[3]\(13),
      R => \^clear\
    );
\slv_regs_reg[3][14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => \slv_regs_reg[3]\(14),
      R => \^clear\
    );
\slv_regs_reg[3][15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => \slv_regs_reg[3]\(15),
      R => \^clear\
    );
\slv_regs_reg[3][16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => \slv_regs_reg[3]\(16),
      R => \^clear\
    );
\slv_regs_reg[3][17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => \slv_regs_reg[3]\(17),
      R => \^clear\
    );
\slv_regs_reg[3][18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => \slv_regs_reg[3]\(18),
      R => \^clear\
    );
\slv_regs_reg[3][19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => \slv_regs_reg[3]\(19),
      R => \^clear\
    );
\slv_regs_reg[3][1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => \slv_regs_reg[3]\(1),
      R => \^clear\
    );
\slv_regs_reg[3][20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => \slv_regs_reg[3]\(20),
      R => \^clear\
    );
\slv_regs_reg[3][21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => \slv_regs_reg[3]\(21),
      R => \^clear\
    );
\slv_regs_reg[3][22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => \slv_regs_reg[3]\(22),
      R => \^clear\
    );
\slv_regs_reg[3][23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => \slv_regs_reg[3]\(23),
      R => \^clear\
    );
\slv_regs_reg[3][24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => \slv_regs_reg[3]\(24),
      R => \^clear\
    );
\slv_regs_reg[3][25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => \slv_regs_reg[3]\(25),
      R => \^clear\
    );
\slv_regs_reg[3][26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => \slv_regs_reg[3]\(26),
      R => \^clear\
    );
\slv_regs_reg[3][27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => \slv_regs_reg[3]\(27),
      R => \^clear\
    );
\slv_regs_reg[3][28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => \slv_regs_reg[3]\(28),
      R => \^clear\
    );
\slv_regs_reg[3][29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => \slv_regs_reg[3]\(29),
      R => \^clear\
    );
\slv_regs_reg[3][2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => \slv_regs_reg[3]\(2),
      R => \^clear\
    );
\slv_regs_reg[3][30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => \slv_regs_reg[3]\(30),
      R => \^clear\
    );
\slv_regs_reg[3][31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => \slv_regs_reg[3]\(31),
      R => \^clear\
    );
\slv_regs_reg[3][3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => \slv_regs_reg[3]\(3),
      R => \^clear\
    );
\slv_regs_reg[3][4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => \slv_regs_reg[3]\(4),
      R => \^clear\
    );
\slv_regs_reg[3][5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => \slv_regs_reg[3]\(5),
      R => \^clear\
    );
\slv_regs_reg[3][6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => \slv_regs_reg[3]\(6),
      R => \^clear\
    );
\slv_regs_reg[3][7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => \slv_regs_reg[3]\(7),
      R => \^clear\
    );
\slv_regs_reg[3][8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => \slv_regs_reg[3]\(8),
      R => \^clear\
    );
\slv_regs_reg[3][9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[3][15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => \slv_regs_reg[3]\(9),
      R => \^clear\
    );
\slv_regs_reg[4][0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => \slv_regs_reg[4]\(0),
      R => \^clear\
    );
\slv_regs_reg[4][10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => \slv_regs_reg[4]\(10),
      R => \^clear\
    );
\slv_regs_reg[4][11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => \slv_regs_reg[4]\(11),
      R => \^clear\
    );
\slv_regs_reg[4][12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => \slv_regs_reg[4]\(12),
      R => \^clear\
    );
\slv_regs_reg[4][13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => \slv_regs_reg[4]\(13),
      R => \^clear\
    );
\slv_regs_reg[4][14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => \slv_regs_reg[4]\(14),
      R => \^clear\
    );
\slv_regs_reg[4][15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => \slv_regs_reg[4]\(15),
      R => \^clear\
    );
\slv_regs_reg[4][16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => \slv_regs_reg[4]\(16),
      R => \^clear\
    );
\slv_regs_reg[4][17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => \slv_regs_reg[4]\(17),
      R => \^clear\
    );
\slv_regs_reg[4][18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => \slv_regs_reg[4]\(18),
      R => \^clear\
    );
\slv_regs_reg[4][19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => \slv_regs_reg[4]\(19),
      R => \^clear\
    );
\slv_regs_reg[4][1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => \slv_regs_reg[4]\(1),
      R => \^clear\
    );
\slv_regs_reg[4][20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => \slv_regs_reg[4]\(20),
      R => \^clear\
    );
\slv_regs_reg[4][21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => \slv_regs_reg[4]\(21),
      R => \^clear\
    );
\slv_regs_reg[4][22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => \slv_regs_reg[4]\(22),
      R => \^clear\
    );
\slv_regs_reg[4][23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => \slv_regs_reg[4]\(23),
      R => \^clear\
    );
\slv_regs_reg[4][24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => \slv_regs_reg[4]\(24),
      R => \^clear\
    );
\slv_regs_reg[4][25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => \slv_regs_reg[4]\(25),
      R => \^clear\
    );
\slv_regs_reg[4][26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => \slv_regs_reg[4]\(26),
      R => \^clear\
    );
\slv_regs_reg[4][27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => \slv_regs_reg[4]\(27),
      R => \^clear\
    );
\slv_regs_reg[4][28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => \slv_regs_reg[4]\(28),
      R => \^clear\
    );
\slv_regs_reg[4][29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => \slv_regs_reg[4]\(29),
      R => \^clear\
    );
\slv_regs_reg[4][2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => \slv_regs_reg[4]\(2),
      R => \^clear\
    );
\slv_regs_reg[4][30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => \slv_regs_reg[4]\(30),
      R => \^clear\
    );
\slv_regs_reg[4][31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => \slv_regs_reg[4]\(31),
      R => \^clear\
    );
\slv_regs_reg[4][3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => \slv_regs_reg[4]\(3),
      R => \^clear\
    );
\slv_regs_reg[4][4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => \slv_regs_reg[4]\(4),
      R => \^clear\
    );
\slv_regs_reg[4][5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => \slv_regs_reg[4]\(5),
      R => \^clear\
    );
\slv_regs_reg[4][6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => \slv_regs_reg[4]\(6),
      R => \^clear\
    );
\slv_regs_reg[4][7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => \slv_regs_reg[4]\(7),
      R => \^clear\
    );
\slv_regs_reg[4][8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => \slv_regs_reg[4]\(8),
      R => \^clear\
    );
\slv_regs_reg[4][9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[4][15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => \slv_regs_reg[4]\(9),
      R => \^clear\
    );
\slv_regs_reg[5][0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => \slv_regs_reg[5]\(0),
      R => \^clear\
    );
\slv_regs_reg[5][10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => \slv_regs_reg[5]\(10),
      R => \^clear\
    );
\slv_regs_reg[5][11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => \slv_regs_reg[5]\(11),
      R => \^clear\
    );
\slv_regs_reg[5][12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => \slv_regs_reg[5]\(12),
      R => \^clear\
    );
\slv_regs_reg[5][13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => \slv_regs_reg[5]\(13),
      R => \^clear\
    );
\slv_regs_reg[5][14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => \slv_regs_reg[5]\(14),
      R => \^clear\
    );
\slv_regs_reg[5][15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => \slv_regs_reg[5]\(15),
      R => \^clear\
    );
\slv_regs_reg[5][16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => \slv_regs_reg[5]\(16),
      R => \^clear\
    );
\slv_regs_reg[5][17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => \slv_regs_reg[5]\(17),
      R => \^clear\
    );
\slv_regs_reg[5][18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => \slv_regs_reg[5]\(18),
      R => \^clear\
    );
\slv_regs_reg[5][19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => \slv_regs_reg[5]\(19),
      R => \^clear\
    );
\slv_regs_reg[5][1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => \slv_regs_reg[5]\(1),
      R => \^clear\
    );
\slv_regs_reg[5][20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => \slv_regs_reg[5]\(20),
      R => \^clear\
    );
\slv_regs_reg[5][21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => \slv_regs_reg[5]\(21),
      R => \^clear\
    );
\slv_regs_reg[5][22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => \slv_regs_reg[5]\(22),
      R => \^clear\
    );
\slv_regs_reg[5][23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => \slv_regs_reg[5]\(23),
      R => \^clear\
    );
\slv_regs_reg[5][24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => \slv_regs_reg[5]\(24),
      R => \^clear\
    );
\slv_regs_reg[5][25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => \slv_regs_reg[5]\(25),
      R => \^clear\
    );
\slv_regs_reg[5][26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => \slv_regs_reg[5]\(26),
      R => \^clear\
    );
\slv_regs_reg[5][27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => \slv_regs_reg[5]\(27),
      R => \^clear\
    );
\slv_regs_reg[5][28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => \slv_regs_reg[5]\(28),
      R => \^clear\
    );
\slv_regs_reg[5][29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => \slv_regs_reg[5]\(29),
      R => \^clear\
    );
\slv_regs_reg[5][2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => \slv_regs_reg[5]\(2),
      R => \^clear\
    );
\slv_regs_reg[5][30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => \slv_regs_reg[5]\(30),
      R => \^clear\
    );
\slv_regs_reg[5][31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => \slv_regs_reg[5]\(31),
      R => \^clear\
    );
\slv_regs_reg[5][3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => \slv_regs_reg[5]\(3),
      R => \^clear\
    );
\slv_regs_reg[5][4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => \slv_regs_reg[5]\(4),
      R => \^clear\
    );
\slv_regs_reg[5][5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => \slv_regs_reg[5]\(5),
      R => \^clear\
    );
\slv_regs_reg[5][6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => \slv_regs_reg[5]\(6),
      R => \^clear\
    );
\slv_regs_reg[5][7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => \slv_regs_reg[5]\(7),
      R => \^clear\
    );
\slv_regs_reg[5][8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => \slv_regs_reg[5]\(8),
      R => \^clear\
    );
\slv_regs_reg[5][9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[5][15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => \slv_regs_reg[5]\(9),
      R => \^clear\
    );
\slv_regs_reg[6][0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => \slv_regs_reg[6]\(0),
      R => \^clear\
    );
\slv_regs_reg[6][10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => \slv_regs_reg[6]\(10),
      R => \^clear\
    );
\slv_regs_reg[6][11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => \slv_regs_reg[6]\(11),
      R => \^clear\
    );
\slv_regs_reg[6][12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => \slv_regs_reg[6]\(12),
      R => \^clear\
    );
\slv_regs_reg[6][13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => \slv_regs_reg[6]\(13),
      R => \^clear\
    );
\slv_regs_reg[6][14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => \slv_regs_reg[6]\(14),
      R => \^clear\
    );
\slv_regs_reg[6][15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => \slv_regs_reg[6]\(15),
      R => \^clear\
    );
\slv_regs_reg[6][16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => \slv_regs_reg[6]\(16),
      R => \^clear\
    );
\slv_regs_reg[6][17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => \slv_regs_reg[6]\(17),
      R => \^clear\
    );
\slv_regs_reg[6][18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => \slv_regs_reg[6]\(18),
      R => \^clear\
    );
\slv_regs_reg[6][19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => \slv_regs_reg[6]\(19),
      R => \^clear\
    );
\slv_regs_reg[6][1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => \slv_regs_reg[6]\(1),
      R => \^clear\
    );
\slv_regs_reg[6][20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => \slv_regs_reg[6]\(20),
      R => \^clear\
    );
\slv_regs_reg[6][21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => \slv_regs_reg[6]\(21),
      R => \^clear\
    );
\slv_regs_reg[6][22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => \slv_regs_reg[6]\(22),
      R => \^clear\
    );
\slv_regs_reg[6][23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => \slv_regs_reg[6]\(23),
      R => \^clear\
    );
\slv_regs_reg[6][24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => \slv_regs_reg[6]\(24),
      R => \^clear\
    );
\slv_regs_reg[6][25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => \slv_regs_reg[6]\(25),
      R => \^clear\
    );
\slv_regs_reg[6][26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => \slv_regs_reg[6]\(26),
      R => \^clear\
    );
\slv_regs_reg[6][27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => \slv_regs_reg[6]\(27),
      R => \^clear\
    );
\slv_regs_reg[6][28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => \slv_regs_reg[6]\(28),
      R => \^clear\
    );
\slv_regs_reg[6][29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => \slv_regs_reg[6]\(29),
      R => \^clear\
    );
\slv_regs_reg[6][2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => \slv_regs_reg[6]\(2),
      R => \^clear\
    );
\slv_regs_reg[6][30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => \slv_regs_reg[6]\(30),
      R => \^clear\
    );
\slv_regs_reg[6][31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => \slv_regs_reg[6]\(31),
      R => \^clear\
    );
\slv_regs_reg[6][3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => \slv_regs_reg[6]\(3),
      R => \^clear\
    );
\slv_regs_reg[6][4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => \slv_regs_reg[6]\(4),
      R => \^clear\
    );
\slv_regs_reg[6][5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => \slv_regs_reg[6]\(5),
      R => \^clear\
    );
\slv_regs_reg[6][6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => \slv_regs_reg[6]\(6),
      R => \^clear\
    );
\slv_regs_reg[6][7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => \slv_regs_reg[6]\(7),
      R => \^clear\
    );
\slv_regs_reg[6][8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => \slv_regs_reg[6]\(8),
      R => \^clear\
    );
\slv_regs_reg[6][9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[6][15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => \slv_regs_reg[6]\(9),
      R => \^clear\
    );
\slv_regs_reg[7][0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => \slv_regs_reg[7]\(0),
      R => \^clear\
    );
\slv_regs_reg[7][10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => \slv_regs_reg[7]\(10),
      R => \^clear\
    );
\slv_regs_reg[7][11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => \slv_regs_reg[7]\(11),
      R => \^clear\
    );
\slv_regs_reg[7][12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => \slv_regs_reg[7]\(12),
      R => \^clear\
    );
\slv_regs_reg[7][13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => \slv_regs_reg[7]\(13),
      R => \^clear\
    );
\slv_regs_reg[7][14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => \slv_regs_reg[7]\(14),
      R => \^clear\
    );
\slv_regs_reg[7][15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => \slv_regs_reg[7]\(15),
      R => \^clear\
    );
\slv_regs_reg[7][16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => \slv_regs_reg[7]\(16),
      R => \^clear\
    );
\slv_regs_reg[7][17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => \slv_regs_reg[7]\(17),
      R => \^clear\
    );
\slv_regs_reg[7][18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => \slv_regs_reg[7]\(18),
      R => \^clear\
    );
\slv_regs_reg[7][19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => \slv_regs_reg[7]\(19),
      R => \^clear\
    );
\slv_regs_reg[7][1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => \slv_regs_reg[7]\(1),
      R => \^clear\
    );
\slv_regs_reg[7][20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => \slv_regs_reg[7]\(20),
      R => \^clear\
    );
\slv_regs_reg[7][21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => \slv_regs_reg[7]\(21),
      R => \^clear\
    );
\slv_regs_reg[7][22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => \slv_regs_reg[7]\(22),
      R => \^clear\
    );
\slv_regs_reg[7][23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => \slv_regs_reg[7]\(23),
      R => \^clear\
    );
\slv_regs_reg[7][24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => \slv_regs_reg[7]\(24),
      R => \^clear\
    );
\slv_regs_reg[7][25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => \slv_regs_reg[7]\(25),
      R => \^clear\
    );
\slv_regs_reg[7][26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => \slv_regs_reg[7]\(26),
      R => \^clear\
    );
\slv_regs_reg[7][27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => \slv_regs_reg[7]\(27),
      R => \^clear\
    );
\slv_regs_reg[7][28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => \slv_regs_reg[7]\(28),
      R => \^clear\
    );
\slv_regs_reg[7][29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => \slv_regs_reg[7]\(29),
      R => \^clear\
    );
\slv_regs_reg[7][2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => \slv_regs_reg[7]\(2),
      R => \^clear\
    );
\slv_regs_reg[7][30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => \slv_regs_reg[7]\(30),
      R => \^clear\
    );
\slv_regs_reg[7][31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => \slv_regs_reg[7]\(31),
      R => \^clear\
    );
\slv_regs_reg[7][3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => \slv_regs_reg[7]\(3),
      R => \^clear\
    );
\slv_regs_reg[7][4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => \slv_regs_reg[7]\(4),
      R => \^clear\
    );
\slv_regs_reg[7][5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => \slv_regs_reg[7]\(5),
      R => \^clear\
    );
\slv_regs_reg[7][6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => \slv_regs_reg[7]\(6),
      R => \^clear\
    );
\slv_regs_reg[7][7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => \slv_regs_reg[7]\(7),
      R => \^clear\
    );
\slv_regs_reg[7][8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => \slv_regs_reg[7]\(8),
      R => \^clear\
    );
\slv_regs_reg[7][9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[7][15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => \slv_regs_reg[7]\(9),
      R => \^clear\
    );
\slv_regs_reg[8][0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => \slv_regs_reg[8]\(0),
      R => \^clear\
    );
\slv_regs_reg[8][10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => \slv_regs_reg[8]\(10),
      R => \^clear\
    );
\slv_regs_reg[8][11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => \slv_regs_reg[8]\(11),
      R => \^clear\
    );
\slv_regs_reg[8][12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => \slv_regs_reg[8]\(12),
      R => \^clear\
    );
\slv_regs_reg[8][13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => \slv_regs_reg[8]\(13),
      R => \^clear\
    );
\slv_regs_reg[8][14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => \slv_regs_reg[8]\(14),
      R => \^clear\
    );
\slv_regs_reg[8][15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => \slv_regs_reg[8]\(15),
      R => \^clear\
    );
\slv_regs_reg[8][16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => \slv_regs_reg[8]\(16),
      R => \^clear\
    );
\slv_regs_reg[8][17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => \slv_regs_reg[8]\(17),
      R => \^clear\
    );
\slv_regs_reg[8][18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => \slv_regs_reg[8]\(18),
      R => \^clear\
    );
\slv_regs_reg[8][19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => \slv_regs_reg[8]\(19),
      R => \^clear\
    );
\slv_regs_reg[8][1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => \slv_regs_reg[8]\(1),
      R => \^clear\
    );
\slv_regs_reg[8][20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => \slv_regs_reg[8]\(20),
      R => \^clear\
    );
\slv_regs_reg[8][21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => \slv_regs_reg[8]\(21),
      R => \^clear\
    );
\slv_regs_reg[8][22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => \slv_regs_reg[8]\(22),
      R => \^clear\
    );
\slv_regs_reg[8][23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => \slv_regs_reg[8]\(23),
      R => \^clear\
    );
\slv_regs_reg[8][24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => \slv_regs_reg[8]\(24),
      R => \^clear\
    );
\slv_regs_reg[8][25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => \slv_regs_reg[8]\(25),
      R => \^clear\
    );
\slv_regs_reg[8][26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => \slv_regs_reg[8]\(26),
      R => \^clear\
    );
\slv_regs_reg[8][27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => \slv_regs_reg[8]\(27),
      R => \^clear\
    );
\slv_regs_reg[8][28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => \slv_regs_reg[8]\(28),
      R => \^clear\
    );
\slv_regs_reg[8][29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => \slv_regs_reg[8]\(29),
      R => \^clear\
    );
\slv_regs_reg[8][2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => \slv_regs_reg[8]\(2),
      R => \^clear\
    );
\slv_regs_reg[8][30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => \slv_regs_reg[8]\(30),
      R => \^clear\
    );
\slv_regs_reg[8][31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => \slv_regs_reg[8]\(31),
      R => \^clear\
    );
\slv_regs_reg[8][3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => \slv_regs_reg[8]\(3),
      R => \^clear\
    );
\slv_regs_reg[8][4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => \slv_regs_reg[8]\(4),
      R => \^clear\
    );
\slv_regs_reg[8][5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => \slv_regs_reg[8]\(5),
      R => \^clear\
    );
\slv_regs_reg[8][6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => \slv_regs_reg[8]\(6),
      R => \^clear\
    );
\slv_regs_reg[8][7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => \slv_regs_reg[8]\(7),
      R => \^clear\
    );
\slv_regs_reg[8][8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => \slv_regs_reg[8]\(8),
      R => \^clear\
    );
\slv_regs_reg[8][9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[8][15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => \slv_regs_reg[8]\(9),
      R => \^clear\
    );
\slv_regs_reg[9][0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => \slv_regs_reg[9]\(0),
      R => \^clear\
    );
\slv_regs_reg[9][10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => \slv_regs_reg[9]\(10),
      R => \^clear\
    );
\slv_regs_reg[9][11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => \slv_regs_reg[9]\(11),
      R => \^clear\
    );
\slv_regs_reg[9][12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => \slv_regs_reg[9]\(12),
      R => \^clear\
    );
\slv_regs_reg[9][13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => \slv_regs_reg[9]\(13),
      R => \^clear\
    );
\slv_regs_reg[9][14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => \slv_regs_reg[9]\(14),
      R => \^clear\
    );
\slv_regs_reg[9][15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => \slv_regs_reg[9]\(15),
      R => \^clear\
    );
\slv_regs_reg[9][16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => \slv_regs_reg[9]\(16),
      R => \^clear\
    );
\slv_regs_reg[9][17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => \slv_regs_reg[9]\(17),
      R => \^clear\
    );
\slv_regs_reg[9][18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => \slv_regs_reg[9]\(18),
      R => \^clear\
    );
\slv_regs_reg[9][19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => \slv_regs_reg[9]\(19),
      R => \^clear\
    );
\slv_regs_reg[9][1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => \slv_regs_reg[9]\(1),
      R => \^clear\
    );
\slv_regs_reg[9][20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => \slv_regs_reg[9]\(20),
      R => \^clear\
    );
\slv_regs_reg[9][21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => \slv_regs_reg[9]\(21),
      R => \^clear\
    );
\slv_regs_reg[9][22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => \slv_regs_reg[9]\(22),
      R => \^clear\
    );
\slv_regs_reg[9][23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => \slv_regs_reg[9]\(23),
      R => \^clear\
    );
\slv_regs_reg[9][24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => \slv_regs_reg[9]\(24),
      R => \^clear\
    );
\slv_regs_reg[9][25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => \slv_regs_reg[9]\(25),
      R => \^clear\
    );
\slv_regs_reg[9][26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => \slv_regs_reg[9]\(26),
      R => \^clear\
    );
\slv_regs_reg[9][27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => \slv_regs_reg[9]\(27),
      R => \^clear\
    );
\slv_regs_reg[9][28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => \slv_regs_reg[9]\(28),
      R => \^clear\
    );
\slv_regs_reg[9][29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => \slv_regs_reg[9]\(29),
      R => \^clear\
    );
\slv_regs_reg[9][2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => \slv_regs_reg[9]\(2),
      R => \^clear\
    );
\slv_regs_reg[9][30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => \slv_regs_reg[9]\(30),
      R => \^clear\
    );
\slv_regs_reg[9][31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => \slv_regs_reg[9]\(31),
      R => \^clear\
    );
\slv_regs_reg[9][3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => \slv_regs_reg[9]\(3),
      R => \^clear\
    );
\slv_regs_reg[9][4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => \slv_regs_reg[9]\(4),
      R => \^clear\
    );
\slv_regs_reg[9][5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => \slv_regs_reg[9]\(5),
      R => \^clear\
    );
\slv_regs_reg[9][6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => \slv_regs_reg[9]\(6),
      R => \^clear\
    );
\slv_regs_reg[9][7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => \slv_regs_reg[9]\(7),
      R => \^clear\
    );
\slv_regs_reg[9][8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => \slv_regs_reg[9]\(8),
      R => \^clear\
    );
\slv_regs_reg[9][9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \slv_regs[9][15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => \slv_regs_reg[9]\(9),
      R => \^clear\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adder_top is
  port (
    s_axi_awready : out STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axis_tready : out STD_LOGIC;
    m_axis_tvalid : out STD_LOGIC;
    debug_leds : out STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axis_tdata : out STD_LOGIC_VECTOR ( 16 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    m_axis_tlast : out STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_arvalid : in STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    clk : in STD_LOGIC;
    aresetn : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axis_tvalid : in STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_bready : in STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axis_tlast : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adder_top;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adder_top is
  signal clear : STD_LOGIC;
  signal count : STD_LOGIC_VECTOR ( 15 downto 8 );
  signal \count[3]_i_2_n_0\ : STD_LOGIC;
  signal count_i : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \count_i[0]_i_2_n_0\ : STD_LOGIC;
  signal count_i_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \count_i_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \count_i_reg[0]_i_1_n_1\ : STD_LOGIC;
  signal \count_i_reg[0]_i_1_n_2\ : STD_LOGIC;
  signal \count_i_reg[0]_i_1_n_3\ : STD_LOGIC;
  signal \count_i_reg[0]_i_1_n_4\ : STD_LOGIC;
  signal \count_i_reg[0]_i_1_n_5\ : STD_LOGIC;
  signal \count_i_reg[0]_i_1_n_6\ : STD_LOGIC;
  signal \count_i_reg[0]_i_1_n_7\ : STD_LOGIC;
  signal \count_i_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \count_i_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \count_i_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \count_i_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \count_i_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \count_i_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \count_i_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \count_i_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \count_i_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \count_i_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \count_i_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \count_i_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \count_i_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \count_i_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \count_i_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \count_i_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \count_i_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \count_i_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \count_i_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \count_i_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \count_i_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \count_i_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \count_i_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \count_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \count_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \count_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \count_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \count_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \count_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \count_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \count_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \count_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \count_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \count_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \count_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \count_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \count_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \count_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \^debug_leds\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal i_register_interface_n_10 : STD_LOGIC;
  signal i_register_interface_n_11 : STD_LOGIC;
  signal i_register_interface_n_12 : STD_LOGIC;
  signal i_register_interface_n_13 : STD_LOGIC;
  signal i_register_interface_n_14 : STD_LOGIC;
  signal i_register_interface_n_15 : STD_LOGIC;
  signal i_register_interface_n_16 : STD_LOGIC;
  signal i_register_interface_n_17 : STD_LOGIC;
  signal i_register_interface_n_18 : STD_LOGIC;
  signal i_register_interface_n_19 : STD_LOGIC;
  signal i_register_interface_n_20 : STD_LOGIC;
  signal i_register_interface_n_21 : STD_LOGIC;
  signal i_register_interface_n_22 : STD_LOGIC;
  signal i_register_interface_n_23 : STD_LOGIC;
  signal i_register_interface_n_24 : STD_LOGIC;
  signal i_register_interface_n_25 : STD_LOGIC;
  signal i_register_interface_n_26 : STD_LOGIC;
  signal i_register_interface_n_27 : STD_LOGIC;
  signal i_register_interface_n_28 : STD_LOGIC;
  signal i_register_interface_n_29 : STD_LOGIC;
  signal i_register_interface_n_30 : STD_LOGIC;
  signal i_register_interface_n_31 : STD_LOGIC;
  signal i_register_interface_n_32 : STD_LOGIC;
  signal i_register_interface_n_33 : STD_LOGIC;
  signal i_register_interface_n_34 : STD_LOGIC;
  signal i_register_interface_n_35 : STD_LOGIC;
  signal i_register_interface_n_36 : STD_LOGIC;
  signal i_register_interface_n_37 : STD_LOGIC;
  signal i_register_interface_n_38 : STD_LOGIC;
  signal i_register_interface_n_6 : STD_LOGIC;
  signal i_register_interface_n_7 : STD_LOGIC;
  signal i_register_interface_n_8 : STD_LOGIC;
  signal i_register_interface_n_9 : STD_LOGIC;
  signal in_handshake : STD_LOGIC;
  signal \^m_axis_tlast\ : STD_LOGIC;
  signal \^m_axis_tvalid\ : STD_LOGIC;
  signal \out_data[11]_i_2_n_0\ : STD_LOGIC;
  signal \out_data[11]_i_3_n_0\ : STD_LOGIC;
  signal \out_data[11]_i_4_n_0\ : STD_LOGIC;
  signal \out_data[11]_i_5_n_0\ : STD_LOGIC;
  signal \out_data[15]_i_2_n_0\ : STD_LOGIC;
  signal \out_data[15]_i_3_n_0\ : STD_LOGIC;
  signal \out_data[15]_i_4_n_0\ : STD_LOGIC;
  signal \out_data[15]_i_5_n_0\ : STD_LOGIC;
  signal \out_data[3]_i_2_n_0\ : STD_LOGIC;
  signal \out_data[3]_i_3_n_0\ : STD_LOGIC;
  signal \out_data[3]_i_4_n_0\ : STD_LOGIC;
  signal \out_data[3]_i_5_n_0\ : STD_LOGIC;
  signal \out_data[7]_i_2_n_0\ : STD_LOGIC;
  signal \out_data[7]_i_3_n_0\ : STD_LOGIC;
  signal \out_data[7]_i_4_n_0\ : STD_LOGIC;
  signal \out_data[7]_i_5_n_0\ : STD_LOGIC;
  signal out_last_i_1_n_0 : STD_LOGIC;
  signal out_valid_i_1_n_0 : STD_LOGIC;
  signal \NLW_count_i_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_count_reg[15]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of out_last_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of out_valid_i_1 : label is "soft_lutpair0";
begin
  debug_leds(7 downto 0) <= \^debug_leds\(7 downto 0);
  m_axis_tlast <= \^m_axis_tlast\;
  m_axis_tvalid <= \^m_axis_tvalid\;
\count[3]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"65AA"
    )
        port map (
      I0 => count_i_reg(0),
      I1 => m_axis_tready,
      I2 => \^m_axis_tvalid\,
      I3 => s_axis_tvalid,
      O => \count[3]_i_2_n_0\
    );
\count_i[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4FB0"
    )
        port map (
      I0 => m_axis_tready,
      I1 => \^m_axis_tvalid\,
      I2 => s_axis_tvalid,
      I3 => count_i_reg(0),
      O => \count_i[0]_i_2_n_0\
    );
\count_i_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \count_i_reg[0]_i_1_n_7\,
      Q => count_i_reg(0),
      R => clear
    );
\count_i_reg[0]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count_i_reg[0]_i_1_n_0\,
      CO(2) => \count_i_reg[0]_i_1_n_1\,
      CO(1) => \count_i_reg[0]_i_1_n_2\,
      CO(0) => \count_i_reg[0]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => count_i_reg(0),
      O(3) => \count_i_reg[0]_i_1_n_4\,
      O(2) => \count_i_reg[0]_i_1_n_5\,
      O(1) => \count_i_reg[0]_i_1_n_6\,
      O(0) => \count_i_reg[0]_i_1_n_7\,
      S(3 downto 1) => count_i_reg(3 downto 1),
      S(0) => \count_i[0]_i_2_n_0\
    );
\count_i_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \count_i_reg[8]_i_1_n_5\,
      Q => count_i_reg(10),
      R => clear
    );
\count_i_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \count_i_reg[8]_i_1_n_4\,
      Q => count_i_reg(11),
      R => clear
    );
\count_i_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \count_i_reg[12]_i_1_n_7\,
      Q => count_i_reg(12),
      R => clear
    );
\count_i_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_i_reg[8]_i_1_n_0\,
      CO(3) => \NLW_count_i_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \count_i_reg[12]_i_1_n_1\,
      CO(1) => \count_i_reg[12]_i_1_n_2\,
      CO(0) => \count_i_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_i_reg[12]_i_1_n_4\,
      O(2) => \count_i_reg[12]_i_1_n_5\,
      O(1) => \count_i_reg[12]_i_1_n_6\,
      O(0) => \count_i_reg[12]_i_1_n_7\,
      S(3 downto 0) => count_i_reg(15 downto 12)
    );
\count_i_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \count_i_reg[12]_i_1_n_6\,
      Q => count_i_reg(13),
      R => clear
    );
\count_i_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \count_i_reg[12]_i_1_n_5\,
      Q => count_i_reg(14),
      R => clear
    );
\count_i_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \count_i_reg[12]_i_1_n_4\,
      Q => count_i_reg(15),
      R => clear
    );
\count_i_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \count_i_reg[0]_i_1_n_6\,
      Q => count_i_reg(1),
      R => clear
    );
\count_i_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \count_i_reg[0]_i_1_n_5\,
      Q => count_i_reg(2),
      R => clear
    );
\count_i_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \count_i_reg[0]_i_1_n_4\,
      Q => count_i_reg(3),
      R => clear
    );
\count_i_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \count_i_reg[4]_i_1_n_7\,
      Q => count_i_reg(4),
      R => clear
    );
\count_i_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_i_reg[0]_i_1_n_0\,
      CO(3) => \count_i_reg[4]_i_1_n_0\,
      CO(2) => \count_i_reg[4]_i_1_n_1\,
      CO(1) => \count_i_reg[4]_i_1_n_2\,
      CO(0) => \count_i_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_i_reg[4]_i_1_n_4\,
      O(2) => \count_i_reg[4]_i_1_n_5\,
      O(1) => \count_i_reg[4]_i_1_n_6\,
      O(0) => \count_i_reg[4]_i_1_n_7\,
      S(3 downto 0) => count_i_reg(7 downto 4)
    );
\count_i_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \count_i_reg[4]_i_1_n_6\,
      Q => count_i_reg(5),
      R => clear
    );
\count_i_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \count_i_reg[4]_i_1_n_5\,
      Q => count_i_reg(6),
      R => clear
    );
\count_i_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \count_i_reg[4]_i_1_n_4\,
      Q => count_i_reg(7),
      R => clear
    );
\count_i_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \count_i_reg[8]_i_1_n_7\,
      Q => count_i_reg(8),
      R => clear
    );
\count_i_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_i_reg[4]_i_1_n_0\,
      CO(3) => \count_i_reg[8]_i_1_n_0\,
      CO(2) => \count_i_reg[8]_i_1_n_1\,
      CO(1) => \count_i_reg[8]_i_1_n_2\,
      CO(0) => \count_i_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_i_reg[8]_i_1_n_4\,
      O(2) => \count_i_reg[8]_i_1_n_5\,
      O(1) => \count_i_reg[8]_i_1_n_6\,
      O(0) => \count_i_reg[8]_i_1_n_7\,
      S(3 downto 0) => count_i_reg(11 downto 8)
    );
\count_i_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \count_i_reg[8]_i_1_n_6\,
      Q => count_i_reg(9),
      R => clear
    );
\count_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => aresetn,
      D => count_i(0),
      Q => \^debug_leds\(0),
      R => '0'
    );
\count_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => aresetn,
      D => count_i(10),
      Q => count(10),
      R => '0'
    );
\count_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => aresetn,
      D => count_i(11),
      Q => count(11),
      R => '0'
    );
\count_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_reg[7]_i_1_n_0\,
      CO(3) => \count_reg[11]_i_1_n_0\,
      CO(2) => \count_reg[11]_i_1_n_1\,
      CO(1) => \count_reg[11]_i_1_n_2\,
      CO(0) => \count_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count_i(11 downto 8),
      S(3 downto 0) => count_i_reg(11 downto 8)
    );
\count_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => aresetn,
      D => count_i(12),
      Q => count(12),
      R => '0'
    );
\count_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => aresetn,
      D => count_i(13),
      Q => count(13),
      R => '0'
    );
\count_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => aresetn,
      D => count_i(14),
      Q => count(14),
      R => '0'
    );
\count_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => aresetn,
      D => count_i(15),
      Q => count(15),
      R => '0'
    );
\count_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_reg[11]_i_1_n_0\,
      CO(3) => \NLW_count_reg[15]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \count_reg[15]_i_1_n_1\,
      CO(1) => \count_reg[15]_i_1_n_2\,
      CO(0) => \count_reg[15]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count_i(15 downto 12),
      S(3 downto 0) => count_i_reg(15 downto 12)
    );
\count_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => aresetn,
      D => count_i(1),
      Q => \^debug_leds\(1),
      R => '0'
    );
\count_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => aresetn,
      D => count_i(2),
      Q => \^debug_leds\(2),
      R => '0'
    );
\count_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => aresetn,
      D => count_i(3),
      Q => \^debug_leds\(3),
      R => '0'
    );
\count_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count_reg[3]_i_1_n_0\,
      CO(2) => \count_reg[3]_i_1_n_1\,
      CO(1) => \count_reg[3]_i_1_n_2\,
      CO(0) => \count_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => count_i_reg(0),
      O(3 downto 0) => count_i(3 downto 0),
      S(3 downto 1) => count_i_reg(3 downto 1),
      S(0) => \count[3]_i_2_n_0\
    );
\count_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => aresetn,
      D => count_i(4),
      Q => \^debug_leds\(4),
      R => '0'
    );
\count_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => aresetn,
      D => count_i(5),
      Q => \^debug_leds\(5),
      R => '0'
    );
\count_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => aresetn,
      D => count_i(6),
      Q => \^debug_leds\(6),
      R => '0'
    );
\count_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => aresetn,
      D => count_i(7),
      Q => \^debug_leds\(7),
      R => '0'
    );
\count_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_reg[3]_i_1_n_0\,
      CO(3) => \count_reg[7]_i_1_n_0\,
      CO(2) => \count_reg[7]_i_1_n_1\,
      CO(1) => \count_reg[7]_i_1_n_2\,
      CO(0) => \count_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => count_i(7 downto 4),
      S(3 downto 0) => count_i_reg(7 downto 4)
    );
\count_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => aresetn,
      D => count_i(8),
      Q => count(8),
      R => '0'
    );
\count_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => aresetn,
      D => count_i(9),
      Q => count(9),
      R => '0'
    );
i_register_interface: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_register_interface
     port map (
      D(16) => i_register_interface_n_6,
      D(15) => i_register_interface_n_7,
      D(14) => i_register_interface_n_8,
      D(13) => i_register_interface_n_9,
      D(12) => i_register_interface_n_10,
      D(11) => i_register_interface_n_11,
      D(10) => i_register_interface_n_12,
      D(9) => i_register_interface_n_13,
      D(8) => i_register_interface_n_14,
      D(7) => i_register_interface_n_15,
      D(6) => i_register_interface_n_16,
      D(5) => i_register_interface_n_17,
      D(4) => i_register_interface_n_18,
      D(3) => i_register_interface_n_19,
      D(2) => i_register_interface_n_20,
      D(1) => i_register_interface_n_21,
      D(0) => i_register_interface_n_22,
      Q(15) => i_register_interface_n_23,
      Q(14) => i_register_interface_n_24,
      Q(13) => i_register_interface_n_25,
      Q(12) => i_register_interface_n_26,
      Q(11) => i_register_interface_n_27,
      Q(10) => i_register_interface_n_28,
      Q(9) => i_register_interface_n_29,
      Q(8) => i_register_interface_n_30,
      Q(7) => i_register_interface_n_31,
      Q(6) => i_register_interface_n_32,
      Q(5) => i_register_interface_n_33,
      Q(4) => i_register_interface_n_34,
      Q(3) => i_register_interface_n_35,
      Q(2) => i_register_interface_n_36,
      Q(1) => i_register_interface_n_37,
      Q(0) => i_register_interface_n_38,
      S(3) => \out_data[3]_i_2_n_0\,
      S(2) => \out_data[3]_i_3_n_0\,
      S(1) => \out_data[3]_i_4_n_0\,
      S(0) => \out_data[3]_i_5_n_0\,
      aresetn => aresetn,
      clear => clear,
      clk => clk,
      \count_reg[15]\(15 downto 8) => count(15 downto 8),
      \count_reg[15]\(7 downto 0) => \^debug_leds\(7 downto 0),
      s_axi_araddr(3 downto 0) => s_axi_araddr(3 downto 0),
      s_axi_arready => s_axi_arready,
      s_axi_arvalid => s_axi_arvalid,
      s_axi_awaddr(3 downto 0) => s_axi_awaddr(3 downto 0),
      s_axi_awready => s_axi_awready,
      s_axi_awvalid => s_axi_awvalid,
      s_axi_bready => s_axi_bready,
      s_axi_bvalid => s_axi_bvalid,
      s_axi_rdata(31 downto 0) => s_axi_rdata(31 downto 0),
      s_axi_rready => s_axi_rready,
      s_axi_rvalid => s_axi_rvalid,
      s_axi_wdata(31 downto 0) => s_axi_wdata(31 downto 0),
      s_axi_wready => s_axi_wready,
      s_axi_wstrb(3 downto 0) => s_axi_wstrb(3 downto 0),
      s_axi_wvalid => s_axi_wvalid,
      s_axis_tdata(15 downto 0) => s_axis_tdata(15 downto 0),
      \slv_regs_reg[0][11]_0\(3) => \out_data[11]_i_2_n_0\,
      \slv_regs_reg[0][11]_0\(2) => \out_data[11]_i_3_n_0\,
      \slv_regs_reg[0][11]_0\(1) => \out_data[11]_i_4_n_0\,
      \slv_regs_reg[0][11]_0\(0) => \out_data[11]_i_5_n_0\,
      \slv_regs_reg[0][15]_0\(3) => \out_data[15]_i_2_n_0\,
      \slv_regs_reg[0][15]_0\(2) => \out_data[15]_i_3_n_0\,
      \slv_regs_reg[0][15]_0\(1) => \out_data[15]_i_4_n_0\,
      \slv_regs_reg[0][15]_0\(0) => \out_data[15]_i_5_n_0\,
      \slv_regs_reg[0][7]_0\(3) => \out_data[7]_i_2_n_0\,
      \slv_regs_reg[0][7]_0\(2) => \out_data[7]_i_3_n_0\,
      \slv_regs_reg[0][7]_0\(1) => \out_data[7]_i_4_n_0\,
      \slv_regs_reg[0][7]_0\(0) => \out_data[7]_i_5_n_0\
    );
\out_data[11]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(11),
      I1 => i_register_interface_n_27,
      O => \out_data[11]_i_2_n_0\
    );
\out_data[11]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(10),
      I1 => i_register_interface_n_28,
      O => \out_data[11]_i_3_n_0\
    );
\out_data[11]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(9),
      I1 => i_register_interface_n_29,
      O => \out_data[11]_i_4_n_0\
    );
\out_data[11]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(8),
      I1 => i_register_interface_n_30,
      O => \out_data[11]_i_5_n_0\
    );
\out_data[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(15),
      I1 => i_register_interface_n_23,
      O => \out_data[15]_i_2_n_0\
    );
\out_data[15]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(14),
      I1 => i_register_interface_n_24,
      O => \out_data[15]_i_3_n_0\
    );
\out_data[15]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(13),
      I1 => i_register_interface_n_25,
      O => \out_data[15]_i_4_n_0\
    );
\out_data[15]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(12),
      I1 => i_register_interface_n_26,
      O => \out_data[15]_i_5_n_0\
    );
\out_data[16]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => s_axis_tvalid,
      I1 => \^m_axis_tvalid\,
      I2 => m_axis_tready,
      O => in_handshake
    );
\out_data[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(3),
      I1 => i_register_interface_n_35,
      O => \out_data[3]_i_2_n_0\
    );
\out_data[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(2),
      I1 => i_register_interface_n_36,
      O => \out_data[3]_i_3_n_0\
    );
\out_data[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(1),
      I1 => i_register_interface_n_37,
      O => \out_data[3]_i_4_n_0\
    );
\out_data[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(0),
      I1 => i_register_interface_n_38,
      O => \out_data[3]_i_5_n_0\
    );
\out_data[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(7),
      I1 => i_register_interface_n_31,
      O => \out_data[7]_i_2_n_0\
    );
\out_data[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(6),
      I1 => i_register_interface_n_32,
      O => \out_data[7]_i_3_n_0\
    );
\out_data[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(5),
      I1 => i_register_interface_n_33,
      O => \out_data[7]_i_4_n_0\
    );
\out_data[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(4),
      I1 => i_register_interface_n_34,
      O => \out_data[7]_i_5_n_0\
    );
\out_data_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => in_handshake,
      D => i_register_interface_n_22,
      Q => m_axis_tdata(0),
      R => clear
    );
\out_data_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => in_handshake,
      D => i_register_interface_n_12,
      Q => m_axis_tdata(10),
      R => clear
    );
\out_data_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => in_handshake,
      D => i_register_interface_n_11,
      Q => m_axis_tdata(11),
      R => clear
    );
\out_data_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => in_handshake,
      D => i_register_interface_n_10,
      Q => m_axis_tdata(12),
      R => clear
    );
\out_data_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => in_handshake,
      D => i_register_interface_n_9,
      Q => m_axis_tdata(13),
      R => clear
    );
\out_data_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => in_handshake,
      D => i_register_interface_n_8,
      Q => m_axis_tdata(14),
      R => clear
    );
\out_data_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => in_handshake,
      D => i_register_interface_n_7,
      Q => m_axis_tdata(15),
      R => clear
    );
\out_data_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => in_handshake,
      D => i_register_interface_n_6,
      Q => m_axis_tdata(16),
      R => clear
    );
\out_data_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => in_handshake,
      D => i_register_interface_n_21,
      Q => m_axis_tdata(1),
      R => clear
    );
\out_data_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => in_handshake,
      D => i_register_interface_n_20,
      Q => m_axis_tdata(2),
      R => clear
    );
\out_data_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => in_handshake,
      D => i_register_interface_n_19,
      Q => m_axis_tdata(3),
      R => clear
    );
\out_data_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => in_handshake,
      D => i_register_interface_n_18,
      Q => m_axis_tdata(4),
      R => clear
    );
\out_data_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => in_handshake,
      D => i_register_interface_n_17,
      Q => m_axis_tdata(5),
      R => clear
    );
\out_data_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => in_handshake,
      D => i_register_interface_n_16,
      Q => m_axis_tdata(6),
      R => clear
    );
\out_data_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => in_handshake,
      D => i_register_interface_n_15,
      Q => m_axis_tdata(7),
      R => clear
    );
\out_data_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => in_handshake,
      D => i_register_interface_n_14,
      Q => m_axis_tdata(8),
      R => clear
    );
\out_data_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => in_handshake,
      D => i_register_interface_n_13,
      Q => m_axis_tdata(9),
      R => clear
    );
out_last_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8FBB8088"
    )
        port map (
      I0 => s_axis_tlast,
      I1 => s_axis_tvalid,
      I2 => m_axis_tready,
      I3 => \^m_axis_tvalid\,
      I4 => \^m_axis_tlast\,
      O => out_last_i_1_n_0
    );
out_last_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => out_last_i_1_n_0,
      Q => \^m_axis_tlast\,
      R => clear
    );
out_valid_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => s_axis_tvalid,
      I1 => m_axis_tready,
      I2 => \^m_axis_tvalid\,
      O => out_valid_i_1_n_0
    );
out_valid_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => out_valid_i_1_n_0,
      Q => \^m_axis_tvalid\,
      R => clear
    );
s_axis_tready_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => m_axis_tready,
      I1 => \^m_axis_tvalid\,
      O => s_axis_tready
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk : in STD_LOGIC;
    aresetn : in STD_LOGIC;
    debug_leds : out STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axis_tdata : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axis_tvalid : in STD_LOGIC;
    s_axis_tready : out STD_LOGIC;
    s_axis_tlast : in STD_LOGIC;
    m_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axis_tvalid : out STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    m_axis_tlast : out STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_adderCore_0_0,adder_top,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "adder_top,Vivado 2017.4";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \^m_axis_tdata\ : STD_LOGIC_VECTOR ( 16 downto 0 );
  attribute x_interface_info : string;
  attribute x_interface_info of aresetn : signal is "xilinx.com:signal:reset:1.0 aresetn RST";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of aresetn : signal is "XIL_INTERFACENAME aresetn, POLARITY ACTIVE_LOW";
  attribute x_interface_info of clk : signal is "xilinx.com:signal:clock:1.0 clk CLK";
  attribute x_interface_parameter of clk : signal is "XIL_INTERFACENAME clk, ASSOCIATED_BUSIF m_axis:s_axis:s_axi, ASSOCIATED_RESET aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0";
  attribute x_interface_info of m_axis_tlast : signal is "xilinx.com:interface:axis:1.0 m_axis TLAST";
  attribute x_interface_info of m_axis_tready : signal is "xilinx.com:interface:axis:1.0 m_axis TREADY";
  attribute x_interface_info of m_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 m_axis TVALID";
  attribute x_interface_info of s_axi_arready : signal is "xilinx.com:interface:aximm:1.0 s_axi ARREADY";
  attribute x_interface_info of s_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi ARVALID";
  attribute x_interface_info of s_axi_awready : signal is "xilinx.com:interface:aximm:1.0 s_axi AWREADY";
  attribute x_interface_info of s_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi AWVALID";
  attribute x_interface_info of s_axi_bready : signal is "xilinx.com:interface:aximm:1.0 s_axi BREADY";
  attribute x_interface_info of s_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi BVALID";
  attribute x_interface_info of s_axi_rready : signal is "xilinx.com:interface:aximm:1.0 s_axi RREADY";
  attribute x_interface_info of s_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi RVALID";
  attribute x_interface_info of s_axi_wready : signal is "xilinx.com:interface:aximm:1.0 s_axi WREADY";
  attribute x_interface_info of s_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi WVALID";
  attribute x_interface_info of s_axis_tlast : signal is "xilinx.com:interface:axis:1.0 s_axis TLAST";
  attribute x_interface_info of s_axis_tready : signal is "xilinx.com:interface:axis:1.0 s_axis TREADY";
  attribute x_interface_info of s_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 s_axis TVALID";
  attribute x_interface_info of m_axis_tdata : signal is "xilinx.com:interface:axis:1.0 m_axis TDATA";
  attribute x_interface_parameter of m_axis_tdata : signal is "XIL_INTERFACENAME m_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef";
  attribute x_interface_info of s_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 s_axi ARADDR";
  attribute x_interface_info of s_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 s_axi ARPROT";
  attribute x_interface_info of s_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 s_axi AWADDR";
  attribute x_interface_parameter of s_axi_awaddr : signal is "XIL_INTERFACENAME s_axi, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0";
  attribute x_interface_info of s_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 s_axi AWPROT";
  attribute x_interface_info of s_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 s_axi BRESP";
  attribute x_interface_info of s_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 s_axi RDATA";
  attribute x_interface_info of s_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 s_axi RRESP";
  attribute x_interface_info of s_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 s_axi WDATA";
  attribute x_interface_info of s_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 s_axi WSTRB";
  attribute x_interface_info of s_axis_tdata : signal is "xilinx.com:interface:axis:1.0 s_axis TDATA";
  attribute x_interface_parameter of s_axis_tdata : signal is "XIL_INTERFACENAME s_axis, TDATA_NUM_BYTES 2, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef";
begin
  m_axis_tdata(31) <= \<const0>\;
  m_axis_tdata(30) <= \<const0>\;
  m_axis_tdata(29) <= \<const0>\;
  m_axis_tdata(28) <= \<const0>\;
  m_axis_tdata(27) <= \<const0>\;
  m_axis_tdata(26) <= \<const0>\;
  m_axis_tdata(25) <= \<const0>\;
  m_axis_tdata(24) <= \<const0>\;
  m_axis_tdata(23) <= \<const0>\;
  m_axis_tdata(22) <= \<const0>\;
  m_axis_tdata(21) <= \<const0>\;
  m_axis_tdata(20) <= \<const0>\;
  m_axis_tdata(19) <= \<const0>\;
  m_axis_tdata(18) <= \<const0>\;
  m_axis_tdata(17) <= \<const0>\;
  m_axis_tdata(16 downto 0) <= \^m_axis_tdata\(16 downto 0);
  s_axi_bresp(1) <= \<const0>\;
  s_axi_bresp(0) <= \<const0>\;
  s_axi_rresp(1) <= \<const0>\;
  s_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adder_top
     port map (
      aresetn => aresetn,
      clk => clk,
      debug_leds(7 downto 0) => debug_leds(7 downto 0),
      m_axis_tdata(16 downto 0) => \^m_axis_tdata\(16 downto 0),
      m_axis_tlast => m_axis_tlast,
      m_axis_tready => m_axis_tready,
      m_axis_tvalid => m_axis_tvalid,
      s_axi_araddr(3 downto 0) => s_axi_araddr(5 downto 2),
      s_axi_arready => s_axi_arready,
      s_axi_arvalid => s_axi_arvalid,
      s_axi_awaddr(3 downto 0) => s_axi_awaddr(5 downto 2),
      s_axi_awready => s_axi_awready,
      s_axi_awvalid => s_axi_awvalid,
      s_axi_bready => s_axi_bready,
      s_axi_bvalid => s_axi_bvalid,
      s_axi_rdata(31 downto 0) => s_axi_rdata(31 downto 0),
      s_axi_rready => s_axi_rready,
      s_axi_rvalid => s_axi_rvalid,
      s_axi_wdata(31 downto 0) => s_axi_wdata(31 downto 0),
      s_axi_wready => s_axi_wready,
      s_axi_wstrb(3 downto 0) => s_axi_wstrb(3 downto 0),
      s_axi_wvalid => s_axi_wvalid,
      s_axis_tdata(15 downto 0) => s_axis_tdata(15 downto 0),
      s_axis_tlast => s_axis_tlast,
      s_axis_tready => s_axis_tready,
      s_axis_tvalid => s_axis_tvalid
    );
end STRUCTURE;
