/*
 * main.c
 *
 *  Created on: 26. feb. 2018
 *      Author: espen
 */

#include "xparameters.h"
#include "xuartps.h"
#include "xil_printf.h"
#include "xil_io.h"


#define UART_DEVICE_ID              XPAR_XUARTPS_0_DEVICE_ID
#define DMA_BASE_ADDR				XPAR_CUBEDMA_0_BASEADDR
#define ADDER_BASE_ADDR				XPAR_ADDERCORE_0_BASEADDR



XUartPs Uart_Ps;		/* Instance of the UART Device */

int uart_init(u16 DeviceId);
void start_transfer(u32 chn, u32 base, u32 offset, u32 length, u32 intr);
void start_mm2s_simple_transfer(int offset, int length);
void start_s2mm_simple_transfer(int offset, int length);
void fill_memory();


int main(void){
	int Status;
	Status = uart_init(UART_DEVICE_ID );
	if (Status != XST_SUCCESS) {
		xil_printf("UART Selftest Example Failed\r\n");
		return XST_FAILURE;
	}
	xil_printf("Uart initialized\r\n");


	//printf("%08x\n", Addr+0x08);
	//fill_memory();
	start_s2mm_simple_transfer(0, 0x0A);
	start_mm2s_simple_transfer(0, 0x0A);
}


int uart_init(u16 DeviceId){
	int Status;
	XUartPs_Config *Config;
	/*
	 * Initialize the UART driver so that it's ready to use
	 * Look up the configuration in the config table,
	 * then initialize it.
	 */
	Config = XUartPs_LookupConfig(DeviceId);
	if (NULL == Config) {
		return XST_FAILURE;
	}
	Status = XUartPs_CfgInitialize(&Uart_Ps, Config, Config->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

void start_transfer(u32 chn, u32 base, u32 offset, u32 length, u32 intr){
	Xil_Out32(chn , 0);
	Xil_Out32((chn+0x08) , 0);
	Xil_Out32((chn+0x0C) , 0);
	Xil_Out32((chn+0x10) , 0);
	Xil_Out32((chn+0x14) , 0);
	Xil_Out32((chn+0x18) , 0);
	Xil_Out32((chn+0x1C) , (offset & 0xFFF));
	Xil_Out32(chn , (((length&0xFFFF << 12)) | (intr << 4) | 1));
}

void start_mm2s_simple_transfer(int offset, int length){
	start_transfer(0x43C00000, 0x00100000, offset, length, 0);
}

void start_s2mm_simple_transfer(int offset, int length){
	start_transfer(0x43C00020, 0x00110000, offset, length, 0);
}

void fill_memory(){
	Xil_Out32(0x00100000 , 0x00010000);
	Xil_Out32(0x00100004 , 0x00030002);
	Xil_Out32(0x00100008 , 0x00050004);
	Xil_Out32(0x0010000C , 0x00070006);
	Xil_Out32(0x00100010 , 0x00090008);
	Xil_Out32(0x00100014 , 0x000B000A);
	Xil_Out32(0x00100018 , 0x000D000C);
	Xil_Out32(0x0010001C , 0x000F000E);
	Xil_Out32(0x00100020 , 0x00110010);
	Xil_Out32(0x00100024 , 0x00130012);
}
