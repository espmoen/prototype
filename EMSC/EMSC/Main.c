#include <windows.h> //UART

#include <stdio.h>
#include <stdlib.h> 
#include <math.h>
#include <float.h>

#include <conio.h>;
#include <string.h>;



double mean(int length, double x[]) {
	double sum = 0;
	for (int i = 0; i < length; i++) {
		sum += x[i];
	}
	return((float)sum / length);
}

double calculateSD(double data[])
{
	double sum = 0.0, mean, standardDeviation = 0.0;

	int i;

	for (i = 0; i<100; ++i)
	{
		sum += data[i];
	}

	mean = sum / 100;

	for (i = 0; i<100; ++i)
		standardDeviation += pow(data[i] - mean, 2);

	return sqrt(standardDeviation / 99);
}

void bsxfun(double *res ,double *a1, double *a2, int length, double std) {
	for (int i = 0; i < length; i++) {
		res[i] = (a1[i] - a2[i]) / std;
	}
}


void getTransposeMatrix(double arr[5][100], double res[100][5]) {
	for (int i = 0; i < 100; i++) {
		for (int y = 0; y < 5; y++) {
			res[i][y] = arr[y][i];
		}
	}
}

void matrixMult(double arrA[5][100], double arrB[100][5], double res[5][5]) {
	double sum = 0;
	for (int row = 0; row < 5; row++) {
		for (int col = 0; col < 5; col++) {
		
			for (int y = 0; y < 100; y++) {
				sum += arrA[row][y] * arrB[y][col];
			}
			res[row][col] = sum;
			sum = 0;
		}
	}
}

void matMult(double **arrA, double **arrB, int rowA, int colB, int elem, double **res) {
	double sum = 0;
	for (int row = 0; row < rowA; row++) {
		for (int col = 0; col < colB; col++) {


			for (int y = 0; y < elem; y++) {
				sum += arrA[row][y] * arrB[y][col];
			}
			res[row][col] = sum;
			sum = 0;
		}
	}
}



//---------------------------------------------------------------------
// http://www.sanfoundry.com/c-program-find-inverse-matrix/
//---------------------------------------------------------------------
double determinant(double a[5][5], int k)
{

	double s = 1, det = 0, b[5][5];
	int i, j, m, n, c;
	if (k == 1)
	{
		return (a[0][0]);
	}
	else
	{
		det = 0;
		for (c = 0; c < k; c++)
		{
			m = 0;
			n = 0;
			for (i = 0; i < k; i++)
			{
				for (j = 0; j < k; j++)
				{
					b[i][j] = 0;
					if (i != 0 && j != c)
					{
						b[m][n] = a[i][j];
						if (n < (k - 2))
							n++;
						else
						{
							n = 0;
							m++;
						}
					}
				}
			}
			det = det + s * (a[0][c] * determinant(b, k - 1));
			s = -1 * s;
		}
	}

	return (det);
}

/*Finding transpose of matrix*/
void transpose(double num[5][5], double fac[5][5], double r, double res[5][5])
{
	int i, j;
	double b[5][5], inverse[5][5], d;

	for (i = 0; i < r; i++)
	{
		for (j = 0; j < r; j++)
		{
			b[i][j] = fac[j][i];
		}
	}
	d = determinant(num, r);
	for (i = 0; i < r; i++)
	{
		for (j = 0; j < r; j++)
		{
			inverse[i][j] = b[i][j] / d;
		}
	}

	for (i = 0; i < r; i++)
	{
		for (j = 0; j < r; j++)
		{
			res[i][j] = inverse[i][j];
		}
	}
}

void cofactor(double num[5][5], double f, double res[5][5])
{
	double b[5][5], fac[5][5];
	int p, q, m, n, i, j;
	for (q = 0; q < f; q++)
	{
		for (p = 0; p < f; p++)
		{
			m = 0;
			n = 0;
			for (i = 0; i < f; i++)
			{
				for (j = 0; j < f; j++)
				{
					if (i != q && j != p)
					{
						b[m][n] = num[i][j];
						if (n < (f - 2))
							n++;
						else
						{
							n = 0;
							m++;
						}
					}
				}
			}
			fac[q][p] = pow(-1, q + p) * determinant(b, f - 1);
		}
	}
	transpose(num, fac, f, res);
}

//----------------------------------------------------------------------



void EMSC(double raw[100][100],double ref_spectra[2][100], double corrected[100][100], int nVars, int nObs) {
	
	//---------------------DECLARATIONS---------------------
	double res[100] = { 0 };
	double temp[2] = { 0 };
	double ** M;
	double ** M_trans;
	double ** Mres;
	double ** mult_test;
	double ** test;
	double mean_t, num = 0;

	M = (double **)malloc(5 * sizeof(double*));
	M_trans = (double **)malloc(100 * sizeof(double*));
	Mres = (double **)malloc(100 * sizeof(double*));
	mult_test = (double **)malloc(5 * sizeof(double*));
	test = (double **)malloc(5 * sizeof(double*));

	for (int row = 0; row < 5; row++) {
		M[row] = (double*)malloc(100 * sizeof(double));
	}

	for (int row = 0; row < 100; row++) {
		M_trans[row] = (double*)malloc(5 * sizeof(double));
		Mres[row] = (double*)malloc(5 * sizeof(double));
	}

	for (int row = 0; row < 5; row++) {
		mult_test[row] = (double*)malloc(5 * sizeof(double));
		test[row] = (double*)malloc(5 * sizeof(double));
	}
	//------------------------------------------------------

	//Calculates standard deviation of the ref_spectra
	double std = calculateSD(ref_spectra[1]);


	//something
	bsxfun(res, ref_spectra[1], ref_spectra[0], 100, std);


	//Construct the M matrix
	for (int i = 0; i < 100; i++) {
		
		//Add 1 in first row
		M[0][i] = 1;
		M_trans[i][0] = 1;

		//Add mean in second row
		temp[0] = ref_spectra[0][i];
		temp[1] = ref_spectra[1][i];
		mean_t = mean(2, temp);
		M[1][i] = mean_t;
		M_trans[i][1] = mean_t;


		M[2][i] = res[i];
		M_trans[i][2] = res[i];
		
		//Add linspace and linspace squared
		M[3][i] = num;
		M_trans[i][3] = num;
		
		M[4][i] = pow(num, 2);
		M_trans[i][4] = pow(num, 2);
		num += (1.0 / 99.0);
	}

	//Multiply M with M transpose
	matMult(M,M_trans, 5,5,100,mult_test);

	//Problematic to use the double pointer, had to use 2D array to calculate inverse
	double a[5][5];
	for (int i = 0; i < 5; i++) {
		a[i][0] = mult_test[i][0];
		a[i][1] = mult_test[i][1];
		a[i][2] = mult_test[i][2];
		a[i][3] = mult_test[i][3];
		a[i][4] = mult_test[i][4];
	}

	//Calculates inverse
	double inverse[5][5];
	cofactor(a, 5, inverse);

	printf("%f ", inverse[0][0]);
	//Creates double pointer from the 2D array, should not have to do this roundabout
	//printf("\n\n");
	for (int i = 0; i < 5; i++) {
		//printf("%f   %f   %f   %f   %f\n", inverse[i][0], inverse[i][1], inverse[i][2], inverse[i][3], inverse[i][4]);
		test[i][0] = inverse[i][0];
		test[i][1] = inverse[i][1];
		test[i][2] = inverse[i][2];
		test[i][3] = inverse[i][3];
		test[i][4] = inverse[i][4];
	}


	//Multiplies the M transpose with the inverse to achieve: M'*inv(M*M')
	matMult(M_trans, test, 100, 5, 5, Mres);

	
	//THIS PART SHOULD BE DONE IN HARDWARE
	double sum = 0;
	double p[5] = { 0 };
	for (int idx = 0; idx < 100; idx++) {

		for (int i = 0; i < 5; i++) {
			for (int y = 0; y < 100; y++) {
				sum += raw[idx][y] * Mres[y][i];
			}
			p[i] = sum;
			sum = 0;
		}
		for (int t = 0; t < 100; t++) {
			corrected[idx][t] = (raw[idx][t] - p[0] - p[3] * M[3][t] - p[4] * M[4][t]) / p[1];
		}
	}
	//--------------------------------------
	}


void write_file(double arr[100][100]) {
	FILE *fp;
	fp = fopen("corrected.txt", "w");
	for (int row = 0; row < 100; row++) {
		for (int col = 0; col < 100; col++) {
			fprintf(fp, "%f ", arr[row][col]);
		}
		fprintf(fp, "\n");
	}
	fclose(fp);
}

void read_file(double arr[100][100]) {
	FILE *fp;
	int row = 0;
	int column = 0;
	char buff[20];
	fp = fopen("test_data.txt", "r");
	while (fgets(buff, 10, (FILE*)fp) != NULL) {
		arr[column][row] = atof(buff);
		row += 1;
		if (row >= 100) {
			column += 1;
			row = 0;
		}
	}
	fclose(fp);

}


double ** initialize(int rows, int columns) {
	double **temp;
	temp = (double **)malloc(rows * sizeof(double*));
	for (int row = 0; row < rows; row++) {
		temp[row] = (double*)malloc(columns * sizeof(double));
	}
	return temp;
}




void main() {
	
	double raw[100][100] = { 0 };
	double ref_spectra[2][100] = { 0 };
	double corrected[100][100] = { 0 };
	
	double **arr = initialize(10,10);

	for (int i = 0; i < 10; i++) {
		arr[i][i] = 10;
	}

	//double **arr = initialize(int rows, int columns, int default_value);

	read_file(raw);
	for (int i = 0; i < 100; i++) {
		ref_spectra[0][i] = raw[2][i];
		ref_spectra[1][i] = raw[92][i];
	}
	EMSC(raw, ref_spectra, corrected, 100, 100);
	
	//write_file(corrected);
	

	

	/*char buff[20];

	double a = 2.141231;
	sprintf(buff, "%f", a);
	printf(buff);*/
	system("PAUSE");

}



