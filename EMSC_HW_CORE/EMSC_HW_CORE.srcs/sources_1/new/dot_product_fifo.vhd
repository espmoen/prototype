library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

package dot_product_reg_small_pkg is
    type bus_array is array(natural range <>) of std_logic_vector(63 downto 0);
end package dot_product_reg_small_pkg;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.dot_product_reg_small_pkg.all;

entity dot_product_fifo is
    Generic (
            bit_width : integer := 32; 
            num_elements_vec: integer := 100
            );
            
    Port ( 
        clk : in std_logic;
        reset_n : in std_logic;
        enable : in std_logic;
        data_in : in signed(31 downto 0);
        data_out : out signed(63 downto 0);
        res_rdy : out std_logic
        );
end dot_product_fifo;

architecture Behavioral of dot_product_fifo is

component fifo_generator_0
    PORT (
        clk : IN STD_LOGIC;
        srst : IN STD_LOGIC;
        din : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        wr_en : IN STD_LOGIC;
        rd_en : IN STD_LOGIC;
        dout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        full : OUT STD_LOGIC;
        empty : OUT STD_LOGIC
  );
end component;

signal wr_en,rd_en, full, empty : std_logic_vector(5 downto 0);
signal intermediate_wire : bus_array(0 to 5);
signal std_in : std_logic_vector(31 downto 0);

begin

fifo: for I in 0 to 5 generate
    fifox: fifo_generator_0 port map(clk => clk, srst => reset_n, din => std_in, wr_en=>wr_en(I), rd_en=>rd_en(I), dout => intermediate_wire(I), full=>full(I), empty=>empty(I));
end generate fifo;



end Behavioral;
