library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity multiplicator is
  Port ( 
        inA : in signed(31 downto 0);
        inB : in signed(31 downto 0);
        Res : out signed(63 downto 0)
  );
end multiplicator;

architecture Behavioral of multiplicator is

begin

Res <= inA * inB;

end Behavioral;
