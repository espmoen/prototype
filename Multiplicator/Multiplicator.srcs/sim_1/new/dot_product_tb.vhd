library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity dot_product_tb is
  --Port ( );
end dot_product_tb;

architecture Behavioral of dot_product_tb is

component clk
    Port ( clk : out STD_LOGIC);
end component;

component dot_product
     Generic ( Bit_width : positive := 16
           );
     Port (    clk         : in std_logic;
           rst_n       : in std_logic;
           enable      : in std_logic;
           num_elements: in integer;
           A           : in std_logic_vector(Bit_width-1 downto 0);
           B           : in std_logic_vector(Bit_width-1 downto 0);     
           Res         : out std_logic_vector((2*Bit_width)-1 downto 0)
       );
end component;

signal clock, rst_n, enable : std_logic;
signal num_elements : integer;
signal A,B : std_logic_vector(15 downto 0);
signal Res : std_logic_vector(31 downto 0);
begin
clk1: clk port map(clk=>clock);
dut: dot_product port map(clk=>clock, rst_n=>rst_n, enable=>enable,num_elements=>num_elements,A=>A,B=>B,Res=>Res);

process
begin
    rst_n <= '0';
    enable <= '0';
    num_elements <= 4;
    A <= (others =>'0');
    B <= (others => '0');
    wait for 0.5 ns;
    rst_n <= '1';
    enable <= '1';
    A <= x"FFFF";
    B <= x"0001";
    wait for 1 ns;
    enable <= '0';
    A <= x"FFFE";
    B <= x"0002";
    wait for 1 ns;
    A <= x"FFFD";
    B <= x"FFFD";
    wait for 1 ns;
    A <= x"FFFC";
    B <= x"FFFC";
    wait for 1 ns;
    A <= x"0000";
    B <= x"0000";
    wait for 20 ns;
    enable <= '1';
    num_elements <= 3;
    A <= x"0001";
    B <= x"0001";
    wait for 1 ns;
    enable <= '0';
    A <= x"0002";
    B <= x"0002";
    wait for 1 ns;
    A <= x"0003";
    B <= x"0003";
    wait for 100 ns;
end process;

end Behavioral;

