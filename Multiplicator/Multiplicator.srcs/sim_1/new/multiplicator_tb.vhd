library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;


entity multiplicator_tb is
--  Port ( );
end multiplicator_tb;

architecture Behavioral of multiplicator_tb is


component loop_calc
    Port ( 
        raw      :       in integer;
        P_1      :       in integer;
        P_4      :       in integer;
        wlens    :       in integer;
        P_5      :       in integer;
        wlensSq  :       in integer;
        P_2      :       in integer;
        quotient :       out integer;
        divisor  :       out integer;
        remainder:       out integer
    );
end component;

signal raw, P_1, P_4, wlens, P_5, wlensSq, P_2 : integer;
signal divisor, quotient, remainder : integer;
begin
uut1: loop_calc port map(raw=>raw, P_1=>P_1, P_4 => P_4, wlens=>wlens, P_5=>P_5, wlensSq=>wlensSq, P_2=>P_2, quotient=>quotient, divisor=>divisor, remainder=>remainder);
process
begin
    raw <= 1007;
    P_1 <= 105;
    P_4 <= 7;
    wlens <= 3;
    P_5 <= 3;
    wlensSq <= 9;
    P_2 <= 8;
    --A<= 57;
    --B<= 7;
    wait for 1 ns;
--    raw <= 10;
--    P_1 <= 1;
--    P_4 <= 2;
--    wlens <= 1;
--    P_5 <= 2;
--    wlensSq <= 3;
    wait for 1000 ns;
end process;

end Behavioral;
