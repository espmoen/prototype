
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity test_flip_Esp_tb is
--  Port ( );
end test_flip_Esp_tb;

architecture Behavioral of test_flip_Esp_tb is

component clk
    Port ( clk : out STD_LOGIC);
end component;

component test_flip_Esp
        Port (      clk         : in std_logic;
                    rst_n       : in std_logic;
                    data_in     : in std_logic;
                    enable      : in std_logic;
                    shift_rd    : out std_logic;
                    data_out    : out std_logic_vector(11 downto 0)
            );
end component;

signal clock, rst_n, data_in, shift_rd, enable : std_logic;
signal data_out    : std_logic_vector(11 downto 0);
                    

begin
clk1: clk port map(clk=>clock);
uut: test_flip_Esp port map(clk=>clock, rst_n=>rst_n, data_in=>data_in, enable=>enable, shift_rd=>shift_rd, data_out=>data_out);

-- Bit Sequence: 1 0 1 0 1 0 1 0 1 0 1 0

process
begin
    rst_n <= '0';
    data_in <= '0';
    wait for 0.25 ns;
    rst_n <= '1';
    wait for 1 ns;
    enable <= '1';
    data_in <= '1';  --1
    wait for 0.5 ns;
    data_in <= '1';  --2
    wait for 0.5 ns;
    data_in <= '0';  --3
    wait for 0.5 ns;
    data_in <= '0';  --4
    wait for 0.5 ns;
    data_in <= '1';  --5
    wait for 0.5 ns;
    data_in <= '1';  --6
    wait for 0.5 ns;
    data_in <= '0';  --7
    wait for 0.5 ns;
    data_in <= '0';  --8
    wait for 0.5 ns;
    data_in <= '1';  --9
    wait for 0.5 ns;
    data_in <= '1';  --10
    wait for 0.5 ns;
    data_in <= '0';  --11
    wait for 0.5 ns;
    data_in <= '1';  --12
    wait for 0.5 ns;
    
    --110011001101  ->  CCD
 
    data_in <= '1';  --1
    wait for 0.5 ns;
    data_in <= '0';  --2
    wait for 0.5 ns;
    data_in <= '0';  --3
    wait for 0.5 ns;
    data_in <= '1';  --4 
    wait for 0.5 ns;
    data_in <= '1';  --5
    wait for 0.5 ns;
    data_in <= '0';  --6
    wait for 0.5 ns;
    data_in <= '0';  --7
    wait for 0.5 ns;
    data_in <= '1';  --8
    wait for 0.5 ns;
    data_in <= '1';  --9
    wait for 0.5 ns;
    data_in <= '0';  --10
    wait for 0.5 ns;
    data_in <= '0';  --11
    wait for 0.5 ns;
    data_in <= '1';  --12 
    wait for 0.5 ns;
    data_in <= '0';
    
    -- 100110011001 => 999
 
 
    wait for 100 ns;

end process;



end Behavioral;
