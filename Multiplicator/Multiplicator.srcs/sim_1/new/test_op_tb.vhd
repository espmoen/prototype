library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity test_op_tb is
--  Port ( );
end test_op_tb;

architecture Behavioral of test_op_tb is

component clk
    Port ( clk : out STD_LOGIC);
end component;

component c_addsub_0
    PORT (
        A : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
        B : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
        CLK : IN STD_LOGIC;
        CE : IN STD_LOGIC;
        S : OUT STD_LOGIC_VECTOR(11 DOWNTO 0)
    );
end component;

signal clock, rst : std_logic;
signal A, A1, B, B1, Res : std_logic_vector(11 downto 0);

begin
clock1: clk port map(clk=>clock);
uut: c_addsub_0 port map(A=>A, B=>B, clk=>clock, CE=>rst,S=>A1);
uut1: c_addsub_0 port map(A=>A1, B=>B1,clk=>clock, CE=>rst, S=>Res);
process
begin 
    rst <= '0';
    B1<="000000000000";
    A<= "000000000000";
    B<= "000000000000";
    wait for 1.5 ns;
    rst <= '1';
    A<= "000000010000";
    B<= "000000000100";
    B1<="000000000100";
    wait for 100 ns;
end process;



end Behavioral;
