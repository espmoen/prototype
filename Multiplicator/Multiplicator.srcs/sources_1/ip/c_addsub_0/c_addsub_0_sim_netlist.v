// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.2 (win64) Build 1909853 Thu Jun 15 18:39:09 MDT 2017
// Date        : Wed Jan 31 14:40:28 2018
// Host        : DESKTOP-0KA73P0 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               c:/Users/espen/Documents/MasterOppgave/prototype/Multiplicator/Multiplicator.srcs/sources_1/ip/c_addsub_0/c_addsub_0_sim_netlist.v
// Design      : c_addsub_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "c_addsub_0,c_addsub_v12_0_10,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_10,Vivado 2017.2" *) 
(* NotValidForBitStream *)
module c_addsub_0
   (A,
    B,
    CLK,
    CE,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) input [11:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) input [11:0]B;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) input CE;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) output [11:0]S;

  wire [11:0]A;
  wire [11:0]B;
  wire CE;
  wire CLK;
  wire [11:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* c_a_type = "0" *) 
  (* c_a_width = "12" *) 
  (* c_add_mode = "1" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "0" *) 
  (* c_b_value = "000000000000" *) 
  (* c_b_width = "12" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "12" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  c_addsub_0_c_addsub_v12_0_10 U0
       (.A(A),
        .ADD(1'b1),
        .B(B),
        .BYPASS(1'b0),
        .CE(CE),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADD_MODE = "1" *) (* C_AINIT_VAL = "0" *) (* C_A_TYPE = "0" *) 
(* C_A_WIDTH = "12" *) (* C_BORROW_LOW = "1" *) (* C_BYPASS_LOW = "0" *) 
(* C_B_CONSTANT = "0" *) (* C_B_TYPE = "0" *) (* C_B_VALUE = "000000000000" *) 
(* C_B_WIDTH = "12" *) (* C_CE_OVERRIDES_BYPASS = "1" *) (* C_CE_OVERRIDES_SCLR = "0" *) 
(* C_HAS_BYPASS = "0" *) (* C_HAS_CE = "1" *) (* C_HAS_C_IN = "0" *) 
(* C_HAS_C_OUT = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) 
(* C_OUT_WIDTH = "12" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "zynq" *) (* ORIG_REF_NAME = "c_addsub_v12_0_10" *) 
(* downgradeipidentifiedwarnings = "yes" *) 
module c_addsub_0_c_addsub_v12_0_10
   (A,
    B,
    CLK,
    ADD,
    C_IN,
    CE,
    BYPASS,
    SCLR,
    SSET,
    SINIT,
    C_OUT,
    S);
  input [11:0]A;
  input [11:0]B;
  input CLK;
  input ADD;
  input C_IN;
  input CE;
  input BYPASS;
  input SCLR;
  input SSET;
  input SINIT;
  output C_OUT;
  output [11:0]S;

  wire \<const0> ;
  wire [11:0]A;
  wire [11:0]B;
  wire CE;
  wire CLK;
  wire [11:0]S;
  wire NLW_xst_addsub_C_OUT_UNCONNECTED;

  assign C_OUT = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* c_a_type = "0" *) 
  (* c_a_width = "12" *) 
  (* c_add_mode = "1" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "0" *) 
  (* c_b_value = "000000000000" *) 
  (* c_b_width = "12" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "12" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  c_addsub_0_c_addsub_v12_0_10_viv xst_addsub
       (.A(A),
        .ADD(1'b0),
        .B(B),
        .BYPASS(1'b0),
        .CE(CE),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_xst_addsub_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
l1NG3g81+vM8a/OECNXckQ6Ih+534PcHu9If3GBzfNiHrQt4ZqWyOCmUfR9HBrKJ6dazleZpBLLQ
VkjiEZOvOw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z0LH98ijrG2zSKQ428sLNLNN8LOYW53zGTr9NWQ16ZrFJS/8H+Sypz2sLY7sCYpj+gN48UB+J20x
PvOEIxFZVPZNmhrcvIxztIiTduaVtyypOS3Jx8r3YE6fOwVJrfZ9eXRQUIdKvbN0bVZFUcjZ2HOu
2IHyG/UIDoelWrgO0GY=

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
QNC1y7mnDIW3BeoEUG5xtIOyuA9VVC6sNeWDOT6S5qoB4e5s9LHwLIGpByH8Fz0PGaafc2Pp1LqC
lzXnevLntdeO+Pz1NMCI4Ojicg8oWhR3msMyGBNzheYZMtUoYnT2zC0eafrxC+G2D8yNbUPHtf++
Y0MP0FYlKg8jJhDSX3w=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bRemsiwW6AApgQWC90BqiBlMFD2jEWM37Ph/oR3/T151pMQpEWD3gu3SGbUebLL6tc97cE/PgSCy
KS0n/kkTjP8yz3D6tbz01kj9QAWvzzWfukcjy4U1p/TuuIIUFcvoH/P+MznfeFrHL11ZwDgOg40p
h2VhrJPIbdfbr6mfhPFTBuWKYDGmQfgZeWyn7BSTLS+wvNUS+AvTJnaPj6O9Szcq/v+sphPqX1F0
wfrQOmRJVSa2EfFZxZ1m3+2NmDfYPFsxF203jiTh4pJ8JuezHfzsaK6jbmB9h7QKD6yMUsYRx/O3
idyz3Inko4ZzfMIyZuEPWm6TSA5xk5DeltwcSw==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
QxSXl52RB2cLbPW8z4SvG8lZpaZT78HIOf6Q3fEiSBBQziVQn5oJPSJW8PdmH2pB/aVZpPeDnm2a
hpp7ddkUsyA+NHykeuxUJfwhWYhyemZLGnSdZXVpbaOhu0TLJiOtAVsDmRdPFs7q6CpwvvPImbuY
YK29r9Mw8w6ATmDgWgWs+wefEAeKjvUWKQmUr9SKi88H0j+hCCdtMoYZnO8+mjtjWlqm6U7NqCPr
K58IyvSojGlh7czWW8xHzOdgUBj872QexFBFCXAZ3caYI+I5tgzbPak7R4g1nHKG2e2HAjefrx2S
bXb3PhWqaTkKbnVCol5mWJuxPclCBxrEtqSjug==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinx_2016_05", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
cz5g65hbB8shmQPsohjzVr+SPRr5lrGQqqziYjnrsCWqDM27xVRVWn5/2voSXHOso9pm8+P7GN+L
Mz6Nn/qXS+pi/muryE2WEZ1r1ya1OKX8h7vIWuyvbtfcgSkvPQb+oNRpZXNONTBmmBFus8k0rin2
VJPxBV5qCpoPGWDVS1QCTJ852TyqzdEMM2h/x7mD8lHifZIkjoWi+O8HkWSf1j2JrYg2fikk0O71
heaF0CkbjGmy7ezg8wpLUywMgWUhCjFFanO1rqzaTEvWYxCjx+FFJRXRyfP3qUeBqMaTvZsGSDDl
/9EG0TehdoOmg4VPXawJiGfiqr2sBbg7f0zlow==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="base64", line_length=76, bytes=256)
`pragma protect key_block
T6BD82mhXAAEaHCIZcJoLF2oQsVuTnaH87pAlPat00WOKey/qzwKXgOxBmy0iqd1ofuLViyT5tl5
jrm+WdCaIMk2uuvk+s8C4bi5DJn7AZiu4La57VTfFmhhrIYRH8W147Vs7viWij1eGd/EaT9VGgih
lr28trEFBLIL+8wDhp6nOeA6ZDfdAm+K2SVDWBD9gNe8C6OdW0p/BIycJbYrCuKLTh+WK7CDxqne
h9KIY6TLyrkRaPIYIDmlfdTHCxCtTjvnrUdtnJBiginqIng3OREGaulEOmIX9pBHEoUccEaZLd9s
8WsldmBn9pk/X44tTWbni0vnRvebiC40u5saDw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="base64", line_length=76, bytes=256)
`pragma protect key_block
Rnb9UHGjpVWCtutRJovSE193xIMzVJgxNB7yk4R+FIr+UaT1jSg6sfoWBvTFvnYHG/B8g6Ax4YiT
6g8WZwi8ffHuDEX+W+k4Gz/mPuOXC1lKGypr/PyR9PyVL2jGWE624iA6Uq6q6AkYfPphNTBzHBSI
TfrHdZ63K5bCLGNecmEEMfIEUAQpgVRkbUeNjWDNvIuyVtpCNH1N9ZHcVM7YkPjiz1DKmY3Fs0SY
9AdHJynwHOafX5VD6sAKLehORci6B/AmNUQyyoF2VtRXIS25sCCcSmHPg4m4ddSoNAizg1eXCNGQ
MDi66U2kt0wgGfLbiRK6ylsot6Ve5/zITEui0w==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 10736)
`pragma protect data_block
xueH9zTMnfVzSDO4uN9Z3GbviWNNkgcqobrzVtHa6dLQRa43GO3kfLsNM9ahFBnayUSwALeA4bkq
Nddo62s1YVAxzbudkuB5S7l8TvRrcIgClGcpWeuIk/uWveraksiULLemFect+Gjqvssh2I3vLO2B
zododt+cuSGmXn2Kme3jE4lxHqoOhaM9YyxmZglKB6fLlDpqDi/oe6iQUJBShV50QhIXkeuhEcrr
t+bcNZnL0iRFhJCnNpGwrn8KLHg7ufokrtWYIcs5kvF9OkXcJ1JSPj8mnqqNxWZxk87u0nK20p90
O4Mkfnv/6Lj0N4tjrZ/pJB7eZbwUsWH7HJRtZdjKdQRTHUvWvyAQvpy5FizvxKoOBrCkWemu5A88
irwk3wsVrOHFUgF2UHyONSClFU8W7ljAFeEVqIrUJLvYhWxD6hetw6pV93FE710i4Wo2mSdu9c/g
/w8Jl2J16o/DcWgSKjuPoeJgSuP9VbDHCa+Me6Km3w5QpHK7e/qs2qebL1eo9zgkG4uEV8/dyt+4
2T1j+zDT1kSz8jCFZjwa3qG+RBfqhrXoaRdN+/kHVwOVMDAqDccj3d80HZtVOoZ3+8NS1MgfszAq
4dT324FUClMevSEP234ecZvP7r+6JuzP7DDYRi6WxdE84qhwFpQ91Ue8OjPsj3sBIKWevYzBkmWn
byApnR8t+Q7O9lsIWbIooSbP7ubYFMBCmE4a/yWk5omdTH5rcrbrXB60O8IwaDtVcdkKQ/2kANmh
w+X53KS69z2ddLZBaQdf03wLDunI2ltyI365Lia6JR7I1ODBWKYpXnKdpOV+oBRCWz6p+WuqYFnN
BaiLQ0GnsPaeEaIdyu1L4+/keuZNMZJ5u128ardTbv0SylL2XMadFm9qm41keZ6BfyBV/K5fUkJi
a52pEG350FyOWjbpbmaXhqYcc28UieakgrmSjuPeTyTh6nz1OcHbn9+hA8cEIaaZjS1nhzUC+tFO
LdCTgrGB6p9cPINKFZAoM4vLxLj2O8tPPy3R1Lyw706P1bAFY6xvqPdTVqD6b9W4bn9TFJvpbFNN
ke8L7ZWs0DR4Syat6BayKFj9uFMOdIstnOlCQB/JqXbmGzbo04vkKAupJjXBLtVB+1AmmvExDFcN
SVxqjujzTcpTWPavGk0ILs5S4mRrBEWJ0stSjeV7LtH/xY25Bu+y27QtwJDa3FgJSyHEuX+QX9i2
G60VUWJs8sklp1fB9cBh0VS8597kg/iKSFYHaoIKT8IFVgh8tM0mFhS1MHSWzkRMoDLSwb80eYEf
K3ujDs0ZPpxdtM4hnPTfcPcvKnZGib7rHi+JUzHf8DzMsXFxBm11DhNGTdqh93I/Yew0LEJr+i6I
dvpi9/GORtXG1GoiNgWYG4MkRx5IJlsSKZNSiZFsdXpp30DFGurRRxhwMcBfV/fDuM4+eRnnrW5W
FdVOnOUW4tZv6//hFO0/2L0JwskUDF4EEqXCXNV7z2yEfxO2qfZ4n2mD3tiM+2lslxdL2PG8H1yR
FT3o1vbKD9zuJF/IxhtCNlwwEMlJJ4JHTzapG5C5cp5F9PnhPRUA5LL7kMaforg+obAlbdusbgUr
cGZDQs9r0kUzWPaguHtvfNRC/3KD9rVYK4gfbwuo4Co0/Ng89iR0tMW1P9Qc/sW9HT6ovtvHVkfC
AAH0i/5iqL4wb+xZQNhaQptLrCGKuV4WfgsU4a1ctLp00QAfPLdM59JH2tbI656jYFfphB1Ew0yH
BfkTCVFhkdcmYjiuydlDqBIG2vF9K4NDDiQiWT23DnDUzQVGbvj2NPC7VRKPgtZnbQaGOPL45tbW
yCNBxljv27HsPcs0JfCqjTzGyRjv/GUgfLOlgjgff+9E8OfMnvdAnd5ONh5GPraY7WvJ8bIDpnmn
qaAXGnDldE+6BMdXfiKfSLZBjqe96cRe9g0jcsOThB5kd0mQG4OC2cYh1tN8ZPIEYjQs4oH+jwdC
aP6cH1UtgGuV5nCAF3V2jicLmC9LALZJHIy9HkJlbMXoBExD/lSyGzzS4g4WV7ggFYDlpoXRUbOe
/ON3fuQvCp2nbzu+TPOz7K0yJ0mFd/Ts1zSN1bDVaQkBaAr5PlYTlVUwTXFcUONM/apCxvcTst2N
RbOrgk1Ck2uhRaarI2vhTeUaPKfCtW/LqDvNUA8VQEVCsC0WpDWbPMSHxXAJA0+7awGCqbYo3DT0
Erffb8o6+ZRSpxIwTYVw1Ov9nHax2dFmat0q3MnQaVUeBfA+ZmsTF2yToO3roVOnZ8JxVhGk9Xgo
1Vs4aXcGhGFdkRpU1kJJX2Rn2J+28wnjh0Cb8jrWyeHGVJ5dCkbbha2ftiFPZq7pspxRKyEW8Fpc
b0ib7+EcmggxAcxldlzCM7bfkhCOGKoCg4yqsqG4z6qJ9jM6enpU/Khv02uPzfe8vmH1GLid0HLF
+LZIBxV5idNQt/iQ7qWkxK7qyH2uukCegS1QUV/0OyxtENSs89iz3uF4v0pTllT9MIDPmJshANIk
wz/EDivdwU60fxt9FA5quy7s39J9tDcUbcYtNGytifLtPtq3if3eftfDAb+j2bfchhmgY4LMgqYj
aLlKo8Q+ft6YZeEx6E6yEpPsrJT4wpIQz4QkUPcHNqvTLDgTiwjj0rG7vbOtUAkTd41d6ezLG8yy
SMscAr3n7g3URZVo4puKzanWWWzJB1M9TMHd3IQzyxsAevjvNNUYfbIGwMZp0TWrPF/HHInmpt7z
CP1HmQ9kzE49KSZkBxNgRC+pqf+aw2jsmaUl6T4a+uLGtBhtxFT/19WWprF0VOeZR4sWaYjJOaVn
02lJx0GsOIbrpJli328flP9Thp8ZUiFWMHpLBiRtXsF5Ym/47BjbEIhB7ciVplczLR2hLh8MsNhx
Ph/DWPjOG5x9Nmjad4abpWPHJ/8s4XrAscYcXY1IgyJGaZzvD3cK1eKdzXdaMUNkmOoQ9JV+KWkr
WDfNh6WSp/khYrWdUJzW5J0goV3kMf8z2tDdZVESh9Ru0qsqReZNfdxpoFIscmtr4s328qwOjAo5
aS66arHUFTimWA5TW45MbMCiCfdAy3bw52qXPtMyY626xq9ANzP3KioSnHBQkL78rMOQVt/t/wHB
XK8lpv2NLZPf9Tm03LNxFNivOuCsojDCRftx7a3E5ezpBwCEKIXomrU1RSNycp3VChrInFdm2Dsf
D2l9GHlch/l+Y42B42uECtK3/skDjqILYcbbqxCxasE22FnQOEWgaX2477CDIxlldzqu7h6YdGWs
9WHx0V13t28D/F1jOhS/1QTChYcten5mqXhOxsoreSk1NdIBHDKTAgS8qVb1n3hNJG/GbAWdIfTt
Ezc7fITLvpwe1N3RsNmVT8NkhV8yNrA5I77LaCjSkcx5i9V7wZpHOgIovA5dKJJvzxlV7iQqduf3
mb0V8lp/gBTEM6WXjLpNtPD4+sM7l3BhC973GOPFUEhJCGt7zbbB3iRelLBNa2jKYIN80H5FQsaX
SP24NCjRkyrIdzDVk+lnqdWt13h7sW1c37sNJWuLrZ+hFIpLL33JPuQuEBwcjbFoGGGbXPQXHoI1
enltbiujt7/phOhMT8jePdkh2JDvTjWZoMqvqAxycXRmubY7CQI9/V/Dz+Z+kU1+ZWDB+qUW3SMT
9M/d1eF1EZb2QaVuz3ZCouZtMCyiAFsGI3aB5BiTe/LEeuhM+tMMJ1lVFQ2/KKoXGPtbSNHabQcG
A2sS4RWNJ3DZAoEzLdH8hqiK7HOlXwk9yo7uU0qnVd+zxRm1vdLb8kL/RPb5TeR6hxWsmV8/5z6Y
YwisZbvjOi93oYEtxvGAmflEesFJTJqisiPJgiM+p1H9RrpzJefx+aNEEGl97hTKiPZ1ylaaM6UF
s24sV/WM1pxo3KD0ZtaaBzwdQk+BD/iKlpg/4R0PEhbb0a6In5umD0eQCq1/QFPS87jEq0UqD9+W
ofhRpD4Pha7QVcH3+y/AUGPHEbM9AVA6hnCP1uYnQbWv25zUt8rdH7+OU3u4R0tQUlpsx1y7kh5U
IMaLeX0y9LhiRgbFXN4Kd/uBAPUmoqNPA9vBzoXgHYJAnMVcudfN0EBqJH2LqGLYipYE5tJX7ukc
ELwGKE6q7Ksy7ZTG+Ei08ELGurdsj5Bw+3mUyiPMFtqTLEMUTmxxhovG9Ypa0RDLJTnjHGaCJtRN
nLUV2x/bq6BNBKQ2KP/vM+yWtvKfJQ3NQ8R4l36N7BxTvfeVe1skH0AC5Sd1qHt4K+VQPOF49NFL
3cVupZ0WPBx8fxLiFIOpAPVLMlVeAbioFtOwkdkBszuBXQ8AJ8MhIdUSgCyy1MLrTm+loxcFobp/
2uQu6N6LMBHHvU3gWNBWkjEEu+cv5u508DrlBgnaou5ktQjqJrXOoXO6hq9SSt5Y/i/H5GY5I8G7
jk4x4UJbD4240wR5xf0SZpHxP0291NYwoAR5eaDaUz+svvUqhj3c2Yz4qZoLF8roACmMyCO5EB3S
t0j9BmBzQHdR1jy3GGr29DJSulAFYtkihjWwsO6yrGH56LFA3ro0Er0qLkuR6FZThWJnH3ohi1dC
y+OiIFRAjbrim9dkSbzYBSeKQ5LWsiB0uUVL0QEvzUZWKzMqzKYj//XWHN2yuiWqa0MTkkoOx+Ya
YuFzHyCRpjY3AWTnvjQ+HDh4J0kMrKc1uNvHyyKtrXztDCTqKe/PXsKRsVt5u/HODXTSSHl3SZ2z
/n/3O4BArDrtuvjF2Q79JSu2lhMLdAjzrCzFBEZHjzJQzAdOXzX3SewD4pJXSTF//XA8/xplryn2
J0OBEXgy3/x408m8GDMtzmGSaS1Sr4oWSF9Dz7wxseth4LBvxJbZuUI6WoxvPPe74PQU4uFlaQFw
RkwLSIQFaCkmWUhhlNc7VYkaovGoTRUkOSG/etMrGSJK0YjgPqppA4TDeMZ8bvEYTHzgAUm1f59T
K/LCVEOjdj9SZaUcx31IbC8JKCViUiLuCV5dqxzg2aWQs/MNaby/i69YF5LmKHx9wRcWz76VXBRR
MpqLuesxOJrdFsg8vbwizK9xXZGz+r2c1QGRLWmpajg/5Zfhg2Von6Rs57NJuB75pVTUs25KL2qA
xabefVY2mjCht5+Dq9P34rbw8hAe/sr22mri99H9ob5sKeopTu/52Ab5cPHZkZI3L7i5elGBhnJc
Eb40aP1guL8zGecvcJaPutWaacv9dNFLwCBt7J7EFvtJSVahG92PdUpMEXb6l8V7ZWCZJ9lg9nX3
Bs1SvFWeTMvEHzzSkMhyCCXxCzZQo+R30TaC/KC75QKZSdju94RuuWM5Gr7gM6w+Y90556MgF47o
uynsCWDG9FxNfxSd4L89tQRNNyT+JFXI8MZzBjPjxfLsYHHB1RtNy3jUAePQu4SPYrzhngsj/PRy
B1pvervcMs6Yzb1gvAYalsFaB9hia0hP27dR1X//FFRGXqqIaA0IxiyLSHb4NI2afr3Gx8USul2G
SexFjserRjP4X2qpgwr0WV5ORJwHoZgT5bUx9G6gqOzvlYn89uM/grg60RxdQ6a+zQy6rY0UbTTy
RTgi7OXFgWFCiahw9GO8RT0jc4BdJ5HPUaKihvDRkCWkxq/4Kjaj+x9x3nXlNIG2EtpOTx2S3ay7
3G3obPoaPAR73B6mPdnyqyFsusjZAcLiluLKwH7WxCF+6fFDUa6FmcoBqCIVZHGYjlpmcRB7MyLs
/awQqfI+NCgB15yAMIZjZsY9bg1vbgHcyukk7pC+oq4+ibbRF2eBSFi0OlAMgz9mc5sSXgan1y2s
5/4l65R8eT1vd7pZfixs86yNrxBh8lNYPaANxS71sbQrgBXCliW/QQ8qEvlxXzl8cjmRcigZ3iG1
pSp8wIqSPJzx6PLkTdQqgyCkqIBqci4EyABy1CUrGjWVC3iK11aRWE32STrztHucouAF+fBqPa31
07QRLVmlomlJRNZA+BIu9V9FL5twgljD7uOK4YlXIhQmHt6xN/ectheA7MeDLa8abhgUpHoSA2UF
zg7i03HZ+hGg3OgcXwBphbxL1kJ2BW3X3tO7t9qVVZg5HAlZxcEQekOiBftsQtcmZbh4ULRIn2YE
cmecU2jV3xHwJQqhFkVHINBT5EVo6eFgOnfYg3cigGayTzRCS2ehVPmt+zSdTeolZi0XErQM/hBR
DY/A3tBpJas+60Zi+2KkeScFBxTmP4TZzuSeej1b9puW24guvSmdLMn/X+ONHDi9XE8qbrhUQEYL
NolJM9mNLAmd21gFAcPk9z3NQWC/PN2K9h0+J4uhkGbnYt49aI7R1/8qHvbaTu+12kB/WT5WE7qj
9yavHu7+dSs7BmPExjNlpARnGtyj4GuNk0RUlyfBYx0MXvqysDqLuuM2SDel6noLZP3l+57d9BbN
cQQQXQ78TvdcVPkiUQkk08mtjZZEfyHkur8O2GSULnoBblRoba1JJ0yg/MlnEonT4qCIvNrmRx+B
fqSPHLChUEGpl7wCBFylEuA6kpcztQThRQpxuDe1kvtXdR68JEGaDg7F+f8byKH1//+/ODDVZoO9
WcTDJ8G5X5D9IfzaKu3Kv5FI4TWgyQrfZH55lQDWPgfL5m4t6O8+LtRrGYy+YOuZvLsQnrAFJ5oD
l7SZhpKeRNEAi85J6mu/iuls6U8tLtTQZ5P1cE/DTY94E95jnz+PHqc9zxzJNhvYYdUNS0JbJvTP
WBSuTIDKqtNv4VJRB/QIvnZTSFsAEFXvyan7L6MoqHjj5KoKLI+dbZwEBFmiG4HlqkW3wwQDx2Rm
lmpRkRkljaPH+cmvo9+TEG6+2FGNDHVnX9XCHK3kklp+o911Bt/nwY+u8BcQGlMQpN9pFR9i6W2Z
w9BDA4v4p73n//4ZC8WcQ4zyhf0L9bOBH1umf1vabXTya7iRu01SuzD9H6jnrx4Gv2TNNcX9uQTF
BFGWp83n8QmUOWKtE2Js/NUiBv4edPj52BRI+3Qyf0TqQ9TKoefsxUN0Q68PhX1I1w7BZcZeH/7/
R9iHMmiAbb1QXCZHsOKkIa6HAj2thRL4VopdJZmiU1pQbYjH1myqatMJ72i5Czkx4QULdl7S7kNL
QCIMgp5DUkjMI9QJFRwyIlgDm7PCA+avw5YEF/eShKSclZ9Qw9mz6JLm8YK9H2TON1yCF194T1jS
DhKCo4FyYi1YPLwd06RFueib+TuDLsQz5bnhwfjJZuqqr8Hb9MR0RSghhGXs807+BXNew8Hwhu/d
g09H2TS/DLjRyBzd1aO5drZgl1p/bw/J/yjVV5xyiUcKK0hFZc+lVwFLveAcNZEIDr5fm2QD4MB1
zJbVYBq3XyfAnrdEjAX9ycyNAPhn2RLTiVLGUjuMhvG2R2KjkO8D196pr6QcJl4DY0Q1JoI4at5X
5KSsfvoRob/WrtrETRiapNe5Jd5IIxDpgIhJm03KtcipOQJDeZjHfMhF0MqniLMjP49SozhyKA8d
hAL22y5Z379Io+ZAYEJJlKAA76od1REzr2UHsvSru6GF/gmiRpljBLunXstzxDe2E+qlUNrauXCS
aGGKZT1x+gcgdcYAdI43QaAYxgNOluTpW24BxB/9ECB5DzOOxLxkmSlJswIPoAhZx0vI41lZ80T/
nedY3bd75ihaVDuhwbhKMx7Xma1TQZ+60wVNPqe8w3pI/R2FYGoYSjlcI/miJaGTRMKJ2O0hks0c
tQj3fFYfGRDFjBbBaK5wNzNbear4plupjWIEv6flrFK+P7MDdLul9zFCvsyA0jhTAX77UrV1Js+K
yJbAGXT4gg6WfLfiVUH5iXv/ixrGSTJyot9FvWQtoUGYECHO6LfuHMuX+lEPaJL/XSVrCMaciVXj
dOWFVrWeYELIqD/YWjTeWofxBSyfgJtFronwTBBq0LjBEvbrTSH9t1h/QJ6baCqKjCfoWKp+VUm3
IBkp6lFk/N4Pv9XkJ6EWIM2lCEH3HeHnPtSqYDrLyR4QJ41gcdZmQXWuB5LOevawSAJBBCOwEpu7
jhq89iQdq0nWI6rJKultXfJiKrgtAWeo2nOnAE7OlY+Rc2BuGPzE6Sp5aV7KTzqIGquVpTbUqx/c
Y+nYl200TlKIVO3G0oNcIq69jl0UU89/h/bghNwTX8zp1q1GONCWZIEfi31q9n/wgoh/rcbPJYkN
DnFZMR/3WzoBwsLQI9UH7E79k32V+oE/5NunNWTlgEXAZtX/jgx4Icbrut2Yz++EWCjPQ8RzKFE5
mRTrIXfK1d50TpleXTIVL9QLe9cuI8a6EbLLBB6FarNYJ27OtOOYjD75L/5v1j0Kc8smu68V7iQ0
s3G4I6WvubD2P9M9nRzKMdmGPk2PQSNHwWET+HFiiCJqh8UoI4818Dy6hBvtHW+xqRrXNszdYlK0
h00d7hjRNNubT5svGLH7yk2zQYY1UlggNoJmJQbLZMBQBD9UBeMVrmCJi6IIPgnoZgST7h0OYbsY
ReIo01YQBzbOeiWwJGvg60elQjkNNv91KkjS3kxYb3xLq8B25UixGYLAry6mnKbQ2snOvdh4jpME
elUZGNrzHs+CgyHvzWRrje6y2X1GEPVThzAV1AFSP50DiZ8Pan8cqzRNxWyMzrllarYZxJ9izlK7
V+hP9RBRR4V4MYz89z5YZw0mmfva2Fgz/1XO5619dyWubO3IqrBBIxXuYo0WadvAot2T3hktaWzQ
zznyR4OP0FzfbaPKRAXIlpTcu7xEEEiUaXt0mFxVw3HmWimUuIjTe2+VooRZdSjHUQ2TBcVdAJE+
wxyEUnquRrh32Qyhcd0FCoJB1ghLlj5Kd41GUwYAwQ2iDzhTJETtuHz2hA8TR34jT6B2Jviobb7j
fp6iv6EiHvzzCpPoY5PcBQMKxyikYINbMvFu+3zo1zKQSWBhvpJ0B33N0ez/K1Mrrdo2cYUUx2RQ
UD4w1GdVFZYhMFckQVGIvr7ykxt8LPcpQp6aGNAd+E4zQGxGRde4Dn8zgVDVPZrkT8jj+y3QpZgC
8DIXiKpumIzQIdzpl7477Yo8bdlxNpDk2Qhx9DM0iBCmt6s1zCiem+RIBpzG8HwNQhH8YYbLrFL6
F1aAnQ5Nu/Zway6UyBoaksfpDfwfWKhKdxmoxq7YWSOjalQ05KNRSEdmevk0lqTUSgaxt4NcIgce
yzYN+xb7+96gsI2eDqtK5QnlnW8sxsJ7zppyYyTIyhecK0RR+RE7BNS/rtC3sJHWb5jFZZvscVmH
CFRSj+fHDlx87EvtYNgEioyz0wdW3yMGNwIiEXQolNBby8HtxEhTwReRSGE9uwNapa3ijS8jR1eE
nOspiyAIrNifp6RxYVOTKmSbTotgAGw9LgzHXkCSwaP39Uc3RV/1hyjdNusTf+9SJuGBW7wgg+2X
BIk8L8UuzZxFoUsyGnTOTM1G9zFdJkbFQTWdUTsBZFDi0dmk5R4KV1sWXGzq/+qGsRDmDMPJwLIf
9wFtBe1XHXDwVNCOvioUFCbGPS2V4PVVhz2Mn2mzMOLsBMicP4oIXTqBNNwE65kl7AXBF/WJ6WEq
qanbeAbgvtXPxEXcdQHszykR/HDtElbOfw0+9gmRRlGoVhkcWue5dx9mrGfGcLZqnag4xBC3YZbK
X6yGkX3ONlXhiwcy0qh67YXKf5IAn3Jsniar9F6zlGb/E9GOj4jU9IGXiA9WX6kBrhV10jD9Wsvn
8XNNfAmAHOdZI0tbtCobH6ShNDtH+mQTvBYTDxGzWmsa4OfZRI/M3jQqPNCZrk136YVhCwUJe16S
CnRbG4iVxIcJlmFO1G7ifRcIR2/TCAJq9pfpgnRjfj1Mm9q3ywXIsfby1IRS7RgSjWghvN0qao3G
6UraKEuU3Nu4pdkS5ZWV88ZuXPWJv2LG3mjIJBqMyIYUsX8iJM3JiC/QkIikGbWAg8kldkQXve6W
1xxrZUX09Q3Z5o6ScL4lwSxSdZTZYuz6Bql36kh4qS+fQQDtsHHIj3AUN/Y1ZdqZmxU3FJj/34+Q
DMcS0ppDljLhMAgrkamx3q8yhjlH0Y/atTqkR9SGkX1bRl72+qc+fqs5EQ6cuN7g6gufW3+FbjOp
fdfzP/eEjgoQCc6PZaSW+NcOeW4Cv0dE6JDO5l28ut4rulz21NUAnx+EpXaGZiLIKJwHJmP8OJT3
OPtB1zLfZrX9aMuLc3Y/FKDBgBrU0vG3WuB7+ClhD/7XMNH2ftgj7pPKQMA36fz96xadPT7A7APn
1qrZVOyjAs09p+8W1VEPRSKY+dCZFtwERg5MmJtXzLMdDCIGxhTcTNsnIpMLM6C4BfAPZaMpFYI4
vz4NbP1loUn6ehAei21tZIcC6grNoCuqFzzyH623Q4/vJvDmvYHtoWNKof0Yo4H9PAZki33kT3hy
5OKSWnnuYGm6DsHKCxCHWLynC2nmluEfC4vMs9UkA+omlQtQie0IohhVcIECBniIrlsr6Bc2GReg
ucCx1BbcRnjKQQ+0jeiSNe7Bzpb8/GTkFbFwGM+f4Lx9vdK7eE0bToohA42qT0BjUmEnQ/xQcyJ4
PXkqAaIS4MDBV5179dcgn4pz6U6Q+NtqKOMJw1PLGNXQAfBD1FLyuWOPaksUBtx4xP05aAVdMoLz
vjPOkS7nHFjzcXV6RRYXbmAIz0XlO3Ve9l/fWW66Aewi/E7PySxm4U2LiDg0LbRF2QHsCF/HxHxY
AgEWBM8gRkjbM+ZHSPVH80a2hh3DuqveezWoMXJNb6d2grDGbWk9V6QiCfbJDy2bFjRBS48XZVoc
hdoGwRcMWkYH/h83sOmu+AINf0xXtXuRAcPcfiQSenuR0KLpHU/zheWvXJa33NEbGlXab0meeMJr
5FxeseYOIRyzGrceunOKyVJwT0nL/DZOtcECgf5HWjZ0AHeR5cV0UWZ/nRpd2LPt92bzq94ogz+o
H+p4BiqYp1FL44hYZ9bG86kFlWPI9Hj3HcLNPAxVh5QHPGrYQF2faMNLmfMXyUifEW9Syg4+P2TT
DnhJ1hsE9TuFXZwVTiNYoRH45N++aFi7S3MGpRGsAIGKgO2cgvm8uQmFvckSr/fUCN/ChtEJD2gM
0PD+q+FA9E3gZ2QHc3/MCO8gIGpPjMLJN5IGHnmEkhUpctMkfkiKl5VuxIW0LL7rVwnwdP/vJ/Yu
Yu1elqtPe2Od4QaSNopwmlM9+HCat1LLySjNtZFoZFBGg+AXHVwNvmhvAV7Zl6mdDrpcCDGsjm3m
13X9guelWOZ3rGoCcYB7cs/VCRCuyIjQ2CZJXU+iwbnZJ0JvmpxQxGMu6E6rik5PEeAhjUn/2PN9
nTImjhKGxuMcsVpN0Wnc5m9uohwyaonX6TkxlI3jiGcN9EXzrEoICMdbYIqkr1CW5MBKFgXzi6IJ
GLlg6gQKhADoj1MoBSWb3t3CfA6TLq2cdQSo+Z1qEQacNh6DpUeMgp4cAvqXcrSzukubeFW7obAo
n12NPFQ8scNLxWkzXYLaN3f03vs3PyZ36D0e5RjpD2aVZPK0K60tK3BeWrzSuYLQivoHjFudfLUL
oHkDpki5MwVdnCbduyfa0obZkbf43mdF7GqmIFy+AYaKO3/cARjr/G3hlGLv54Swt3gpTa2ASn+x
pkuhjOmeVYfGXgyRL6EEp5XSM9nvb8xyf2bCicuKVioc77E1WFVvVfQEiKOLEmvBeNDpEefOL2aN
F37CHMQ0JbqYwgJONhZINSOpGRqjyap9SrRLAPLAQFhjlVdw9OHnIgy7IGx1U+FV0rFym+TbyiMx
bd+qysIGluG6gUhA0dSnCCvEsXSggu0DAqNc4deEuzmyMVH5NYTMWvw+iKIxA6Qc5tX32dxjK0CI
VD6ZWJ7JVpmc7dtvYtm10QsFbOv7+UikFiO7dkg/1PvVQkz7PoHhBmSzivI8EfX+m/mNQUEd1fOC
CNX7+TDThY2ABvQRrAd79tDlTggHXtTt/hgQrH7RKDx2uPZUIWtRXXrRcj00IvVywdBO9LJCC1S1
BttPLLRb7POLUT82KWJ+gzyLmRmdsktT5WzjCeJZp3CUNtNUzGuMzZxTG/DpjuRE1C2cRXCcMVgs
AIbM3lVtuTDHVTuwEkiiNKliCn6VUk8FX+ik3pQY4IGyDN60eUujSfWWrzNM8dxqYOXscKJj5GPw
SrZDtABi6HJbWdTiwWZKgyp4MHxq6bHE67028UcP04Vq8zHYDY71HqSKshLZwhHpwHTgUXPiauW8
Oq4TY107DcOuaY4RKm6sQkWriHXdtWyUgGNKoRPaJFy3cftTFpKrVHUBb2YUswTnCSelSGfmrmLl
7bMpwxfwy6VIZGFT+ioa/0mBSanoLCAtD5ADV3ZbNUpS9uE/uCH/GPHUZPXHUvoZxxphX0epLjhk
7G7vM8mlA5qzJrrwIr9NPcf1X0p3bvhrJEHOA5Bi6nwgYsZED0zHKDwYTRwjSHtMYNVyMTUiZwK+
6UmpEfi+xHOqsPQ8e1QyUKzN5VBTiS0NxQhCr5XSij20GYhQaxBubCGCdS8lK+gdnIrCj6bAbcWm
N49JXAX/HM7oDUV3lktXxwvQc6cT2Gq0ZyYk64aKwYWS9zi1eCcOcY+K4FItIwUOax3I0tce1LP4
Rzt1MJq3S4HXVBL7dCZuh5rs67TNGm2Zy6cHMHR7NyO8ugOV76hojU3QWr3M6Md/tKxBUjxsRrYV
XPzK/UMDsCNnHVEqmZaJpmVwLPFRwcgnKqXrbGLBNQUsoPlmEopxyDpv+8G2Wxl4Xr+G4FSmsIQB
dga7RoHDBHINJjSM8vy4PaE7UxPUeYXiKijrZbjjJ3ar5Mup9XgQLNuUH94GG439TcXU5dV8iPy0
6eoQapb+YrUEM5Vf4XornS/b0y1uTjVdM+XPm518lBK7XY0QwCyGg+XQZW1ccMb3t9y3YCgcv43+
vMBPSIHRg+5jPK9bociZo2l5l1sfYCAt9XnxnJfW+4i6QWH345cb4GeXm9LcfkIiJCENqmd82YdB
P6gliY0k2rOyQJrn2lGwW0daP5qDr+/EAsgtAAIfgEiaYigEgSEoK8uz67o4zIdsOLd7WDJjV4cZ
yfGv0WmaIOKBgWsZHkooKuuINrEldKIy7vnXwb3Ouh9WZI4fvkOPJavTCHdTsTYkXIGXcDygSLGC
BlsU/IV9tzMwzvsNzSfZLaKsIbPABwx7zPZSjKUokZ2u3ZfzfveSQZF7YJsOpP4OqDm2PRZeTQua
j4Mh949IMl2qlYrHClV720jQCwZa1gomtth5dMZ6PvTmf/O4coD4GqSyTqfhlRs/fbl10Hx4Zbgd
H6uULd64uUKfpF9E9uHXSqfjovhU41VsANthDgcIHl+U8XVzZnALDjWLJrg8aboWg1YIZdY/dnCL
cgIFVwOth98KKi/ho2hvHkr3Z8MzcvKCQNuEq+TgTKLl6Dy/75rImhO2Ypk6JFb/zqXmtj+uTY8b
49TJntOEvWB+WtZDT71awhzq3xd0ZtDOJ7JiKo78nhJXrbg/2u0x1OT5exdwrn5e2RMIlwi54v58
X+4lsZLZHGHmYEzhIc1bVf+xFrH/evzUN3eBXt8BflX0P/vLuIANukmc2cBDj46HtSdjyhrPwwck
tf2OdajQcQQ/N4r4TbYjl/y2Kbbbi2xoK2NTqcKDYdTCSH15xcaX5U3G1SsImgk/SHCtRqVORw3H
gAp4dQrmq/uto/myn7mDhtpnPDPtI3bCG+/nzp9ax76wW2pX03cC01kQ07hiOic9Lr+n4C8QktpA
V6aBIId9LyPA8dhVH/PWssGfDP1Nl+0D/ynqv3SoqSFXnSDv7h6YNXJy7m1yqIib4O8bibRhk/Nz
FSTYKpeTTCZoynWOQiKc44FuAvqc2qqcyDcemDte18gSBIeKdD3/75bMy6Xrl2Kikw2vAb6KKsY1
9qTWr5aZUBUDJXdQA6vPDAOWBgNNU/jsYDxvJOOgO+9NwcgvAbvosqcad/PFJu2L3l1y1q9+m0ac
/bfUpRuXUafxl6AaF/UUC5Wu9ih6+44Z8D6dnxT7uKYDeJlJo2Aq+SN9HgQtRByQunSovrBT3LpO
nYqS6xh/YrwdOOOJ85/XoNYApIw1m1ZyHM0EBOTeo/2t07ycwwqCepamgkme3UjvA5jVOnaHkjob
uukrqICsolJjOWu7r+GefQBRC+PMDSbh6O2vu05Hz1oD+h8fAc8k5FRxVbBlQ4wPEpc2cUL9oNxz
0bUPbM5mplnJspfNQG/nWyHoV0IoYUicWNdwwi1ZTtkWtL0Cvcw0DkU4EkmRDafygQf7kqsmeV1K
whcxn1QJXAMSkU0ZYp6jdb9t/lv2LQow7dL9me4/U9jaRl21Zsox5ZwjtIBFajdtyVz9fob/qWsZ
WoOks+qBpEA9u6acQxG18Z1Xry4=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
