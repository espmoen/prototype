-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.2 (win64) Build 1909853 Thu Jun 15 18:39:09 MDT 2017
-- Date        : Wed Jan 31 14:40:28 2018
-- Host        : DESKTOP-0KA73P0 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               c:/Users/espen/Documents/MasterOppgave/prototype/Multiplicator/Multiplicator.srcs/sources_1/ip/c_addsub_0/c_addsub_0_sim_netlist.vhdl
-- Design      : c_addsub_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
ZPVr6gPhgA50m5jEg/cloU5pPzs5ur+MQW36xOcR8MdF8wWaUr8zIKTkcmQ2+yydX1fZ4Dyi3sIY
TpR8Ac269Q==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
m4q9MtEWGVJ85SvGPq9Dz0jIl7zWLDc7qOxdMPlcTg9T1T9M5FPPiGgkxREX6nE7+9JKkFDwnsA9
8+fSm6Oi0bE3MkKI9FO2ZXM7K+4Rk8vDA92zhdLKaJ34nA7vjScrX2b/LBmzP8q6nQDO40WeaUg8
L807mVHk8Be+E1biF6U=

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
aatvZnYN0uh5k7QSlNnnB6bvhD3FbME0tc3JI5aMmMGgeBi70uOFdm+jeJ/aiZunLfXWyCxoMCdc
dBKB//l+xg2I91pEyCdzuoUrT048IsBLQwoZokH799mJgNx9daihUGv5ybbWk1i/wA12WcjDOJJC
Er52KQagyjyU7tEBN74=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
dJns6s7QA8cdhO4jfiphF7PmkVvVC6Dhh0L6aFNskwPuc6Jos8rWXR3Jsgb9Qh/ak0KSbr9NkeLv
RgWhqRWe6LELfKQmcczmKfG1JTRag1Ex4E/VjixkGn64jC/UtyXNaVM5yfO4VMr/fepoyu097gcT
77pUFtteJrLFft6+LFiCSz1u1409YDqqA4/3ehiUO5JOTCVkxneqaqe+aoE9AvaxJhXMmJbXxuX2
8tKWpWklIhCC6AmEZ6vWD2uWzR5I+9OwtvmDMR4Kdzhy9mgzB/ud3Zwi7GCYMIYw3y7JpPsofCgb
v7QgaSwvHIaUuArSLJSXzaiJo+vv2cUDfF7Kbw==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
D2SIfMgicSOsDpsKHm+y5JCS0B9zepyfPIW5qstyEt9dSu53QxJ8dnCG/hq1rHPPNh7Ynj1WE5Vr
omRCeZE+4pAJd56hxgT6gDGsB9CWHv564ekGt+/ni622rk32WJgUuR1+z1V93RmKfyOTETzifJzW
c87TjOtsORPS4hAn8ZDvknAumyZPKMcIx2qqbUG6HkU0plfnmrKVtvmQFuscX/So3RuqQmaVrgEV
NxM86dJR4oU66dzjwOUynRyBsQ6WtLWtBkJ2Q58nTXYozeEQ2np76d0RpZpbLNyp0May2HmzXMGV
nCucai8VYz/d2AjP0bysze55WqGsL+qEO8jKzQ==

`protect key_keyowner="Xilinx", key_keyname="xilinx_2016_05", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
tMh7AxjNYMBJGMu4nT7+MUezQs9H8HlH2v9U7UVzGDM33uFa169W7t3PYXdwUnixdASrg4Ii+jDe
wcKq5RtvD55ebjKBFk+AJkdG5+4o0RsJmF8MRdgGUjYsu+yc0E70kG7GISyz5If8VZRuf5sfEebp
MAVhUoIklYMjXV0641B+WCQ851H+VAB7G65Z3dPbNwkIDySVa6ZdOY8Mh7SSRTyPs2u1iaLTACcr
cNLUc3i16RbKgf7QG7DMwDeInwsRMlfNO7eZeGEpaTeNPJNGd4TDVZJBkI1mLP8U6MFEOk+3GROr
H6jgeyhRsDgX3tTSgmba6DM90HFy2Y2meS+LLw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="base64", line_length=76, bytes=256)
`protect key_block
huz8belMn0CEwl03bXHAyqJ/O6IAbKf5TPCysveXEmBdVjC0BCDnF8MTAwXEnemY8UXNKJeoj+Pe
EGDBVTaNOQsopmJ98IOn6jTdzGhV0paqdyU/b+4GptTuh+NtCID42YcL8wOk3mphxvxphbkYrsAi
ZXTj5F+/29GSSnrLVKsE3jynmqfOtWDsUvnWx01SWLEqdYbEpXM4prCegLjxelfDWOKDlnXGJzCg
0KdH8zpgUN7ZLATVURcLOwq5BXx2JMA6i1fYWR31J2wzC46OL5Whuup4S3rVasAdb/RBgaV/DU90
GljWC7MUaCsPVcnXDlPbaKXKs337OIZJlQOFYg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="base64", line_length=76, bytes=256)
`protect key_block
UdofYrufSYaLVjkF/sPh02szeddZWIk0A5TtlRXOfFGSRi/giVdA8t7KFEERrphkfBLGWZAu/TVq
0aOezwMLBc19UTRm7NA6o+Lpd0AXzrqFuOSuXon90O8iZZ3daQi/+umDPBWi35s9XzDckoLMH8l4
/4zniWT532Hmm8xGieyhUwim0N+w2HfodHeaD8gcKsuVIQAqPZBi5xXbljurgQfReF7w47Rfajti
gC2p2k6gAy2NN99svqwzlfOql8IKrWmZXo5Ulpt57r793UoDbKy+HYh0hpdwMhgFlVv1s22l0W2c
Fqx3FVPxgEDMEIhd0cURwq8G7aIY2usmLZnxKw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18288)
`protect data_block
51o0UtFdn55hPaaFXhWSFrm6oPSS1I4D+GUf2G27ki5LvEstji6RW0eZEPSBxfeVa/me598DeTRK
79celFSaGUIx2mH2dTu2zhS0v+sIHTHN06pktIEUmAiHnAlDk7cmoM/hYX+JwtXRoaVx+bvtnGDk
uM6W6DaqBi7n3wkz0T0f1XohWHFC27pOemJPZoNeFTJWuALviu8nXls4RRDWHDyytprmc4MVRgoM
Ekmu88Ehl2gKaPLTCBce6wXpl/4rxgm14+cB0oMieqxca5oRW/6aNA0KBs1/abDi43WtYJp/OjZA
2kAow69ZZvTqT/i5NBdz2qxggwAB5rHRS23gG3hjJ0JLdfqCSNL0i5G5SI0octzjfHboFEhjAyUr
7vB0LC9bzDqJUFbAyrzvir/++MfHfUZiKq/wM8I746FW6YPC06Efj5pz7LJ79BvB898151CSaD2d
Y5baAE+HRAu9eGVb7PsJoS6amFUAS8XDI8U0kpEBsFGnTrXDNPSIVGjZw9AnormYwx7ineLt8Zpy
ug23NbjliBuEJRRT1LzaURKNdeJBjnz1Ew7wt09x0WDba9LM3MtBQhEcXoZRcKIZZraKS0XwniMS
ww5F4IjU7heMQKvQ9XnG5nvNvCnEN5Odr4pTHSDoIJnBNItTCs9ts9CdaPjMT6SP2R99po6cnmGW
JBYyAQaizAQCHszd8YvzjjIM+cLQjfnv90K9jTeQ5FGiXbHNAwTrHHNNKHHzrDMr7mWMFdtg2d6e
kLYduHb/AsxUXECPvwkSB5jIBqG5s2vbQxA8Sn1keWYsbRSL3Jn2YC04pl952WK5UrzTE4Un+Vqo
4L+g73ozDGknR1FRC40avgq/nIbETU9/eCszbagO1gDvmQtPVhqqvD57YmKpctmK98Hy4RrD2Pby
0R5WouDAmOb3zWGpmw+e7Kbg79QvLBK4mntO29NMTNJYXjCOMRtymR6uFGHEviqNLlzlmeEhk43j
Kkl/Aj5cJU2gfbnuOWG+it0JQqTr7bOsIs0vLn/W119uMn5UywLVdLsX/3TG1DIG572jWjy9Akpa
huR/2U0jF1DCDYt8bvnd9DbFES9P0CJHiTPZ07r/lSuEigE918vtLX4UoowxWc0hfP9gPXlj54iP
zLIbr47y6hmpaLQIAZHUGP0DT3Wx4EujHvCmAAcJqdtJhUDDONqH/NLN2qb4GfmTPzFcLbr4myvT
TZWPeGNIFzCwHw4QZLI0sQrLpvmt23QciZT38v8n4/yDgf53BxvfMdNKEyqH3y0SfxzqjOLdzOyb
A1ntMKUtKcFbnJWc8gFXMhkiLM/RNWSApCsABOp0l7XDGozMmnpgujTxswk1F3FD6zyUy7wPcF1B
KceE4vAuLl+D7rlQt0EJS35Wchb77OpYqE1pdEsBayXeJRHdyYbnFl2OYfZWcyguvDIbsn2qOBCN
NOWpATQJtNgq5qnVpGY9dmWd53FF0s5jh3DWHczvX0ENBMiXCaNVxWgkxov+K79qCi9A8oeeSW/K
5Hsw0SKDdTLZNO8vgpqRwURMRnmpHQVfO9J8uS6rr5WPhGRL6peai9sSiJ9aAhDlXcobVJz1sKGq
K8khtG7mjggan/siImT2vSiFlXE29g+HinCcmj48rRqpnJxYs0HDdRjWKK+aMsgCBVq9EX8X2h0J
5PFQsUNT6klhvbYlgZicxwywbVQ4k7+JIBWjgsQxEKZJp1L2DNFvp9mG7mqjop+iuRiS5P6RkAs6
HLetjwos1XRE+b7Sr1/Iwqj1BIn10oADAjLXvkKtJ/w3EStew2/oOns+ALmnAlqDfcPkmy76SzNH
MuHCYfBi2gFeQmPvOG1Os1CwtXhyITkkpzKAfu5mdmnKm1/BhxoqP0QIqxE6kVEF2943l9K9D0bK
jkXKao/x6+XyBpO7h/juLczTPZQ49CUTXyvuyzlu3I+P39yLnWU5EILUyOPdT13J1l5gstFOjS8c
Ow6rqskUNhC53ZXCZerxgocgjdCit4fs2AV81BorxmZVUrUY5WABNPKV0jlBgL8BSGt2Ltnzp4Cs
aPnPtjUKZNNv4HX6nwdVRiCIHRrCef3g78sv0SvkCnIhvul5NXpKs841aofkPEvwUEeemj73SgCa
K0boQ8+N8n2iPlhxIOi3d/WxODb5jes0hHj/TdQm36+d/5C6kCrAc5LfqXQ9MLX0TGE9AxHVfAu3
aRdiq2lR7Z5VKENusqzwrIpk4Fz/C+5+4oGQ0y3PGYZpExvv0Ln55DLWhinYGU6gX0z4bNmcccsn
5tBdErsoWlyRz5Nuv3lAnTC+HwlbR+LutkIRh18tQBPUANrDvSrxYjnNu6bYmK86C4LX6fkiOlvO
WQjU/VjIJnSePDdzU+fSzJK+fquz+dfqD8il/u3do0pKP3jo0fePk11lrp5LyTIVW8CidcqS8par
89QVp9+N1CqHCl+6hDST4MlC8ICMIf1SKN5WwAUhOYjRMsS85UtxUXsjhL783CcCEUYZOu+DuX/t
KZv9Vn4RcOoK5tOI6utTniSrjxvuPuygC9iQNLeXxfyqPhcuEYtJvhprYS8yTcwGsQHiHFOXwDhB
tbnq3Y+nf4JAMWvaIAy8ej/kANuV4L12V4NktMxF57zNfgL42oNX+GLtt27mg6WLk1iZ2Xe407ZI
1LhnHb/6uAgqcW2BZKpz1PbAlPZ8JY6FqB03zu6CCiDQmCBeQLAxmaGKNMWQyR92EN16vih61wMX
gWPxfeEceafu62cOeNpv2J4BEtQgNtukRUP9nnSPTci6TJJzytR8GGAjGQRZ0X4jJQjbfLZxMPKg
4I5LmFd/HmE4cyU4im3PMNFqWiSNpwmz4fOSAds9mzDYBayKJgwTvgQpvln6kF0xYV62h2B/xfi6
zkdjIsaRRaNnmwbmFaWvedvzem3fS0orBb2X5Riq6cpBr7MmrMzsIk4tDErIz2XG1vX/2G/+Za34
uFF1jBCFl6p085v0+PR6ANixFIEho7jDdTEE94UedW2U22BFjpYht5o8P54J9GQfaYuAEyuBjXbT
m+7sZ9tMyUjmV5BxanQqO53TPTQgiglD5lH+0VBic4qhnh1rP2OJV4TV1FdYDkamvArn/qxfIu6I
v3VWljlFa5pd8+86PGACtSBHJOM41wz2pNsJhR+reQHAzxITI7yUBi0D4WSKhyN+XuahfOa6w87y
ehnHSFNNK/f0FDvbiuM9ckLP8gipWw1l3epFxW5XF/mzXJuNqC96LIGbTDIhu+PGCpuTDwa30vL+
y+kJz8HJZ1XzoncCke6cia74ciajNoIvsBHsrC+OguxWT0KWiNrf82qmmLURQ46QjmbTmHOmPD9v
zvsuMN2VsIzuXI59FVnbZlj1a12ulPeU1zVFjHJBH47x0/eRE1EGM+y9VE0mciq27QesmOhi/xC7
HBYdB+6/r0ah97NWBz9w+/uguqAWNNl4JTgHn9n5OqzQqF5qQJBIo+mc5j2sxFwwNO1UxxCV01iu
VJCm7UqmN2mGEVSgVEVn0yrWtBm0u4CK01VJ/LmL+CItKMxtOcHWoOb7DLbu+s2V7gnaPzUN3jUY
4JWJWON5VAE1kpHYCTpoNhKIGIDBxKWlk+Y3ow4JMSvLDrIuJQ5X/lAK/Ug25TX1xSaKuEfKGtqs
tgKyvDSAXhhP6hHo7Js/HrL3CcGzUr0aolB5LZMhfKE3b+RI5W501DAsiA4C0tcBUqAFeTjG8rUi
756tr5kHeDEs/sPp96wXIrqIE1ChnhTq5RbIXTEjE38LZCWronwaB6wVx1xEIWl9CkTVsR6ByBS6
LcSVEidYpMX67G9+Jh1Fxrze75JH0kTfqf+iW9DvvXNsKYEreFLFgVyXxIDfhDN1dmRz5rxWWyIC
RQR+hQiHZIjXUfG4PGVmy/9qnOokoblXKJtp0c3cmwhxTzgExAk9sl5gMd0MTSKHJMiLJO2ozGe4
3ojJjnhDAaHmAc+BndlfaU6wE3NQ9Qvu2eHJOWbu6Iq7m0z5PHXYsnNJgOwxDsPmHSplI7GM4wJV
ChJYjuEwt7AbZ4SQ2zmFY1QJVaIJ3gGlHK8RFKNuT4dY00Q5LmlWANio1iktxF6Vej7k3hlH7/Q1
0JrCE6lbbENSANTvj1NR87p4RHrw8ic44s1bN5XWiII5oogo8ECO38OJx9osko0sV/dEfXs3LMpT
4oHUnnwAhhphAgi0HUVHq87fdPY/QahL5JV3Y7v7zVM/NxDDdJVoR0DN6GWpvqC4qrG5Hgs3ji6J
av9wAzhEjFNJrRkea9bmtYtpgPn1hpcSB1F2Q3Y9/iyQxuoM2Hc740bkzM5ahb/4ttBFEAUCEAsp
w7CxNhz+LP8lARx3NDZdZUziqNLH+NibSdgwDDfFgp8FtRFRUpolO9/cyHMIZGVGevlpKrvdIXq+
rE9EiKdlZRZPHVKxOOi4F5aa054jD1WXVfZtSeTmR8qsKUuEB4U8LNMd30isRQRFsnm+sLRjizzA
8pRzahsncajJa/N08FfD6rhU9ylXvJZWAMkQp+Ov8uJ/qslRF4TBoHaKRXPEQWb4ustZi+Igknqo
5qbZTe8EXHsXSiv4jE1vfdzZ8FNlAlpaiwJ8vLikJGaZqfXP148XLzZD3K2cIWQCVXFbMuY9Zogf
SEAy8USPR9S+yLu1mjdIf8ybblcqF9Ne7JfROlXBKKjUsUdcy5iSWiLko4ShI2EUUinDH6yWFsvJ
K6DELLmqJPXjjXKSHlU809Iok13OFjoHRilI+yxVnd3VRPIDJ7uzmPJ3kAo4ERJBJ13qsU0HCB0Z
3GHIKeEwHLnSIN7CLWq5ZnYHkzkjYF8yj0rqM5vmMuNW5p0ki+LQpoa7KgcmKlxkr70D+T8jmPns
qJSRP/0GhHx/Z7KW/CCluQC44wYkTRFlmz5MVo3CFlDtqdCLcvdI8Aa1lrOdNWmVABLyO9xVP8Yb
qLAvQIwy/K/wpW4FcUnScLgSqHv8+oyYiJRgJ4Wv3atQ8lG+LNivapxo0jQw5cM/SGTly3I3zDQN
VGrYJ8oAv0m9clX/SwmcHG+1pvI5Zej8pPhUqSqCpFcloRT5JJv5qiSKk6IRdlPpp8cPc+WqndyY
gvpCCKlZNmGNISqY/8OmB8lHNXmvfUTFmJoaEvDyOmph20Dr1XdcPoKdZdpDcrduQWm9Sd3jN1b+
rA7ZYf5vDzYrMavbYjZGWgf5HgkyFVN0dBrFsig8yzRf6qld0SgZs0bf/9vzdvl2byZceW4upTL9
32kf15g4dnVwSGVSuYe0VBaR7cfn57lkauZZkU3esy5Lc4i8YKwTmtOnJnRLL+DbwQMQ9+HxiXBN
Jxt0/1sK3JZBpBr5XVU91RbZUkw+aCHjYBwBNJOr0cxMw4d4By5F8Mn4a0ojw179ulEfXF9qmw1V
b4JLfj4s+p9zLllEO58Ann/G+B/anpIzP7EycmrH94KpG1m/KywI73JXfyfnVYrojJpXiBm7Z0zB
N5DkFB8cIpEU1UXIv4OvKPQsZf35dm0gqv6j9Yer/7IxggBrtgmXT35itoIypBUtpk2trYp93TgM
6ph1PpMGjyMSvFUR2efz/AuFbK5TnyvyHStIhoROLwwpGVDVdPnrKBXGMx/3JRh2Zb1vBzoZzczN
YV3YX5WUtlIEt1FJciPsdV4mKlkS1JB65XEG52RWoj8+ju9CEa4zJd2it56amMmTW2EDvglu/ThD
eQ4WE3kfrTYHLTqcip6ndDAk3EaNMTeSdAvdqNBKrGSAswPWs2zwJhsC227NTTbI43EtFDqAyLud
eBOLxZGfzOjkOs4P6M6BoWKYfF/qaXiNnIMdN7uAa0Wk7RCl1v22fjnM21lgJOxe3/iM50VP7dta
07a6okH/S50afAxfwWByuyBd0v8TunkCfKShJh95YsJ2SJwksu3BUGTutLJOJEi4Prk13LvM9MK9
SWoXubDmvv0B/z4MmEPtHhz6fv/2k4nQjSgSr2xUmxfprHnRkaKq36pdD/ayAjkXQYCvJNoOi0Fr
QvsrEYGGwoWtJuESRBVuIWeskgjMI6jHmE/3h4e2b4GBVAwBrL1f1Spc479hLGcqlVZOj+cFzuCj
/96RVuEcIDKHB5ndBn2YYkS08lPFXQ/FbMuEJWYr/QLCZlBiGvHox2STEwgcxQ1pg770GxZ3H4IP
7SqUq1aCHT4txnwhhhlXe0O26U7UzChsQn5EwP6QKgR0H+qrXDhrX/v+3gY84oUCAkg3Kq4/jBQi
Ijjbz5/H2LYFi2xaah9SyiKA3ycigx+H3KWydx6d4jp9PA8vRvdsPx+h8HmUNUVyxgJaXgAyjx29
OvpuCUH8W9FdwpE8r1mDIVHaSwI3Dkwi7f78FEUDjvoSkGACha34GyVTohtAJRHbiMi31SQDqqHm
puqT1zbTOHRdcxqFSTihGLP0aWGJS4slUu+NwzaBVmDCfly+KvM+IJYQgxM8h/lZjQmr2NcDUUl8
QVhqAUScY5/keR6z4k706V0MiY5b6KlM9lHODs/luJl73gcexcH1Wd3F5SrnZ1VAMs8XPveo5S6S
gMuuC/j1xS90/S0IxVnJrRvU0lpmiWpSkiHh4BTf4300/mCunryJ0eo+0X5Ct5n1E5MZPgSzDAUW
7lDlegtOyBFdgeINdSW6EaxTboEzDEeSrQ8aTMz4armEAY+RDvlIg75X9db3qXwve+FV5O12zWp0
IWEddBfJOtLiQfaBLn9kWhbD4BqUcx+88Was7iTJrQOUmbrWpJxks8Rln2iclSNn09AxNKU7/DNZ
me+w7O1mD9g0PgATSBw9cLTqAEtYEku7FACxJfBZ4CX468rHX9rsCl1gp237vHIU/A9XCS8CuxNV
ajFEW40RO75i2y/HwJcRXd1DLIEvIq9DrEmq4yOapZyqKIWAHtYBVbB9vlew9DWMU+auZvroj0lo
f+ouFiY/zYOuyMEJrLyKQE0wIz49qzn0GJEnBH957kTjUqr+nf/6fcgPNMSJlCtkAQHG0yelJMyE
MoD4oszNugzl9xwdUwfDCLS9oxmCCHlcmVP7ModtffQvUtCX3N4xLFPM9dee1brwN6UI1hqmGxzF
2na1hELl0kcdjzEAiFTVuOc4+8v5ugehE39Cxou5fR/qnmBtZMaun1KqF4pzTA6P7E0LCg36l1KU
stZYlkxk8e3OSJYAOsd6ljKP5KrPwtPi/UL60iLzwo/RdOKkZzK+N5o7r2n+nGYdiurr3WGEpjTV
W8oz8CII6vVibf8Y7cRfkHZJpXbcc92cCL0G/f9Y+uaO3Xo2zedznvOeLFu58GYRwjOM+DbShQF2
lRaJb9mfGfiWTOrI+3nrQgk1BK0/HvrXSQ9QOZo6qmQPhFKZY5DT/xqHosUt3dTHLq7mSWStfxni
O6gywjCW1yMDtM91+P/QtR3JoZY052/Y77QaSgeXCB69/eXUyL1N9rKdbKD9SCEgBEB6I3sfDtIE
6EO2JD26+Oi6rIEbTp5nUHrMg/QmN5tM2dVUiz66vrOgN3FW8L6K6BFK0qSB0vIQoT6QIF3pfxzr
2G16rJgZcYsHbnCTdIfBrxJOyYTgjAzrVUAkkZnSttTnf8nNOygP7O8BlEEbQBgkSGcBBPM231ly
Oak15c9HDIP+5pvRQq1IO+gOEh9FhYRfdt6adK9N1cZq5Rl5v7dLjPP6On0SXgmOc/wZZmG3XQsv
to13ygWIAszX/4XH8InjjKHV6hkw+sCRIF6D2PRF/TsZsHVErr5DmTG1jFJboruFMU4cQufYKQs6
qan5YFGsCwBpZ7RNWisRqHs5TiJ5v64Rt1f+Wi//Y0MdAbJlWA08FS2ZN2QCtm4hFzHtKgjYvx7m
muGCSCZmWZ5HznIOqeGMMPKiepNwBr6+fqumlUqxcmWWv6eU4gWwaFCBLtI9hY7TAbG8HuhwV+PR
LrWtwubZvomxOC1Dt4IgEc7JTReBA6aGWHrYvPRxYXgZZcKoiPE6OGgJ2ZG2paJvW/5ejRitmuPB
OlWsAQOxO+uI8+WjzHJpC3htFq0MZz3/1MCX/wKe0SQGjmmOQulDswE1arTAmN64RhrXexwJ2cvr
KCDRXMc80ptIQxfY5MvVnY3Pg34wG4FFtI60jcuJo+MZ2EOZsUZs0fdRnNdiVc022973VHWkVAnN
eaD0ZKFRQCl5Y0CP9IzetG6zPaafUKd6wNZaKTww4crhkl/JotUOcyuX/PWfq07P4oWdbmZKnc+C
Pydelg8itJwql42LIH/O5cXLXfQuaSS8Z0yROFtQBkt6ggJPNZO7v1jPpA40XWfoScYuLbr6EziV
SKO14ZQL0woBBMDqyCwmZyW99zO3Z876IaZhh863Hf0Tk86fIYrGJrqE0Rtexk3/HyxnxGoUZikz
6OoqY7vO6KctfbcUqNGM5Sgk9VdJu+n0KzJnGYx6m8dVbFa1N03NRPMxs2prs0WGj70Uns8NaXxV
JmcsHY9+HC8Kq7QFkBmarvdOCmtV0N/m0mnUvXAd6gdv26kSS4rba1Nax2wc4rVbHrImpL4BBl+f
3OH5OLuRYi6CM0FP6biXfvyZX68ALQNqLTC000a8n7C7M//EP9y0eexYd2JyYveJ/TJdiZVOkQXN
bmKQy6HyTlmFEkGRsotkJpT6LgvcWsbecnB7F+bj/M/3SKnKb+DBNc7Z2c8kXTtyRn6o520Glb92
640xX/Zbbypl10dQ0A+uI2AY/pS2LihUaw8v9KLys4wCY7xhHkAO042fyydT1yv3NfjHB6JJ6ntw
7mFBzz6mdhCVduiQX6aiQiCHJ9Lg5xA8ifkeHbMVqLn42XrTlEOJyn9xKFi8t09L8okRYECCWXO4
xg9zuLlDwurJj1IMYCP6f9Xi9aTHHCX4m4vqpmaFrTrMG/4zd2h3TqjGJr0CvmZW61hfykhUSYDj
gRt+6m6FLm8UBHhZt6x4u0cObd+cJZPaz9h4FmMZfOlC1VqRKaCgEaaDsT85XIiuPB8Q0Xfsruj6
L0pZ7upLy/CJ9zm8vCynjyRH2odbpW3LV/ldIBnB2+5V47J5l1Nr0isEEdJwOf+EML/1NszhbSq8
vWbvjBfdkgH98Mogro8STMCqjRYTmsSzah81TR2/3TJ/4jIceH8QkPLvf32a7Z17SmdqW/3Dp/+Z
yd0AawHkJWt0FOtjs6kusEnSSYosmkHoWIIwOE6O3gkFbQ3Mo5EHr91Ti1bnx2oTTTmfobIjR0q/
OUL2J1d6teGdl9jXIzdUuVzyc1SEvMNH9m+UJjdNovrqI+Zwbz8HtJZxb0EWtqryIrQhrAiYgkv3
2SUhKp0HRNEQzK2fZsZLUQWES3Gd1X5ejqbJ2LX3A1Z9FsqmsbOI/TDCS/fMTt6dsWYRnTBQvekp
2JbVlv8lIT1cPVCFsAzCLPGm7oHzHFFIOvDvB6B+entmTDaZJu3coP7qGA7YcVHeXVjkokYzvscR
TOmp7yUilpRJlLkRZtsLhTRVKwDot1XjLCZJj3TdttYNsMaNuRDen65mSrnrkihz6AIFyU0HdDSi
ApZMYfPlICUb8MQgkL2BrhKze1jLomDoW8rSJv+quv3L1NFvk06FHnXBI44UdPtU85RUXsRABAl/
ZKM9OBrIgWqcTZseyoWvlNHGm3TMGYFy/Lnb2yroHlM4IDaoM1rDic+azWmeTcp78W3i1sSlHgjo
sjGls54VYwgiCopO/1uqZ/siwVC37yp2t/bDWzqUcCxHbdkEfVqBFXcZXY2jArCrGyLVh9FnpNaA
aarasr3+h8v3L6W14H6sjNy1U3ZJ4Knd5EeMA4LPq8jknmWHufNQ88KZ+YvCyrGKtidbSAmaF3it
Clad0yq8oeBHDXBt4ApCr5ZysDPjgqq4zWC642lKG0qfpLP0XfLeFZNRLqywCuphDsr5Buod7ZDs
p+otzyufx9ThcsoekFRrUnLloqLk2vbRnuDtF9eFogGMlxCg8Q/vmmfdEERcW2lf6R1VbmcPx0IF
miuat5ZwnYfD2wkv8WRPysn2E4N1KvhLjKf/9QEX6XtBLy5x4a35irLlorZc3ID2FfvMQA7ELpXi
eKoRKlFKuyrrYUtkSmXzoto41B6cCZE9WVZPN5xzS8K+/3T3y6ABK6W+USsIDL4b+8VCTCWsta+7
d3C7/5OlJkDhSWvcll4/nsnaCWqoiq+ZMy+L4mCGXQqU7ArmMDZUFUcynKy35z3YlXQDg2w9ywbm
zAABJYQA5ZyGV9WQbxDNdgMyAwoRejdx+DIEbGOBVP5saibxFen1bb9C+GHqWfhZoEfLMSDrxqB6
lz9AhYoDw/Fn15map6sGmmjMSD96sGAj2uRuSFI+emxqNtJRafdAGXyYepu+LzC2Ix9gmmxlXBfF
C6tTERZ//y4z3XDGBYFTJBDsIuhIN74McqNCOhv1QbiGPGg3IkddonXOz2FjxkqqvKHIPyqlLYZ2
t9LlFHG20mYb4gu0O3CQgV8z15KbkHH4bk+4o6RjIPgALh4KxgfTWdwcRevZHktqy2AvHHK6B5kg
orVnZOh9kFLH3V2DJlFrMHkorpHqRALb4+a9/im1s4Ji09El56F5u6vMyAcq5dSr8YtWs5/OvNjx
oonjczaDOHinBMdmkfAgzV7LCeveiC9Ead25KzOOb2ntGGouXWc1CwzYCS3BiNLBJlSmbJclNpLO
7HPxbVd51uG4ZcufXXkV8nYIQe0EbY/GQ04O9kNsVEsegg2lNzptpJBzwkeva9S6EIMb12euYOom
KXrfBZ9rMd+/ZuvEQkIAzI5oJ3XYJhf+WlnfQRYkEnfLAWBFtXcfa0rnmysiSInfnSBozBrahm23
d+fhYeL2rFZg3Cplja++b1hnSouyY2R0yXKOLaL3lgLJd0+PvJw6tkPoByhcnf6KxKA6Z3YyeD23
bX13txhFHGg0AyQ45Zy8roC8DhXf6pFIsHOxmM75p2nXCIugDrzxLYvSzuLLjsTHVikJc0EmBET3
HsZzb/1KPz3GIDpgaKPqOcFempHoYwzL3+SNmUBURERCDpfEndmrSIT0RwwjpgHrgugvXKsHweLE
Pp0b9WZz4pCwwiXOtn1sTqlKbp3nw+sZhcozGw+IorSoIN0voBdAvPayMQFs2ZVPLEIlSvqle+w0
umOE9NBnz4NUEYDN9FeE2QnbCAiaQwtRf6mK55CDxIxqjlsZMlmLk+z/K6SNaVJQPxlVSn3o5cpn
Hzipq9055Q7m6oAjS3n/Pm0eu84W8oZqQIc0+8lH7NpammSj3lRVUvLwjr26csRoGuiWMG/g4T+6
EjS8PSaAXqb9j6IMHKXsukzLnSZOco0MHoU6G6YUwclR/IM0jhtcpJoEFqnW0FnggpNxZ7T3VCl3
EP5K2yeB4mqsqYQKOt1n7pqPe9qeeXE4PpCD/yn4TS+6IZoBbw6u6HnSCylx5XE0C27tzzL3RR/t
7smrGTXFikci0DPMg4YeYcFH6WEf5BncKsc090Dqn25rW4PHl5TzZXxJ8d1QLQyutBTMXXaBhqJ9
7XdSqGS52oOisB43n1fBJB+rgW7sc6avBTBMUu9Tk6rJQh/Oyf2y2FE1a1l8KhucD4Tvja64aWmN
KZfK0+TNcfkK17XYMUJwuTG9UpaICU14I4hiqL5S5x2CD32DrbdnFB4TABtAfB6hQod1WqwNeou6
nurKWriWayDUFJV5AinDwD5cb956yeXTkZlZ6jXWaWlQAl58NIwX0OpJrJwT6PNPSvFelk2ztnRo
bSk/1TBQGUGQCOGPIFDoUloJaJL9YvjB9aW1X2bTChDwcDfCQf0wx67kgLRRhR5w82dmz3a8rRf+
UtftSIQ+YmldKU6YQqSR58aasKJZWkR5ZeojHyGxS1VEtUnm8p/Yjx56zwlgjq9FUf90HsedRPze
Pgm36U9pAW8TIk8qkTOh7xAkrDcjU69JBCMKnOq7yCJl3PqzVCw65VemOlRRFVhiivDDw9hJ1ya8
+w7myxL8LI9Ez9kpyX27TkyXp4Ok93XeYPr64DRj/z7EDLu1T5bLBItc8+5C0q0xW468UB9O2cuU
nGcaQJ1c6F8cc64qC8uS/oiUjJi6L6dmk+grRR+CJpUB1B2WEINUTp1SEkYIfL1UOx5u4TgeDOKd
LzsOlzgSkU/EgJ3DWtBdM5mpcdc1+EDkIT4RkG/1ZhwTJc7XmNfM47Pdv08tJKbVS0m1lI6JB46R
tLdtx2QK7/s+7kse+FWVd2axlv4YLAyaMjVuwnokDc2+iAZt5I1fQIQNgPwBDSj1Pt+XwQRlAfV8
lk19zZfpwSrNRsR8QCIgLmpGROeXrAEMGqK9Fl4ddyd0lThuUqUwzTUhsNUGYxdSmzdBWKOTffqp
ohXdnl7U+bZuv9LnrAwttsy7/1exxudL43CVWpvDgFBqj6877LMNy+LThtT0Lw6SF4sOI7EeuXNs
oA8kNmZfyIl/f/OPRN+hjAvvLCsPI7FooTciknOiIAM86A+asoTnXix9GN1+WDejP6dQdTf+CQ66
qfJLpxH3g8GddN3IV6nzJlBWv2pzN0uESfU/vux1fc3jY0kPzQYUMun6tCJsSd7VoNDii0izebfl
tABxVCfjaD/0MM65mZmVhjjY94MopmQyKEjOytCFPy8W8U0H87ehj7/AQoH7o3c/yfyFvvdVK8b2
H7fXoDcYI8iVWJjmV21e+h4cEDpt8t2y12M0sSZ3CkiOo5ttU3WxXYUvG9Rwk0xEgSZXQcpF6aKp
asxsRD/LCQblpIbs5vUo16KAgjTqOUF32pboecHiW+F9dUfLp6c6Ttp8CxW4Wzdv4QrJ22wfQn6G
m+xHPsB13Qkls6CDJ/f8e5XDYqAWChgp7C8lsjP+Ui5ZPbB2q3/sVCJ93J5sOj0DxqU2WxdIZ4IC
tII7m2QeeWwd3NisvYrzmu/tspohDN3wK9d26puDvS4cNb/AIvbe2ce9vpTeVloQdUEm12UCyC/0
TcbrXrVIn0tkBfG4nhTNze/k4ummJGM4tf0dV+kntde+A1aQNVd/Eu9RWSV312lb7FU7u1eZDvC9
0Xj7l73qS2ACIccdpZZdQiXSyNcSFXpAmV+JKHilE+7tgBApdssDZE7t/0EgEnxU2VWuCCDg5S7l
b+/UIVp/HrcEx8nYfCjJ4aK+oQxQWeSYWJDqSz3fgRBRCI7Xz2bNayj4G2AB5g3WK6rZSvhTm5D+
KvmwaCJqDjnoF9bMIXGlw+tkJVEEEx814bB/eL/K5Y8AdRpb3890iUlpG//cic0ecjvOdCH0GOJq
76AFvVQF69gyR/4kkEahE+DZ9DhHo9drNywWipkHd6CGulmqOToA1ZTZkqg0JvPNPlICgHWT9BuZ
L5455m7fIRGDwUjDQTcVSjsNk1yeHzqtpY+OpDKIXWN/HFtCKU2TgX4nrpWsaFmw3QmUklA922Cw
tTmM8x7vO3A77f2NsaafPRE+ew4hJL1ivoBpOE1IrBQp+ZHDSgJUYYu+/2vSwOnCpWYjUxJMa8uY
ScFPuJty6AkwyI2dYOdT1dzsjLzJ1DNdU1ZtPNC8dxWSY2ZL7jCYU04CNNOl2F9Ix8DPa3v6/NEG
+MjosDI6PHdS/+ifPfr4rIEnsTu3v+eFaN6NPvcFmcWNnBTLHFw+lN4NftLNozCyS+YeD3gqfEHZ
u0cE4bfTa+hntjQyQL+WZSGEUjPZd7gKCIqq2c453EMrsYcct0Ld2E5Ss6Tvv0pRqzsLXc2dR7q2
zzthRxDxcfRHI9DBuBVv3WPrRAR1uDn8/EpKh+cBOujY+KYvlYfdmUGdelxtucmSGQqdfuSrlSm3
/up9QDIs/gX6kAEPDsPoIao3gM0KGXW1WY7UynParNhY9PyK7Ctdt/7eigXwNIf4TyzaoZnHoysN
VolcUdZY+Qwsnf0gXWvXs+X7yixOn79+ZZjq0QGh4sfwG+oaUKMV4j7M5W67o8a2gkRKAVvopKoO
fI2ENYvgCMBOwcaskWTkmIJeb356mT07uULFiNO+KUUcacq/nMCLgN5L5Bi+01tfTk/ZPnAjJjXM
lhqWUvkPKEZ/uhhHqpT7idFs8GawgLkkV7RS7mpitmFGYELL3fb1CgzqNx4At91cFAj5ZBDYqo1L
vuNDe3VX7OlyYFF5mqHTqdi66cRjwpfxB7mhRnWkt05ZAfPu2bRCIc799C0fMCaxsnwqgEhO3V8Y
MyW2md1LToryoUIxqU0LarNyMDnWsM/2xsn6fpx0XrxBElGkA7i8k6+o2b0zSOm92zk4mHu0IjVz
9rADlslFtqHCUdZtnGLFfJL7f4zv+8m04VLNf1jTlvPtZlq4D3nmJdNneha3M//QwPeCv6cwvOYu
y2j+SD+xisT8P1+htsoxN9PhfizwaI4GsCdDkKLNBPkG18PsxhQZ6ZkN5hCDoAFiplVg5nnyYwNy
hJdnZ6MeO2fu9mnthygZGUVd8yexr33xmim5WTacq3leOYcUffENz7F75JfbgqY7bJe6Fk9bscXp
TY30D9tBV1Ut/1moyimu8jz+LUBmWCMknQWv60EvUJZRmmWk1090yS9xBTPXP4WRVQl7ESKflaw/
/c8tQw60I1+o16wAu3BPdM/JI5Ox0QW3rrDPDObY1qvIklU5LMW8hn+/1CImBD0Fgwf12U3xILjl
euqnmKpLxlC+U3d2H1/qQp2QgJi7NKXwEJvSTaPY4c8iGb8DvmDYKxE4QOzYySy3qGQmdfCOvBXj
JmgLuyloR5tXp/ZK8hZwl3JGVeCnCcufqazupAF28w0ClGXzUpA+RR6Krn9PCA0CklFmpwU7qM8w
3VXqa83So3JwaEOJ678+iRabs5XI9fX1DY6wdKYCZWpPze8Qo2mw4f24f1PfAUyJWDEWSZbVB4X/
DJwSAatNsmjxl5+8yPEWcfSblhRAztfkra9/teaEborp2clbpmaq6+zeFQcJbsDXdPjCs3AgxtVe
Gbma6XlhIByHiV16YxXJMLTjODi11YxtskWel08aph/Z9eUcFo0XqpmfpUMJtV7Xliz1kUMq8Ydi
qwTy6neBrd774+e4vDAa7qc7CaUJdWu7/HaynvcKZn8N1lTH0oMcRCNm0JiSwek1Au8szvuTBojY
cIeO07t+fr86HBApOR71BhtBiGwbvQRq5svFfFZAHD4/KaFqVt+O/ZMw8NESvBQ64XkBTY8TbGKb
wDS/UCmmMpI4AqYUhXuHsHP+WeYOAgM9V+5g39Lp+OOw032aZ1QOkQuhDzYy5PCQ3yctBeiUnBtA
BR/XZXqLDX46a5mw320PyPMc2sSIMxrhw0YITrCzyMic0KItP2AKKpuneMKPfbzlTWkwDwD+yZz1
IG7dNfxRsoyb+UYgbwO6mJWBQDcg3YbpI1sytrb6aB5YztsDuskrWbjBRff6BNUgobLsjTkCrxg2
aSmCZUdwvQfnYy964xFjShmSOlOQBhF8YFVePneaR1GW3dMN8y86kILwYpdjxkRaJhHFhtDTe833
YhrQD4qhNATSuFCVavxAj8dFnn+XQvqy2k86xdTH5qPpikdQ/HEHRh9O2b8x5fil2xexFhqkEg/b
BlHEuQ2s9skkZOgYlFWa5mjdc+kpw+zRReOo1kGJlzeHe/qWiNK+LYyN8VZk6dxj9nTSC27g3mPk
tA3QGq3znVcg9PyCs59FkWxSOHJhHlAzxukL8ZNEPcQfZBc7qgIPnx17Zw8t3HqoN28x0kvloZ3J
U2HtQUiBATDHU2PV+02ulqCpYa/3Lq7BJzcPvXSSHtkAncPrYyRUHMkm+xCEybmBUt2G67lcF6Iy
E6gAqP+SQvzlB0x8ad8Z3nDyQuOYLX79hjjV0+0nsZr4xuCPv9njkuzGjfvg4aNVfdlNawMPzl6P
w2UGzVJo80qJplBxmALKDRARPeK7rFdUdvAyq8lOm3jcqKzk4d4WsTA/vPprDqrtkXx525XZp0mP
G2gL1r1fMN3Wfujlz4RDnSTSd2d4FjhfpmNannapSw7nkhyEHy56YMhbTPEa4SXdz229wDDeYBvA
kX+jRbvO7YO3NrTahaXI15bPKAhZM25Mfp5DnFOcHDZRYNeLMyTKMM/jN4OMaY+d9a77v7+9orbq
lVqNjvX6BjWo3MVL9t9CY1+m7uqj171LWpWRJ9FsMhOEh6KncyahsM+HBtauBBPfJbziiTzF3F6U
dd3qYojEuR3Rb3LT7GqSqMbZycuWM/33gzQlB0HWbo9rColW5MgzNFEy6vzcrEj4VvQfnwTn7+2J
hKPaaXxkJx0xOqv9Uy5cZ817wLHU+c5QWTCkExkBbZ3Y5w695mS8T8o7c9vzMG01VoXyEQsNKfmS
Aa9vFAmFv7bSUVIgW9KqlM9Eq6Xy6wGTUVB7imrR7Gga/FuERQikZEp3eB9Lz8PuEpCvO3UQouu6
2/n9aY6168GRUHLQ4bHOLXKSvcRAPvn6FrPxX9P6CFVqgkVIjD6EqCgsl69dvBCt40BugdjYdKer
2Fz4mrrl93XPprqgGl6qinDKd3X7G1TdOxjev949PV+QdKWrJ8ZMjpRnQnGKvXkkWCDmiVth3yEb
UDYBy1nJ/ut8+c9VcYXzKlgll3wgYH20DJXFwAP+cFFnFNvEosh7TvbNH+LVqZucXPQZcvCxQS6W
5NMDFQe7WI2mies2ovc35CAVvLNDV9bJ/rS6TpHjNAjua/6tuPChg9PQrieEhStFWDxpSOA+heXp
5v4qMqdoIyVacmlQzBnLXaYJ1AMP7ZBWIGA74W5hyweCLO2A14rR4k60X4Qpz5JCvQkynKKEGgH2
rfTU92or4pi8sFGrNiy+L4Xg28bjP8NMDfafjYZX9Iu4jeuOR4gbopaNsT2n8sH8xyJTu14sgBDV
/0lTi7d9idq3vBBsAjwzG3cGA/oqcZoL5MttnRBSO9CTu8J12c//DX4vMaxCEfKYaLBKoPSJzyXL
YWxSb+3NPJvcI5mq1MOoPOVWwr3F6T9TJYWLJEjDgSdsCmhwdDDRRVgsxnZ2VCbFjQs9BSbcBWye
gGhAIi41AxQ+B1fsNuvg8cLtwEIiZVZoAYWTDVKa3WPzIiyN758LH/IqX8rd9pZ01smhZ/FDTJP4
sTVFFIHmPYJ6lGJ2CnU2Q+SmpPvkisEv745jzWmIwrQulNrerz4Lh04+uy0Q7ZIokJ2lJkcPmysh
Cb+WoXV8ZbW1tB9uDRDHKdPMMkl37mvxOqsvPN1Xt9Dxs2IXM8rNSzykD9AWN5nxoz06hWhCOnid
CY4x6Q4POeTYLyy5w5TnDvn1v+4+2XJ4u2HqxT/CKO+iiGh9BkpKKHignrnlHACa/89hiUIkydK0
NU1RUFd4RUUV9jv+KImkv7fWNtTxnZWxnjYRh5DdwrKT5sVpcFUV/xCuVW00akM+OfObI9pZeYD1
s9t8GpgDoOg590D5QErf8ktoIS3ApjpUwtXC/1joI44HaiN4cBGQG4/d1OApSo4uKlhT8agI+Qed
lZh6zBDxACJljW+yeVxIYrigSoQoDCPZBO3dQNoVYymNv2uhCh3JfKtLytgbsbizVsfNPWo2/6n7
tU+8v0AkXu3vK/u6ehujlH6dVnAo67H5ki/nNLcbQZ78e3xJjeRjojdlQid3S+5H7GHMjcgK0dxG
lvsot573MbFxRUcK/VcQysaEL2rA31YVKl+WvwIAqJqLg4OYOVjBZi9b/dG/3Hxw0DLCHkymYnnA
B0eeXLM3z2m4ghGPp1xvYNmy7Eox4i8fSQhA54kSjSlWZ91AKCcPg/3aQz9UdziqcyfPMb+rJzKz
UgZg5KHExpnmEr7hA1ciVb94e75KvTRBAaxmNV3pWrftFixeGhPEe4S0tYh4/TwS0tLvC6o+Ultb
sVRAs2DD2Ueu97vJYh0S69PTzXCB4Av+mnYanNGsQmpqBAVAGUytotkJx/HMO9uaaiKJSpq6KSop
XLSD4ZiMmRKUAUSYTTs9OhK+0qfPNj4GaHNJThfjen+nOtBlQrXLu9j90JxJHt3yaDbSkVBKs9ap
piJbEVTW8/jHonG0Cb4sFjlX85OsYAfOWs4nzLbTtbNFvMP1QWD51sMO3rx5IaD/8H371sxApHJJ
+iV2ST0psT6CrdidqISZ38hF/PXSXg2B8uKsBzj5iyzryLEZRGy/VXiBjlt/2HRXt0m3co1it8kZ
dJ8LGeDSCf5wEQUpbvb6GmenUDdrMuDRsfRZeLnLFG6Si7r9wUWSYDTHGZ0c9WrS3L8S/dt7BXlI
p1Yvyall+BO5YiY3Pt7BUNr1mRHPN8518mJ8trAVWIQR57dwWFehcF7jBrK4sGDO5/FQ8BmYUc0l
2YZC1mXRxn1IpfN/ntrRXaZiPx7v2zBS8mmLSJmL/MnP6Kp6TxdArAhAcINhQNj/96RHygHDAIZn
cy5fJExq9R2gTFmDxCEb/CFH1VmlYaBT8x+2CILhPugrcWv+yLXi+6yvL5X6417pMrihaCt/3FEP
IczyO639DgCt/YVFv1OIS4npbk2yBUd2YcxovVhT2GWfsw7NYTOME7xPkODoN0ClPbqSms4PGB88
rO95sqIiLrpidDDlasGEH0fvgpVUs3ISSGJImsQEQiAatdYEgemRJUBLSlm8XfxvzN2ZLdhydrfH
UHC0mWwSDRhFcPF9zE91Q7uS4DK0Ik7kaLHZyk/aOsP0AC/iQmyNv/QNZzJlLxwaCL/CMbun5N77
YpH39tlP/L4Tp8Z+PrKMRVPmQL7/l4RKYLWym9LqnGmWupCwYUe4Rc/E8V3sixwOJIRFhsQrrhYy
vz24m5HgZmaAfJ7wgN/rbmlt9mNdIW/SRckwuTuzwMwrMpnT1GjRWuN724VOtHNfuIjtnAXBzbp2
OA/a93ZllLin8AV/tZ2/tASHGyk1AYuHgfNFt2vPkPlUzvhVDY5ivQRoeOwTM4IGrdoTyXc46POp
ERrf/u5+xArkCXAAIF+8zGcQyg588CV699BBkEiXyklVHskJ9oeHJ2qbSmtmubPGAU1ZYcJSDuu5
TL64bUV4dmZsllcqg/Ga1MDkCQ678Fz33PdAIT+WH3txv27FvTSXBLO2z/OdgrvSKs8ZK1hVK365
V2KjTT6fHqeoDAbDNA2fLnqpwdXFNpNq+H74XP85nMaCRLrY7x9F+nMqe7J3N/5LXjjcao4mQFKy
p9nnTMZeZB1LCceYiA80WZvGCiEWGs7xRZdOa7EEOxWv32yxvTPekswni5XdGezPCqK/O/W6zjF1
hIp2u6Ki9GcizMbLnKv9hdP73x71RDg1rELT3JHW5FL1abvyysl1bBKUmcfmA4mKRB7+i7Xk8Jw4
lV8Y7skjTm1QnB0LC2ZnFn+YhSRLvuqDYV0swDOSKHO2UMbX4wd5AHyjYTfajSlupGp5XKKt172z
wejtJSxFFKMsKM4Sm5dVo9oOcHSjdPAtNNJ92+TQnMMsmj6zPh6ntN3s8I2I19ur0S1ukHPJ+6W2
lKCTiXhUgh49qbzWzy273Wk8/dq66FH2reT1VhXcmExv2Ew36dUF9oPYIGLIjI1MJroUnLFY4GCc
t4v4cmdEuufacUmNQZaaTPO/TYxMIlRW0nEbP0wr2kiaEGprFO5lfcjIcQWxHQUytO7d81wJ8xAW
tJOmP6N0eC40uOc8QlxFjx3guGh+Ikz+qUGp0lH9IMfIkUnfrHnLU2+o48pLeIpGziJaSM1rAqng
m8jKC9Hz6zvgLF8XNkE1E81iLp2qpiGlGZdvoyLnvzBr2gws1PATZHGqUpOdzevT4r6KtdcUc2ep
XGjw0HUphq25oxcmtBsf3F4ZxJVDXlE50KR/tVdp/oIUVPIz2DvYDGumFPh5sna7bYzaOBukRRft
ygdk5I0WruO8aNOtu6iNpvk/rPVGa8Uw+aeaBrJrtprNOif3UT3PQpLDd9uTvFL+t6M9/ouGALaX
mjzY3h2RR4xG7gTXQET3+7gzC+AkTECJwgOggo3s82atVovDGJylw+EAYC9Mjf/oO99SfLDWJ7yA
SUUQKbmlpT0RcV/3NthXiD0Om5KTy917zSjwEh787WBRgYubuuiVPksf1CiDI/yfsfnf1N5p7r+q
3Gmm6RBiPOZKiV6qOBUOMiOiSZ8Q2BtRyETR173iIXmVNxHIYcW8f017M8Sl/cNbCi4kgM95++84
z8ljP37C69WpT6fzo4EEyIc7mvNFcBPFrTIYCeY/ZVODd66qv+5OWMg3C6AaqAYzJaUtX5VT+dnJ
QT1twzQMUYVn4xIscVUr0t0ZkY4+X2SLkvJtff8Wgz0kEaOmUjKcvGS/a9Q4sf1Bcelr5OlZ591j
p6J4oQfFCpDb4kLC10PETIcXDYpjxre/hm02EdBYt6WGiMdbipYs99TCHXTXuWHgRIo4rysi8mBy
Rju7WzICGPX90xHOJKHR6jgmfJdtk2UujoB5On6beixwf0sZHWvkOhhjmD5CFA+hF43BjuEuQ4iG
svQnz+V0Oc67MSCg/uwxMLj23orjZ1sXQHBqbdGhuRdxAUw/A1q93Tjp8dIMrn12QvW/kAXcZSfJ
rLnRX3TVK0EzbCJjbxDh6yP10EF6BD0fl9ZNoIxT/7D2cqAVyyJMPglBUyY4fm8Ve8Pw5UbwiLdZ
32UECqLWZ8znjpgTJhC0xlW+TzJd8BxXXF++OGHB3DmTPHQ5ZKSrwdA88yxJHHV4MmB911t8Bn2t
4SO44j1YjmnGtuSYAN/uoY2RevzGYAg404GT3ROdYVZk0Shs7MI2QoCJkwjP6J9FF3MWXf3CUxzb
RjrRiGeRTZeDGm/dUnVu2NQk1S/G+5iFYbBJI62u5hQerNK0d9vDSLeGlwDhEqg7oLiqLWVLDwOP
VdD1835MYpohlxjR1JOTD1oam2Uelu7c7a0r4WY1q4JEhy7EHmV6x+QjX0rmvDbLxWWshnWB+G++
tg9gpilRNgdyGLc812xkBWPxMhPYEF/cF8qYYqlIHc1+jMhBNk2UB9UK+5j5cY35tgUJUpppSzCy
MFo1aFArltFzbceUxLF83/dg/vKj49p85H36lZTJUkunQdSeNh+f0WR3Fhzi105J4R4MBFIM2zCr
n8rW18ebFxUhgbZOZq5fKy1DRdz9iwnA5TLr7dyNijN0LbCxMKPMz4WhOD//+drakujBPIFzKtYq
zLWrKdsQRcWaeTv2rpYlV99BQX1PU8K4lNCO0IjhDOYxSI7h3G1hVG9oagyJK320lEXqJV1pE/Gk
nmASOsZ+sz54L6t2uKmgNoHVjotnIxLFaQpyp2eFl5IRYvXK80HafQclojQKDMxtAPRU2s5p6m30
Cxi30G+gxcMgEko/YxOB3II4wLFfGFc9wVH7qkjhmOCukI4kL9htgJsgBnL7Iir1+WMhdQX7N47F
EdleVYRoN8VYRG/QQmrDdAyAe66qBJxcePwgwyF8j72oclYTeG3wMAQRAf/g7+4fe0/oW0R7Nupt
hwzXFRMaJ49RCe38DJpiqCBPCVPNtuisPtVzJCuHxp9a9GjbBn6Q5kuMV/IVDm1fKINZvxhEKgFx
lvnKZ6KkRkgR7udtGSyvWzQG6F4FX/GCawHeKB+y8myCd2gC9r5heb0BDD9ydlokwIJO6J6YA5si
OK6yrLBzW5sxunFxFz9xtIJ9XUqXSpyvGkEnGhJrclr7TMN0ncR76xoPvZbGsyDrHkdDUcfwjw4O
Er0ZHGrNYTLlMqhwxCSg0ioFig7pjThYQa98eZyqjZJlG69hKZSF0ZWbjoBLAc8r9Sus+JIWedTV
d6GNmTQHTtpfVJRi1l2NAuujlxC1ubPLSjkSXRd5sxv2rY3en5hz/jpBEik379Hg0OsxftuHt1fI
vM3RwSO0LFa7QxcGi2BtTPs1xGdMER4MjG5ijfTTrvJD1HNyofUoUeyq3xOt1cjRoSnxuP133CRH
4XQM2vWctP3u9XsDKah6lQJKSWA1nI39pPtwFQRlDaQt2PhUwBE931pOjIKV9EFYaN/WXTigENgA
tihAirph4T8OPBpjq35nrlgiXizGcUIFe/ogoDCf6MDkkhqHZ5X0ymSVfEDL5A3RKJbhmEocaYNq
RaH/gou/EesmFY0CMueWOR3hJ87+uuFY9OvnwREpQHdgpON8gdJP74PCkFlS3giTT5oWNkowqVZi
WTV2Kf6M6cc3bYUNqEZ6TpCpKEaY8bldrtfZkpkVONzE43I2aIQYdhQBt82dfbM1tOwIM1KTDLo9
2XjCsfrgvi1/oc2XiJHA/4LtJ2fPtFttEHDM4gmLfz+Cv1/Xr5P9xkPz8NsOP1nuVV7jOBcbOuc3
yh54s6UuDv9pAATsF9x14Xdn/SRECgmHbgQUFpPjXR6YlaRz+DPTYcOHEvkt59f0GeA4ILB4mi/G
d2gkP1w4409QCqi95tX/lRt1p0Ee+gO1z9OQlxg+b3f7Wp/Bl1EfW/3Ar9yKDVURZYWMI39NGEp/
Z36Pt8vsZoRKAHat3zwEnu2z9YQFCnjU3PT9F9e8u3Ewp/0vMmIhPCUlMPUvUKJQ2x5kaLuXw3cC
yTzQC0RqSc0iANx6ig6//MOSY6BDD+FvJ43zvCKuEVHqcDYcC2HeT9O93PNR7c6gDAhCmp5XZk1i
aEbc4RU6KT8xX1cedDfegthJOSZQBwMo+p7e8Ve5A4WMUivTwZpuCsmW+c8VFxgp+VBaiaHFXQ2o
urOeFZSIhMRLVHxec0bawaNOlqxqiH991tRuHWSJ8Ql7McKtgHKlB8BVQnfns9dr98Vxc9IzWfn8
KjRZWFmFv7pIzFpYO//3GKgkYmmPSir2Hxwyub+fjA2rRnJ68cBK1SoHgZMi3LR0xjR160ogdgXw
kTIj4ApiZd1rax7LgUiXKORKFgxiiZo2lj+Z+5CPQEQSjtDHKyaD1PLQ4B9N2r3vhZSoxQiLiZqs
KKcUsbgwBidtMLJn4KM/IWFjGjOfWqszf4bBbQgDuj8v0V1f+OWOVUsR8lj3yhOnCSpONXuuKG9Q
ivwV4MU3lvuL6RZY2hKdkdYqbcnw+z3MYSNQ7MJpl3nZKC5MTpdP0pMMprNR85lbA6j/JxiTrhVg
0qkK4iAa7zMbvfOn62soLtqn0WH6bxtmAwCiLHmGganFrWz+NCejw4/ll/Bz9LMvBBPdKASWFu2G
KVLFZX5ExfGdvOIb0aOMwx/OeaURKOCuDmaw11BjMshVQHCHnqvtEWejKKkxuU1tYHSvgH5Zibpe
e6eCoo2ldGK4pVzlQe3gQNvWfALfQBmuUA7mzy032UHy7iDK+w8kg6u/aDYZ4wg6GtH0HAYndbBD
sRuP1fjkRdjVQL0gvKuC8ePQXMTy3/BTr3n6nlBjtzfXucucafBD5cYsCyc1KRatK7yfxBB4uNC7
BwferWYxULVZWcYGBsM0lXBAjShsrJIvhRmkmCjxM+Dc8+fV2rpvUXoZT47UNjLvHb2wEoaUHNye
RizL+2cW4+cUWWReDO8gMQ7MXXIKj2tgQf6MSLqik2bO1eTaiqALSerVCf9Sx15qyl367FQ8IIOZ
kQYYuo6IFY1mbeE9PgrJQLVanmJjwMNyvVnm5k7MW8skUx9kNErKShm1xtgbcbNUolXfQJ927its
qDK9jsVC6yywE3qjqxov3cveDWm1Zvl6IGwhhlvRZBFnNTl20PFnNU8pFsiO6n/uBnfC1PwiohtW
KK/cw8NiuOLVWILqrdVJh4C6+M0VdqJWyQICota3X7I6tfmOuUFCMX47h1u9VPzTbMHk0rSNaqol
BnnjDmlqQVd5gnwWQYeGJO+KvelrHcfID5M75qpwSluD+IJAkVD+/XybLOSjuZ3hBr0hJlUGUVAu
whg2GGkN3pZoaVcLaGnwGaJM/9iyKUzoq9kqdGEuWGLR7BqiPbbUsHdkxoKbsuSkY8PPL3XmnguI
Ku2J8Uf8zftjNAVMJHHzS/lYSambmivZys4qleDR8pMlbhFKL05bRre3lFwub8V7dJh4CAgR3dcY
xpHIXlnm5lWEsccIOTmCJxdYjgIwIMmDUVUf7l0cjwmlnteL3jh35yUVQ6wLFi7YmZAA+ZMp9kiy
YvcqrAYWrSZqfCaO6M4rLFvlcoIFT0/0eO8RLrqtSCDzhrpUzXNNPx8Nb/BUG4gf4203BUeAaIsx
8+Lxp9uBc3Yj+SntvOwaZG6S85i1njeaemHpPddzTILKlRjs7GBaMNn8htwyQA9DXPo7z2pej0Y7
mvjuN5EtGKOfZRIsKojIifya82MR0sx6149Uf83KVONsgP0WV+1xAfcrvxxqgVdnhmSf13GZ//Dz
Igfrk+VHXFmTmLG0x7HjWKvhmuYehwyrN0JB0d1DJ8mJuRsTN6fDJBtnKCDCv4bVLrvSTYgRyucc
mj6d9qWnPKbH4adWZeN8t2ax0hqzbjx9H75HZbZAcrxAj3bIzIpIrjdKQO/IvE7LOWTBCHFX8Mj1
G4iaXguWvtIiVyh4rbFaEg0IDVCUH1oD+5Yz9QDT/GDOICTrqHGifZe7iN4XtMLy
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity c_addsub_0_c_addsub_v12_0_10 is
  port (
    A : in STD_LOGIC_VECTOR ( 11 downto 0 );
    B : in STD_LOGIC_VECTOR ( 11 downto 0 );
    CLK : in STD_LOGIC;
    ADD : in STD_LOGIC;
    C_IN : in STD_LOGIC;
    CE : in STD_LOGIC;
    BYPASS : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    C_OUT : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 11 downto 0 )
  );
  attribute C_ADD_MODE : integer;
  attribute C_ADD_MODE of c_addsub_0_c_addsub_v12_0_10 : entity is 1;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of c_addsub_0_c_addsub_v12_0_10 : entity is "0";
  attribute C_A_TYPE : integer;
  attribute C_A_TYPE of c_addsub_0_c_addsub_v12_0_10 : entity is 0;
  attribute C_A_WIDTH : integer;
  attribute C_A_WIDTH of c_addsub_0_c_addsub_v12_0_10 : entity is 12;
  attribute C_BORROW_LOW : integer;
  attribute C_BORROW_LOW of c_addsub_0_c_addsub_v12_0_10 : entity is 1;
  attribute C_BYPASS_LOW : integer;
  attribute C_BYPASS_LOW of c_addsub_0_c_addsub_v12_0_10 : entity is 0;
  attribute C_B_CONSTANT : integer;
  attribute C_B_CONSTANT of c_addsub_0_c_addsub_v12_0_10 : entity is 0;
  attribute C_B_TYPE : integer;
  attribute C_B_TYPE of c_addsub_0_c_addsub_v12_0_10 : entity is 0;
  attribute C_B_VALUE : string;
  attribute C_B_VALUE of c_addsub_0_c_addsub_v12_0_10 : entity is "000000000000";
  attribute C_B_WIDTH : integer;
  attribute C_B_WIDTH of c_addsub_0_c_addsub_v12_0_10 : entity is 12;
  attribute C_CE_OVERRIDES_BYPASS : integer;
  attribute C_CE_OVERRIDES_BYPASS of c_addsub_0_c_addsub_v12_0_10 : entity is 1;
  attribute C_CE_OVERRIDES_SCLR : integer;
  attribute C_CE_OVERRIDES_SCLR of c_addsub_0_c_addsub_v12_0_10 : entity is 0;
  attribute C_HAS_BYPASS : integer;
  attribute C_HAS_BYPASS of c_addsub_0_c_addsub_v12_0_10 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of c_addsub_0_c_addsub_v12_0_10 : entity is 1;
  attribute C_HAS_C_IN : integer;
  attribute C_HAS_C_IN of c_addsub_0_c_addsub_v12_0_10 : entity is 0;
  attribute C_HAS_C_OUT : integer;
  attribute C_HAS_C_OUT of c_addsub_0_c_addsub_v12_0_10 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of c_addsub_0_c_addsub_v12_0_10 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of c_addsub_0_c_addsub_v12_0_10 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of c_addsub_0_c_addsub_v12_0_10 : entity is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of c_addsub_0_c_addsub_v12_0_10 : entity is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of c_addsub_0_c_addsub_v12_0_10 : entity is 1;
  attribute C_OUT_WIDTH : integer;
  attribute C_OUT_WIDTH of c_addsub_0_c_addsub_v12_0_10 : entity is 12;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of c_addsub_0_c_addsub_v12_0_10 : entity is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of c_addsub_0_c_addsub_v12_0_10 : entity is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of c_addsub_0_c_addsub_v12_0_10 : entity is 0;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of c_addsub_0_c_addsub_v12_0_10 : entity is "zynq";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of c_addsub_0_c_addsub_v12_0_10 : entity is "c_addsub_v12_0_10";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of c_addsub_0_c_addsub_v12_0_10 : entity is "yes";
end c_addsub_0_c_addsub_v12_0_10;

architecture STRUCTURE of c_addsub_0_c_addsub_v12_0_10 is
  signal \<const0>\ : STD_LOGIC;
  signal NLW_xst_addsub_C_OUT_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL of xst_addsub : label is "0";
  attribute C_BORROW_LOW of xst_addsub : label is 1;
  attribute C_CE_OVERRIDES_BYPASS of xst_addsub : label is 1;
  attribute C_CE_OVERRIDES_SCLR of xst_addsub : label is 0;
  attribute C_HAS_CE of xst_addsub : label is 1;
  attribute C_HAS_SCLR of xst_addsub : label is 0;
  attribute C_HAS_SINIT of xst_addsub : label is 0;
  attribute C_HAS_SSET of xst_addsub : label is 0;
  attribute C_IMPLEMENTATION of xst_addsub : label is 0;
  attribute C_SCLR_OVERRIDES_SSET of xst_addsub : label is 1;
  attribute C_SINIT_VAL of xst_addsub : label is "0";
  attribute C_VERBOSITY of xst_addsub : label is 0;
  attribute C_XDEVICEFAMILY of xst_addsub : label is "zynq";
  attribute c_a_type of xst_addsub : label is 0;
  attribute c_a_width of xst_addsub : label is 12;
  attribute c_add_mode of xst_addsub : label is 1;
  attribute c_b_constant of xst_addsub : label is 0;
  attribute c_b_type of xst_addsub : label is 0;
  attribute c_b_value of xst_addsub : label is "000000000000";
  attribute c_b_width of xst_addsub : label is 12;
  attribute c_bypass_low of xst_addsub : label is 0;
  attribute c_has_bypass of xst_addsub : label is 0;
  attribute c_has_c_in of xst_addsub : label is 0;
  attribute c_has_c_out of xst_addsub : label is 0;
  attribute c_latency of xst_addsub : label is 1;
  attribute c_out_width of xst_addsub : label is 12;
  attribute downgradeipidentifiedwarnings of xst_addsub : label is "yes";
begin
  C_OUT <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
xst_addsub: entity work.c_addsub_0_c_addsub_v12_0_10_viv
     port map (
      A(11 downto 0) => A(11 downto 0),
      ADD => '0',
      B(11 downto 0) => B(11 downto 0),
      BYPASS => '0',
      CE => CE,
      CLK => CLK,
      C_IN => '0',
      C_OUT => NLW_xst_addsub_C_OUT_UNCONNECTED,
      S(11 downto 0) => S(11 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity c_addsub_0 is
  port (
    A : in STD_LOGIC_VECTOR ( 11 downto 0 );
    B : in STD_LOGIC_VECTOR ( 11 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 11 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of c_addsub_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of c_addsub_0 : entity is "c_addsub_0,c_addsub_v12_0_10,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of c_addsub_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of c_addsub_0 : entity is "c_addsub_v12_0_10,Vivado 2017.2";
end c_addsub_0;

architecture STRUCTURE of c_addsub_0 is
  signal NLW_U0_C_OUT_UNCONNECTED : STD_LOGIC;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_BORROW_LOW : integer;
  attribute C_BORROW_LOW of U0 : label is 1;
  attribute C_CE_OVERRIDES_BYPASS : integer;
  attribute C_CE_OVERRIDES_BYPASS of U0 : label is 1;
  attribute C_CE_OVERRIDES_SCLR : integer;
  attribute C_CE_OVERRIDES_SCLR of U0 : label is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of U0 : label is 0;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of U0 : label is 0;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "zynq";
  attribute c_a_type : integer;
  attribute c_a_type of U0 : label is 0;
  attribute c_a_width : integer;
  attribute c_a_width of U0 : label is 12;
  attribute c_add_mode : integer;
  attribute c_add_mode of U0 : label is 1;
  attribute c_b_constant : integer;
  attribute c_b_constant of U0 : label is 0;
  attribute c_b_type : integer;
  attribute c_b_type of U0 : label is 0;
  attribute c_b_value : string;
  attribute c_b_value of U0 : label is "000000000000";
  attribute c_b_width : integer;
  attribute c_b_width of U0 : label is 12;
  attribute c_bypass_low : integer;
  attribute c_bypass_low of U0 : label is 0;
  attribute c_has_bypass : integer;
  attribute c_has_bypass of U0 : label is 0;
  attribute c_has_c_in : integer;
  attribute c_has_c_in of U0 : label is 0;
  attribute c_has_c_out : integer;
  attribute c_has_c_out of U0 : label is 0;
  attribute c_latency : integer;
  attribute c_latency of U0 : label is 1;
  attribute c_out_width : integer;
  attribute c_out_width of U0 : label is 12;
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
begin
U0: entity work.c_addsub_0_c_addsub_v12_0_10
     port map (
      A(11 downto 0) => A(11 downto 0),
      ADD => '1',
      B(11 downto 0) => B(11 downto 0),
      BYPASS => '0',
      CE => CE,
      CLK => CLK,
      C_IN => '0',
      C_OUT => NLW_U0_C_OUT_UNCONNECTED,
      S(11 downto 0) => S(11 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
