// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.2 (win64) Build 1909853 Thu Jun 15 18:39:09 MDT 2017
// Date        : Thu Feb  1 18:11:45 2018
// Host        : DESKTOP-0KA73P0 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               c:/Users/espen/Documents/MasterOppgave/prototype/Multiplicator/Multiplicator.srcs/sources_1/ip/xbip_multadd_0/xbip_multadd_0_sim_netlist.v
// Design      : xbip_multadd_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "xbip_multadd_0,xbip_multadd_v3_0_10,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "xbip_multadd_v3_0_10,Vivado 2017.2" *) 
(* NotValidForBitStream *)
module xbip_multadd_0
   (CLK,
    CE,
    SCLR,
    A,
    B,
    C,
    SUBTRACT,
    P,
    PCOUT);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) input [15:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) input [15:0]B;
  (* x_interface_info = "xilinx.com:signal:data:1.0 c_intf DATA" *) input [15:0]C;
  (* x_interface_info = "xilinx.com:signal:data:1.0 subtract_intf DATA" *) input SUBTRACT;
  (* x_interface_info = "xilinx.com:signal:data:1.0 p_intf DATA" *) output [47:0]P;
  (* x_interface_info = "xilinx.com:signal:data:1.0 pcout_intf DATA" *) output [47:0]PCOUT;

  wire [15:0]A;
  wire [15:0]B;
  wire [15:0]C;
  wire CE;
  wire CLK;
  wire [47:0]P;
  wire [47:0]PCOUT;
  wire SCLR;
  wire SUBTRACT;

  (* C_AB_LATENCY = "-1" *) 
  (* C_A_TYPE = "0" *) 
  (* C_A_WIDTH = "16" *) 
  (* C_B_TYPE = "0" *) 
  (* C_B_WIDTH = "16" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_C_LATENCY = "-1" *) 
  (* C_C_TYPE = "0" *) 
  (* C_C_WIDTH = "16" *) 
  (* C_OUT_HIGH = "47" *) 
  (* C_OUT_LOW = "0" *) 
  (* C_TEST_CORE = "0" *) 
  (* C_USE_PCIN = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  xbip_multadd_0_xbip_multadd_v3_0_10 U0
       (.A(A),
        .B(B),
        .C(C),
        .CE(CE),
        .CLK(CLK),
        .P(P),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(PCOUT),
        .SCLR(SCLR),
        .SUBTRACT(SUBTRACT));
endmodule

(* C_AB_LATENCY = "-1" *) (* C_A_TYPE = "0" *) (* C_A_WIDTH = "16" *) 
(* C_B_TYPE = "0" *) (* C_B_WIDTH = "16" *) (* C_CE_OVERRIDES_SCLR = "0" *) 
(* C_C_LATENCY = "-1" *) (* C_C_TYPE = "0" *) (* C_C_WIDTH = "16" *) 
(* C_OUT_HIGH = "47" *) (* C_OUT_LOW = "0" *) (* C_TEST_CORE = "0" *) 
(* C_USE_PCIN = "0" *) (* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "zynq" *) 
(* ORIG_REF_NAME = "xbip_multadd_v3_0_10" *) (* downgradeipidentifiedwarnings = "yes" *) 
module xbip_multadd_0_xbip_multadd_v3_0_10
   (CLK,
    CE,
    SCLR,
    A,
    B,
    C,
    PCIN,
    SUBTRACT,
    P,
    PCOUT);
  input CLK;
  input CE;
  input SCLR;
  input [15:0]A;
  input [15:0]B;
  input [15:0]C;
  input [47:0]PCIN;
  input SUBTRACT;
  output [47:0]P;
  output [47:0]PCOUT;

  wire [15:0]A;
  wire [15:0]B;
  wire [15:0]C;
  wire CE;
  wire CLK;
  wire [47:0]P;
  wire [47:0]PCOUT;
  wire SCLR;
  wire SUBTRACT;

  (* C_AB_LATENCY = "-1" *) 
  (* C_A_TYPE = "0" *) 
  (* C_A_WIDTH = "16" *) 
  (* C_B_TYPE = "0" *) 
  (* C_B_WIDTH = "16" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_C_LATENCY = "-1" *) 
  (* C_C_TYPE = "0" *) 
  (* C_C_WIDTH = "16" *) 
  (* C_OUT_HIGH = "47" *) 
  (* C_OUT_LOW = "0" *) 
  (* C_TEST_CORE = "0" *) 
  (* C_USE_PCIN = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  xbip_multadd_0_xbip_multadd_v3_0_10_viv i_synth
       (.A(A),
        .B(B),
        .C(C),
        .CE(CE),
        .CLK(CLK),
        .P(P),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(PCOUT),
        .SCLR(SCLR),
        .SUBTRACT(SUBTRACT));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
b/QhBR/oOzwaIZ6E7xuGaVjTIqrrrk1JJQRhGHM3PGlr0wSnnQxll/0isGyM+wjDSK9GtAlYP0OM
/PCkyb+ehw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
WJYbCKycBENXWINGjywfHsrNXZknL7yjgguwmqs6OwjbxK0hf5LYRBnuDpYwQhonmgh8FspAKN7S
vBI1o5pda3s0NrnqYv/G6epYOX6UDWwAVMwCaLpfxBgAA/lPO47odG5bWak48ZfirMNoqxPrYu/X
xn6bfuLcmjfyW6TeE4M=

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mW//b3sbAmqxeiGHJDCiVCvPi9GZokMwU+5dRZ6OAcIpAn4OGe3GYmtpujCuVoiFy4oJaeHTE0DN
0VSZByGuwXomWUNjVxzi6wOCqyMnHN+CyPAWgXBhdnVWIXrkwfog4y5TSHD++gZeUJPFrxmlbbwN
+DAsGPPK04f6ZjdOYfI=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qfQO4VdbKTpU6s25roZ9u0W2IQ08y+6LFnuMKThrKN1hhqSYoKZqaxCw7+7+dOwYONUoVDh6Wi59
Y+hGQ6HVycgFcoV3PaEcdVB0RoESzqmpiYJ6SrD7h8mfEIcp8t/XKFfDABpO6nrhgegzhtWEYOGW
zNnM/aMonrPoXnt40S3FQWlio5xbBJxLFXmdWCC1wAOsQdYsVK8EQJIFPrau95y+alu7rU9ksc+/
3L14+fqyd229GgD6dpTKDZDDB4x9rEW8XXVQwPX0lSPpwjPUyfMNaFv3y5Qs5okbJBAUJO+a4OxB
UKx1FvIAwLTAhlwqHDdnjdWxezTwyvyk0LCzug==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
tm5E70kopcZVk2lyLZL2DhsNkZZ9007bUVlLF0bQzFHvYXYgcsXAQwflg5D1YrTQxGemPecou1PI
Wg3CmGsY5A9b6uGz1Xjtt6J/eMcflQGIF0plxkFJ9Yh0+Ud7+r8n4mljCP6SGYZHkRKF7XmNsEdo
vHkHZqPf2LZqnoTfmz875NP1mZsee1CGNEDbbZ1ILj8vkJo1u8ENiebnBG6kJtocnIpotFSnXK61
5FL1/B4oI0S4Us4bs5vvGLigC1RPIv1QZ9y0LcCax4QWJvscGfW/bGdGhqFZXQpbYuMzfxl6GI6w
wh7v2MHjv+Lr2OrwkHgkZx+XLyAwfzJL/FyEKA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinx_2016_05", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
wpqBV3cdN1DAgn0KCpa1kC9lWQ1VJzcuAx5h8HQTGLCAzRUFQnCa/EFt6R0Qy7R3hEJGwHtvMUHP
h5h22kDwO5bAhKqaf1yU2gCwDqXG3DewDm2wwPj3TBQ5BBHJapwykdeKGMkQImwpxaRWQvu6I3xK
IUFYyggVvKKZnjDCYJHQIqubpmbB3Z3L3Z7uiKTwmQU0S5eCklRLzKPcMiaVKsrXf/3wA9mLjywg
udEIgv3oufyZDG+pbbDReiiG46DHu4cKmfKmIhwVc68tb/KLSnrROnfM1e43PKLLlC2Nb57FzIFr
D0AvLBs1wTe5j0nEI6Po55dq7Mi3efdkT/iW2w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="base64", line_length=76, bytes=256)
`pragma protect key_block
NeiF3LXSCzhQx/dk2oirLmukZYNWn0ohwTmn148urG6IR6oph3EOvs8FCkS+k/7JnAkM6VHclqMC
wR6dpUaGp90uymBP7Uznlj4AdjfuMtToiA/0UudHE1UVhJSMdKLBIEXHZ+PC3ntFerMTwvKCD28K
hcrc9J2/x1LQtYLFXOgs72meg5ouK8dSjZ/l0JJtMhw+Cg/OpXN3EI21ykP7s2Err6UTshYFQDGU
uT/iypCBvgSAEb8YrxsZJigqTXu1oPoNTH0Qi4m6u7hwJl+Rl6ejrvrOKG1JP8dkE2LjC4AzyOju
X5lTdOPWMNJw1UCkgE7tYfkqzjyc8nasopkIUg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="base64", line_length=76, bytes=256)
`pragma protect key_block
XjRQ+m78NYowxNHW2SuhNHOHeS/DbqcOEGUYsYFaTi1LRV4Da/9O/cw6FeOA/HLk0yLU0atDc5y6
OY1zG846WJLj0lkL/0Z01O0PH0htbBx45olMBlsIOfNXGmC/ggBYgmgY8fxRxl+9f0mfH3L6xGFx
CGTgB0KHoPE3EWHCbzQB6V1XJ2L3d9Iw1ncexhD1Qy8SZthHW+oTaTJYq4PXvvOhf6mHhCrNmEM7
2No/CQ4tTAwKz+PeEU3x82Fg0JQSgySXFSMb/SP5Ija18Wi5IwFXQ3iGFDlsAdkuxjsSnT7fkL48
V9bFUl2tueV3SEiGSOjhFB2CuO2BEE2N0VrTBg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9296)
`pragma protect data_block
0NQstTYmUgXjVSDypytRnnJENBxOhTDB8fHvNz2WBBU6FhdCGRml3nki4wAn4fKB+VCDI/mlVM45
0wkFdgCQikNE12pEjRKh+1uFrY2h8rmm5j4vjtCVDbS/RUqE/Uf/7f+OD0nK+/p1xKSis9xiOflF
b0sQdKLX8xrLerYfvfRtXbxlqftCwIPmuhUME8eJfGuGR0//lF92C0PqzdkrNU+SXkwEm7++Bi8F
rIgsLiiZzbo+4bsdSj52BEgDxtO2FX9fKCUMaONtNHqCMrGwwoRShceboD5IHF5bd2uAb0uMAcvW
tSswnCoaG1P2cznKHUXE++1KYauVIwcCmef2/LoVnB7uLjXkzgz4AZIXW/lGgfnqOEgCgfJEZmmY
xnXUg/Z2qvtyQcx10k7XdCjgkvqVs/eg98QZqI9yO5fTxWnX6dee/dPazx9aAiNpr4lySwmbnWpN
hHTor3GWW9G23XdfeCvWFuI4wWkYde4WiSRYDOECgtgrakuvtr5htceVioP1f11/hvXhFVX7ooFJ
DoMlZdLezUPPFjUGvDQORJefjgbzzHOe5QsD3Ddow+86RNb0Ta66z8W7VCfozouBqEHH9W7k+vNU
V3Q7KsnbJry/INqGQtS/FURKYHfYkte5MK+rEOrwXL4b/dth1lru+gH+SOf07Z+96U1XGhVtYmCJ
4prPsotMDHgJX69ObgIkYwDy5D3n0U9OTU8x2gzkNNw2u/guPPfVNOZu4YzN3MutPFhcqD/RbFGW
hqIk8Bo5L10T6HWUVor48YSOxyHmVS0AX9Y/0ru0BRSCMN0TpER6Bz7Q+L/ZH80k9igvjRMvEmD3
o0SG7uGJmn76wz/D753rs4xAYUAqeYRCJq7GUgLMzPIwMZJaAZ6yrA1jhu2atB2vXkffKujcmTOG
H83D6RHO3JXSkLmUH3gPjDIz8pnScOhxUO98yiY1DkMAftrZlfADgcTX4uGlFe0Ap7RtCk+zUAut
r5S+fu23FHvrRDDS22qPfx++ULnsBlv9M4U27eWIGT7OuXjmo5aqLI9DFflRuidu8Zm0VB7OpYPO
7o0A6+EMc8F4vL4S/SsiACcBeN0wY4/EbKBBv9wYQ3txtxKkcSvGLf7jGB4YF3OUF8B/BlKw3s/l
RWOXSKwZS4vrlZ7PINR8Zr6e/BS6N+XLgGqQb9N334tyc/Cex5CG2koIuaiiP3v7UXPhe9xdHLU2
gO/BfgF4ou12T01UsSgtjs+8VuyS+yCfZBLWfbBkyIzpXUJKYXS41dJn8b1BePRnJSAKzON1MJxY
7fVJkOg5+0n6Hl0NFDXdnuqg3f3gPMczI8ALoOpSRbLYGtuHHsFBXOHhcxk2j8GbPZ0IjIU/aozT
rwuB1ehyi6HIGVIaCnAezOvJDMmuG835qz0gcYKDOL5+Gtw/VHWOwkG/DvsILsC65SRiRDZNIndL
HVPybFhEXrfrcMF1l3KLKRINbTYcQYKKDC5WaHdDdCNKw8r9L3MFawIT9EMD0TR3C1JUMgc+WnU/
rN8Q6547ZqV8gntO3YC/HpggKBMrlkH9VUhPlgC6QlN24CzQZyKicFWkPuC6uvJGXAiGNET4mjPf
asxA4f+/Gf7aSDSZuw4EI7eBqyZeXfl55awxJvTChCNtsKEelUXVvwSHojryR8CRP9UXMaIbLVG8
rfBU3zSPCYArjkApjhZnPg1ksz8cm9FbIECHrDoHkU7Lv71xQXcamDMul/ZH/b3YedHb82GlQvGd
zx9agLlfl3+ELI3OxT2e/7eRbGEQwI+D8WqJzi1gmvBjWgprlysbP2Cyl1YnMt/ojWsXcAqyDqay
2aO/xEo8AbALX7jW4Kvimul7e7VFFCbsHLeVRuUFRmkn7wK5zIQ9AOv0xYHaKP1RKU++6EHHpxwz
l6qDVcVR6FFH2hjNSGl6B8K0zsYjlWPhJOJCBU2XOUfCvSIF3E5r0IsrAdCN/ZNxY/M9A5VWRDpD
1BhCeT/xMhaaINBJrrBcxHkKRjhbhg90r4KD+wGlQ+w4xTKB3gyTnIPWwyv3ASj7XlfFFn7N54J2
FofQEV9oAJFVlCjGbsMrBjxpWgM4qAnMGmvSDbt9EGfpr1AFXd92LM4uHBuuP8b+mFZmxpgQxXmj
NloY3OKtmTxhsnpeOhiqFumffUaKM0epArMPuY8PgB2pnhYn5eTyTf6P600Xuviwi67qR8ZKungK
bG5jCjSoNLl9ixMITcbYgVGSF3Zzng3dN9N7WbmoxLrgFB+iAixNbS+uZYRGEX7As+z9DdRO4eFS
Y+RYa/6RaQOIuC5hlnwuqfOxyA5loOJhIjlPFfvw8OjMayXqinh91Y2Idu+M4DeE4sZlRLeBrHog
GHOw1ChZ6ZwVxG6890WDi7frufyGJzG7KR4ilGG+lCS3FWK10swjm/jwKrkim3XkMJP9eiQjNCV5
SgAk+H1nSJuwzuyxr8SvXwklCiiY/yPDB7f7zBdPpOz0uqKDxkRmRIJWEL9ETw5UNZRDnXwOZeBX
bRoO0Cy6xL2/RaSJp9czp0Un1fCqGq/GzpEpcTtcNdi/Kc49xJOkEw7xCA/5taL4p/0UmK9U7Rok
mqnlQM/ae7YFOfDl51dK0LoSAadkK0yZEq+RhJm8GaKaWK8x8e7tifkWzG4NY1/pzIyjVsq7mxy2
5t7VoLy3RdF6KuQLc44FEn/0ky4+FVuTdjeKOvnwkqbmMISbPpcM8JjsNN1QsEQgZ8xoKERR4rEz
4OPbe/M+y52FUpPpPVffmI/NpTY/jdzpWN3nyeU03XLvvm+ERRQG/v2YI581VWhNUKIvpr6SA/yn
3VG/nv+zUyM7KE8nrJjlbWEkKzS2FiTDxHRUz7Zxsi1TxOYw6vyZr2T4W7gWIiRmKKOvlBH/j/Du
6TMwpBKPajktU/JY2umknqnqxfD2fsijd9jg7zx2YTW6qPZ6i3OF4f8IbV0c+ugsjKYu7+iMDLQM
m7lz7Tzs94oRhwLOTab93IBI9hM/OW832YZtckSccuIo8uOnwhlmxDpGy7QRClZDlDy15rZ2yW5i
cgwatlGormt8vjsDM+BkPOMU+69qpFBoiKlKSC/0C2gG2rKIQdtSyBE6ugLO7LcVzDnzvkJae6n1
8uWVPO0sESk2zgtt5466qQju1HMSQ8vHbMvNM59BFrSOWUe+hgKsLzSxLC8UUZuLfkxNdYBlwNrE
DgiOJUm5eBjC/YbEsw45eC2/YmUrfRoGrNpKYI6Mh8FRt9ANlFd7avNuzsKF6vAxP3XGtRomhHcl
ZhKywW2vPsTWYUw/rrxTeGiuSavNjcy3fbn4+dsQG4xW6MmfrvJCrieh9lsVewm7sIZuiw+7VDG4
yjt5+sSkVocE7pzqDzJgC2NCBRJrq2JQKDBbgDqtXbu8lg+75RgJkN8cg5uAX+j9SnoUTYrkm3JG
agat+KVBfbOxKoenGiF8Uzj3fF6A0RS8RdPvaxpVTZTDSTKVVmRx7fX9BoLZKHvfQoMwXSA52Awf
KWmFhkCZh8jdpYi2C9QSB46iSb1+2hkRpxAvw5UteHf2HrPZXgewb5TGhZo80HSmhxxywEwAPxir
RNH+X7LZmXuPV+RgmuR+IP8Mo5Q4A9EAckE/UFmZWTEsWF/4uHy6xa4wOTjVLcHvMyrmIouF6K5H
e9u7BjQX52FrgeQoi0IiegZk2AlOjojVrN3KQW3MGXoMAphwXvZprtrivXvWzrnwkjWaj/1STIqE
qHrJZOa+MjkHf9onqkctArwiXxpfbB8WbrU4rLdYcfvfUJzTltwcABldSEkVTaqz2whTWlsitS0+
7cL6eotO/bnB4rViVO7ee8puWKrgRWIL+BiWtzn4lTvl5zdL6IwpOXLGRjGyCcSnk/wzR8UEIe0S
K/+gtwucdhyYJe61WTkhQOAtnRV/pGvYj4hxUP/q6jbnMlHxTHzZVfpQrL4hAcQ4kAIpqMpeb2K3
u5pGx3nStZU7iIoa65fDTmn3V9O8fYfd8SI+b8fwY7Zu9yCrzE/toC4Q76fBTzv4lZ2x3ar9WV/6
HKKDYsJTa3sRsB/rf+PNzkfu65jGOntXiU/X23qxye7q1sTH95943AFDdNeRAomoTIXYpO/Q4WcT
LFVidgFFaShdusYOZu/EyUYUquVA7IumdD27UmqLHkYlhh1OgAO/Y8Emv7HAMkHB5Y0dzdoRzCSk
5HA08fByBs8cWGWJ+jlurjzPDUHSx0DWcDlM1ZiIcs9LCf6FQrlr0/TgTbMbJdEGlpFHqoRA7bdG
zo77J5v8ZbIVRKLoMTxPOXAkUAYWMvUZfn0WDjaxyNKaClge8pVKtMmpWCP6JhYob5HGovsMAY0e
3OvhZaSorETZTVPhNpSZWd+D4du8XTmiHrQFKNqrkjiiintIx5I2krfP8UOVnH5TDFyR2NkX16Vh
3WVurfMzpj2bNv9U5BzSTlwfDx1+EVn6Dbgpt5OlMCKTD8hOQLW9V5tS/bYJoP0/qlTnlwIBHGn4
FIj3gwbDblgQsMyrRopF7PpLBdCyoSbJkTbei+y+/2IFgLbr2OHKfuPRhCro7BHtbyXGhCCFeVxt
ky/0Jb6bjlItXI4dIg0LgVuKHtijhYlliS7WkuDYGypvvkIPZBejZCLL9Hcxt2QFtAyA+zySg/1c
WhheypyLB0gPqm/evI88aYWe9DNOXMTTmJ30Gv2PVxoiZ60zuVIwrXdX1s8p4jQPQZfrKYvreadk
lNCt7PLj8QWVQt8BFK9bEpK2fkRdAY8gwVA5Skuv5CMGUtDOdndhEcPkM+bAyMaSA200xd7HTT28
QIw4TUCH5dp/7BALyMR0Uaq9U8wFkuw0MHZOBvJ5ajxoh8nHcvAerb183EznDqzRKCShMupwvRp4
xmOn5Dtiyi89mRq+Vq8AFBwf9rdxoZNuuoHa5TtlWR6JqLHRKCyfmfR+uwT3UfmjUpJdvtjhH8dN
3Q5NM/fPdWeob21Csmfvl2Autp+V6vRc/5v9FCj6QGLO+arwbPJGGO8FzdH1n0HUGItyIfCZa1fn
g6LSaTAQfM673WGRVf9Poyg0sVP+o97BaFNIHWGyHE1pf/5wig+sz/gSg/+4hHEOdeyG4SqD7WwY
e/CcFUaO1MAeG7RtzBZpjK8FHm7amyDPhTAA5sNls+6g/9BCRAnm1WC44dpJ9fJAyIn+akKkbCch
a1s94LYL6x0PfNOgbbuOmrhICqgVRn3MOyxD4aYQ4eAT3kw4zQrXSCeL/f5k7Cnvc8WTtj0ztQxC
Z5Xhanhxn5mXyVNx/XkNwliKj/4n5kzwDVUBo8ZIlQKLwG0RlgSS6FmEnllJy+CPMHtQ0dQWoJ9g
rPF6O2JcJ7FxNKEsPwDFChfAKflWnLOkAnlT+9rE+ddbt/4uVIm47FxhI8MsrDA3GPmvL+BLQdMC
Ixf1jUWeL5PN7+RXzqsT5pCovD4lIbtj5PghPkkF1ImwMk3XbEmLBM6j6nNfd85czKqnrWsX/e3v
+/SqdaCM2VBwM/O33VnzfLHWqUqOPOQ7QRWoIxBBQ0F3wej3ac77d0C0o4/pl8hj7kiW3Hj7eD9M
WIv9TB0GTYWY5o8Zg3HksxtIGqghG2ugTvmFjSQB1EuiJ8+YGlgm1CVmWIkOd+6wVAW+ByGtvjec
X5VMiMGR3d8bAk6Wc0smHnh+4AWtyD4+LL3g+5UxPGgtm+mLnU/Rvxgu0cKHdHigUWX9RtfrkIfI
AfgsiaMQ73Mz0HszzxuzquWz1K4XKw1ezbaxSHaCB7kI0fzPY1K9G7TDBrbBozkCegTxRC4Y13ow
6QrYekxwsiXu2ga7wA9gGMq/SPZG3RrzW6Tf8gJYtyHCOUjG8OuVw+E8a9o0X6he2h0RtbhNg0V6
JfZCUdhRBUwE7h7pljuel8HNdOjORquWs/FF7ihrCuTr7ESjmv9bVp2HPoTSk2ErBYQ8Y0UqrMD8
0bs8CM49L9zvjGbM0lW6bMh3fMlqPHjGOCrrCVzyLzor01tHZZ4m/j+kvMpHi6IB0b7t0PrtgwF9
OG25IoNycG4jw8+D6r1ceY77F4eKPG/3aRwAvkENaw3HIBmDdIDlW0Pm0BO9FdnZa5BIdte5MtXe
mpNN0XoB5v5ras7UpFRoX0Tlt8sS6sT+DBNUGHUHBK4DV222avP+BL7m+eFvJUuC0gNZoaRUHp4G
GrIIVUydp2sVkOFtqJ4xeqS5myP5V+vlxGEsqtuFEuDynIpzfgLMA49NIDnSzeI3xXKUv7TjKhr0
GM+ViTEGs0/dryG2f7zfu8FwF6n+sTxzBWUz+6LVllp6dOPPWLYudOGcpDnIVTchdYhoylmEp1U9
hfRE4RHjV86rcV4ZAJIt3eXCUskhKncUj8XJvz3PCkmik+ChgPM23T3Qop7wMLkKyTVGRdt6ztyU
BxXTuCtR1O/E2iUzRTWfKTN34HArCuIugr5NO+tLi7TA1H6wKtHT7qNNFxS1NrEN5dNuF1Up276m
jMgo4OCzYNPJFVDpxOBpoIjGI+h8wJMF0XVQ44MpKQEbJJF1hw4Jy3h6EIEuY0b/rG+IqSnPPcXN
BRNy79atA5vz9oLhlv1weU5ph+bKWpZ8D6cHSgxtMB/imDjkt65Qjy6mh8fzK6xedh4/BmrwnizI
nMKvpp8/4G3sZgc2xaiR6rxcHFpSpkgm8anXrfUAOo2ttfOZgN6JZEeWyLVlRZxk5L2F0qaiYL8e
PML26Hh1GgUzqZxrxa4zBERgJP3CB30b1jCLeR10PrDgfkq9GLLQU6gGq+GIO7hM7jv96+7xNDS0
x0+EpJYEfrTRDcxOQr6WYjI2ixN2yMkqwGKfRjzfpVcHhZtdKrtE6lCkXlN6APSI6xMif0KP4NJu
OEwRtwp9WVqQtPxhLgJWbBQjn19A6NspBBATaZaooLnr6Qrlg/EkYihiTo0CYXR5zKLOlkwHPdVi
HcNshL39gI3yfjiQ7cKNrbZBdb9SJ7nl6etMHLUDJqz8Qa92YK1NXbwtw3tIUysGu8V85xGOGlFq
kT72kXMQlpPgp1J9e7EM2d3PTiU60cRaaFx4F7M5F8kepTyZU1DkhzZ6EbX6GC04fi8WphRlx7Pr
4y+okexwPqtD65zu1KngHNg8t4dDQKoCZ2xYwjF7BNXUdUKZwRsRV7RjnmSr6MzFBc3G99FoTUh7
qHyNAeoyZGGOJg5kQE+y+cT5LcOwQFuGsDJJ4icVBPnFXHVQbMUWU2EY57UDx26MOWe1zWM/nt0R
4tXcPvfWPknAaiAupHCxnBuay1Jx5KUtXybobmUGxrjoBfgrDZgk7rkVi7ZPU9QKUGM5w9v8swTS
N5buO5fY4CaQUq14l3dJKu4R1qcm+UE0APFL6QpAmZ2RIF0Pl/5ChksnW3BgSpf3ormCxlbcgDUv
DXpnJCdg5QrvNRrr3UEbFlY8BedQC/D7NGxnAzdIXc+bN7qPpmRfZqvLbRJ5hVfWubAAGajajlCD
9Uj2FBEHPzOVWhcvpGGny82QAR4Hh+FGWUGuRt5LdTfl/rkEYnGCWg45mOgRM3uWsCJa2JtiC6a8
Tq9NRA3tqB1Bn9DCDDn8Y+6R/bBeLwYh2a3Xb25R+ER4dKEzTd4fJj57kvWwfFwBS14wdHRXhxEj
UlogoIV5Elj/Erh9Fxk/NO7Ox3351hRmRutGwX7j60UCDCHy9bphUY1aQ/dN6HI6XBEXBgOAbFS9
STQ0r4Xzrz3wJWEMzI+fHygyB4vHa1d90VHNY5tje1bHUi/jteOx+8G2ILhGz7gCUr1Iv7MYTAA3
Ai65f/yN8euOaQdxFm6vwWew7SSVVOuh7qYfmzjqCwSY0XpA1vmyIjJnLdhx+PVoIZM2kJUpNTVP
LNCle2znPtlrBYDWSniaUQrZauKLCC71ijEWgQorscA1Em+DhF+rjdVYHwNvxHVrN2xf0MGajoJk
KDgzokrZu6lF17LB2PMqUsOII7YUZKRRy6FTzbOQSPlfXtZRPbD1u5Y6DI8JOU2ffTLIMj7KSf3J
hnvwxuTt0axQbGH8h8diP7J8KoRWffiVhCFtPhjeoqpQ7lU0yNBFg6y5focbmT0xlSu+Mmu3wkuc
WUgGsvS5YzNFK+rRZ53khgJmfBDUGCx2S3ci/r+GNnn365YJs43CIGOJZZ4R+qxOsKa5sXXJ3Zot
4a2CjQnUFo9QC/LGWBjTpjSPgZXUGPHuMs+mvYBAi8zXjdf7TM3vw5eHy7SKA8uJYg07sCPSWoqE
5qL55k53P8tOG7CZQsTvs+MtU2EnRn1o42R0a6vpBRsdZy544u5oAG1o/E6Vyg1AsppAryqF/yJa
lJAtTVH66e4wSNDwhWYmlHvH7/Cq+HeW+ZsL8LKqrwv8xqT+a2vpwxz46i+yoaQpBhx9nv6q8O61
v5AezcpUXFlT28IGvbvT2bOB0kwAea77MjhbIjzzu9YPbF4wNBoJkMIhNk6fG7oR29t8juCa2xeW
Nza2qCPuJsmKOy+Tkd3P2tgQ2oSAv0DRq109AbpAP00wSlRIE98ZFRDn3x3absl9sraFFc5liwy9
fhWBF3pTVkIIVHeYJZ9QAgHdc3DNXiLhr/XuFbzuuCLGBoELgiVhbi0wektZIutl05aOvMqa8112
dwz3xUBYoqP1X9BTN7XKS5WBo7dPJiArzWypJsbxF4j9hcukh+eSzJe4hTf7K02jHPhxJJwsqito
Ek0OOjIo/Ai61tGTU0KBjXscYVW0akKoOJKwda0Hv1kI2Hx3N2dRxoHX5u6aoEi5JCxr8W66BV24
HEvfaghxK3H6Pz7hP3tSFC0QQUCATAgYkkESreP7EeQ2JiaaT6BEHE3hmoLOvg0eO4vfmHg9a7se
SDZkLyLi5TmVfnXspN8DZiTWyNzTlmaTsH6uDw78DE2lgZ28wAWyqNXlnSlaNVtXuM4jXrl+7tct
wxnFc9kvGRZwvEUcSwaS20ZPpnV31fJuCvglQ6T3c+oH3VSJkXiErv4OAO16xyjFqB/qveXd3i0j
dFGqe3O/Jc7ylfyfL8pRpEg/HmrrdzGKAi4pA+3VtovGcxhnYnRbB6xql9BD9kGPZdM1DcP8RqOd
vdgL3bK57NvXxSNlbWATnQNSL9uu71UQe2FL7dkg8P+WFZiBAT5EOg+85kPG6OkP+aW48gVk0F0s
EYpHHc9BTjZgsk7hs2hjTkZaP197c1cU58wdMe8bmGDfeQAr6zad0St0MbTtKlkgk7RPpSefCiot
oSfv1azUBCnwwj2w0ByNeJYJ1xgM5kV1JXA64M9wnqtG7qmoGX3MZvTWrSslkcomfVWh1fnMcKZt
HhKzOswf35D0nAx/sUF+toD3U46c9pnwx5yt9+See69KC9AZsqTDy3cG2BgfxS4Wk9fURZQcJWyA
S2VaK7Gf3tBDQdHAcn75DTPt9xjGOWYLYngQR5Zloq5gz1ch6zWldGC8BxkN6g5hiTPzOZu2pCL6
UtrKr24r9FFyqAcXBBeGk69Ik/DSdRYyjZ5uU+zKnK/UfcFr88/rlfv3XulY+vlg3iZZeQoLVVsu
gQ5Cf0lAqNHyvFYwQGzxcC8aVR8Q6OCzxEFw0cutnxWAbGfliejq3bDXhqQP4cYy0XHVhKT30orn
oZb8A5Y+PNWgS9AbKmVMPf4QfIv8uEzwjdqX8RcS9qqFq8XfIa7SgKKMFUlIAkEzi1U54jx86oJD
7bNtlCQlAqm4832oRDpYrcOVfpDmNKIuPL9X3NHeMhv1QPz+snQ1sckmgwRchBkupjT/eKijcKr3
rDrPWq4HgIHJkkLyKHzl/BVm9aTbq5yCh0zjfvTE1j7uKz2asp+sGKhhFdeg/bzFa42Tg2mZx1mw
AFymHbl6uaRAHa1ABp+22r9yBPJFfKQYrVzwp+4oASvf8biwCnTtDTQkxQZAXMZWyDsyBqzHDFSW
n1/Qs1uvsSYFTmM2C5sRxmsleS1B3L+9fRmoG1PpQ3emJ53969+M+CKIuyNTHYm1ZkltVUacMYmU
tT5AWO4JDSRZPsgA5ofbLvwec3YqSGqt6+Vc/Y2GSAVX5sbkOkHrVPMTg0VCRWgxH2XBTcItDaZb
adYwWAsbhyZAlNuYn5hmOHvIQjpzDzVGh7SVSZb06PmBO+FUZHVEiSpIlLT6jmgRHBHBHzxXDtWX
Hd/7pSITPdbTZgS9S0HuWRh/P1CmfA1LYOxbiPSOHF/zltWaBCGzTpvxG8XO2ql6rN0gFSZbVLeq
gTUXhEp7F2rSIxUuXcNP3A7G3YFDHTohH7dvOjl6mcwM5RBy7BxFf+RDVX+jsCHJCXmS8g9H1IwR
WN+ZA7wTQAsIiZU/IMsYpqpKISZ4s/AmhLTwYTlGWZZzN1UF3+qOjY/focNAcboy7scbqYuQfqlg
aTg3SjhMEKLMG9qb6j/o3ldNqrlUB1wEz3cvRy55mgAvEJeZDC48Kp2RREv76Lu/0T+Mm9mHtz+C
+ZfkkDNREexegmmMo/vnOOBOd8TSSZI3/81ZQ7FC6XNM6gvME7s73WggGnHxAlElFc5OJs3I5e/u
qYKX+4wkSn0uLloE+tsbmIjI6/xOm2HLcTjVOWdO0hT/QXEn75DUFoMoxOEJTrV3LzaHwGJv+/uz
4pUlx9O6NT5tI9/rsz5gYiHH1Azy4FjNOZ0nE4lKVxm04TXrHZXUWi8qtlKG1wdLK6ReOxi+DhcL
uCRVyX92gpndDUleqZfQnxwwR3vSRdmjCoM8pcyA8vpe7PwYrvnXbYuTo8Gc9PzTzYxxF54iVEXb
wJBt3dceKqbWwB6WJmmmJbEGUAIpR3E8ZqN6pgww4LUJp27BWI73V9ymmyw+vARUmq46lNGrAyjH
+xGAoeNSYp0rFT9mjVJzU8kpqOx1eOdFX14UY8ACp3Ins9j39UaBxVWvFLfVTnh/iajFM8cr2yK+
Yeqr8hrDqiCW9zmFmcsvzdqvPRIwwYEhPjHL+6dsZgMVJ3hxst8Wp/Gsjb4yi2lUH6Ai3D6V/M0p
c10ZNGI1/oYK+vxg6JOH+NOEpVjb+VvJ/omzP5yTpiQqpSjYpFuzIkCNvxpqM29L8d9IRDfKcpv9
ZgzAR1x8nhmg2pAewki3Nrjv5pSDrrZtdFMKxHmI5Sp16vAcqG7RgmNt8bw6aj74VDM2GX6Bl5Tz
pjdS0V4MFpBDoS0EkH5nkQgZJ/GgwmXPW4hZtQEsrYKouOAcBJWx5pqDPYoKMMYasDgZlRUmr3S7
PN10WS89brY25tpF70txvrWw3pf8Rbyz/dWEk4/NXksonyuWbpv/Jnqewa/ES9Y271uw//rHbsT6
EQZaMUil3k7NKDR8NkP9RTw5xyICFncz/Zup5HojEcXNxS7gR3WYRVDvQXdraxVZtfKYZBpqjoR4
ZC0d7JHZarj0I40VC00o54IsDMSjQyg5dhqUPvcKOvrmk8dogWplYoqVijb1N0CbN6PKHj1jzCi8
PlZuxcaW9mVF8OkMfwSiQzG3H0mg7KNs5RdDJemss/Wdx+Q1Lw5lpujNZZvOKkYY7eE8nTCxGfmt
MXuW41jngORA8fggZlNqMCEXr9ic8R8JyF2Fndqb34GuX9isw0rqw9Ms1pFdvcqGVxOFsTTiJlyF
VMxKz8Dqouhil1g/LPKQ7GLvjTsqhCOvO8vxPfoA8x6Rklhz2qHzbVvtaVYfDBdrjK5KZ8hifSzp
Q34IdtlynfPnQ1Gk0IiqXipyEeKmOkEPhhMZEMUNJ4YtoNZSTyzwKVaAtkHczLchqRtoURa6S9hW
V8AiQrqz7Ewg5zgv1NqtR03iyx2/buMTsIDlALCppDDa1VxqkY02QSNnl2B3zzqdJIzVCNs8JQqy
gA3PyO+ugw3KRjrWfDXYl8C3wi9/FWM1nSSlThHq3jFQ6mYZD5oQzog2D0EogamUbwgLvtJSwkQ0
31eNah1Vs+wjgHMzcF6IKfXRgrWzy9j4RhmSBpAMaDeAeH9wjAJXvKUbSCyCcYI5lhqKUuo4S1cU
YUcotL1RCl9AU0JpMHj8OiZWxk2AQoIiuH+4Cez8HW66NW8FXLqdTc3XCXED2Vu4SUu2P9K30RGg
1uP+Wv3mzS6JgIIvToTrrlPp22mxY1TWJWZ0n9Zcv+K+lq5PNkXLwBJgXUnQtcznPpgxgz20Jb93
QdtiXfo5Y+uHRjSMuG14h6N5jl9hypZIZnjCfaYqd9pDP4bXd2ncmO+M2ZTT/UodJl6CfMkf2jxZ
+SD3aJcqrmCHg1WPvWyiDoE55pJ7sS4HPyvVv/ZJu+2OF27UnORvFVQghaqHtHWH17FDCIO9iyRJ
bhTfdBX/t1cDMzXzOISeYHrl8xb/Lf7n89+JuZduDq69s0Ba8guXzb4/LKRuJeCFGhnfD6XU3540
sZZdOByvdApokxuxndRu1PmA04HljfCVK69x1MD88bOfiVQ8FgPtrQ44T721HXDowJnO090L2JZZ
hFcGZXc=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
