// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.2 (win64) Build 1909853 Thu Jun 15 18:39:09 MDT 2017
// Date        : Thu Feb  1 18:28:05 2018
// Host        : DESKTOP-0KA73P0 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               c:/Users/espen/Documents/MasterOppgave/prototype/Multiplicator/Multiplicator.srcs/sources_1/ip/xbip_multadd_1/xbip_multadd_1_sim_netlist.v
// Design      : xbip_multadd_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "xbip_multadd_1,xbip_multadd_v3_0_10,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "xbip_multadd_v3_0_10,Vivado 2017.2" *) 
(* NotValidForBitStream *)
module xbip_multadd_1
   (CLK,
    CE,
    SCLR,
    A,
    B,
    C,
    SUBTRACT,
    P,
    PCOUT);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) input [15:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) input [15:0]B;
  (* x_interface_info = "xilinx.com:signal:data:1.0 c_intf DATA" *) input [47:0]C;
  (* x_interface_info = "xilinx.com:signal:data:1.0 subtract_intf DATA" *) input SUBTRACT;
  (* x_interface_info = "xilinx.com:signal:data:1.0 p_intf DATA" *) output [47:0]P;
  (* x_interface_info = "xilinx.com:signal:data:1.0 pcout_intf DATA" *) output [47:0]PCOUT;

  wire [15:0]A;
  wire [15:0]B;
  wire [47:0]C;
  wire CE;
  wire CLK;
  wire [47:0]P;
  wire [47:0]PCOUT;
  wire SCLR;
  wire SUBTRACT;

  (* C_AB_LATENCY = "-1" *) 
  (* C_A_TYPE = "0" *) 
  (* C_A_WIDTH = "16" *) 
  (* C_B_TYPE = "0" *) 
  (* C_B_WIDTH = "16" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_C_LATENCY = "-1" *) 
  (* C_C_TYPE = "0" *) 
  (* C_C_WIDTH = "48" *) 
  (* C_OUT_HIGH = "47" *) 
  (* C_OUT_LOW = "0" *) 
  (* C_TEST_CORE = "0" *) 
  (* C_USE_PCIN = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  xbip_multadd_1_xbip_multadd_v3_0_10 U0
       (.A(A),
        .B(B),
        .C(C),
        .CE(CE),
        .CLK(CLK),
        .P(P),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(PCOUT),
        .SCLR(SCLR),
        .SUBTRACT(SUBTRACT));
endmodule

(* C_AB_LATENCY = "-1" *) (* C_A_TYPE = "0" *) (* C_A_WIDTH = "16" *) 
(* C_B_TYPE = "0" *) (* C_B_WIDTH = "16" *) (* C_CE_OVERRIDES_SCLR = "0" *) 
(* C_C_LATENCY = "-1" *) (* C_C_TYPE = "0" *) (* C_C_WIDTH = "48" *) 
(* C_OUT_HIGH = "47" *) (* C_OUT_LOW = "0" *) (* C_TEST_CORE = "0" *) 
(* C_USE_PCIN = "0" *) (* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "zynq" *) 
(* ORIG_REF_NAME = "xbip_multadd_v3_0_10" *) (* downgradeipidentifiedwarnings = "yes" *) 
module xbip_multadd_1_xbip_multadd_v3_0_10
   (CLK,
    CE,
    SCLR,
    A,
    B,
    C,
    PCIN,
    SUBTRACT,
    P,
    PCOUT);
  input CLK;
  input CE;
  input SCLR;
  input [15:0]A;
  input [15:0]B;
  input [47:0]C;
  input [47:0]PCIN;
  input SUBTRACT;
  output [47:0]P;
  output [47:0]PCOUT;

  wire [15:0]A;
  wire [15:0]B;
  wire [47:0]C;
  wire CE;
  wire CLK;
  wire [47:0]P;
  wire [47:0]PCOUT;
  wire SCLR;
  wire SUBTRACT;

  (* C_AB_LATENCY = "-1" *) 
  (* C_A_TYPE = "0" *) 
  (* C_A_WIDTH = "16" *) 
  (* C_B_TYPE = "0" *) 
  (* C_B_WIDTH = "16" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_C_LATENCY = "-1" *) 
  (* C_C_TYPE = "0" *) 
  (* C_C_WIDTH = "48" *) 
  (* C_OUT_HIGH = "47" *) 
  (* C_OUT_LOW = "0" *) 
  (* C_TEST_CORE = "0" *) 
  (* C_USE_PCIN = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  xbip_multadd_1_xbip_multadd_v3_0_10_viv i_synth
       (.A(A),
        .B(B),
        .C(C),
        .CE(CE),
        .CLK(CLK),
        .P(P),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(PCOUT),
        .SCLR(SCLR),
        .SUBTRACT(SUBTRACT));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
b/QhBR/oOzwaIZ6E7xuGaVjTIqrrrk1JJQRhGHM3PGlr0wSnnQxll/0isGyM+wjDSK9GtAlYP0OM
/PCkyb+ehw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
WJYbCKycBENXWINGjywfHsrNXZknL7yjgguwmqs6OwjbxK0hf5LYRBnuDpYwQhonmgh8FspAKN7S
vBI1o5pda3s0NrnqYv/G6epYOX6UDWwAVMwCaLpfxBgAA/lPO47odG5bWak48ZfirMNoqxPrYu/X
xn6bfuLcmjfyW6TeE4M=

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mW//b3sbAmqxeiGHJDCiVCvPi9GZokMwU+5dRZ6OAcIpAn4OGe3GYmtpujCuVoiFy4oJaeHTE0DN
0VSZByGuwXomWUNjVxzi6wOCqyMnHN+CyPAWgXBhdnVWIXrkwfog4y5TSHD++gZeUJPFrxmlbbwN
+DAsGPPK04f6ZjdOYfI=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qfQO4VdbKTpU6s25roZ9u0W2IQ08y+6LFnuMKThrKN1hhqSYoKZqaxCw7+7+dOwYONUoVDh6Wi59
Y+hGQ6HVycgFcoV3PaEcdVB0RoESzqmpiYJ6SrD7h8mfEIcp8t/XKFfDABpO6nrhgegzhtWEYOGW
zNnM/aMonrPoXnt40S3FQWlio5xbBJxLFXmdWCC1wAOsQdYsVK8EQJIFPrau95y+alu7rU9ksc+/
3L14+fqyd229GgD6dpTKDZDDB4x9rEW8XXVQwPX0lSPpwjPUyfMNaFv3y5Qs5okbJBAUJO+a4OxB
UKx1FvIAwLTAhlwqHDdnjdWxezTwyvyk0LCzug==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
tm5E70kopcZVk2lyLZL2DhsNkZZ9007bUVlLF0bQzFHvYXYgcsXAQwflg5D1YrTQxGemPecou1PI
Wg3CmGsY5A9b6uGz1Xjtt6J/eMcflQGIF0plxkFJ9Yh0+Ud7+r8n4mljCP6SGYZHkRKF7XmNsEdo
vHkHZqPf2LZqnoTfmz875NP1mZsee1CGNEDbbZ1ILj8vkJo1u8ENiebnBG6kJtocnIpotFSnXK61
5FL1/B4oI0S4Us4bs5vvGLigC1RPIv1QZ9y0LcCax4QWJvscGfW/bGdGhqFZXQpbYuMzfxl6GI6w
wh7v2MHjv+Lr2OrwkHgkZx+XLyAwfzJL/FyEKA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinx_2016_05", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
wpqBV3cdN1DAgn0KCpa1kC9lWQ1VJzcuAx5h8HQTGLCAzRUFQnCa/EFt6R0Qy7R3hEJGwHtvMUHP
h5h22kDwO5bAhKqaf1yU2gCwDqXG3DewDm2wwPj3TBQ5BBHJapwykdeKGMkQImwpxaRWQvu6I3xK
IUFYyggVvKKZnjDCYJHQIqubpmbB3Z3L3Z7uiKTwmQU0S5eCklRLzKPcMiaVKsrXf/3wA9mLjywg
udEIgv3oufyZDG+pbbDReiiG46DHu4cKmfKmIhwVc68tb/KLSnrROnfM1e43PKLLlC2Nb57FzIFr
D0AvLBs1wTe5j0nEI6Po55dq7Mi3efdkT/iW2w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="base64", line_length=76, bytes=256)
`pragma protect key_block
H2WpLNBPHCU++ACvKdB+gQqFb7t2pgmJbWndpkkQCVLyp7BZK4j4LQBH8G1iIBgZ6oe8nt9AZwXA
qNlN4Wx7XWJM8ewKbbIN6WSVGN7fFaTHLXZKyLxbLc/awaPSa8miVeYnMB2n0046nx7Z7JtPuyy8
tXP2Bt+NVVvQr2S8RrKE1adnVsc/Z7vugEt/sWpgroOw3kng6SLWDFo5GYLoh/MaQt5mOwO9ykB/
Zwhy3bi8dGVy5cImKX34W0O0RFwWaAKlFIMddHDGq76HFAjVQJ+0eB1am/7cvrfKnXDwXJOwL2XA
lOwTC0xM4IRNSjVwNzaMYQY+1heOzpan5tDPDg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="base64", line_length=76, bytes=256)
`pragma protect key_block
mCAIhaGj0QijJgyclPdKEnBJ8ig445wuJ5j5C+YyIFOwdKNO1jEzwWHrlsRk/hqwhatcQNfBHUog
1bxSBxiYQOqXzmaFnHqYRJQbJl+4SuDVICvpm3i3Eu3V6XhETRMsLbtMmKUWASeu/RQ3OUZKSRNd
AitEdq0DNceQLVeRm2uRw9oF9KBtxsXJ8Xci0OCz63igxDd4M0lNxryYMcf7PXu7IioNP3mk7jq7
j7jMnn0hEasUiA6LySd2C6d4CErDcW9BgRwg5uZi3AnkVmMKVlACkzSCWJvWWXziGlywddHbcLEU
Y0Wys03FhqnSjvHtT7Cm2MfMxfoFhhAzmOYdCA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9088)
`pragma protect data_block
Tq5mUBgrJ/C93bzImV58Pkz9loJ7P8LfE2vQx0kt0mvuMP1Eqtx7+mf7a+Hxu3IRyBjSkBNrK4tE
0l/sl1Jcn6GeHKsvL9HD29K5lPZfFU66OEc/VAmC7+suwyJGftwzIZCzfj9JP7gPj2I+yPKGBJmw
CYOBuIAXjUWhVER8xEAPkdjjjEd9JTP3JpewhYk28D/IN3H0bje1RL+lqlCEsSCzSi8PJYzL4mGq
l17yyMv6+ERfK1YZP3kMnHHszxWtFgfyCydtXH5siez+zZm9VQ6AaHUE46KWTtNNrN1LpUlfKQ51
YWBGjsKWX6diRX8YKed3eTLe3HZ2HWjcmqgsDQx/4NmFHPgVzt9c2uIj9FwfW+Q5t4hPxtyofXn8
t2YfalU9dTWbbVaV7d23xeOsQrY1gxkX16ROP7sxTUTcZWcfjwuiZcydNY9/2/z5AKvrPQOPnx/f
efXJp3x4bHw7ixyoFgkVR6B/so9/rmwVFLgu/ZxjF5gnrRj1pRoHfLcpe/ueKBVp3Uigwx5pFhPf
Zqj4k47Y0vC2n41TaVOB3uJYsmwzlqvoexxKytAT8r4oGrEZIOyQCFbQM1Y4BiUu1aNSiRl1KVNh
73xu5oKDYcrGQAKfrFsB8oXtztZEjenLvr/sLzopwj5YpAyx4r7EwH2xGOSjPtpEHI314KHIGzqD
MYU4CJwuDe9UOJ0zRusGNIDYNBY2LinsvxH5J+kLZNczQLzIqXu8mkMebyQbHU+HZ0RAdt/pINSD
M6aU6knFJruS6XAcUkH1CgtKaqaWAvjq7zJsDvMfX+XJRJE+8iViPj9V5+DlhI/iZEGLHZUC0S3k
VW7KDiMg5LSBQtDvLg15F+XLwVPjZIJuDdJfCd3niHJ2Y4MOAgoS25le/uE+Ihnh2vzFlxb98ToP
sSdr9MuEqpJEfJ4iQH1TBU+ita8jG7qSJz34q7vBfvEoDX2v0zPFzb2HOEAXHj2BI3CspRVuqvWj
eGvrvXkNxwM5dswhIlykrLA7eM3BO0O0hunR1up56FMzoxCzyKWXO6SPmy/IUqBN0jRnFchWZpOP
wJ5TLPCMbL7SiR/+zmiXUjtHew6Wcy2soBzo9aBbkMECV3QMr8tfcMuLcFHJWzxd9eMd11srn/vg
kHuROwPS6MSYUHRDn5qw+OH//vvfj0n+T6c2Y4+OqLMJb79JN8ERNvhKHY8rMSuoa2WlcbLcKh18
46t2LH3G/H040sFn3sq7jkiZ/33mhloVYF1ESJLaZ6flu1YZmMtzQenjbPSJWQEzvT2HgWJSi5oK
3TUJa3Wd6peZve4OufIhvrJ+Ni1NMN5UbDmmz6IWofKInFAkYI8k7xsfcdNdTeVKOgG6UOIDgt+L
vwC59QuSrhEYTh3LnQ/YY+dwH+DcgKUsO7c/F218Yv1Hdyhg0t0TFkHu7MgBt6R6xbUjG23rlZXA
HBaM4vuPTYwmI+glefOl8etpedRPcBsGbTuRHWnyl/2TabUcccuc2kBZL1Pci4zDI5asbPgiSAD7
YgdkGFlMRrfWi0mbSB0Hrj95/VuOF5jOvWHeVnEO1zktkq9qHeyMzyhMttaNSwyLGo0gXNR94O8X
9VgyQ0jBPmntE+k9ZwPg54Ynvnqf9R+4792sLBey3a6Vy+v3BYkKosZ2z+ylQTYHmYIRzkjuSHsJ
nxEZJSnhauwrJYlmYegfxwdNdRJ0yZjyJszvqKvxIesz6W9E+fIN67QB7yN35Pb4s2jnI2ikcY7N
s0A51pYVp4UioGj6jt4GSVT4TIgHF72t/eitZ7Pi0sXF4NMN332Nb7WQr71jD4ze1GH9nR5w6Vbp
Vv5x77P7TyQU+jpjIdoiHiKKR1HnINqv8nSkHnK9gPPJctAjtjPnJeoh6ZcJbmRbCjqX03Mamj+3
93QykvLqnSCbvBKwcDd9Y9Z3QFusOmlVw9QmYSzg/iRqjB/ieIMW5eYRF80WE9Lg3kp+rhTCHUCG
v4pEAo77IOIa5wiADthKBLK3SlWbv5P2Nf1m6qiiEACSlSq1FAHGAH8Bb8LaSW7vgbxIVONj3Ubh
qlQv/rwnfaf6fHfCRXgTcZa7NvI0LZ/YBcW1XbkkIqIcKKMJljfto2b8GArDBUKmBAc+Qehba3jw
08R+rpt6s9nUW1gJHSIyQei+ZKYZS9Tn1Uie4SaSLkV8J0pnP9QA4qAH08MDPHezTEVPGnpVb26f
s8WgOI7kYu1vxyGqKN73Xs7Cce56OAqQg6eSRyPTwb1hG7gx4ESfpip+LIHb6mGMvUskz2si2wNC
v4raqqz563WFY5Y7lpyy6ykbvvKLfnrYOYeGlet/SZo5KuOcVNwHPoHYblW2jYaKDTA0qbk2DbD0
f3VZz1sb4isBrrm9XuMw5CAI17qTw+4C94p0PD1d6Pmkm0nUkxVGoIbC9F3wX3EV4T+8TqYI0DaQ
ujF5/ri938b8fpqcDQ2o9yAv2xpClZACniU3dSUnVguwuQkM8WtBqlTy+kZTymQkrQPkmyXSnbiv
+u5+v4Fi+OZCTpYgCfAjNJLxx/6LVSjGMrOyYdRZP3pd4aD9xbKtpP3JpUe3otVJbDUbXYg3ioUL
Z1PqH5ZCUbvemxQowmeiCAYn+QmmIrJGwA/hN510trlTRHSwW1fOoYGhG7oqqJJW4ziEKuQKoUZK
JSIShrEuYSrucqxtOZVeusr00quXxQEyMb3/JzuTyAf8sW/jgbR41Y92cv2yiw9lF5V9JU7d64eC
ABREjeqGg4epJ8UYRiIYrHSdEIElVwk92O9093gQOoQmyMEzU/e0/oQvruQUh1gAZFraoEsngoVC
+hAgSvPy+LfDTNFrHOXm5jcpYs2yHGmAmzYql4oWyX30VxU0mHholXaKk1xnIH6wL3fdOHtHJiwz
ZTp+HreCh3cwl2hlAibfauixtLN3GJnLxoBwki4vIBnN5sIu/8ieysuLczPWPkKa0cXuLLXD61I6
zeYCfbe7AVTO7M0SkPPofbfJcTVi0A2hzDKK2SoDKNb1mn46wNmDDp64rLhlHNmH5yuGZAXSpQV5
J2PQA8/cF9QpUuP6d3ovJZyLEm2FaujpkZb2uhZdFYUNmFfPZpe9AYRjRUGaer0S3Hq6GTPrmHzi
g9Tc1pqTwnL29jJPyrGbFTmVhUnpWWb5XBh4knlrnxu4FF/g4BfFF2fOh6NEc+6K8VsB5VPzK3ye
ewh5aWI5OfYxI9M48zclaD/vaDmmx7TYIwwQ3wbtiXd9XRf3YtbWnDf+z/JvTubpVkLkl388832e
E3QOq8sJDHcvWRr3vnqG+rSgcpufkMq+y6ccjBrv1znxsYf0L+1WuK46jTU2PeITEMKWjEbORMtz
j+votUb5cDJ33V5YHnG2dogitHDqIJxt9S6r7yv6s+3L4+eWhhVYUonc9u1WLePnAVWLf+ofiSwe
xYjPghbIrGL/nK0eg8wFYrY/AO0D4Ip4dtolAcfeqXpAInxAH0Tdi1nKm6Osvys2o9IzhYwYZ/tO
zlpLoQnOErx94g9x++hhhCQ9zzq67G6UXf7kYdLuNSC2SkWOfszbnS3+wLpnjfgFJd/l0owniUeW
VH5aQNpsg7UDKGirMyfJOHGfyTUylYIcSia4qk8PUxdFmNgUYWLnBSrwd7c5sKiehSVWt9Y9jFHA
nlhtPkkqWOVgnQXlrAIZ40l8nSX6aQPOhVjeOVVYznQAmLNDeKXSEHxnHfyr49gcfZfprXaZJQMk
D0avXAzG/HztFSvx2IiKXQ64p72YaBYTcZFdcAVwMqoi5K1rNrMvO4M3Z6qSfdly+DE/UGINJjYK
UhoWhGog9sVaHG5HTSluw+XD4AGtDLzEhseljYJQI33Pck0POD4CDhYyUGn5KlykmEPtJAtfDAfw
p8y0lCDpLonla8LMCr54JGLPkTDyRPaALusVF6mAwh9n3kKIRYuH1VeiU3HX7YJxI169p5eUvXIY
pQNFtZGGVmnSkOe6dTitvdy2pYHep6nJAW/y3VAdu/Zcrc1rBt96XG9lZ7GtlqtT09gxuJ61LE/t
Igge4C+0j+EJ+1zZACr9mExfqrIPGIZ4BzB/6uuWF4Lglmyrtoo8S+hfx+oG2WvIQ8QtNoVDOqog
YBmiQgna78I5umv1f0nIR5GBIVezJirCWR8iryYl8Q9eJUAZA1iaqNgbSqQCkX/EVuX7igKIWx+0
uPwhADyt+d75oG1ar5TWIlMZ7XnvV02TgzXgibikdvNMitsSbfTt4Gf01jc5GCFC9Flq1ZCYjkmi
13XL2LMoIadEgOzg2aHpBEN8Z5aBmNI5Fdzylp4vghWyUz/DFyyymYWcuxQHT9ZxNpzNH/qbJSLL
8waEmK0We0ufPSXXzmg+S/W+LKqD1sTA0NT2wR4ZefhZ6XUS1Zq3tJTJjB1ejBcslhrF2geAxgaK
qY19zsYhRWx35N2VviUZ8lIsZBb5T0HUg6tPoLt11Z7mSSUM/dIn5l/0Ss9CVCHwz55lAJbfRg4b
hYTm/Uk1Ozbf6rb9N3vs54+QcFMw7EAg60yV0SgBkwLNAhxdAlWOCiaMROlwF3D+Y0jub+slos8W
XpuEjTiYCEJx8bGL7VisdGbxNRzyNVxEkqrzS/C26Q63yUq2VfrInRVKWoY7FYpgIcKaikkA6NrI
z5VgSyesELClOzvPsfe/+OSZzYBesY4SaNznlC6pk2TKQxZ4QzRrcUyMGjksvW6qNOiIiGUOtH4a
zsza9Slj9nOY7j9ilDhrvt/zfCXiO00+sYfCb5FXMR7Aibw+l8O/i/M7KD0lG3EZD6aSUqPjDe7O
ZeoNkAa8E4TxTgJNbvPuavGMxW1SSKoAAa+HV/WdGEFfJHhkT100XBoSBeS0mnpZZG3gwoPBHWld
zGucpW8UL5P9Wjt/9R4leuYx3GyHHnuhk1/H2nxSACTy2UL96BjHcD6TAwLe4UEEYduSRZu0RpYr
n7CmVU0gmlbGQaybwMyet2ewxWVJwosiEN8hHgC6Gb2PWT4jIZtWmWyGwxRBE2CmOpbgqGlNTb/A
S4kCBJiHKzBmdFToe9r19NcOfLGnHunHi0x4sG+ydZeOgQTz44o8dgd/6qy+TYRnQcDJ9/uUuyzG
mR/VJlq+Pul4DP/0lTRDya2/agmQxcqaXJbJMg7AR11VNYYWzQ0S3Z2kNs27K6T/VfI4yuwVweQi
yPAF/KjXfjDf8dNbxuaTKPncolUs+JRb7qUtCvwuQbc02JFLVwhL8jZolG4qSGSEYOXyxiZl9Mn5
d6UHzE5V+r0ShAHr48QnZIp9LQMwzpwMiVkR7KAyRicz70+7RrsssoWED5tn5wFnlTrNCgwldjOQ
U5BwthixvoX2UuczlVf0Vl+/XwouTnpyOrWDaKVcAJOlO+GyUjCcVbPv80gl97PWVlkVsEzGeudP
ANLy0EYc4V/FSoj/7ff0xINfOn7KeeqVXDeOnt8qntbC+CQAu2h3lRnXPKeZ39RrsEMS9vRjwvHF
fIAEEzACjT0qDK6+K7BWPZT3cjFYNEuAw0hHWTISPQtpt8IjIIBuHDdyamtFMUihww58MX5IAu07
W7uXbYFx58gakOfTzUEjnC057kFAT4XJ8pRf5qJRea7ZOtEl0a6bFSYCPXErMFC2LlZ4kWHItik/
p1zZTjSycKuFfY5uvXvXs3YisBNQVPghmfSLyUc+eoyKpGW7PUIA8hPiGWQTdRFfr9OVUSwKdfzo
mbFlXzNaKSsx00VBgdZPeclerP5k4LWP9FJJoI8P+5psNafHRa/6fVfbd4kQ+qaaWBKt/mSwHwDu
MhbgTJ5ItqftTda/4+2JtwOBWP6B+0lRJadDAAb4T8/oowX1OQ5MJbrg+IhQDiy0TSgqeT+HYL//
klEPhpf2viSvXhhrU4OirzwBNiqWOvIa4hkyRvGpXKaxxLOPKn4U9mjF2YQoEhMp0nwvrfSGNQJt
ZYry2itf1RnruuUIxoOogT4UhVZ60Bl9RO8XzeTRmX2Mlk7BRk8GXHwEl2I0dsaZRk6a4FJQmdxz
rC6nShxHPuvUxY4PD7uCQcAfeILmx4xalDEThj9DV+z0V/ysO1ENRIic3L3h8edVChxic+8LgTNI
HkVZCG1iQaeDshLiHYifKqXhf8H/1OGfWsVu3GIIZkUGsdkm/YpL1nILhPQqhnDjtn0LC7AECCIC
C6xqfqGQCzxpsAkPZiIjmEApRGbsVfuQBbVRHFRY+N7celqhI8wtk0G1rpP1E82EN7nS1zMZxH5Z
Qoox7KKdkNePB3CRMAgNuz/eYzmExSPXX6ijOpEqd4hkoBwy0UR2LWfBVRC7XoCaKYxF9Y3eEqap
KBO0bT3//7WeA8pCeWkmqu9WKvL53YvLQ0hC7sJ56P/LR97ZsWcpCy64iBaubOyisl7I7hlpzyUN
Wxs+oVPneUgOpdfmdzwED6cDIuuzaCsr7lK6ZoOCewh0vifyzpZNp4flwCM2nND5YUGH8o25KBAO
pjxJHTGonEghfHW2qVBH7yOAj0PjqRkjAEm8tA9Xi8q9Z6SVlkShYejucl8rLHGbiffZtcXlAJ8O
da+bC4Uz/9faGQESRPkPHMNP2G7Js9zEKOpf2jhZwDh29mXVbOC2uQ+wMmht7+dIHfRmgTQsKq2R
aPYESaXp0N1l2e5ily+iloNm3GKR33WV5LcTPanSyA+ekZxKI0hQWzv8gSSpwG7idAcCjO/MMKzH
1BogCakiYZ/vCVR+A5zmxRsx4SiqQcIndeM2E4D7IvhJZwk2361G2GLk5RcKJoIK0l1bEJQLg6YK
XYKtwPXaSJ9P044E8aAw58ef+bg6sT4DscVXKNH31MxrWo4JMvkWwgvBNnbjTI6fAcgoCFaFM/pF
y1o5YOETAD8c0KFOI+A6FSdMXbZgdnv3G/DMvxLtBPN+tCKwAfA5kyjA9jHDkCY6UiljF+piveBk
dE1oZIMBjFt2oS63VwLQ4SrJrL8RwhJy77s3KXXpJiDienN9xzYmsjr/3JNiqhPFZTzl5Hq8U1G3
pAsWr4a73xrb092pZFSefECDQu2LHQN3DVjQBYACpfley3nnkobFeGqluB5AtJcII/CtJcBgFSaM
c/wJni17H70Svc4AtKMhBr1pTV2DjEFg5sNje3/Ppzgz602Dt413cCrPojIP+nGhdyh37imrp1OL
wWn2wj2kbAXnczIcEaH3ys80N6yxVH4c9ZI4vHw8nlA2q/De0Cktw8VSrCZeJr5qa+Mx+P3Y5JGi
s2dNWBrHt4Ab6TK3GQZpvF5DknAL4xwnk28t1iDxMg6O6XV857w2hu4DsMEOa/8nXTD2Up/9K7sT
R4+D+/9u6Ipvk/blb+p3jV9XaltyhSdOaYMLPJio4uTVaABeiCr/EgQ+azjvDIq0uP0Mea1B+v/P
cZAG4s1fjOgQuCuQ1bfdSP/COzuRw6Zr+6Z/7JcGBpqH9IcJjG98xeUy0B9iwcLSk9k4UiszY0/c
5a14ubf0B12J+mUYADuPTzBBOu1r1o8GXhQlcOV+uoggqfAyFHOvMjbjJAI690LV4gyd1brMrIuq
iDF2DzQQL7rBh3ELnkbr59vhUqhZspETvwIurBMxkc5JKJjiAgjG+2Egxfym00+KNANOdQe9cvvV
F3Aupww0SdBqQ8YXpYjus3WO8+BBtLHd6CmEJCcqVMnO4JVethy9onK17qvYTw6I+X4f2ww8J9e+
OhTHcKwt6sskr84lizLd0gCrOtwpp3KVyKY/1lmOC1NmdTly+W8hbpTLT5lFbDO38PkveHEs7pS5
NVMXNZow+Z34X2kfwusomCJokmvnSmyZHlA4Q4VI99T8aIDVB/igAoUqaSuUFfyOYG5SFEeqRIZR
aIkA7W1j3bzb6GdS4Q+EOLESA0uotf5IMm02dZxEFmxhq4MtjbNzhisPXLfub0H9FHclquPYujbn
gDjwuZhCeP5XOe4bYrTdwh+/ITV3MAbCtOy5fNIL+udN9PdrLQZyNAfBDcC5+u5bvEL4eh7IUxwv
sS8hxZpsy9BuX9nwWj8thhWFsoa3Px6ITHJkiQCm8wW9HoMW56hDvOVZL05e4yac1kwBE39gxxs1
iWqDgjt93hdqMf0aCe8/n/2t9Cuz3u7a2TKkD/TZVs+88UomNbjkJZNG4Ehp2Hbm/yw8dZhHX/LQ
hmKsQ6x89J/fnoRFchYjNQUDDGA2MkZoFgAzp3l2FN/CDmNYVaMlIjno/XNGqEL+RuhReZh28Twa
Hesokf5rxRBS6SMRuizXPPm7Tm21lWBgZRpZDcwwzLD7Hrwc15m5GZNHNYuouqmvIkQlg16cfeX4
wJXXWCtiJjAcZCxNv8as40fptli9Ad89IlvAseE/S6NpR0CnEDleItEn4gRE+LYr62rjBgutdIHl
p5RYElh6yRePUAiooRrLD1xyeOK49qK+tEoZCyyHqKZ9JgexJbjRaFP/AEDiUX03gYwiJ4DdSSoc
fZY306HFhFmKi/cVh7O4JSLGfWLM5n9tFjOfCEXx38U/xoZBb3NFrsVngSiyaLxe/qf55U1pemRQ
m5Yy1jdHcCZfVRQBnlOfUL8j54Q08VN5lcQugBY0VMpdsHOy6dJT96OLYbpnjEzCrE94cg5ZaHdE
XaM5C96z2y8HfUx/GmEk6Elde1k4sFRBxij9OKuBjXEHZn9q1tacl6727QKv33xJSwMdBeJljg4/
W1vL+w3ti3AAP8DhTBoxzs23OVM9MNVhD+9PfNPSz3shW8vr+C6nIhY9TYEs99UPuhb9f+ABk3dL
AxnMqY9WhUDfcmYp9DbZYkVXT9KFC9iv23GiM8cqVMttG/+vObeYYW30xHmW2J6S6fT6qIEe5xGr
lAkJa9gHh+dNQLzLUA+AgCOj4SZm2GY9WZ67dtfeJyeOTGtdsgXntzk4pcqpa+Jfmh7/sdRosII2
YysOq4zk8s7PZzTZHDFZj16gbihQ/E9g/qs1DEnO3kcLVDebJHTsHcgJZ+K/Z86d2WzkSeFHPXL3
xqMss6arwO4i16t5w47qWMZzAfBIQuaTWA4I3H51sIbDr1aaSms4juMbLLig38vEcNM31gMyWM1i
mnUCa2itQcAqv0nTIQ8z9CpM20gPzjuOsLBQ3ZzcTaJ6aZdB72se6xtuHlidZ2DznX2sGsbiX7uM
hTsTbnSGO2Y/qKX1ADBsaGDOgNjt0QYKYjows4aCH2wbuD03ZTkmNgahFrFu1/vIdwpCeqCxozXa
2FEJ3K0+gFocJvIV+ahrRjGlX8TRr/elQal++7IyVXSq/oqcG9BrD6uGZOCo1PwjHkucFwfPZsST
gx9b7mlIHQz444ZDjT7R5YBTByHhe0cZJKv9gQRgkm3unrnVRpVffw3fA8qwYTPlrBQr21+4Dovc
PL/vsoN6ZmcS1w8VdnKt9Hm4BuOJZiJ/OyFIaAM/1rg6RBG/ViYmMpyNaveDrBVwF78chjg2xkrT
h3Xd69G5sAehKdX9CZrCcZHhTznPpe/rDE5Mls9evJJcn8+0wtw+PFdBlV7m4p6mR27wo06vvwpw
Go266yXhkA8Ku8V8QUm/HfmQteO6UhmGsjrycjx+Rnt/KcVF38dkjHfNAG4n80rLSmCxvhu+jDPl
hu0rsOOw/LKDtrv8+oFpCDPqne+Vf+TX+YNpVsy6WbiXPEBaQ2Xtd4HMc9uV/ZMgIaH7/CmJ3jNe
qdbiS8YqgJoDDntqBeoKEX0vnOXxshurf0vG2UlHHXYfyaOsr9kQPBr6sel9c6RvRWKNcJPMfegI
N288WzSnq8fTjQPAMGDjvfoQHRmlF/7gdFDU6N5QLw95q6gGcI+aSZ7+51T5K3+YCBolwtx5y0nw
/SRDL068ioitTSUaumejGykIX+Udac/ihgX8+JqHZH0BKq1hxGYxQdJvSyCEHyPgctOWfkt1bI+d
jS68mAFQ4b5KoyhVpz/iL1ehDPb0u5gsLOouowC8nzy5sHewB7sWm2NMzpPno92NN6h08iB0gMQf
u67Lza+t3+FJJhzHUsO8bree2pcGl5GrwMe3CtFU+KsJrM2ipYvb+C43lfgdI5AIoy7OrCV2OJnx
vgzLTKHY6Wn23gnsTu4El+F/IBFdFYh/MR3STdnAuBfma9LEZwbD6eUg+0MBYFICKc5s5QuGuSFT
vHPia+5/mGYAFSE9L3C89wtcw6yRqc4kEZghNZAizLolqOJfdnBzSl8UhLpgfi7nC4rS22MWjmky
WMKJTBc5S2LfmJPLrePC0FH64KytwxzygxA+GjpJS8FNqVpxN7RxNY1ldbJO9ocued9AdX+BXXm1
eJFgT9S4oA13/Z+NBvBIpeBvTl4+twbCVSL80XscxKo3Kk81+b6aTr27gvRWhtp++k484lefI/MN
lochbuW1WB+b+iw77DfXhC1Vs1PJdgHCXTFsiNA9pFAJexARQjrLOjbWYmKGVb9lVEkvkdUdqb+Z
WO+7Jgk8vTPPc2uGpQ3y4F3o7ZQ+yAF68nif2kHTWxpae5HsSK9rFJAVEUFyqSghCbz1owl1rGGK
HAFElKJhXwCTYnwPyKi2C5BjX4to0thdeom5runY3XCiwp6678+ytwWMWgz5h6Hs2FHxBh9ZPvxO
gzwO7+kYY5tiLtXX4jXNVD9oLKfdA47iaDSG9zpfJBb25rf5XKx4Hb8CtK7UTp2tvTh8p/CnX3pj
b7XGpF6PMuK1S4mqspI+QYIRigtPnyDIWk3LK+Otac5cz5AD0N4he4hbKWt2SprbjGiyamZvlPXa
WcRooClJCoj/OrLVTurUDd1GfukZKF4Lt8unylgFOAyygW3gQPhvf5dKlSXcf8Z/BV4Oyx+mYhTu
RnrT86q1CcMrYzBJ+5MVwbdkT/f588XabUV689zv141f2H7J2k/wd/Uc1zj21cANrSQZ86SHtKUU
HTXwuOKzwcH0W0MhGcewMV3ikrO3QQM+Ik87Z68351UszU4R6xNpslqmG66L0dBpFoByh0nAPXGB
yFrwkPrPzxSdL1DzYunfv2El/KE/trw6RL8DhYpI4MGZ8tuTqBcpEekn2ONUGow5vzXm4Vn5PcOC
sssTUdxh6kRrnrApC5s/0JlZAKz9ITzGDfOcy8NkG/8QaG+KGoHqTEeC2guH/vmolBNGolwzGz0I
el8qlnD9dPyrMTO1gvWUYanD/yENEUgW41SZxokSISqWP/hYl7xsnPTn4KmCEM799adJUopBTxwY
gfDyj35xZojSTMYhm4Uu4tK3XCTpa2h3I6kQ/Rcd+J1w8Uv4mcES/ka59ViiqocqYg/k9Di5VcRs
7A0hQkjVraFH4XYJi2bQfPhWvn/bH9jVI4/cOYC+c+Q75oofu+qU5FYPCXs39Q2qunoTWERcW7HM
U4HedoG30eRhOGk0qHbcQuPcqXQb2aHsvnpfwkk+0/HjqIRAst+nVTGqHBe0usvrCOewoyaO8005
6ET8XH8DMQ9kMpx8w/W6tY4FYkklO9khYP0zTgVMZMQhnw3rC0ioN7uxe2DHfjcxdHu4kAXnIWwL
Dz577NvtE1vDqB6V8v4HarC8O4F9q6B6e7alpb3VzdDZ+xBEYjZaHoWCeFDEE7T99blxEA6ZElTH
rZ5m0ZvtZXDMNgw0SddDpu9B8bDkrrEKs3+cjDCCB5FMgKZ0Sbfnl5hlH5Gb9+BCjEadLwlRLpkw
Vkw+Rq83KWCxiXuM8MZ484uXBrPWiwVB8c/areyT4CWcHmDmkwl0kl/8ptOTmoujNR4Z5VoLaYUM
QOgPiLU3b9CLIXbGe0XI75U92ACIaRegp1tO5N47Bw76k21U5OXqUs7jaDmP7uUmnleJQ4m621Fq
oFe4MieF61wXz9fb/qBleUHQkfX8sJWoC2dJMP6OCD63IyUuCg/MBoETIrK6Qgl24b8YxdP/WhKI
6Ksw1scXxa4YmaaRxXF2I393NVzIIy/nu+sHOB9V3JDoxfGw4GrIHO0r2n9od7ccyeYTNKD/x9Mq
MrSqkEyhz5AJcxOf61exdoj17//s7wBIzx/Q7/SHrsOwkXyJMkNB/3FNRkKfYhXZtBhxKIdglFGq
5r3GZfiDZ0LUlGQkovutVUGpu1EGNi6gSNmEEZe1hYe8RK8/6gX+Wwxc9SNxbeLBMUFBsH40mdru
DzVYo10CbMVafgBFHM1/AcM+RD6xpT/+6Q==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
