library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;


entity divider is
    Port (
          A : in integer;
          B : in integer;
          divisor : out integer;
          quotient : out integer;
          remainder : out integer
    );
end divider;

architecture Behavioral of divider is

--function  divide  (a : UNSIGNED; b : UNSIGNED) return UNSIGNED is
--    variable a1 : unsigned(a'length-1 downto 0):=a;
--    variable b1 : unsigned(b'length-1 downto 0):=b;
--    variable p1 : unsigned(b'length downto 0):= (others => '0');
--    variable i : integer:=0;
--begin
--    for i in 0 to b'length-1 loop
--        p1(b'length-1 downto 1) := p1(b'length-2 downto 0);
--        p1(0) := a1(a'length-1);
--        a1(a'length-1 downto 1) := a1(a'length-2 downto 0);
--        p1 := p1-b1;
--        if(p1(b'length-1) ='1') then
--            a1(0) :='0';
--            p1 := p1+b1;
--        else
--            a1(0) :='1';
--        end if;
--    end loop;
--return a1;
--end divide;


--component div_0
--    PORT (
--    aclk : IN STD_LOGIC;
--    s_axis_divisor_tvalid : IN STD_LOGIC;
--    s_axis_divisor_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
--    s_axis_dividend_tvalid : IN STD_LOGIC;
--    s_axis_dividend_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
--    m_axis_dout_tvalid : OUT STD_LOGIC;
--    m_axis_dout_tdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
--  );
--end component;


begin
--divisor: div_0 port map(aclk=>clk, s_axis_divisor_tvalid=>valid_A, s_axis_divisor_tdata=>A, s_axis_dividend_tvalid=>valid_B, s_axis_dividend_tdata=>B, m_axis_dout_tvalid=>valid_out, m_axis_dout_tdata=>res);

process(A,B)
begin
    if(B /= 0) then
        --quotient <= A/B;
        quotient <= 0;
        divisor <= B;
        remainder <= A mod B;
    else
        quotient <= 0;
        divisor <= 0;
        remainder <= 0;
    end if;
end process;

end Behavioral;
