library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity dot_product is
  Generic ( Bit_width : positive := 16
            );
  Port (    clk         : in std_logic;
            rst_n       : in std_logic;
            enable      : in std_logic;
            num_elements: in integer;
            A           : in std_logic_vector(Bit_width-1 downto 0);
            B           : in std_logic_vector(Bit_width-1 downto 0);     
            Res         : out std_logic_vector((2*Bit_width)-1 downto 0)
        );
end dot_product;

architecture Behavioral of dot_product is

component mult_gen_0
        PORT (
            CLK : IN STD_LOGIC;
            A : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
            B : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
            P : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
        );
end component;

signal mult_A, mult_B : std_logic_vector(Bit_width-1 downto 0);
signal mult_P, P_reg : std_logic_vector((2*Bit_width)-1 downto 0);


TYPE State_type IS (S0, S1);
SIGNAL State : State_Type; 

begin
mult: mult_gen_0 port map(CLK=>clk, A=>A, B=>B, P=>mult_P);

process(clk, rst_n)
    variable counter : integer := 0;
begin
    if(rst_n = '0') then
            P_reg <= (others => '0');
            state <= S0;
    elsif(rising_edge(clk)) then
        case State is
        
            when S0 => -- wait state
                P_reg <= (others => '0');
                counter := 0;
                if(enable = '1') then
                    State <= S1;
                end if;
            
            when S1 =>
                    P_reg <= std_logic_vector(signed(P_reg) + signed(mult_P));
                    counter := counter + 1;
                    if(counter > num_elements+1) then
                        state <= S0;
                        Res <= P_reg;
                    end if;
        end case;
    end if;
end process;

end Behavioral;
