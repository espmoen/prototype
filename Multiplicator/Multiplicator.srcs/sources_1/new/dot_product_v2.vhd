library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

package sorting_network_pkg is
    type bus_array is array(natural range <>) of std_logic_vector(15 downto 0);
end package sorting_network_pkg;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.sorting_network_pkg.all;



entity dot_product_v2 is
  Port ( 
        clk     :   in std_logic;
        rst_n   :   in std_logic;
        A       :   in bus_array(0 to 2);
        B       :   in bus_array(0 to 2);
        C       :   out std_logic_vector(47 downto 0)
  );
end dot_product_v2;

architecture Behavioral of dot_product_v2 is

component xbip_multadd_1
    PORT (
        CLK : IN STD_LOGIC;
        CE : IN STD_LOGIC;
        SCLR : IN STD_LOGIC;
        A : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        B : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        C : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
        SUBTRACT : IN STD_LOGIC;
        P : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
        PCOUT : OUT STD_LOGIC_VECTOR(47 DOWNTO 0)
    );
end component;

signal W1, W2, W3 : std_logic_vector(47 downto 0) := "000000000000000000000000000000000000000000000000";
signal temp1,temp2,temp3 : std_logic_vector(47 downto 0);
begin
uut1: xbip_multadd_1 port map(CLK=>clk, CE=>'1', SCLR=>'0', A=>A(0), B=>B(0), C=>W3, SUBTRACT=>'0', P=>W1, PCOUT=>temp1);
uut2: xbip_multadd_1 port map(CLK=>clk, CE=>'1', SCLR=>'0', A=>A(1), B=>B(1), C=>W1, SUBTRACT=>'0', P=>W2, PCOUT=>temp2);
uut3: xbip_multadd_1 port map(CLK=>clk, CE=>'1', SCLR=>'0', A=>A(2), B=>B(2), C=>W2, SUBTRACT=>'0', P=>C, PCOUT=>temp3);
end Behavioral;
