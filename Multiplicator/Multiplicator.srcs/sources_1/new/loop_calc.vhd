library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;


entity loop_calc is
  Port ( 
        raw      :       in integer;
        P_1      :       in integer;
        P_4      :       in integer;
        wlens    :       in integer;
        P_5      :       in integer;
        wlensSq  :       in integer;
        P_2      :       in integer;
        quotient :       out integer;
        divisor  :       out integer;
        remainder:       out integer
  );
end loop_calc;

architecture Behavioral of loop_calc is

component multiplicator
       Port ( A : in integer;
       B : in integer;
       res : out integer
    );
end component;

component subtractor
    Port ( A : in integer;
       B : in integer;
       res : out integer
    );
end component;

component divider
    Port (
          A : in integer;
          B : in integer;
          divisor : out integer;
          quotient : out integer;
          remainder : out integer
    );
end component;

signal t1, t2 ,t3, t4, t5 : integer;

begin

sub1: subtractor port map(A=>Raw, B=> P_1, res=>t1);
mul1: multiplicator port map (A=>P_4, B=>wlens, res=>t2);
sub2: subtractor port map (A=>t1, B=>t2, res=>t3);
mul2: multiplicator port map (A=>P_5, B=>wlensSq, res=>t4);
sub3: subtractor port map(A=>t3, B=>t4, res=>t5);
div1: divider port map(A=>t5, B=> P_2, divisor=>divisor, quotient => quotient, remainder=>remainder);
end Behavioral;
