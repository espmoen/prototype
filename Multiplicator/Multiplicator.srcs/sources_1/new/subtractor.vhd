library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity subtractor is
  Port (A : in integer;
        B : in integer;
        res : out integer
  );
end subtractor;

architecture Behavioral of subtractor is



begin

res <= A-B;

end Behavioral;
