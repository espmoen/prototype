library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
library UNISIM;
use UNISIM.VComponents.all;


entity test_flip_Esp is
  Port (    clk         : in std_logic;
            rst_n       : in std_logic;
            data_in     : in std_logic;
            enable      : in std_logic;
            shift_rd    : out std_logic;
            data_out    : out std_logic_vector(11 downto 0)
       );
end test_flip_Esp;

architecture Behavioral of test_flip_Esp is

signal reg_fall, reg_rise : std_logic_vector(5 downto 0); 
signal Q1, Q2 : std_logic;
signal counter : natural range 0 to 6;


TYPE State_type IS (S1, S2);  -- Define the states
SIGNAL state : State_Type;    -- Create a signal that uses 

signal write_en : boolean := false;
begin
IDDR_inst_a: IDDR port map(Q1 => Q1, Q2 => Q2, C=> clk, CE=>rst_n, D=>data_in, R=>'0', S=>'0');


process(clk, rst_n)
begin
    if (rst_n = '0') then
        reg_fall <= (others =>'0');
        reg_rise <= (others =>'0');
        --shift_rd <= '0';
        state <= S1;
        counter <= 0;
        write_en <= false;
    elsif(rising_edge(clk)) then
        if(enable = '1') then
            reg_rise <= reg_rise(4 downto 0) & Q1;
            reg_fall <= reg_fall(4 downto 0) & Q2;
            counter <= counter + 1;
            if(counter = 1 and write_en) then
                shift_rd <= '1';
                --data_out(11 downto 0) <= reg_rise(0)&reg_fall(0)&reg_rise(1)&reg_fall(1)&reg_rise(2)&reg_fall(2)&reg_rise(3)&reg_fall(3)&reg_rise(4)&reg_fall(4)&reg_rise(5)&reg_fall(5);
                for i in 0 to 5 loop
                    data_out(2*i) <= reg_fall(i);
                    data_out(2*i+1) <= reg_rise(i);
                end loop;
           else
                shift_rd <= '0';
           end if;
           if counter = 6 then
               write_en <= true;
               counter <= 1;
           end if;
       end if;
    end if;
end process;



end Behavioral;



-- case state is
--            when S1 =>
--                shift_rd <= '0';
--                counter <= 0;
--                if(enable = '1') then
--                    state <= S2;
--                    reg_rise <= Q1 & reg_rise(5 downto 1);
--                    reg_fall <= Q2 & reg_fall(5 downto 1);
--                end if;
--            when S2 =>
--                reg_rise <= Q1 & reg_rise(5 downto 1);
--                reg_fall <= Q2 & reg_fall(5 downto 1);
--                counter <= counter + 1;
--                if(counter >= 5) then
--                    data_out <= reg_rise(5)&reg_fall(5)&reg_rise(4)&reg_fall(4)&reg_rise(3)&reg_fall(3)&reg_rise(2)&reg_fall(2)&reg_rise(1)&reg_fall(1)&reg_rise(0)&reg_fall(0);
--                    shift_rd <= '1';
--                    state <= S1;
--                end if;
--        end case;