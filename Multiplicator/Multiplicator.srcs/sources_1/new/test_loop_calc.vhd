library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity test_loop_calc is
      Port (    clk      :       in std_logic;
                rst      :       in std_logic;
                raw      :       in integer;
                P_1      :       in integer;
                P_4      :       in integer;
                wlens    :       in integer;
                P_5      :       in integer;
                wlensSq  :       in integer;
                P_2      :       in integer;
                quotient :       out integer;
                divisor  :       out integer;
                remainder:       out integer);
end test_loop_calc;

architecture Behavioral of test_loop_calc is
signal i1, i2, i3, i4, i5, i6 , i7: integer;
signal o1, o2, o3 : integer;

component loop_calc
   Port ( 
        raw      :       in integer;
        P_1      :       in integer;
        P_4      :       in integer;
        wlens    :       in integer;
        P_5      :       in integer;
        wlensSq  :       in integer;
        P_2      :       in integer;
        quotient :       out integer;
        divisor  :       out integer;
        remainder:       out integer
  );
end component;

begin
uut: loop_calc port map (raw=>i1, P_1=>i2, P_4=>i3, wlens=>i4, P_5=>i5, wlensSq=>i6, P_2=>i7, quotient=>o1, divisor=>o2, remainder=>o3); 

process(clk, rst)
begin
    if(rst = '1') then
        i1 <= 0;
        i2 <= 0;
        i3 <= 0;
        i4 <= 0;
        i5 <= 0;
        i6 <= 0;
        i7 <= 0;
    elsif(rising_edge(clk)) then
        i1 <= raw;
        i2 <= P_1;
        i3 <= P_4;
        i4 <= wlens;
        i5 <= P_5;
        i6 <= wlensSq;
        i7 <= P_2;
        quotient <= o1;
        divisor <= o2;
        remainder <= o3; 
    end if;
end process;


end Behavioral;
