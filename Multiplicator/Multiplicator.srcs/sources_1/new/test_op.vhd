library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity test_op is
    Port (clk   : in std_logic;
          rst   : in std_logic;
          A     : in std_logic_vector(11 downto 0);
          B     : in std_logic_vector(11 downto 0);
          Res   : out std_logic_vector(11 downto 0);
          Res_valid : out std_logic
          );
end test_op;

architecture Behavioral of test_op is

component c_addsub_0
    PORT (
        A : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
        B : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
        CLK : IN STD_LOGIC;
        CE : IN STD_LOGIC;
        S : OUT STD_LOGIC_VECTOR(11 DOWNTO 0)
    );
end component;

signal A_i, B_i, S_o : std_logic_vector(11 downto 0);


begin
uut: c_addsub_0 port map(A=>A_i, B=>B_I, CLK=>clk, CE=>rst, S =>S_o);

process(clk, rst)
    variable counter : integer range 0 to 2 := 0;
begin
    if(rst = '0') then
        A_i <= (others => '0');
        B_i <= (others => '0');
        Res <= (others => '0');
    elsif(rising_edge(clk)) then
        A_i <= A;
        B_i <= B;
        Res <= S_o;    
    end if;
    
end process;

end Behavioral;
