`timescale 1ns/1ps

module lvds_deserialize_tb;

parameter     PERIOD = 10;
parameter DOUT_WIDTH = 12;
parameter     n_data = 10;

integer i, j;
reg [DOUT_WIDTH-1:0] data [n_data-1:0];
reg [DOUT_WIDTH-1:0] current_word;

reg clk, rst, data_in, enable;
wire[DOUT_WIDTH-1:0] data_out;
//wire[11:0] dc_out;

test_flip_Esp dut(clk, rst, data_in, enable, shift_rd, data_out);

always #(PERIOD/2) clk = ~clk;

initial begin
    $readmemh("testvector.txt", data);
    //f_out = $fopen("output.txt","w");
    clk <= 1'b0;
    rst <= 1'b0;
    enable <= 1'b0;
    #13 rst <= 1'b0;
    enable <= 1'b1;
    for (i = 0; i < n_data; i = i + 1) begin
        current_word <= data[i];
        for (j = 0; j < DOUT_WIDTH; j = j+1) begin
            data_in <= current_word[j];
            #(PERIOD/2) i = i;
        end
    end
//    @(posedge clk);
//    @(posedge rdy_out);
//    //@(posedge CLK);
//    //$fwrite(f_out, "%h", dc_out);
//    for (i = 0; i < 63; i = i + 1) begin
//        @(posedge CLK);
//        if (rdy_out)
//            $fwrite(f_out, "%b", data_out);
//    end
//    @(posedge CLK); flush <= 1'b1;
//    @(posedge CLK); flush <= 1'b0;
//    while (rdy_out == 1'b0) @(posedge CLK);
//    $fwrite(f_out, "%b", data_out);

//    $fclose(f_out);
    $finish;
end

endmodule
