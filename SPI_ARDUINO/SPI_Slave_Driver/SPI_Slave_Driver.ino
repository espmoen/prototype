// Written by Nick Gammon
// February 2011
/**
 * Send arbitrary number of bits at whatever clock rate (tested at 500 KHZ and 500 HZ).
 * This script will capture the SPI bytes, when a '\n' is recieved it will then output
 * the captured byte stream via the serial.
 */

#include <SPI.h>

char buf [100];
volatile byte pos;
volatile boolean process_it;


void setup (void)
{
  Serial.begin (115200);   // debugging

  // have to send on master in, *slave out*
  pinMode(MISO, OUTPUT);
  
  // turn on SPI in slave mode
  SPCR |= _BV(SPE);
  
  // get ready for an interrupt 
  pos = 0;   // buffer empty
  process_it = false;
  
  // now turn on interrupts
  SPI.attachInterrupt();

}  // end of setup

bool IsBitSet( unsigned char byte, int index )
{
  int mask = 1<<index;
  return (byte & mask) != 0;
}

// SPI interrupt routine
ISR (SPI_STC_vect)
{
byte c = SPDR;  // grab byte from SPI Data Register
  
  // add to buffer if room
  if (pos < sizeof buf)
    {
    buf [pos++] = c;
    // example: newline means time to process buffer
    if (c == 0b10101010)
      process_it = true;
    }  // end of room available
}  // end of interrupt routine SPI_STC_vect

// main loop - wait for flag set in interrupt routine
void loop (void)
{
  if (process_it)
    {
    byte A = buf[0];
    if(IsBitSet(A,7)){
      byte B = buf[1];
      Serial.print("Write data: ");
      Serial.print((B >> 7) & 1);
      Serial.print((B >> 6) & 1);
      Serial.print((B >> 5) & 1);
      Serial.print((B >> 4) & 1);
      Serial.print((B >> 3) & 1);
      Serial.print((B >> 2) & 1);
      Serial.print((B >> 1) & 1);
      Serial.print((B >> 0) & 1);
      Serial.print(" to address: ");
      Serial.print((A >> 6) & 1);
      Serial.print((A >> 5) & 1);
      Serial.print((A >> 4) & 1);
      Serial.print((A >> 3) & 1);
      Serial.print((A >> 2) & 1);
      Serial.print((A >> 1) & 1);
      Serial.println((A >> 0) & 1);
    }
    else{
      Serial.print("Read from address: ");
      Serial.print((A >> 6) & 1);
      Serial.print((A >> 5) & 1);
      Serial.print((A >> 4) & 1);
      Serial.print((A >> 3) & 1);
      Serial.print((A >> 2) & 1);
      Serial.print((A >> 1) & 1);
      Serial.println((A >> 0) & 1);
    }
    
    pos = 0;
    process_it = false;
    }  // end of flag set
    
}  // end of loop
