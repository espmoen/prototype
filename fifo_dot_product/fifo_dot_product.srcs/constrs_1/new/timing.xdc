create_clock -period 12.000 -name sys_clk -waveform {0.000 6.000} [get_ports -filter { NAME =~  "*clk*" && DIRECTION == "IN" }]

create_clock -period 10.000 -name sys_da -waveform {0.000 5.000} [get_ports -filter { NAME =~  "*clk*" && DIRECTION == "IN" }]
