library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;


entity dot_product_tb is
--  Port ( );
end dot_product_tb;



architecture Behavioral of dot_product_tb is

component dot_product_fifo
    Generic (bit_width : integer := 32; 
            num_elements_vec: integer := 10
            );
    Port ( 
        clk : in std_logic;
        reset : in std_logic;
        enable : in std_logic;
        valid : in std_logic;
        data_in : in signed(31 downto 0);
        data_out : out signed(63 downto 0);
        res_rdy : out std_logic
        );
end component;

component clk
    Port ( clk : out STD_LOGIC);
end component;

signal res_rdy, valid, clock, reset, enable : std_logic;
signal data_in : signed(31 downto 0);
signal data_out : signed(63 downto 0);

begin
uut: dot_product_fifo port map(clk => clock,reset => reset,enable => enable,data_in => data_in, valid => valid, data_out => data_out, res_rdy => res_rdy);
cl: clk port map(clk => clock);


process
begin
    reset <= '1';
    enable <= '0';
    data_in <= (others => '0');
    valid <= '0';
    wait for 0.5 ns;
    reset <= '0';
    wait for 1 ns;
    enable <= '1';
    valid <= '1';
    for i in 0 to 9 loop
       data_in <= data_in + "00000000000000000000000000000001";
       wait for 1 ns;
       enable <= '0';
    end loop;
    valid <= '0';
    wait for 1 ns;
    for i in 0 to 3 loop
        data_in <=  "00000000000000000000000000000001";
        wait for 1 ns;
        enable <= '0';
    end loop;
    valid <= '1';
    data_in <= x"0000000b";
    wait for 1 ns;
    for i in 0 to 49 loop
        data_in <= data_in + "00000000000000000000000000000001";
        wait for 1 ns;
        enable <= '0';
    end loop;
    wait for 999.5 ns;
end process;
end Behavioral;
