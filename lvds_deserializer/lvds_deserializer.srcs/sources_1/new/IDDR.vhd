library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity IDDR is
  Port (    clk : in std_logic;
            rst : in std_logic;
            data_in : in std_logic;
            Q1 : out std_logic
        );
end IDDR;

architecture Behavioral of IDDR is    
begin

    process(rst, clk) 
        begin
            if (rst = '1') then
                Q1  <= '0';
            elsif  rising_edge(clk) then
                Q1 <= data_in;
            elsif falling_edge(clk) then
                Q1 <= data_in;
            end if;
    end process;
end Behavioral;
